﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class PublicConstant
    {
        //trai thái lich trinh
        public const int ORDER_PENDING = 1;
        public const int ORDER_ACCEPT = 1;
        public const int ORDER_CANCEL = 2;
        public const int ORDER_CARRYING = 3;
        public const int ORDER_FINISHED = 4;

        // trạn thai

        public const int STATUS_PENDING = 0;
        public const int STATUS_ACTIVE = 1;
        public const int STATUS_DEACTIVE = 2;
        public const int STATUS_DELETE = 3;

        // Da khop lenh
        public const int STATUS_ACCEPT = 4;

        public const string ROLE_NHA_XE = "Nhà xe";
        public const string ROLE_ADMIN = "Admin";
        public const string ROLE_KHACH = "Khách";

        public const int NHAXE_BOCHANG_DIEMDEN = 4;
        public const int NHAXE_BOCHANG_DIEMDI = 3;
        public const int KHACH_BOCHANG_DIEMDI = 1;
        public const int KHACH_BOCHANG_DIEMDEN = 2;

        public const int CODE_VANDON = 1;
        public const int CODE_LICHTRINH = 2;

        public const int CAR_TYPE_CONTAINER = 1;
        public const int CAR_TYPE_CHUYENDUNG = 2;
        public const int CAR_TYPE_XETAI = 3;

        public const string DONVI_SOLUONG_THUNG = "Thùng";
        public const string DONVI_SOLUONG_BAO = "Bao";
        public const string DONVI_SOLUONG_HOP = "Hộp";
        public const string DONVI_SOLUONG_KIEN = "Kiện hàng";

        public const string DONVI_KHOILUONG_TAN = "Tấn";
        public const string DONVI_KHOILUONG_TA = "Tạ";
        public const string DONVI_KHOILUONG_YEN = "Yến";
        public const string DONVI_KHOILUONG_KG = "Kg";

        public const string DONVI_KHOILUONG_THETICH = "m3";
        public const string DONVI_KHOILUONG_DIENTICH = "m2";

        // Tiền mặt
        public const int PAYMENT_TYPE_DEFAULT = 0;
        // thẻ visa
        public const int PAYMENT_TYPE_VISACARD = 1;
        // Chuyển khoản ngân hàng nội địa
        public const int PAYMENT_TYPE_BANKING = 2;

        public const string FORMAT_DATEVN = "dd/MM/yyyy";

        public const int MATCH_EXPECT = 0;
        public const int MATCH_DIADIEM = 1;
        public const int MATCH_XE_TAITRONG = 2;
        public const int MATCH_THOIGIAN = 3;
        public const int MATCH_VITRI = 4;

        // Loai Xe Tai
        public const float LOAIXE15T = 1.5F;
        public const float LOAIXE25T = 2.5F;
        public const float LOAIXE35T = 3.5F;
        public const float LOAIXE5T = 5;
        public const float LOAIXE8T = 8;
        public const float LOAIXE10T = 10;
        public const float LOAIXE3CHAN = 16;
        public const float LOAIXE4CHAN = 19;
    }
}
