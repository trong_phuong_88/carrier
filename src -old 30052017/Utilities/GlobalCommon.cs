﻿using System;
using Models.Entities;
using System.Text;
using System.Security.Cryptography;

namespace Utilities
{
    public static class GlobalCommon
    {
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        /// <summary>
        /// Get Cell address of Excel file
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static string GetCellAddress(int row, int col)
        {
            int dividend = col;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName + row;
        }
        public class SelectListCombo
        {
            public SelectListCombo(string key, string value)
            {
                Key = key;
                Value = value;
            }
            public SelectListCombo()
            {
            }
            /// <summary>
            /// Key of Item (for Display)
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// value of Item (for get value)
            /// </summary>
            public string Value { get; set; }
        }

        public static string OrderStatusToString(int status)
        {
            var result = string.Empty;
            switch (status)
            {
                case PublicConstant.STATUS_ACTIVE:
                    result = "Đã kích hoạt";
                    break;
                case PublicConstant.STATUS_DEACTIVE:
                    result = "Hủy kích hoạt";
                    break;
                case PublicConstant.STATUS_DELETE:
                    result = "Đã xóa";
                    break;
                case PublicConstant.STATUS_PENDING:
                    result = "Chờ kích hoạt";
                    break;
                default:
                    break;
            }
            return result;

        }
        public static string CarTypeToString(int type)
        {
            var result = string.Empty;
            switch (type)
            {
                case PublicConstant.CAR_TYPE_CONTAINER:
                    result = "Xe Container";
                    break;
                case PublicConstant.CAR_TYPE_CHUYENDUNG:
                    result = "Xe chuyên dụng";
                    break;
                case PublicConstant.CAR_TYPE_XETAI:
                    result = "Xe tải";
                    break;
                default:
                    break;
            }
            return result;
        }
        public static string PaymentTypeToString(int type)
        {
            var result = string.Empty;
            switch (type)
            {
                case PublicConstant.PAYMENT_TYPE_DEFAULT:
                    result = "Tiền mặt";
                    break;
                case PublicConstant.PAYMENT_TYPE_VISACARD:
                    result = "Visa card";
                    break;
                case PublicConstant.PAYMENT_TYPE_BANKING:
                    result = "Chuyển khoản ngân hàng trong nước";
                    break;
                default:
                    break;
            }
            return result;
        }
        public static float HesoX(float distance)
        {
            if (distance <= 100 && distance >= 0)
            {
                return SecureConstant.km0100;
            }
            else if (distance <= 150 && distance >= 101)
            {
                return SecureConstant.km101150;
            }
            else if (distance <= 250 && distance >= 151)
            {
                return SecureConstant.km151250;
            }
            else if (distance <= 450 && distance >= 251)
            {
                return SecureConstant.km251450;
            }
            else if (distance <= 1000 && distance >= 501)
            {
                return SecureConstant.km5011000;
            }
            else if (distance <= 2000 && distance >= 1501)
            {
                return SecureConstant.km15012000;
            }
            else if (distance <= 2500 && distance >= 2001)
            {
                return SecureConstant.km20012500;
            }
            else if (distance >= 2500)
            {
                return SecureConstant.km2500;
            }
            else
            {
                return 1;
            }
        }
        //10:00/13/12/2016
        public static string GetHourFromDateString(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                return strDate.Split(':')[0];
            }
            return string.Empty;
        }
        public static string GetHourAndMinuteFromDateString(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                return strDate.Split('/')[0];
            }
            return string.Empty;
        }
        public static string GetMinuteFromDateString(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                var strHM = strDate.Split('/')[0];
                return strDate.Split(':')[1];
            }
            return string.Empty;
        }
        public static string GetDayFromDateString(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                return strDate.Split('/')[1];
            }
            return string.Empty;
        }
        public static string GetMonthFromDateString(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                return strDate.Split('/')[2]; ;
            }
            return string.Empty;
        }
        public static string GetYearFromDateString(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                return strDate.Split('/')[3];
            }
            return string.Empty;
        }

        //9:00 vs 8:30
        public static bool LessThanOrEqualHour(string str1, string str2)
        {
            if (str1.Equals(str2))
            {
                return true;
            }
            string h1 = str1.Split(':')[0];
            string m1 = str1.Split(':')[1];
            string h2 = str2.Split(':')[0];
            string m2 = str2.Split(':')[1];
            if (h1.ToIntOrZero() < h2.ToIntOrZero())
            {
                return true;
            }
            else if (h1.ToIntOrZero() == h2.ToIntOrZero())
            {
                if (m1.ToIntOrZero() < m2.ToIntOrZero())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else { return false; }
        }
        //9:00 13/12/2016 vs 9:00 14/12/2016
        public static bool LessThanDate(string strDate1, string strDate2)
        {
            var arrDate1 = strDate1.Split('/');
            string day1 = arrDate1[0];
            string month1 = arrDate1[1];
            string year1 = arrDate1[0];

            var arrDate2 = strDate2.Split('/');
            string day2 = arrDate1[0];
            string month2 = arrDate1[1];
            string year2 = arrDate1[0];

            if (year1.ToIntOrZero() < year2.ToIntOrZero())
            {
                return true;
            }
            else if (year1.ToIntOrZero() == year2.ToIntOrZero())
            {
                if (month1.ToIntOrZero() < month2.ToIntOrZero())
                {
                    return true;
                }
                else if (month1.ToIntOrZero() == month2.ToIntOrZero())
                {
                    if (day1.ToIntOrZero() < day2.ToIntOrZero())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else { return false; }
        }

        public static bool CanMatchDateTime(string value1, string value2)
        {
            var arrValue1 = value1.Split(' ');
            var arrValue2 = value2.Split(' ');

            var strDate1 = arrValue1[1];

            var strDate2 = arrValue2[1];
            if (strDate1.Equals(strDate2))
            {
                var time1 = arrValue1[0];
                var time2 = arrValue2[0];
                if (LessThanOrEqualHour(time1, time2))
                {
                    return true;
                }
            }
            return false;
        }
        //9:00 13/12/2016 vs 9:00 14/12/2016
        public static bool GreatThanDate(string strDate1, string strDate2)
        {
            var date1 = strDate1.Split(' ');
            var arrDate1 = date1[1].Split('/');
            string day1 = arrDate1[0];
            string month1 = arrDate1[1];
            string year1 = arrDate1[2];

            var date2 = strDate2.Split(' ');
            var arrDate2 = date2[1].Split('/');
            string day2 = arrDate2[0];
            string month2 = arrDate2[1];
            string year2 = arrDate2[2];

            if (year1.ToIntOrZero() > year2.ToIntOrZero())
            {
                return true;
            }
            else if (year1.ToIntOrZero() == year2.ToIntOrZero())
            {
                if (month1.ToIntOrZero() > month2.ToIntOrZero())
                {
                    return true;
                }
                else if (month1.ToIntOrZero() == month2.ToIntOrZero())
                {
                    if (day1.ToIntOrZero() > day2.ToIntOrZero())
                    {
                        return true;
                    }
                    else
                    {
                       if(LessThanOrEqualHour(date2[0], date1[0]))  {
                            return true;
                        }
                       else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else { return false; }
        }
        //9:00 13/12/2016 vs 9:00 14/12/2016
        public static bool EqualDate(string strDate1, string strDate2)
        {
            var arrDate1 = strDate1.Split(' ');
            var arrDate2 = strDate2.Split(' ');
            string date1 = arrDate1[1];
            string date2 = arrDate2[1];
            if (date1.Equals(date2))
            {
                return true;
            }
            else { return false; }
        }
        //9:00 13/12/2016 vs 9:00 14/12/2016
        public static bool EqualDateTime(string strDate1, string strDate2)
        {
            DateTime dt1;
            DateTime.TryParse(strDate1, out dt1);
            DateTime dt2;
            DateTime.TryParse(strDate2, out dt2);
            if (strDate1.Equals(strDate2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        public class MatchCondition
        {
            public bool MatchXa { get; set; }
            public bool MatchTinh { get; set; }
            public bool MatchHuyen { get; set; }
            // Trường hợp xe 1 đã chở và map với xe 2 còn trống
            public bool MatchTrongTai1 { get; set; }
            // Trường hợp xe 1 còn trống và map với xe 2 đã chở
            public bool MatchTrongTai2 { get; set; }
            public bool MatchDate { get; set; }
            public bool MatchDateTimeDen { get; set; }
            public bool MatchDateTimeDi { get; set; }
        }   
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ConvertToCurrency(double number)
        {
            // number.ToString("N"); //12.345.00
            return string.Format("{0:N}", number); // 12,345.00
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string DistrictOrWardFriendlyName(string name)
        {
            double number;
            if (double.TryParse(name, out number))
            {
                return string.Format("{0} {1}", "Quận", name.ToIntOrZero());
            }
            else return name;
        }
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::                                                                         :::
        //:::  This routine calculates the distance between two points (given the     :::
        //:::  latitude/longitude of those points). It is being used to calculate     :::
        //:::  the distance between two locations using GeoDataSource(TM) products    :::
        //:::                                                                         :::
        //:::  Definitions:                                                           :::
        //:::    South latitudes are negative, east longitudes are positive           :::
        //:::                                                                         :::
        //:::  Passed to function:                                                    :::
        //:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
        //:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
        //:::    unit = the unit you desire for results                               :::
        //:::           where: 'M' is statute miles (default)                         :::
        //:::                  'K' is kilometers                                      :::
        //:::                  'N' is nautical miles                                  :::
        //:::                                                                         :::
        //:::  Worldwide cities and other features databases with latitude longitude  :::
        //:::  are available at http://www.geodatasource.com                          :::
        //:::                                                                         :::
        //:::  For enquiries, please contact sales@geodatasource.com                  :::
        //:::                                                                         :::
        //:::  Official Web site: http://www.geodatasource.com                        :::
        //:::                                                                         :::
        //:::           GeoDataSource.com (C) All Rights Reserved 2015                :::
        //:::                                                                         :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        public static double Distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

    }
}