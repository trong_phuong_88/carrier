﻿using Carrier.Models.Entities;
using Carrier.PushNotification;
using Carrier.Repository;
using Carrier.Utilities;
using EntityFramework.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Carrier.ConsoleService
{
    class Program
    {
        static private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private static string log_carrier = "C:\\Users\\data\\Log\\Log_Carrier.txt";
        static void Main(string[] args)
        {
            Console.WriteLine("Lac lac running.....");
            while (true)
            {
                AutoMatchingNewOrders();
                Thread.Sleep(1000);
            }
            Console.ReadLine();
        }
        public static void AutoMatchingNewOrders()
        {
            try
            {
                using (var db = new Carrier3Entities())
                {
                    var lstOrderPending = db.Database.SqlQuery<OrderTracking>("SP_GetUpdateOrderTracking").ToList();
                    foreach (var item in lstOrderPending)
                    {
                        // Không tìm thấy tài xế sẽ update trạng thái tài xế cancel đơn
                        item.Status = 2;  //Change to Driver cancel
                        item.Updated_At = DateTime.Now;
                    }
                    if (lstOrderPending.Count > 0)
                    {
                        EFBatchOperation.For(db, db.OrderTracking).UpdateAll(lstOrderPending, x => x.ColumnsToUpdate(c => c.Status));
                        EFBatchOperation.For(db, db.OrderTracking).UpdateAll(lstOrderPending, x => x.ColumnsToUpdate(c => c.Updated_At));
                    }
                        
                    var lstOrderExpired = db.Database.SqlQuery<OrderTracking>("SP_GetAllOrderExpired").ToList();
                    foreach (var item in lstOrderExpired)
                    {
                        // Không tìm thấy tài xế sẽ update trạng thái tài xế cancel đơn
                        item.Status = PublicConstant.ORDER_EXPIRED;  //Change to Driver cancel
                        //item.Updated_At = DateTime.Now;
                    }
                    if (lstOrderExpired.Count > 0)
                    {
                        EFBatchOperation.For(db, db.OrderTracking).UpdateAll(lstOrderExpired, x => x.ColumnsToUpdate(c => c.Status));
                        EFBatchOperation.For(db, db.OrderTracking).UpdateAll(lstOrderExpired, x => x.ColumnsToUpdate(c => c.Updated_At));
                    }
                        
                    db.SaveChanges();
                    var lstNewOrders = db.Database.SqlQuery<SP_GetAllOrders_NeedMatching_Result>("SP_GetAllOrders_NeedMatching").ToList();
                    foreach (var item in lstNewOrders)
                    {
                        var userId = item.Created_By;
                        MapOrder userMapOrder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefaultByParameter(o => o.ToUser.Equals(userId));
                        if (userMapOrder == null)
                        {
                            var listnearDriver = GetDriverNearOrder(item.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                            foreach (var nearDriver in listnearDriver)
                            {
                                Ultilities.SaveTextToFile("Program: nearDriver " + nearDriver.UserId + "order_id" + item.Id, log_carrier);
                                var state = new Dictionary<string, object>();
                                state["driverid"] = nearDriver.UserId;
                                state["orderid"] = item.Id;
                                Thread thread = new Thread(new ParameterizedThreadStart(PushMessageForDriver));
                                try
                                {
                                    thread.Start(state);
                                }
                                catch (OutOfMemoryException)
                                {
                                    Ultilities.SaveTextToFile("Program: Method: Program: OutofMemoryException", log_carrier);
                                    //DebugHelper.SaveTextToFile("Class: Program, Method: ProcessCard: OutofMemoryException", log_TrumThe);
                                }
                                Thread.Sleep(2000);
                            }
                            //if (nearDriver != null)
                            //{

                            //    PushMessageForDriver(nearDriver.UserId, item.Id);
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Program: AutoMatchingNewOrders " + ex.Message, log_carrier);
                Console.WriteLine("Exception" + ex.Message);
            }
        }
        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="orderId"></param>
        public static void PushMessageForDriver(object state)
        {
            var data = (Dictionary<string, object>)state;
            var driverId = data["driverid"].ToString();
            long orderId = long.Parse(data["orderid"].ToString());
            try
            {
                PushCompletedCallBack callback = PushNotificationCallBack;

                string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
                DevicesPush device = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
                if (device == null)
                {
                    var objOrder = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
                    var obj = new OrderTracking();
                    obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                    obj.Updated_At = DateTime.Now;
                    obj.Created_At = DateTime.Now;
                    obj.DriverId = driverId;
                    obj.OwnerId = objOrder.Created_By;
                    obj.OrderId = orderId;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                    _unitOfWork.SaveChanges();
                    return;
                }
                if (device.Platform.ToLower().Equals("ios"))
                {
                    PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                    PushServices.SetupPushAPN(true);
                    //PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH;
                    //PushServices.SetupPushAPN(false);
                    PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
                }
                else
                {
                    jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                    PushServices.GcmKey = PublicConstant.GCM_KEY;
                    PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                    PushServices.SetupPushGCM();
                    PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
                }
            }
            catch (Exception ex)
            {
                var objOrder = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
                var obj = new OrderTracking();
                obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = driverId;
                obj.OwnerId = objOrder.Created_By;
                obj.OrderId = orderId;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
        }
        public static void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            long orderId = long.Parse(obj2);
            //var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
            if (result.Equals("Success") == false)
            {
                // Cap nhat lai trang thai don hang gui loi
                var obj = new OrderTracking();
                obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
            else
            {
                var obj = new OrderTracking();
                obj.Status = 0;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
        }

        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        static private List<SearchDriverNearOrder_Result> GetDriverNearOrder(long orderId, float distance)
        {
            try
            {
                var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
                //var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
                var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId", sqlOrderId).ToList();
                return lstSearch;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ex GetDriver" + ex.Message);
                return null;
            }
        }
    }
}
