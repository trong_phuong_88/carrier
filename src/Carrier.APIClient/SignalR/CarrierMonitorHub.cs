﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Http;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using APIClient.Comon;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Web.Routing;
using APIClient.Helpers;
using System.Net;
using Newtonsoft.Json;
using Carrier.PushNotification;
using System.Data.SqlClient;

namespace Carrier.APIClient.SignalR
{
    [HubName("CarrierMonitor")]
    public class CarrierMonitorHub : Hub
    {
        private static GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private readonly ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        #region Persitence connection
        public override Task OnConnected()
        {
            //string name = Context.User.Identity.Name;
            string userId = Context.QueryString["UserId"];
            //_connections.Add(userId, Context.ConnectionId);
            Groups.Add(Context.ConnectionId, userId);

            var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
            if (userOnline != null)
            {
                userOnline.IsOnline = true;
                userOnline.ConnectionId = Context.ConnectionId;
                userOnline.Onlined_At = DateTime.Now;
                _unitOfWork.GetRepositoryInstance<UsersOnline>().Update(userOnline);
                _unitOfWork.SaveChanges();
            }
            else
            {
                userOnline = new UsersOnline();
                userOnline.ConnectionId = Context.ConnectionId;
                userOnline.IsOnline = true;
                userOnline.Onlined_At = DateTime.Now;
                userOnline.UserId = userId;
                _unitOfWork.GetRepositoryInstance<UsersOnline>().Add(userOnline);
                _unitOfWork.SaveChanges();
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string userId = Context.QueryString["UserId"];

            //_connections.Remove(userId, Context.ConnectionId);
            Groups.Remove(Context.ConnectionId, userId);
            var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.ConnectionId.Equals(Context.ConnectionId));
            if (userOnline != null)
            {
                userOnline.IsOnline = false;
                userOnline.Offlined_At = DateTime.Now;
                _unitOfWork.GetRepositoryInstance<UsersOnline>().Update(userOnline);
                _unitOfWork.SaveChanges();
            }
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            //string name = Context.User.Identity.Name;
            string userId = Context.QueryString["UserId"];
            //if (!_connections.GetConnections(userId).Contains(Context.ConnectionId))
            //{
            //    _connections.Add(userId, Context.ConnectionId);
            //}

            Groups.Add(Context.ConnectionId, userId);
            var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
            if (userOnline != null)
            {
                userOnline.IsOnline = true;
                userOnline.ConnectionId = Context.ConnectionId;
                userOnline.Onlined_At = DateTime.Now;
                _unitOfWork.GetRepositoryInstance<UsersOnline>().Update(userOnline);
                _unitOfWork.SaveChanges();
            }
            else
            {
                userOnline = new UsersOnline();
                userOnline.ConnectionId = Context.ConnectionId;
                userOnline.IsOnline = true;
                userOnline.Onlined_At = DateTime.Now;
                userOnline.UserId = userId;
                _unitOfWork.GetRepositoryInstance<UsersOnline>().Add(userOnline);
                _unitOfWork.SaveChanges();
            }
            return base.OnReconnected();
        }
        #endregion
        public void UpdateDriver(object obj)
        {
            Clients.All.ReceivedData(obj);
        }
        public void ReloadData(object obj)
        {
            Clients.All.ReceivedData(obj);
        }
        public string SendMessage(string userId, string mess)
        {

            foreach (var connectionId in _connections.GetConnections(userId))
            {
                Clients.Client(connectionId).SendData(mess);
            }
            return mess;
        }
        public string ReceivedData(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return json;
        }
        public string AcceptOrderId(long orderId)
        {
            var json = JsonConvert.SerializeObject(orderId);
            return json;
        }
        public void SendData(string toUserId, object obj)
        {
            Clients.Group(toUserId).ReceivedData(obj);
        }
        public bool Accept(int orderId, string ownerId, string parrentId)
        {
            string userId = Context.QueryString["UserId"];
            try
            {
               var obj = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x=>x.OrderId == orderId && x.OwnerId.Equals(ownerId));
                if(obj == null)
                {
                    obj = new OrderTracking();
                    obj.Updated_At = DateTime.Now;
                    obj.Created_At = DateTime.Now;
                    obj.OwnerId = ownerId;
                    obj.OrderId = orderId;
                    obj.Status = PublicConstant.ORDER_ACCEPT;
                    obj.DriverId = userId;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                }
                else
                {
                    obj.Updated_At = DateTime.Now;
                    obj.Status = PublicConstant.ORDER_ACCEPT;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(obj);
                }
                _unitOfWork.SaveChanges();
                var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                Clients.Group(ownerId).AcceptOrderId(orderId);
                Clients.Group(parrentId).AcceptOrderId(orderId);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string Ping()
        {
            return DateTime.Now.ToString();
        }
        public bool Cancel(int orderId, string ownerId, string parrentId)
        {
            string userId = Context.QueryString["UserId"];
            try
            {
                var orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(userId));
                if (orderTracking != null)
                {
                    orderTracking.OrderId = orderId;
                    orderTracking.DriverId = userId;
                    orderTracking.OwnerId = ownerId;
                    orderTracking.Status = PublicConstant.ORDER_CANCEL_BY_DRIVER;
                    orderTracking.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(orderTracking);
                    _unitOfWork.SaveChanges();
                } 
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckIn(double lat, double lng,string parrentId)
        {
            string userId = Context.QueryString["UserId"];
            try
            {
                var dr = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                if (dr != null)
                {
                    dr.Updated_At = DateTime.Now;
                    dr.Lat = lat;
                    dr.Lng = lng;
                    _unitOfWork.GetRepositoryInstance<Driver>().Update(dr);
                    _unitOfWork.SaveChanges();
                    Clients.Group(parrentId).ReceivedData(dr);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public abstract class SignalRBase<THub> : ApiController where THub : IHub
    {
        private readonly Lazy<IHubContext> _hub = new Lazy<IHubContext>(() => GlobalHost.ConnectionManager.GetHubContext<THub>());

        protected IHubContext Hub
        {
            get { return _hub.Value; }
        }
    }
    public interface IUserIdProvider
    {
        string GetUserId(IRequest request);
    }
    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }

}