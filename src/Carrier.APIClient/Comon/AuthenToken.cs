﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;

namespace APIClient.Comon
{
    public class AuthenToken
    {
        public static Token getToken(string username, string password)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    var domain = WebConfigurationManager.AppSettings["PublicDomain"];
                    if (string.IsNullOrEmpty(domain))
                    {
                        domain = "http://api.vantaitoiuu.com/Token";
                    }
                    //domain = "http://localhost:12473/Token";
                    var token = client.UploadString(domain, "POST", "grant_type=password&username=" + username.Trim() + "&password=" + password);
                    return JsonConvert.DeserializeObject(token, typeof(Token)) as Token;
                }
            }
            catch (Exception ex)
            {
                string mss = ex.Message;
                return null;
            }

        }
    }
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }
}