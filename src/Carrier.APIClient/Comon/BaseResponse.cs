﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIClient.Comon
{
    public class BaseResponse<T>
    {
        public string Result { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }
    }
}