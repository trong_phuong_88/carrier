﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Web;

namespace APIClient.Comon
{
    public class LocationHelper
    {
        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var point = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                     "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(point, 4326);
        }
        public static string NiceLocation(double latitude, double longitude)
        {
            int latSeconds = (int)Math.Round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.Abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int)Math.Round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.Abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            string strLocation = string.Format("{0}° {1}' {2}\" {3}, {4}° {5}' {6}\" {7}", Math.Abs(latDegrees),
                 latMinutes,
                 latSeconds,
                 latDegrees >= 0 ? "N" : "S",
                 Math.Abs(longDegrees),
                 longMinutes,
                 longSeconds,
                 latDegrees >= 0 ? "E" : "W");
            return strLocation;
        }
    }

    //namespace Haversine
    //    {
    //public class MyClass
    //{
    //    CoOrdiantes Position1;
    //    CoOrdiantes Position2;
    //    DistanceCalculator Calculator = new DistanceCalculator();

    //    public void MyMethod()
    //    {
    //        Position1 = new CoOrdiantes();
    //        Position1.Latitude = 40.7486;
    //        Position1.Longitude = -73.9864;

    //        Position2 = new CoOrdiantes();
    //        Position2.Latitude = 24.7486;
    //        Position2.Longitude = -72.9864;

    //        double Distance = Calculator.FindDistance(Position1, Position2, DistanceUnit.Kilometers);

    //    }
    //}

    public class DistanceCalculator
    {
        //Haversine's Formula
        public double FindDistance(CoOrdiantes position1, CoOrdiantes position2, DistanceUnit unit)
        {
            double CalculatedDistance = 0;

            //Choosing Earth's Radius
            double EarthRadius = (unit == DistanceUnit.Kilometers) ? 6371 : 3960;

            //Finding the delta of latitude i.e difference between two latitudes in radians.
            double dLat = GetRadianValue(position1.Latitude - position2.Latitude);

            //Finding the delta of latitude i.e difference between two latitudes in radians.
            double dLon = GetRadianValue(position1.Longitude - position2.Longitude);

            //Get rad of Lat1
            double rLat1 = GetRadianValue(position1.Latitude);

            //Get rad of Lat2
            double rLat2 = GetRadianValue(position2.Latitude);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(rLat1) * Math.Cos(rLat2) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

            //Calculate the Angel of Corner Opposite
            //This also can be calculated with other formula mentioned below.
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));

            CalculatedDistance = c * EarthRadius;

            return CalculatedDistance;
        }

        private double GetRadianValue(double value)
        {
            return (Math.PI / 180) * value;
        }
    }

    public class CoOrdiantes
    {
        private double _Latitude;
        public double Latitude
        {
            get
            { return _Latitude; }
            set
            { _Latitude = value; }
        }

        private double _Longitude;
        public double Longitude
        {
            get
            { return _Longitude; }
            set
            { _Longitude = value; }
        }
    }

    public enum DistanceUnit
    {
        Kilometers,
        Miles
    }
}

//}