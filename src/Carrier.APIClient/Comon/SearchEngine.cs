﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Carrier.Models.Entities;
using Carrier.PushNotification;
using Quartz;
using Quartz.Impl;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Security;
using Carrier.Utilities;
using static Carrier.Utilities.GlobalCommon;
using Carrier.Repository;
using System.Data.SqlClient;
using Carrier.APIClient.Comon;
using System.Data.Entity;
using System.Threading;

namespace APIClient.Comon
{
    [Quartz.DisallowConcurrentExecutionAttribute()]
    public class SearchEngine :IJob
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public void Execute(IJobExecutionContext context)
        {
            object execution_lock = new object();
            if (!Monitor.TryEnter(execution_lock, 1))
            {
                return;
            }
            MatchingOrders();
            Monitor.Exit(execution_lock);
        }
        public void MatchingOrders()
        {
            AutoMatchingOrdersPending();
            AutoMatchingNewOrders();
        }
        public async void AutoMatchingNewOrders()
        {
            try
            {
                var lstNewOrders = _unitOfWork.GetRepositoryInstance<SP_GetAllNewOrders_Result>().GetResultBySqlProcedure("SP_GetAllNewOrders").ToList();
                foreach (var order in lstNewOrders)
                {
                    var nearDriver = GetDriverNearOrder(order.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                    if (nearDriver != null)
                    {
                        CarrierHubClient client = new CarrierHubClient();
                        if (client.isOnline(nearDriver.UserId))
                        {
                            await client.SendMessage(order.Created_By, nearDriver.UserId, order.Id);
                        }
                        else
                        {
                            PushMessageForDriver(nearDriver.UserId, order.Id);
                        }
                        var obj = new OrderTracking();
                        obj.Status = 0;
                        obj.Updated_At = DateTime.Now;
                        obj.Created_At = DateTime.Now;
                        obj.DriverId = nearDriver.UserId;
                        obj.OwnerId = order.Created_By;
                        obj.OrderId = order.Id;
                        _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                        _unitOfWork.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public async void AutoMatchingOrdersPending()
        {
            try
            {
               // var nearDriver = GetDriverNearOrder(126, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                //nearDriver.UserId = "8375152d-4388-4242-ab76-7798de4cbc86";
                var lstOrderPending = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetListByParameter(x => x.Status == 0 && DbFunctions.AddMinutes(x.Updated_At, 5) < DateTime.Now).ToList();
                foreach (var item in lstOrderPending)
                {
                    // PushMessageForDriver()
                    var nearDriver = GetDriverNearOrder(item.OrderId.Value, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                    if (nearDriver != null)
                    {
                        item.Updated_At = DateTime.Now;
                        item.DriverId = nearDriver.UserId;
                        CarrierHubClient client = new CarrierHubClient();
                        if (client.isOnline(nearDriver.UserId))
                        {
                            await client.SendMessage(item.OwnerId, nearDriver.UserId, item.OrderId);
                        }
                        else
                        {
                            PushMessageForDriver(nearDriver.UserId, item.OrderId.Value);
                        }
                        _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(item);
                        _unitOfWork.SaveChanges();

                    }
                    else
                    {
                        // Không tìm thấy tài xế sẽ update trạng thái tài xế cancel đơn
                        item.Status = 2;  //Change to Driver cancel
                        item.Updated_At = DateTime.Now;
                        _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(item);
                        _unitOfWork.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="orderId"></param>
        public void PushMessageForDriver(string driverId, long orderId)
        {
            try
            {
                string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
                DevicesPush device = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
                if (device == null)
                {
                    return;
                }
                if (device.Platform.ToLower().Equals("ios"))
                {
                    PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                    PushServices.SetupPushAPN(true);
                    PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken },null,null,null);
                }
                else
                {
                    jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                    PushServices.GcmKey = PublicConstant.GCM_KEY;
                    PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                    PushServices.SetupPushGCM();
                    PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, null, null, null);
                }
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            try
            {
                var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
                var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
                var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId,@distance", sqlOrderId, sqlDistance).ToList();
                if(lstSearch !=null && lstSearch.Count >0)
                {
                    return lstSearch.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }

    public class PushOrdersScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<SearchEngine>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInMinutes(1)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
                  )
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

    }
}