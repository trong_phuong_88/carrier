﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.APIClient.Comon
{
    public class UtitlitysApi
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
    enum Tran_Type
    {
        tru_tien = 1, // Type = 1 là trừ tiền 
        cong_tien = 2 // Type = 2 là cộng tiền
    }
    enum Tran_Status
    {
        pending=0, //chưa chấp nhận
        accept =1, // đã nhận
    }
}