﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Carrier.APIClient.Models;
using Carrier.APIClient.Providers;
using Carrier.APIClient.Results;
using Carrier.Repository;
using APIClient.Helpers;
using APIClient.Comon;
using Carrier.Models.Entities;
using System.Net;
using System.Linq;
using ExtensionMethods;
using Carrier.Utilities;
using Carrier.APIClient.SignalR;
using System.Data.SqlClient;
using GoogleMaps.LocationServices;
using Carrier.APIClient.Comon;

namespace Carrier.APIClient.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : SignalRBase<CarrierMonitorHub>
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private Carrier3Entities db = new Carrier3Entities();
        private ApiResponse response;
        private string log_carrier = "C:\\Users\\data\\Log\\Log_Carrier.txt";
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        //// GET api/Account/UserInfo
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("UserInfo")]
        //public UserInfoViewModel GetUserInfo()
        //{
        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    return new UserInfoViewModel
        //    {
        //        Email = User.Identity.GetUserName(),
        //        HasRegistered = externalLogin == null,
        //        LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
        //    };
        //}

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            try
            {
                var uID = User.Identity.GetUserId();
                Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
                var obj = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(uID));
                if (obj != null)
                {
                    obj.Status = PublicConstant.DRIVER_OFF;
                }
                _unitOfWork.GetRepositoryInstance<Driver>().Update(obj);
                _unitOfWork.SaveChanges();
                Task.Run(() => Hub.Clients.All.UpdateDriver(obj));
                response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                return Ok(response); ;
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Account: Logout " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        //// GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        //[Route("ManageInfo")]
        //public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        //{
        //    IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

        //    if (user == null)
        //    {
        //        return null;
        //    }

        //    List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

        //    foreach (IdentityUserLogin linkedAccount in user.Logins)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = linkedAccount.LoginProvider,
        //            ProviderKey = linkedAccount.ProviderKey
        //        });
        //    }

        //    if (user.PasswordHash != null)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = LocalLoginProvider,
        //            ProviderKey = user.UserName,
        //        });
        //    }

        //    return new ManageInfoViewModel
        //    {
        //        LocalLoginProvider = LocalLoginProvider,
        //        Email = user.UserName,
        //        Logins = logins,
        //        ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
        //    };
        //}
        //// POST api/Account/AddExternalLogin
        //[Route("AddExternalLogin")]
        //public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //    AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

        //    if (ticket == null || ticket.Identity == null || (ticket.Properties != null
        //        && ticket.Properties.ExpiresUtc.HasValue
        //        && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
        //    {
        //        return BadRequest("External login failure.");
        //    }

        //    ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

        //    if (externalData == null)
        //    {
        //        return BadRequest("The external login is already associated with an account.");
        //    }

        //    IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
        //        new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RemoveLogin
        //[Route("RemoveLogin")]
        //public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result;

        //    if (model.LoginProvider == LocalLoginProvider)
        //    {
        //        result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
        //    }
        //    else
        //    {
        //        result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
        //            new UserLoginInfo(model.LoginProvider, model.ProviderKey));
        //    }

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogin
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        //[AllowAnonymous]
        //[Route("ExternalLogin", Name = "ExternalLogin")]
        //public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        //{
        //    if (error != null)
        //    {
        //        return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
        //    }

        //    if (!User.Identity.IsAuthenticated)
        //    {
        //        return new ChallengeResult(provider, this);
        //    }

        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    if (externalLogin == null)
        //    {
        //        return InternalServerError();
        //    }

        //    if (externalLogin.LoginProvider != provider)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //        return new ChallengeResult(provider, this);
        //    }

        //    ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
        //        externalLogin.ProviderKey));

        //    bool hasRegistered = user != null;

        //    if (hasRegistered)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //         ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            OAuthDefaults.AuthenticationType);
        //        ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            CookieAuthenticationDefaults.AuthenticationType);

        //        AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
        //        Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
        //    }
        //    else
        //    {
        //        IEnumerable<Claim> claims = externalLogin.GetClaims();
        //        ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
        //        Authentication.SignIn(identity);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        //[AllowAnonymous]
        //[Route("ExternalLogins")]
        //public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        //{
        //    IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
        //    List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

        //    string state;

        //    if (generateState)
        //    {
        //        const int strengthInBits = 256;
        //        state = RandomOAuthStateGenerator.Generate(strengthInBits);
        //    }
        //    else
        //    {
        //        state = null;
        //    }

        //    foreach (AuthenticationDescription description in descriptions)
        //    {
        //        ExternalLoginViewModel login = new ExternalLoginViewModel
        //        {
        //            Name = description.Caption,
        //            Url = Url.Route("ExternalLogin", new
        //            {
        //                provider = description.AuthenticationType,
        //                response_type = "token",
        //                client_id = Startup.PublicClientId,
        //                redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
        //                state = state
        //            }),
        //            State = state
        //        };
        //        logins.Add(login);
        //    }

        //    return logins;
        //}

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response);
                }
                var user = new ApplicationUser
                {
                    UserName = model.UserName + ":carrier",
                    TwoFactorEnabled = true,
                    PrivateKey = TimeSensitivePassCode.GeneratePresharedKey()
                };
                var checkUser = UserManager.FindByName(user.UserName);
                if (checkUser != null)
                {
                    response = new ApiResponse(ApiResponseCode.ACCOUNT_EXIST_CODE, null, "Tài khoản đã được đăng ký!");
                    return Ok(response);
                }
                user.PhoneNumber = model.MobilePhone;
                user.UsersInfo = new UserInfo();
                user.UsersInfo.FullName = model.FullName;
                user.UsersInfo.ApplicationUserID = user.Id;
                user.UsersInfo.Created_At = DateTime.Now;
                user.UsersInfo.Updated_At = DateTime.Now;
                user.UsersInfo.OrganzationId = 1163;
                user.UsersInfo.LastLogin = DateTime.Now;
                user.UsersInfo.Status = PublicConstant.STATUS_ACTIVE;
                user.UsersInfo.BirthDay = model.BirthDay;
                user.UsersInfo.MobilePhone = model.MobilePhone;
                user.UsersInfo.Address = model.Address;
                user.UsersInfo.CMND = model.CMND;
                string messageT = string.Empty;
                var point = GetLatLongByAddress(ref messageT, model.Address);
                if (point != null)
                {
                    user.UsersInfo.Lat = point.Latitude;
                    user.UsersInfo.Lng = point.Longitude;
                }
                else
                {
                    user.UsersInfo.Lat = 0;
                    user.UsersInfo.Lng = 0;
                }
                var result = await UserManager.CreateAsync(user, model.Password);
                if (model.IsDriver || model.ToString().Equals("True") || model.ToString().Equals("1"))
                {
                    result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_DRIVERS);
                    var driver = new Driver();
                    if (point != null)
                    {
                        driver.Lat = point.Latitude;
                        driver.Lng = point.Longitude;
                    }
                    else
                    {
                        driver.Lat = 0;
                        driver.Lng = 0;
                    }
                    if (model.Gender == 0)
                    {
                        driver.Sex = "Nam";
                    }
                    else if (model.Gender == 1)
                    {
                        driver.Sex = "Nữ";
                    }
                    else if (model.Gender == 2)
                    {
                        driver.Sex = "";
                    }
                    driver.BirthDay = model.BirthDay;
                    driver.Address = model.Address;
                    driver.Phone = model.MobilePhone;
                    driver.Name = model.FullName;
                    driver.UserId = user.Id;
                    driver.Created_At = DateTime.Now;
                    driver.Updated_At = DateTime.Now;
                    driver.OrganizationId = 1163;
                    driver.Status = PublicConstant.STATUS_PENDING;
                    _unitOfWork.GetRepositoryInstance<Driver>().Add(driver);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
                }
                if (result.Succeeded)
                {
                    var token = AuthenToken.getToken(user.UserName, model.Password);
                    response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id }, { "Token", token.AccessToken } }, "Success");
                    var walletuser = new WalletInfo();
                    walletuser = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId == user.Id);
                    if (walletuser != null)
                    {
                        walletuser.Updated_At = DateTime.Now;
                        _unitOfWork.GetRepositoryInstance<WalletInfo>().Update(walletuser);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        walletuser = new WalletInfo();
                        walletuser.Balance = 0;
                        //walletuser.Commission = 1000000;
                        walletuser.Commission = 0;
                        walletuser.Updated_At = DateTime.Now;
                        walletuser.UserId = user.Id;
                        _unitOfWork.GetRepositoryInstance<WalletInfo>().Add(walletuser);
                        _unitOfWork.SaveChanges();
                    }
                    return Ok(response);
                }
                else
                {
                    //result = await UserManager.SetLockoutEnabledAsync(user.Id, true);
                    //result = await UserManager.SetLockoutEndDateAsync(user.Id, DateTime.MaxValue);
                    if (result.Succeeded)
                    {
                        response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id } }, "Success");
                    }
                    else
                    {
                        response = new ApiResponse(ApiResponseCode.REGISTER_LOCK_FAILURE, null, result.Errors.FirstOrDefault<string>());
                    }
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Account: Register " + ex.Message, log_carrier);
                return Ok(response);
            }

        }

        [AllowAnonymous]
        [Route("RegisterDriver")]
        public async Task<IHttpActionResult> RegisterDriver(RegisterDriverBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response);
                }
                var user = new ApplicationUser
                {
                    UserName = model.UserName + ":carrier",
                    TwoFactorEnabled = true,
                    PrivateKey = TimeSensitivePassCode.GeneratePresharedKey()
                };
                var checkUser = UserManager.FindByName(user.UserName);
                if (checkUser != null)
                {
                    response = new ApiResponse(ApiResponseCode.ACCOUNT_EXIST_CODE, null, "Tài khoản đã được đăng ký!");
                    return Ok(response);
                }
                user.PhoneNumber = model.MobilePhone;
                user.UsersInfo = new UserInfo();
                user.UsersInfo.FullName = model.FullName;
                user.UsersInfo.ApplicationUserID = user.Id;
                user.UsersInfo.Created_At = DateTime.Now;
                user.UsersInfo.Updated_At = DateTime.Now;
                user.UsersInfo.OrganzationId = 1163;
                user.UsersInfo.LastLogin = DateTime.Now;
                user.UsersInfo.Status = PublicConstant.STATUS_ACTIVE;
                user.UsersInfo.BirthDay = model.BirthDay;
                user.UsersInfo.MobilePhone = model.MobilePhone;
                user.UsersInfo.Address = model.Address;
                user.UsersInfo.CMND = model.CMND;
                string messageT = string.Empty;
                var point = GetLatLongByAddress(ref messageT, model.Address);
                if (point != null)
                {
                    user.UsersInfo.Lat = point.Latitude;
                    user.UsersInfo.Lng = point.Longitude;
                }
                else
                {
                    user.UsersInfo.Lat = 0;
                    user.UsersInfo.Lng = 0;
                }
                var result = await UserManager.CreateAsync(user, model.Password);
                if (model.IsDriver || model.ToString().Equals("True") || model.ToString().Equals("1"))
                {
                    result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_DRIVERS);
                    var driver = new Driver();
                    if (point != null)
                    {
                        driver.Lat = point.Latitude;
                        driver.Lng = point.Longitude;
                    }
                    else
                    {
                        driver.Lat = 0;
                        driver.Lng = 0;
                    }
                    if (model.Gender == 0)
                    {
                        driver.Sex = "Nam";
                    }
                    else if (model.Gender == 1)
                    {
                        driver.Sex = "Nữ";
                    }
                    else if (model.Gender == 2)
                    {
                        driver.Sex = "";
                    }
                    driver.BirthDay = model.BirthDay;
                    driver.Address = model.Address;
                    driver.Phone = model.MobilePhone;
                    driver.Name = model.FullName;
                    driver.UserId = user.Id;
                    driver.Created_At = DateTime.Now;
                    driver.Updated_At = DateTime.Now;
                    driver.OrganizationId = 1163;
                    driver.Status = PublicConstant.STATUS_PENDING;
                    _unitOfWork.GetRepositoryInstance<Driver>().Add(driver);
                    _unitOfWork.SaveChanges();

                    var checkcar = new Car();
                    checkcar.Address = model.AddressCar;
                    checkcar.License = model.License;
                    checkcar.Leng_Size = model.Leng_Size;
                    checkcar.Height_Size = model.Height_Size;
                    checkcar.Width_Size = model.Width_Size;
                    checkcar.Payload = model.Payload;
                    checkcar.Lat = model.Lat;
                    checkcar.Lng = model.Lng;
                    checkcar.OrganizationId = 1163;
                    checkcar.Created_At = DateTime.Now;
                    checkcar.Created_By = user.Id;
                    checkcar.Updated_At = DateTime.Now;
                    checkcar.Updated_By = user.Id;
                    checkcar.Description = model.Description;
                    _unitOfWork.GetRepositoryInstance<Car>().Add(checkcar);
                    _unitOfWork.SaveChanges();
                    //Add bang drivercar
                    var findcar = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(c => c.License.Equals(model.License));
                    if (findcar != null)
                    {
                        var userId = user.Id;
                        var drive = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(d => d.UserId.Equals(userId));
                        if (drive != null)
                        {
                            var iddriver = drive.Id;
                            var driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(c => c.DriverId == iddriver);
                            if (driverCar != null)
                            {
                                driverCar.CarId = findcar.Id;
                                _unitOfWork.GetRepositoryInstance<DriverCar>().Update(driverCar);
                                _unitOfWork.SaveChanges();
                            }
                            else
                            {
                                driverCar = new DriverCar();
                                driverCar.CarId = findcar.Id;
                                driverCar.DriverId = iddriver;
                                _unitOfWork.GetRepositoryInstance<DriverCar>().Add(driverCar);
                                _unitOfWork.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
                }
                if (result.Succeeded)
                {
                    var token = AuthenToken.getToken(user.UserName, model.Password);
                    response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id }, { "Token", token.AccessToken } }, "Success");
                    return Ok(response);
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.REGISTER_LOCK_FAILURE, null, result.Errors.FirstOrDefault<string>());
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Account: RegisterDriver " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [Route("Login")]
        public async Task<IHttpActionResult> Login(LoginBindingModel login)
        {
            try
            {
                var uName = string.Format("{0}:{1}", login.UserName, login.ParentCode.ToLower());
                var token = AuthenToken.getToken(uName, login.Password);
                if (token != null)
                {
                    var dataResponse = new APILoginModel();
                    dataResponse.CurrentToken = token;
                    var user = UserManager.FindByName(uName);
                    var prUser = UserManager.FindByName(login.ParentCode.ToLower());
                    var userInfo = new UsersInfo(user.UsersInfo);
                    userInfo.ParentId = prUser.Id;
                    userInfo.PrivateKey = user.PrivateKey;
                    var car = (from c in db.Car
                               join dc in db.DriverCar on c.Id equals dc.CarId
                               join d in db.Driver on dc.DriverId equals d.Id
                               where d.UserId == user.Id
                               select new
                               {
                                   c.Id
                               }).FirstOrDefault();
                    if (car == null)
                        userInfo.Status = 2;
                    dataResponse.Profile = userInfo;
                    var driverInfo = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(user.Id));
                    dataResponse.DriverInfo = driverInfo;
                    response = new ApiResponse((int)HttpStatusCode.OK, dataResponse, "Success");
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.LOGIN_ERROR, null, "Lỗi không lấy được token, hoặc nhập mật khẩu sai");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Lỗi xảy ra khi đăng nhâp.Vui lòng đăng nhập lại sau ít phút.");
                Ultilities.SaveTextToFile("Account: Login " + ex.Message, log_carrier);
                return Ok(response);
            }
        }
        [AllowAnonymous]
        [Route("SignIn")]
        public async Task<IHttpActionResult> SignIn(SignInBindingModel login)
        {
            try
            {
                var token = AuthenToken.getToken(login.UserName, login.Password);
                if (token != null)
                {
                    var dataResponse = new APILoginModel();
                    dataResponse.CurrentToken = token;
                    var user = UserManager.FindByName(login.UserName);
                    var userInfo = new UsersInfo(user.UsersInfo);
                    dataResponse.Profile = userInfo;
                    response = new ApiResponse((int)HttpStatusCode.OK, dataResponse, "Success");
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.LOGIN_ERROR, null, "Lỗi xảy ra khi xác thực tài  khoản");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Lỗi xảy ra khi đăng nhâp.Vui lòng đăng nhập lại sau ít phút.");
                Ultilities.SaveTextToFile("Account: signIn " + ex.Message, log_carrier);
                return Ok(response);
            }
        }
        [AllowAnonymous]
        [Route("SignUp")]
        public async Task<IHttpActionResult> SignUp(SignUpBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response);
                }
                var user = new ApplicationUser
                {
                    UserName = model.UserName,
                    TwoFactorEnabled = true,
                    PrivateKey = TimeSensitivePassCode.GeneratePresharedKey()
                };
                var checkUser = UserManager.FindByName(user.UserName);
                if (checkUser != null)
                {
                    response = new ApiResponse(ApiResponseCode.ACCOUNT_EXIST_CODE, null, "Tài khoản đã được đăng ký!");
                    return Ok(response);
                }
                user.PhoneNumber = model.MobilePhone;
                user.UsersInfo = new UserInfo();
                user.UsersInfo.FullName = model.FullName;
                user.UsersInfo.ApplicationUserID = user.Id;
                user.UsersInfo.Created_At = DateTime.Now;
                user.UsersInfo.Updated_At = DateTime.Now;
                user.UsersInfo.OrganzationId = 1163;
                user.UsersInfo.LastLogin = DateTime.Now;
                user.UsersInfo.Status = PublicConstant.STATUS_ACTIVE;
                user.UsersInfo.BirthDay = model.BirthDay;
                user.UsersInfo.MobilePhone = model.MobilePhone;
                user.UsersInfo.Address = model.Address;
                var result = await UserManager.CreateAsync(user, model.Password);
                result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
                if (result.Succeeded)
                {
                    var token = AuthenToken.getToken(user.UserName, model.Password);
                    response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id }, { "Token", token.AccessToken } }, "Success");
                    var walletuser = new WalletInfo();
                    try
                    {
                        walletuser = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId == user.Id);
                        if (walletuser != null)
                        {
                            walletuser.Updated_At = DateTime.Now;
                            _unitOfWork.GetRepositoryInstance<WalletInfo>().Update(walletuser);
                            _unitOfWork.SaveChanges();
                        }
                        else
                        {
                            walletuser = new WalletInfo();
                            walletuser.Balance = 0;
                            //walletuser.Commission = 1000000;
                            walletuser.Commission = 0;
                            walletuser.Updated_At = DateTime.Now;
                            walletuser.UserId = user.Id;
                            _unitOfWork.GetRepositoryInstance<WalletInfo>().Add(walletuser);
                            _unitOfWork.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        string mss = ex.Message;

                    }
                    return Ok(response);
                }
                else
                {
                    //result = await UserManager.SetLockoutEnabledAsync(user.Id, true);
                    //result = await UserManager.SetLockoutEndDateAsync(user.Id, DateTime.MaxValue);
                    if (result.Succeeded)
                    {
                        response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id } }, "Success");
                    }
                    else
                    {
                        response = new ApiResponse(ApiResponseCode.REGISTER_LOCK_FAILURE, null, result.Errors.FirstOrDefault<string>());
                    }
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Account: SignUp " + ex.Message, log_carrier);
                return Ok(response);
            }

        }
        [Route("UpdateProfile")]
        public async Task<IHttpActionResult> UpdateProfile(UsersInfoUpdate userInfo)
        {
            try
            {
                var userId = User.Identity.GetUserId();
                var oldUser = _unitOfWork.GetRepositoryInstance<Carrier.Models.Entities.UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(userId));
                if (oldUser != null)
                {
                    if (!string.IsNullOrEmpty(userInfo.FullName))
                    {
                        oldUser.FullName = userInfo.FullName;
                    }

                    if (!string.IsNullOrEmpty(userInfo.MobilePhone))
                    {
                        oldUser.MobilePhone = userInfo.MobilePhone;
                    }
                    if (!string.IsNullOrEmpty(userInfo.Address))
                    {
                        oldUser.Address = userInfo.Address;
                    }
                    oldUser.CMND = userInfo.CMND;
                    oldUser.BirthDay = userInfo.BirthDay;
                    oldUser.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<UserInfoes>().Update(oldUser);
                    _unitOfWork.SaveChanges();
                    response = new ApiResponse(200, null, "Successfully");
                    return Ok(response);
                }
                else
                {
                    return BadRequest("Can not found user");
                }
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Account: UpdateProfile " + ex.Message, log_carrier);
                return NotFound();
            }
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, MyExtensions.GetFirstErrorMessage(ModelState));
                    return Ok(response);
                }

                IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                    model.NewPassword);

                if (!result.Succeeded)
                {
                    response = new ApiResponse(ApiResponseCode.CHANGE_PASS_WORD_FAILURE, null, GetErrorResult(result).ToString());
                    return Ok(response);
                }
                response = new ApiResponse(200, result, "Successfully");

                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Account: changePassword " + ex.Message, log_carrier);
                return NotFound();
            }

        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, MyExtensions.GetFirstErrorMessage(ModelState));
                    return BadRequest(ModelState);
                }

                IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

                if (!result.Succeeded)
                {
                    response = new ApiResponse(200, GetErrorResult(result).ToString(), "Successfully");
                }
                else
                {
                    response = new ApiResponse(200, result, "Successfully");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Account: Setpassword " + ex.Message, log_carrier);
                return NotFound();
            }

        }

        public MapPoint GetLatLongByAddress(ref string message, string address)
        {
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);
                return point;
                //var latitude = point.Latitude;
                //var longitude = point.Longitude;
                //result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
                return null;
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
    public class UsersInfo
    {
        public int Id { get; set; }
        public string Picture { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public string ParentId { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
        public int Status { get; set; }
        public System.DateTime LastLogin { get; set; }
        public string CMND { get; set; }
        public string FullName { get; set; }
        public string PrivateKey { get; set; }
        public string MobilePhone { get; set; }
        public string ApplicationUserID { get; set; }

        public UsersInfo(Carrier.Models.Entities.UserInfoes u)
        {
            Id = u.Id;
            Picture = u.Picture;
            BirthDay = u.BirthDay;
            Address = u.Address;
            Created_At = u.Created_At.Value;
            Updated_At = u.Updated_At.Value;
            Status = u.Status;
            FullName = u.FullName;
            MobilePhone = u.MobilePhone;
            ApplicationUserID = u.ApplicationUserID;
            CMND = u.CMND;
        }
        public UsersInfo(UserInfo u)
        {
            Id = u.Id;
            Picture = u.Picture;
            BirthDay = u.BirthDay;
            Address = u.Address;
            Created_At = u.Created_At;
            Updated_At = u.Updated_At;
            Status = u.Status;
            FullName = u.FullName;
            PrivateKey = null;
            MobilePhone = u.MobilePhone;
            ApplicationUserID = u.ApplicationUserID;
            CMND = u.CMND;
        }
    }
    public class UsersInfoUpdate
    {
        //public string Picture { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public string CMND { get; set; }
        public string FullName { get; set; }
        public string MobilePhone { get; set; }
        public int Gender { get; set; }

    }

}

namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static string GetErrorMessageForKey(this ModelStateDictionary dictionary, string key)
        {
            return dictionary[key].Errors.First().ErrorMessage;
        }
        public static string GetFirstErrorMessage(this ModelStateDictionary dictionary)
        {
            var key = dictionary.Keys.ToList().FirstOrDefault();
            return dictionary[key].Errors.First().ErrorMessage;
        }
    }
}