﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Carrier.Models.Entities;
using Carrier.Utilities;
using System.IO;
using Newtonsoft.Json.Linq;
using GoogleMaps.LocationServices;
using System.Data.Entity.Spatial;
using APIClient.Comon;
using static Carrier.Utilities.GlobalCommon;
using Carrier.PushNotification;
using APIClient.Models;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using APIClient.Helpers;
using Carrier.APIClient.SignalR;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Carrier.Algorithm;
using Carrier.Algorithm.Entity;
using Carrier.APIClient.Models;
using Carrier.APIClient.Comon;
using System.Text;
using System.Globalization;


namespace Carrier.APIClient.Controllers
{
    [Authorize]
    [RoutePrefix("api/VanDon")]
    public class VanDonController : SignalRBase<CarrierMonitorHub>
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;
        public const string strAutoCompleteGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?&address=";
        public string[] strGoogleApiKey = new string[] {"AIzaSyDS0qjNAu5q2f6RVrIQPOpX6p1YIKsfRAk", "AIzaSyCsLVOjL7LNgcQGsYOvKPOVZSKsa6EnpRQ", "AIzaSyCARfHAFgolXR9E9UeyhagoHbiIGYJ_-BA", "AIzaSyA7M707k10-GilO-n5v0XXd5DU35lvXDWo",
            "AIzaSyAXERtjOd_62dRzzFW_pkWisvelbj8-dow", "AIzaSyD8rWL1vLSsISFmz7PsiNKhIBttOjMDswg", "AIzaSyBfDanw4xVAj_pLahkFEsjbKqyl4jdWqb4", "AIzaSyAt7iM_2g_VlIGtUAJG2O-OIlxLXTazfEs",
            "AIzaSyB1Nd0PSoUIJJf0Nux81R7U6P-EThk9_TM","AIzaSyCR2kBa7TNKIOf3BELdxWyL5npuuJ_YuUM"};
        const string urlLocations = "https://maps.googleapis.com/maps/api/directions/json?";
        private string log_carrier = "C:\\Users\\data\\Log\\Log_Carrier.txt";
        private string userId { get { return User.Identity.GetUserId(); } }

        [Route("GetPrice")]
        [HttpPost]
        public async Task<IHttpActionResult> GetPriceAPI(ParameterGetPriceAPI obj)
        {
            try
            {
                var cti = new CultureInfo("vi-VN");
                var cen = new CultureInfo("en");
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ",";
                provider.NumberGroupSeparator = ".";
                provider.NumberGroupSizes = new int[] { 3 };
                //load cau hinh
                string uid = User.Identity.GetUserId();
                var tinhgiarieng = _unitOfWork.GetRepositoryInstance<SysSetting>().GetFirstOrDefaultByParameter(s => s.UserId == uid && s.SettingKey == "BANG_GIA_VAN");

                string message = "";
                double price = 0;
                double priceOne = 0;
                string type = "";
                int quanlity = 1;
                var arrayAddresssug = obj.addresssug.Split(new[] { "|" }, StringSplitOptions.None);
                var arratAddressApi = obj.formataddress.Split(new[] { "|" }, StringSplitOptions.None);
                //var arrayAddress = address.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLat = obj.lat.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLng = obj.lng.Split(new[] { "|" }, StringSplitOptions.None);
                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                List<Algorithm.Entity.Location> listLC = new List<Algorithm.Entity.Location>();
                List<string> ListAddress = new List<string>();
                double Lat, Lng;
                for (int i = 0; i < arrayLat.Length; i++)
                {
                    Algorithm.Entity.Location lc = new Algorithm.Entity.Location();

                    Lat = double.Parse(arrayLat[i], NumberStyles.Any, cen);
                    Lng = double.Parse(arrayLng[i], NumberStyles.Any, cen);

                    lc.Lat = Lat;
                    lc.Lng = Lng;
                    ListAddress.Add(arrayAddresssug[i]);
                    listLC.Add(lc);
                }

                double origirnDistance = obj.distance;

                type = obj.cartype;
                priceOne = 0;
                if (tinhgiarieng != null && tinhgiarieng.SettingValue != "0")
                {
                    priceOne = SysLogistic.GetPriceForEnterprise(uid, type, origirnDistance, ListAddress);
                    if (priceOne == 0)
                    {
                        response = new ApiResponse(ApiResponseCode.GETPRICE_FAIL, null, "Bảng giá chưa có sẵn cho tuyến này");
                        return Ok(response);
                    }
                }
                else
                {
                    priceOne = SysLogistic.GetPrice(type, origirnDistance, ListAddress, arrayAddresssug, arratAddressApi, out message);
                    if (priceOne == 0)
                    {
                        if (priceOne == 0)
                        {
                            response = new ApiResponse(ApiResponseCode.GETPRICE_FAIL, null, "Bảng giá chưa có sẵn cho tuyến này");
                            return Ok(response);
                        }
                    }
                }

                if (priceOne > 0)
                {
                    price = price + (priceOne * quanlity);
                }

                if (message.Length == 0 && price > 0)
                {
                    //price = GetDiscount(price);
                    string p = price.ToString("#,###");
                    //response = new ApiResponse(HttpStatusCode.OK, new { distance = obj.distance, totalPrice = p, oldPrice = totalPriceNoVat, message = "Tính giá thành công" }, "Successfully");
                    response = new ApiResponse(HttpStatusCode.OK, price, "Successfully");
                    return Ok(response);
                }
                else
                {
                    if (message.Length == 0 && price == 0)
                    {
                        response = new ApiResponse(ApiResponseCode.GETPRICE_FAIL, null, "Bảng giá chưa có sẵn cho tuyến này");
                    }
                    response = new ApiResponse(HttpStatusCode.InternalServerError, null, "Lỗi tính giá vận đơn");
                    return Ok(response);
                }


            }
            catch (Exception ex)
            {
                //TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                //string log = @"C:/Log/ExceptionCMS.txt";
                //System.IO.File.AppendAllText(log, System.Reflection.MethodBase.GetCurrentMethod().Name + DateTime.Now + ex.InnerException.Message + System.Environment.NewLine);
                response = new ApiResponse(HttpStatusCode.InternalServerError, null, "Lỗi xảy ra, không thể tính giá");
                Ultilities.SaveTextToFile("Method: GetPriceAPI " + ex.Message, log_carrier);
                return Ok(response);
            }
        }
        public class ResponseGetPrice
        {
            public string price { get; set; }
            public string quangduong { get; set; }
            public string thoigian { get; set; }
        }
        public double GetDiscount(double Gia)
        {
            double discount = Gia;

            return Gia * 0.9;
        }
        // GET: api/VanDon/5
        [ResponseType(typeof(OrderBindingModel))]
        [Route("Detail")]
        [HttpGet]
        public async Task<IHttpActionResult> GetOrder(int id)
        {
            //var objStatsu = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.DriverId.Equals(userId));
            //if (objStatsu != null && objStatsu.Status == 2)
            //{
            //    response = new ApiResponse(-1, null, "Bạn đã hết hạn nhận vận đơn này.Đơn đã được chuyển tới tài xế khác!");
            //}
            //else
            //{
            var OrderViewModel = new OrderBindingModel();
            Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(id);
            OrderTracking orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(o => o.OrderId == id);
            if (orderTracking != null)
            {
                OrderViewModel.TrackingStatus = orderTracking.Status;
                Driver dr = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(d => d.UserId == orderTracking.DriverId);
                if (dr != null)
                {
                    OrderViewModel.DriverName = dr.Name;
                    DriverCar driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(dc => dc.DriverId == dr.Id);
                    if (driverCar != null)
                    {
                        Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(c => c.Id == driverCar.CarId);
                        if (car != null)
                        {
                            OrderViewModel.License = car.License;
                            OrderViewModel.Payload = car.Payload;
                        }
                    }
                }
            }
            else
            {
                OrderViewModel.TrackingStatus = null;
            }
            order.Gia = order.Gia.Value;
            var orderid = order.Id;
            var lstDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
            OrderViewModel.Id = order.Id;
            OrderViewModel.Gia = order.Gia;
            OrderViewModel.Status = order.Status;
            OrderViewModel.Note = order.Note;
            OrderViewModel.SoKmUocTinh = order.SoKmUocTinh;
            OrderViewModel.TenHang = order.TenHang;
            OrderViewModel.DiemDiChiTiet = order.DiemDiChiTiet;
            OrderViewModel.DiemDenChiTiet = order.DiemDenChiTiet;
            OrderViewModel.ThoiGianDi = order.ThoiGianDi;
            OrderViewModel.ThoiGianDen = order.ThoiGianDen;
            OrderViewModel.UserName = order.UserName;
            OrderViewModel.LoaiThanhToan = order.LoaiThanhToan;
            OrderViewModel.MaVanDon = order.MaVanDon;
            OrderViewModel.CreateAt = order.CreateAt;
            OrderViewModel.UpdateAt = order.UpdateAt;
            OrderViewModel.FromLat = order.FromLat;
            OrderViewModel.ToLat = order.ToLat;
            OrderViewModel.FromLng = order.FromLng;
            OrderViewModel.ToLng = order.ToLng;
            OrderViewModel.Height = order.Height;
            OrderViewModel.Width = order.Width;
            OrderViewModel.Lenght = order.Lenght;
            OrderViewModel.VAT = order.VAT;
            OrderViewModel.TrongLuong = order.TrongLuong;
            OrderViewModel.Created_By = order.Created_By;
            OrderViewModel.ParentId = order.ParentId;
            OrderViewModel.NguoiLienHe = order.NguoiLienHe;
            OrderViewModel.DienThoaiLienHe = order.DienThoaiLienHe;
            var dth = String.Empty;
            var lstTH = new List<string>();
            foreach (var item in lstDiemTraHang)
            {
                dth = item.FormatAddress;
                lstTH.Add(dth);
            }
            OrderViewModel.DiemTraHang = lstTH;
            string name = string.Empty, phone = string.Empty;
            var lstSettings = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetListByParameter(o => o.UserId.Equals(order.Created_By)).ToList();
            if (lstSettings.Count > 0)
            {
                name = lstSettings.Where(x => x.SettingKey.Equals("TenDieuPhoiVien")).FirstOrDefault().SettingValue;
                phone = lstSettings.Where(x => x.SettingKey.Equals("DienThoaiDieuPhoiVien")).FirstOrDefault().SettingValue;
            }
            var dieuvanInfo = new Dictionary<string, string> { { "Phone", phone }, { "Name", name } };
            //var dieuvan = _unitOfWork.GetRepositoryInstance<SysSetting>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(order.Created_By) && x.SettingKey.Equals("DIEU_VAN_INFO"));
            //dynamic dataInfo = JsonConvert.DeserializeObject(dieuvan.SettingValue);
            response = new ApiResponse(HttpStatusCode.OK, new Dictionary<string, object> { { "Order", OrderViewModel }, { "SubPerson", dieuvanInfo } }, "Successfully");
            //}

            return Ok(response);
        }
        [Route("GetCarByDriver")]
        [HttpGet]
        //public async Task<IHttpActionResult> GetCarInfo(string userIdDriver)
        public async Task<IHttpActionResult> GetOrderByMVD(string mavandon)
        {
            try
            {
                //var sqlDriverId = new SqlParameter("@driverId", System.Data.SqlDbType.NVarChar) { Value = userIdDriver };
                var sqMavandon = new SqlParameter("@mavandon", System.Data.SqlDbType.NVarChar) { Value = mavandon };
                var objCar = _unitOfWork.GetRepositoryInstance<SP_GetOrder_By_MaVanDon_Result>().GetResultBySqlProcedure("SP_GetOrder_By_MaVanDon @mavandon", sqMavandon).FirstOrDefault();
                if (objCar == null)
                {
                    response = new ApiResponse(ApiResponseCode.MAVANDON_INVALID, null, "Không tìm thấy thông tin đơn hàng");
                    return Ok(response);
                    //sqMavandon = new SqlParameter("@mavandon", System.Data.SqlDbType.NVarChar) { Value = "80699056" };
                    //objCar = _unitOfWork.GetRepositoryInstance<SP_GetOrder_By_MaVanDon_Result>().GetResultBySqlProcedure("SP_GetOrder_By_MaVanDon @mavandon", sqMavandon).FirstOrDefault();
                }
                response = new ApiResponse(HttpStatusCode.OK, objCar, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Method: GetCarInfo " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }

        [ResponseType(typeof(OrderBindingModel))]
        [Route("OrderDetail")]
        [HttpGet]
        public async Task<IHttpActionResult> OrderDetail(int id)
        {
            //var objStatsu = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.DriverId.Equals(userId));
            //if (objStatsu != null && objStatsu.Status == 2)
            //{
            //    response = new ApiResponse(-1, null, "Bạn đã hết hạn nhận vận đơn này.Đơn đã được chuyển tới tài xế khác!");
            //}
            //else
            //{
            var OrderViewModel = new OrderDetail();
            Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(id);
            OrderTracking orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(o => o.OrderId == id);
            if (orderTracking != null)
            {
                OrderViewModel.TrackingStatus = orderTracking.Status.ToString();
                Driver dr = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(d => d.UserId == orderTracking.DriverId);
                if (dr != null)
                {
                    OrderViewModel.DriverName = dr.Name;
                    DriverCar driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(dc => dc.DriverId == dr.Id);
                    if (driverCar != null)
                    {
                        Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(c => c.Id == driverCar.CarId);
                        if (car != null)
                        {
                            OrderViewModel.License = car.License;
                            OrderViewModel.Payload = car.Payload.ToString();
                        }
                    }
                    OrderViewModel.DriverLat = dr.Lat;
                    OrderViewModel.DriverLng = dr.Lng;
                }
            }
            else
            {
                OrderViewModel.TrackingStatus = null;
            }
            order.Gia = order.Gia.Value;
            var orderid = order.Id;
            var lstDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
            OrderViewModel.Id = order.Id;
            OrderViewModel.Gia = order.Gia;
            OrderViewModel.Status = order.Status;
            OrderViewModel.Note = order.Note;
            OrderViewModel.SoKmUocTinh = order.SoKmUocTinh;
            OrderViewModel.TenHang = order.TenHang;
            OrderViewModel.DiemDiChiTiet = order.DiemDiChiTiet;
            OrderViewModel.DiemDenChiTiet = order.DiemDenChiTiet;
            OrderViewModel.ThoiGianDi = order.ThoiGianDi;
            OrderViewModel.ThoiGianDen = order.ThoiGianDen;
            OrderViewModel.UserName = order.UserName;
            OrderViewModel.LoaiThanhToan = order.LoaiThanhToan;
            OrderViewModel.MaVanDon = order.MaVanDon;
            OrderViewModel.CreateAt = order.CreateAt;
            OrderViewModel.UpdateAt = order.UpdateAt;
            OrderViewModel.FromLat = order.FromLat;
            OrderViewModel.ToLat = order.ToLat;
            OrderViewModel.FromLng = order.FromLng;
            OrderViewModel.ToLng = order.ToLng;
            OrderViewModel.Height = order.Height;
            OrderViewModel.Width = order.Width;
            OrderViewModel.Lenght = order.Lenght;
            OrderViewModel.VAT = order.VAT;
            OrderViewModel.TrongLuong = order.TrongLuong;
            OrderViewModel.Created_By = order.Created_By;
            OrderViewModel.ParentId = order.ParentId.ToString();
            OrderViewModel.NguoiLienHe = order.NguoiLienHe;
            OrderViewModel.DienThoaiLienHe = order.DienThoaiLienHe;
            var dth = String.Empty;
            var lstTH = new List<string>();
            foreach (var item in lstDiemTraHang)
            {
                dth = item.FormatAddress;
                lstTH.Add(dth);
            }
            var listImage = new List<string>();
            var lstPicture = _unitOfWork.GetRepositoryInstance<OrderMedias>().GetListByParameter(x => x.OrderId == orderid).ToList();
            var img = string.Empty;
            foreach (var item in lstPicture)
            {
                img = item.FilePath;
                listImage.Add(img);
            }
            OrderViewModel.Image = listImage;
            OrderViewModel.DiemTraHang = lstTH;
            string name = string.Empty, phone = string.Empty;
            var lstSettings = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetListByParameter(o => o.UserId.Equals(order.Created_By)).ToList();
            if (lstSettings.Count > 0)
            {
                name = lstSettings.Where(x => x.SettingKey.Equals("TenDieuPhoiVien")).FirstOrDefault().SettingValue;
                phone = lstSettings.Where(x => x.SettingKey.Equals("DienThoaiDieuPhoiVien")).FirstOrDefault().SettingValue;
            }
            var dieuvanInfo = new Dictionary<string, string> { { "Phone", phone }, { "Name", name } };
            //var dieuvan = _unitOfWork.GetRepositoryInstance<SysSetting>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(order.Created_By) && x.SettingKey.Equals("DIEU_VAN_INFO"));
            //dynamic dataInfo = JsonConvert.DeserializeObject(dieuvan.SettingValue);
            response = new ApiResponse(HttpStatusCode.OK, new Dictionary<string, object> { { "Order", OrderViewModel }, { "SubPerson", dieuvanInfo } }, "Successfully");
            //}

            return Ok(response);
        }

        // POST: api/VanDon
        [ResponseType(typeof(VanDonBindingModel))]
        [Route("AddOrder")]
        [HttpPost]
        public async Task<IHttpActionResult> PostOrder(VanDonBindingModel mode)
        {
            try
            {
                var newOrder = new Order();
                newOrder.UserName = User.Identity.Name;
                //newOrder.Status = PublicConstant.STATUS_ACTIVE;
                newOrder.Status = PublicConstant.STATUS_ACTIVE;
                newOrder.TenHang = mode.TenHang;
                newOrder.VAT = mode.VAT == 1 ? true : false;
                newOrder.Gia = mode.Gia;
                newOrder.Note = mode.Note;
                newOrder.TrongLuong = mode.TrongLuong;
                newOrder.DiemDenChiTiet = mode.DiemDenChiTiet;
                newOrder.DiemDiChiTiet = mode.DiemDiChiTiet;
                newOrder.SoKmUocTinh = mode.SoKmUocTinh;
                newOrder.Width = mode.Width;
                newOrder.Lenght = mode.Lenght;
                newOrder.Height = mode.Height;
                newOrder.ThoiGianDen = UtitlitysApi.UnixTimeStampToDateTime(mode.ThoiGianDen);
                newOrder.ThoiGianDi = UtitlitysApi.UnixTimeStampToDateTime(mode.ThoiGianDi);
                newOrder.LoaiThanhToan = mode.LoaiThanhToan;
                newOrder.DienThoaiLienHe = mode.Phone;
                newOrder.FromLat = mode.FromLatitude;
                newOrder.FromLng = mode.FromLongitude;
                newOrder.ToLat = mode.ToLatitude;
                newOrder.ToLng = mode.ToLongitude;
                newOrder.CreateAt = DateTime.Now;
                newOrder.Created_By = userId;
                newOrder.NguoiLienHe = mode.NguoiLienHe;
                newOrder.IsViewPopupOrder = false;
                newOrder.UpdateAt = DateTime.Now;
                //newOrder.MaVanDon = Ultilities.GenerateOrderCode(mode.ThoiGianDi.ToString(), User.Identity.Name);
                newOrder.MaVanDon = Ultilities.RandomNumber(8);
                _unitOfWork.GetRepositoryInstance<Order>().Add(newOrder);
                _unitOfWork.SaveChanges();

                //var ordercreate = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.MaVanDon == newOrder.MaVanDon);
                //MapOrder orMap = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefaultByParameter(o => o.ToUser.Equals(userId));
                //if (orMap == null)
                //{
                //    //var nearDriver = GetDriverNearOrder(newOrder.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);

                //    var nearDriver = GetDriverNearOrder(newOrder.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                //    if (nearDriver != null)
                //    {
                //        await Task.Run(() => Hub.Clients.Group(nearDriver.UserId).ReceivedData(newOrder));
                //        PushMessageForDriver(nearDriver.UserId, newOrder.Id);
                //    }
                //}
                response = new ApiResponse(HttpStatusCode.OK, newOrder, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Method: PostOrder " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                return Ok(response);
            }

        }

        // DELETE: api/VanDon/5
        //[ResponseType(typeof(Order))]
        //API Cancel đơn hàng. Trường hợp đang chạy không được xóa hoặc hủy
        [Route("Delete")]
        [HttpPost]
        public async Task<IHttpActionResult> DeleteOrder(int orderid)
        {
            try
            {
                //_unitOfWork.GetRepositoryInstance<Order>().RemoveByWhereClause(x => x.Id == id);
                //_unitOfWork.SaveChanges();
                var ordertracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderid);
                if (ordertracking.Status != PublicConstant.ORDER_CARRYING)
                {
                    string driver_id = ordertracking.DriverId;
                    //Được phép xóa vận đơn
                    ordertracking.IsDelete = true;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(ordertracking);
                    _unitOfWork.SaveChanges();
                    PushMessageCancelOrder(ordertracking.DriverId);
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");

                    var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderid);
                    if (order != null)
                    {
                        TransactionOrder transaction = new TransactionOrder();
                        var wallet_user = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId == driver_id);
                        if (wallet_user != null)
                        {
                            var tran_acceptorder = _unitOfWork.GetRepositoryInstance<TransactionOrder>().GetFirstOrDefaultByParameter(x => x.OrderId == orderid);
                            if (tran_acceptorder != null)
                            {
                                wallet_user.Commission += tran_acceptorder.CommissionChange;
                                wallet_user.Balance += tran_acceptorder.BalanceChange;
                                transaction.CommissionChange = tran_acceptorder.CommissionChange;
                                transaction.BalanceChange = tran_acceptorder.BalanceChange;
                                transaction.CurrentBalanceWallet = wallet_user.Commission + wallet_user.Balance;
                            }
                        }
                        transaction.Created_At = DateTime.Now;
                        transaction.Created_By = userId;
                        transaction.Description = $"Cộng tiền : {order.Gia} VND do khách hàng hủy đơn hàng, với mã đơn hàng {order.MaVanDon}";
                        transaction.OrderId = order.Id;
                        transaction.Price = order.Gia;
                        transaction.Status = (int)Tran_Status.accept;
                        transaction.ToUser = ordertracking.DriverId;
                        transaction.Update_By = "CARRIER";
                        transaction.UpdatedAt = DateTime.Now;
                        transaction.TransactionType = (int)Tran_Type.cong_tien;
                        transaction.MaVanDon = order.MaVanDon;

                        _unitOfWork.GetRepositoryInstance<TransactionOrder>().Add(transaction);
                        _unitOfWork.SaveChanges();
                    }
                }
                else
                {
                    string mss = "Vận đơn đang khớp lệnh không được xóa!";
                    response = new ApiResponse(ApiResponseCode.VAN_DON_KHONG_DUOC_XOA, null, mss);
                }
                return Ok(response);

            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Method: DeleteOrder " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);

            }
        }

        #region Private method

        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
            //var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
            var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId", sqlOrderId).FirstOrDefault();
            return lstSearch;
        }

        private string GetDistance(string origin, string destination)
        {
            // Build your request to the API
            var request = WebRequest.Create("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destination);
            // Indicate you are looking for a JSON response
            request.ContentType = "application/json; charset=utf-8";
            var response = (HttpWebResponse)request.GetResponse();

            // Read through the response
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                // Define a serializer to read your response
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                // Get your results
                dynamic result = serializer.DeserializeObject(sr.ReadToEnd());

                // Read the distance property from the JSON request
                var distance = result["rows"][0]["elements"][0]["distance"]["text"]; // yields "1,300 KM"
                return distance;
            }
        }
        public void PushMessageForDriver(string driverId, long orderId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device == null)
            {
                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(driverId));
                if (old != null)
                {
                    old.Updated_At = DateTime.Now;
                    old.Status = PublicConstant.ORDER_CANNOT_SEND;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                    _unitOfWork.SaveChanges();
                }
                return;
            }
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
        }
        public void PushMessageCancelOrder(string driverId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Chủ hàng đã hủy đơn hàng\",\"sound\":\"default\"}}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, null, callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Chủ hàng đã hủy đơn hàng\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, null, callback);
            }
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            long orderId = long.Parse(obj2);
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
            var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
            if (old == null)
            {
                var obj = new OrderTracking();
                obj.Status = 0;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                if (result.Equals("Success"))
                {
                    obj.Status = PublicConstant.ORDER_PENDING;
                }
                else
                {
                    obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                }
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
            else
            {
                old.Status = 0;
                old.Updated_At = DateTime.Now;
                old.DriverId = obj1;
                old.OwnerId = order.Created_By;
                old.OrderId = orderId;
                if (result.Equals("Success"))
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                }
                else
                {
                    old.Status = PublicConstant.ORDER_CANNOT_SEND;
                }
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                _unitOfWork.SaveChanges();
            }
        }

        private DbGeography GetLocationFromAddress(string address)
        {
            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(address);
            var address1 = locationService.GetRegionFromLatLong(point.Latitude, point.Longitude);
            var latitude = point.Latitude;
            var longitude = point.Longitude;
            var location = LocationHelper.CreatePoint(latitude, longitude);
            return location;
        }

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        public async Task<string> fnDownloadString(string strUri)
        {
            WebClient webclient = new WebClient();
            webclient.Credentials = CredentialCache.DefaultCredentials;
            webclient.Encoding = ASCIIEncoding.UTF8;
            //wc.Encoding = UTF8Encoding.UTF8;
            string strResultData;
            try
            {
                Uri address = new Uri(strUri);
                strResultData = await webclient.DownloadStringTaskAsync(address);
            }
            catch (Exception ex)
            {
                strResultData = "Error:" + ex.Message;
            }
            finally
            {
                webclient.Dispose();
                webclient = null;
            }

            return strResultData;
        }
    }
    public class OrderBindingModel
    {
        public long Id { get; set; }
        public Nullable<double> Gia { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public Nullable<double> SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public string UserName { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
        public string MaVanDon { get; set; }
        public Nullable<System.DateTime> CreateAt { get; set; }
        public Nullable<System.DateTime> UpdateAt { get; set; }
        public Nullable<double> FromLat { get; set; }
        public Nullable<double> FromLng { get; set; }
        public Nullable<double> ToLat { get; set; }
        public Nullable<double> ToLng { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
        public Nullable<double> Lenght { get; set; }
        public Nullable<bool> VAT { get; set; }
        public Nullable<double> TrongLuong { get; set; }
        public string DienThoaiLienHe { get; set; }
        public string Created_By { get; set; }
        public string NguoiLienHe { get; set; }
        public Nullable<long> ParentId { get; set; }
        public Nullable<int> TrackingStatus { get; set; }
        public List<string> DiemTraHang { get; set; }
        public string License { get; set; }
        public Nullable<double> Payload { get; set; }
        public string DriverName { get; set; }
    }
    public class OrderDetail
    {
        public long Id { get; set; }
        public Nullable<double> Gia { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public Nullable<double> SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public string UserName { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
        public string MaVanDon { get; set; }
        public Nullable<System.DateTime> CreateAt { get; set; }
        public Nullable<System.DateTime> UpdateAt { get; set; }
        public Nullable<double> FromLat { get; set; }
        public Nullable<double> FromLng { get; set; }
        public Nullable<double> ToLat { get; set; }
        public Nullable<double> ToLng { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
        public Nullable<double> Lenght { get; set; }
        public Nullable<bool> VAT { get; set; }
        public Nullable<double> TrongLuong { get; set; }
        public string DienThoaiLienHe { get; set; }
        public string Created_By { get; set; }
        public string NguoiLienHe { get; set; }
        public string ParentId { get; set; }
        public string TrackingStatus { get; set; }
        public List<string> DiemTraHang { get; set; }
        public string License { get; set; }
        public string Payload { get; set; }
        public string DriverName { get; set; }
        public List<string> Image { get; set; }
        public Nullable<double> DriverLat { get; set; }
        public Nullable<double> DriverLng { get; set; }
    }
}