﻿using APIClient.Helpers;
using Carrier.Algorithm;
using Carrier.Algorithm.Entity;
using Carrier.APIClient.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Carrier.APIClient.Controllers
{
    [Authorize]
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;

        [ResponseType(typeof(ParameterGetPriceAPI))]
        [Route("GetPrice")]
        [HttpPost]
        public async Task<IHttpActionResult> GetPriceAPI(ParameterGetPriceAPI parameterGetPriceAPI)
        {
            try
            {
                string message = "";
                string totalPriceNoVat = "";
                double price = 0;
                string type = "";
                int quanlity = 0;
                string uid = User.Identity.GetUserId();

                var arrayAddresssug = parameterGetPriceAPI.addresssug.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayAddress = parameterGetPriceAPI.address.Split(new[] { "|" }, StringSplitOptions.None);
                var arratAddressApi = parameterGetPriceAPI.addressApi.Split(new[] { "|" }, StringSplitOptions.None);
                var _arrayDistance = parameterGetPriceAPI.arrayDistance.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLat = parameterGetPriceAPI.lat.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLng = parameterGetPriceAPI.lng.Split(new[] { "|" }, StringSplitOptions.None);
                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                List<Location> listLC = new List<Location>();
                List<string> ad = new List<string>();
                for (int i = 0; i < arrayLat.Length; i++)
                {
                    Location lc = new Location();
                    arrayLat[i] = arrayLat[i].Replace(".", ",");
                    arrayLng[i] = arrayLng[i].Replace(".", ",");
                    double Lat = Convert.ToDouble(arrayLat[i]);
                    double Lng = Convert.ToDouble(arrayLng[i]);

                    lc.Lat = Lat;
                    lc.Lng = Lng;
                    ad.Add(arrayAddress[i]);
                    listLC.Add(lc);
                }

                float newDistance = parameterGetPriceAPI.newdistance != null ? float.Parse(parameterGetPriceAPI.newdistance) : 0;
                float origirnDistance = parameterGetPriceAPI.origirndistance != null ? float.Parse(parameterGetPriceAPI.origirndistance) : 0;
                long newTime = parameterGetPriceAPI.newtime != null ? long.Parse(parameterGetPriceAPI.newtime) : 0;
                long origirnTime = parameterGetPriceAPI.origirntime != null ? long.Parse(parameterGetPriceAPI.origirntime) : 0;

                string[] strCarTypeSelect = parameterGetPriceAPI.strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();

                if (SysLogisticValidate.IsValid(newDistance, origirnDistance, newTime, origirnTime, listLC, ad))
                {
                    if (strCarTypeSelect.Length > 1)
                    {
                        double priceOne = 0;
                        foreach (string item in strCarTypeSelect)
                        {
                            //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                            type = item.Split('-')[0];
                            quanlity = int.Parse(item.Split(':')[1]);
                            priceOne = 0;
                            if (quanlity > 0)
                            {
                                //priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ad, arrayAddress, arratAddressApi, out message);
                                //priceOne = SysLogistic.GetPriceForEnterprise(uid, type, origirnDistance / 1000, ad);
                                priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ad, arrayAddresssug, arratAddressApi, out message);
                                //priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ad, arratAddressApi, arratAddressApi, out message);
                                if (priceOne > 0)
                                {
                                    price = price + (priceOne * quanlity);
                                }
                            }
                            if (message.Length > 0)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        price = SysLogistic.GetPrice(strCarTypeSelect[0], origirnDistance / 1000, ad, arrayAddresssug, arratAddressApi, out message);
                    }

                    if (message.Length == 0 && price > 0)
                    {
                        price = GetDiscount(price);
                        string p = price.ToString("#,###");
                        var result = Json(new { distance = newDistance, totalPrice = p, oldPrice = totalPriceNoVat, message = "Tính giá thành công" });
                        response = new ApiResponse(200, result, "Successfully");
                    }
                    else
                    {
                        //TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                        message = "Lỗi tính giá vận đơn:" + message;
                    }
                }
                else
                {
                    //TempData["error"] = "Đường đi không phù hợp vì vượt quá thời gian hoặc quá dài: " + message;
                    message = "Đường đi không phù hợp vì vượt quá thời gian hoặc quá dài";
                }
                response = new ApiResponse(ApiResponseCode.LOGGIC_CODE, price, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(200, null, ex.Message);
                return Ok(response);
            }
        }
        public double GetDiscount(double Gia)
        {
            double discount = Gia;

            //select from database ...
            //if (Gia > 1000000) return Gia * 0.9;
            //if (Gia > 300000 && Gia < 1000000) return Gia * 0.95;

            return Gia * 0.9;
        }
        // GET api/orders/5
        public IHttpActionResult Get(int id)
        {
            Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(id);
            if (order == null)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            var orderPlaces = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(op => op.OrderId == id).ToList();
            OrderCar orderCar = _unitOfWork.GetRepositoryInstance<OrderCar>().GetListByParameter(oc => oc.OrderId == id).FirstOrDefault();
            OrderDto orderDto = new OrderDto(order, orderPlaces, orderCar);

            return Ok(orderDto);  // Returns an OkNegotiatedContentResult
        }

        #region PostOrder tham so Jobject
        //// POST api/orders
        //public IHttpActionResult PostOrder(JObject jobject)
        //{
        //    try
        //    {
        //        dynamic json = jobject;

        //        JObject jOrder = json.OrderDto;
        //        var order = jOrder.ToObject<OrderDto>();
        //        JObject jOrderCar = json.OrderCarDto;
        //        var orderCar = jOrderCar.ToObject<OrderCarDto>();
        //        JObject jOrderPlaces = json.OrderPlacesDto;
        //        var orderPlaces = jOrderPlaces.ToObject<OrderPlacesDto>();
        //        response = new ApiResponse(HttpStatusCode.OK, order, "Successfully");
        //        return Ok(response);
        //    }
        //    catch (Exception)
        //    {
        //        response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
        //        return Ok(response);
        //    }
        //}
        #endregion PostOrder tham so Jobject

        // POST api/orders
        public IHttpActionResult PostOrder(OrderDto o)
        {
            try
            {
                var order = new Order
                {
                    Id = o.Id,
                    Gia = o.Gia,
                    Status = o.Status,
                    Note = o.Note,
                    SoKmUocTinh = o.SoKmUocTinh,
                    TenHang = o.TenHang,
                    DiemDenChiTiet = o.DiemDenChiTiet,
                    DiemDiChiTiet = o.DiemDiChiTiet,
                    ThoiGianDen = o.ThoiGianDen,
                    ThoiGianDi = o.ThoiGianDi,
                    UserName = o.UserName,
                    LoaiThanhToan = o.LoaiThanhToan,
                    MaVanDon = o.MaVanDon,
                    CreateAt = o.CreateAt,
                    UpdateAt = o.UpdateAt,
                    FromLat = o.FromLat,
                    FromLng = o.FromLng,
                    ToLat = o.ToLat,
                    ToLng = o.ToLng,
                    Width = o.Width,
                    Height = o.Height,
                    Lenght = o.Lenght,
                    VAT = o.VAT,
                    TrongLuong = o.TrongLuong,
                    DienThoaiLienHe = o.DienThoaiLienHe,
                    Created_By = o.Created_By,
                    NguoiLienHe = o.NguoiLienHe,
                    ParentId = o.ParentId,
                };
                order.UserName = User.Identity.Name;
                order.Created_By = User.Identity.GetUserId();
                order.MaVanDon = Ultilities.RandomNumber(8);
                order.CreateAt = DateTime.Now;
                order.UpdateAt = DateTime.Now;
                // check validation

                _unitOfWork.GetRepositoryInstance<Order>().Add(order);
                _unitOfWork.SaveChanges();

                var lstOrderPlace = new List<OrderPlaces>();
                foreach (var v in o.ListOrderPlaces)
                {
                    var orderPlace = new OrderPlaces();
                    orderPlace.Created_At = DateTime.Now;
                    orderPlace.OrderId = order.Id;
                    orderPlace.FormatAddress = v.FormatAddress;
                    orderPlace.PlaceLat = v.PlaceLat;
                    orderPlace.PlaceLng = v.PlaceLng;
                    orderPlace.Updated_At = DateTime.Now;

                    _unitOfWork.GetRepositoryInstance<OrderPlaces>().Add(orderPlace);
                    _unitOfWork.SaveChanges();
                    lstOrderPlace.Add(orderPlace);
                }

                var orderCar = new OrderCar
                {
                    CarTypeId = o.OrderCarDto.CarTypeId,
                    Number = o.OrderCarDto.Number,
                    OrderId = order.Id
                };
                _unitOfWork.GetRepositoryInstance<OrderCar>().Add(orderCar);
                _unitOfWork.SaveChanges();

                OrderDto orderDto = new OrderDto(order, lstOrderPlace, orderCar);
                response = new ApiResponse(HttpStatusCode.OK, orderDto, "Successfully");
                return Ok(response);
            }
            catch (Exception)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                return Ok(response);
            }
        }
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
