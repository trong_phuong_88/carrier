﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Carrier.Models.Entities;
using Carrier.Repository;
using APIClient.Helpers;
using APIClient.Models;
using Microsoft.AspNet.Identity;
using Carrier.Utilities;
using Newtonsoft.Json.Linq;
using Carrier.PushNotification;
using Carrier.APIClient.SignalR;
using Microsoft.AspNet.SignalR.Client;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Security;
using Carrier.APIClient.Models;
using Carrier.APIClient.Comon;
using System.Globalization;

namespace Carrier.APIClient.Controllers
{

    [RoutePrefix("api/Main")]
    public class MainController : SignalRBase<CarrierMonitorHub>
    {
        #region Property
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        private string log_carrier = "C:\\Users\\data\\Log\\Log_Carrier.txt";
        #endregion
        #region GetPrice old
        //[Authorize]
        //[Route("GetPrice")]
        //[HttpGet]
        //public JObject getTotalOrderValue(float distance, int cartype, string subtype)
        //{
        //    string message = string.Empty;
        //    double totalPrice = 0;
        //    if (cartype == PublicConstant.CAR_TYPE_XETAI)
        //    {
        //        totalPrice = API.Common.ImportExcel.GetPriceForOrder(subtype, distance, ref message, 1);
        //    }
        //    else if (cartype == PublicConstant.CAR_TYPE_CONTAINER)
        //    {

        //    }
        //    else if (cartype == PublicConstant.CAR_TYPE_CHUYENDUNG)
        //    {

        //    }
        //    if (100 < distance && distance < 200)
        //    {
        //        distance = distance * 0.9f;
        //    }
        //    else if (200 < distance && distance < 300)
        //    {
        //        distance = distance * 0.8f;
        //    }
        //    else if (300 < distance)
        //    {
        //        distance = distance * 0.6f;
        //    }
        //    //totalPrice = 700413.9f;
        //    //float totalPrice = CalculatorPrice(cartype, distance, khoiluong, tinhden.Name);
        //    JObject jsonObject = new JObject(
        //     new JProperty("price", totalPrice.ToString("N0") + " VNĐ"));
        //    if (!string.IsNullOrEmpty(message))
        //    {
        //        jsonObject = new JObject(
        //     new JProperty("message", message));
        //    }
        //    //,new JProperty("distance", strDistance));
        //    return jsonObject;
        //}
        #endregion
        public CultureInfo cti = new CultureInfo("vi-VN");

        [Authorize]
        [Route("RegisterNotification")]
        public async Task<IHttpActionResult> RegisterPushNotification(DeviceInfoViewModel pushinfo)
        {
            try
            {
                var current = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
                if (current == null)
                {
                    current = new DevicesPush();
                    current.DeviceToken = pushinfo.DeviceToken;
                    current.Platform = pushinfo.Platform;
                    current.UpdatedAt = DateTime.Now.ToString("dd/MM/yyyy HH:mm"); ;
                    current.SystemVersion = pushinfo.SystemVersion;
                    current.AppVersion = pushinfo.AppVersion;
                    current.UserId = UserId;
                    current.Status = 1;
                    _unitOfWork.GetRepositoryInstance<DevicesPush>().Add(current);
                }
                else
                {
                    current.DeviceToken = pushinfo.DeviceToken;
                    current.Platform = pushinfo.Platform;
                    current.UpdatedAt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    current.SystemVersion = pushinfo.SystemVersion;
                    current.AppVersion = pushinfo.AppVersion;
                    current.Status = 1;
                    _unitOfWork.GetRepositoryInstance<DevicesPush>().Update(current);
                }
                _unitOfWork.SaveChanges();
                response = new ApiResponse(HttpStatusCode.OK, current, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Main: Registernotification " + ex.Message, log_carrier);
                return Ok(response);
            }

        }

        public async Task<IHttpActionResult> CheckVersion()
        {
            var version = WebConfigurationManager.AppSettings["APIVersion"];
            response = new ApiResponse(HttpStatusCode.OK, version, "Successfully");
            return Ok(response);
        }
        [Route("SendMessage")]
        [HttpPost]
        public async Task<IHttpActionResult> SendMessage([FromBody] MessageModel model)
        {
            try
            {
                string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + model.orderId + "\"}";
                //string jsonData = "{\"aps\":{\"badge\":7,\"alert\":\"THong bao khuyen mai\",\"sound\":\"default\"},\"OrderId\":\"" + "66" + "\"}";

                if (model.platform.ToLower().Equals("ios"))
                {
                    // Fake to test
                    //15B84D44F356451A1D88A2FC89A8186C97DF3D6BA2A21A9DC95773E22A7460A2
                    //PushServices.Instance.PushAPNMessage(jsonMessage, new List<string> { device.TokenDevice });
                    PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                    //PushServices.strFileP12 = "C:\\Certificate\\PushDevDriver.p12";

                    List<string> lstDevice = new List<string>();
                    //lstDevice.Add("F365851577AD0F9EBF156F9183614FDD11153A404A5F0A0B0D374593055D7B18");
                    lstDevice.Add(model.deviceToken);
                    PushServices.SetupPushAPN(true);
                    PushServices.PushAPNMessage(jsonMessage, lstDevice, null, null, null);

                }
                else
                {
                    PushServices.PushGCMMessage(jsonMessage, new List<string> { model.deviceToken }, null, null, null);
                }
                return Ok("Successfully");
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Main: Sendmessage " + ex.Message, log_carrier);
                return Ok(ex.Message);
            }

        }

        [HttpPost]
        [Route("TestSendMessage")]
        [Authorize]
        public async Task<IHttpActionResult> TestMessage([FromBody] MessageModel model)
        {
            try
            {
                string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + model.orderId + "\"}";
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                List<string> lstDevice = new List<string>();
                lstDevice.Add(model.deviceToken);
                PushServices.SendNotificationFromFirebaseCloud(jsonMessage, lstDevice);
                return Ok("Successfully");
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Main: TestSendmessage " + ex.Message, log_carrier);
                return Ok(ex.Message);
            }
        }
        [Route("SendNewOrderTest")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> SendNewOrderTest(int orderId, string driverId, int pr)
        {
            try
            {
                var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(orderId);
                var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
                //if (userOnline.IsOnline.Value)
                //{
                //    var hubConnection = new HubConnection("http://api.vantaitoiuu.com/", new Dictionary<string, string>{
                //             { "UserId", driverId }});
                //    var hubProxy = hubConnection.CreateHubProxy("CarrierMonitor");
                //    await hubConnection.Start();
                //    await hubProxy.Invoke("SendData", driverId, order);
                //}
                //else
                //{
                PushMessageForDriver(driverId, order, pr);
                //}
                return Ok();
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Main: SendNewOrderTest " + ex.Message, log_carrier);
                return Ok(ex.Message);
            }

        }

        [HttpGet]
        [Route("Messages")]
        public async Task<IHttpActionResult> ListMessages(string fromUser = null, string toUser = null, int pageNumber = 1, int pageSize = 10)
        {
            try
            {
                var skip = pageSize * (pageNumber - 1);

                var lstResult = new List<Messages>();
                if (!string.IsNullOrEmpty(fromUser) && !string.IsNullOrEmpty(toUser))
                {
                    lstResult = _unitOfWork.GetRepositoryInstance<Messages>().GetListByParameter(x => x.FromUserId.Equals(fromUser) && x.ToUserId.Equals(toUser)).Skip(skip).Take(pageSize).ToList();
                }
                else if (!string.IsNullOrEmpty(fromUser))
                {
                    lstResult = _unitOfWork.GetRepositoryInstance<Messages>().GetListByParameter(x => x.FromUserId.Equals(fromUser)).Skip(skip).Take(pageSize).ToList();
                }
                else if (!string.IsNullOrEmpty(toUser))
                {
                    lstResult = _unitOfWork.GetRepositoryInstance<Messages>().GetListByParameter(x => x.ToUserId.Equals(toUser)).Skip(skip).Take(pageSize).ToList();
                }
                else
                {
                    lstResult = _unitOfWork.GetRepositoryInstance<Messages>().GetAllRecords().Skip(skip).Take(pageSize).ToList();
                }
                response = new ApiResponse(HttpStatusCode.OK, lstResult.OrderByDescending(x => x.Created_At).ToList(), "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Main: ListMessages " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        [HttpGet]
        [Route("GetMessagePopup")]
        public async Task<IHttpActionResult> GetMessagePopup(string userType)
        {
            try
            {
                MessagePopup mss = new MessagePopup();
                if (userType.Equals("User"))
                {
                    mss.title = "Carrier";
                    mss.message = "Quý khách đã đăng ký tài khoản thành công và được tặng eVoucher trị giá 1 triệu đồng có  thời hạn từ 02/4/2019 đến hết 02/5/2019";
                }
                else
                {
                    mss.title = "Carrier";
                    mss.message = "";
                }
                response = new ApiResponse(HttpStatusCode.OK, mss, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Main: GetMessagePopup " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="order"></param>
        private void PushMessageForDriver(string driverId, Order order, int pr)
        {
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + order.Id + "\"}";
            var device = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
            if (device.Platform.ToLower().Equals("ios"))
            {
                if (pr == 1)
                {
                    PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                    PushServices.SetupPushAPN(true);
                    PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, null, null);


                }
                else
                {
                    //jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"}}";
                    PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH;
                    PushServices.SetupPushAPN(false);
                    PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, null, null);

                }
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + order.Id + "\"}";
                //PushServices.GcmKey = PublicConstant.GCM_KEY_LACE;
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, null, null);
            }
        }
        [Route("GetVersion")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetVersion()
        {
            try
            {
                var currentVersion = WebConfigurationManager.AppSettings["VersionName"].ToString();
                var currentCode = WebConfigurationManager.AppSettings["VersionCode"].ToString();
                VersionModel vr = new VersionModel();
                vr.versionCode = int.Parse(currentCode);
                vr.versionName = currentVersion;
                response = new ApiResponse(HttpStatusCode.OK, vr, "Successfully");
                return Ok(response);

            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Main: GetVersion " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        [HttpPost]
        [Route("ListDrivers")]
        [Authorize]
        public async Task<IHttpActionResult> FetchAllDrivers(string orderCode = "", string bsxCode = "")
        {
            try
            {
                var sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
                var sqlBsxCode = new SqlParameter("@bsxCode", System.Data.SqlDbType.NVarChar) { Value = bsxCode };
                var lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId_Enterprise_New @userId,@orderCode,@bsxCode",
                     sqlParentId, sqlOrderCode, sqlBsxCode).ToList();
                var dictData = new Dictionary<string, object>() { { "count", lstSearchOrderTracking.Count() }, { "drivers", lstSearchOrderTracking } };
                return Ok(new ApiResponse(HttpStatusCode.OK, dictData, "Successfully"));
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Main: FetchAllDrivers " + ex.Message, log_carrier);
                return Ok(response);
            }
        }
        [HttpGet]
        [Route("ListOrders")]
        [Authorize]
        public async Task<IHttpActionResult> FetchAllOrders(string mavandon = "", int sttOrdertracking = -1, string timeFr = "", string timeTo = "", int pageNumber = 1)
        {
            try
            {
                var lstResult = new List<OrdersViewPage>();
                var RoleType = "";
                DateTime TgDen = DateTime.Now, TgDi = DateTime.Now, firstDayOfMonth, lastDayOfMonth;
                firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                if (!string.IsNullOrEmpty(timeFr))
                {
                    TgDen = DateTime.Parse(timeFr, cti, DateTimeStyles.None);
                    firstDayOfMonth = TgDen;
                }
                if (!string.IsNullOrEmpty(timeTo))
                {
                    TgDi = DateTime.Parse(timeTo, cti, DateTimeStyles.None);
                    lastDayOfMonth = TgDi;
                }

                if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
                {
                    RoleType = PublicConstant.ROLE_ENTERPRISE;
                }
                if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
                {
                    RoleType = PublicConstant.ROLE_FORWARDER;
                }
                if (User.IsInRole(PublicConstant.ROLE_USERS))
                {
                    RoleType = PublicConstant.ROLE_USERS;
                }
                var sqlRoleType = new SqlParameter("@RoleType", System.Data.SqlDbType.NVarChar) { Value = RoleType };
                var sqlStatus = new SqlParameter("@Status", System.Data.SqlDbType.Int) { Value = sttOrdertracking };
                var sqlOrderStatus = new SqlParameter("@OrderStatus", System.Data.SqlDbType.Int) { Value = -1 };
                var sqlType = new SqlParameter("@Type", System.Data.SqlDbType.Int) { Value = 0 };
                var sqlParentId = new SqlParameter("@ParentId", System.Data.SqlDbType.Int) { Value = 0 };
                var sqlOrderType = new SqlParameter("@OrderType", System.Data.SqlDbType.Int) { Value = 0 };
                var sqlUserId = new SqlParameter("@UserId ", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.NVarChar) { Value = firstDayOfMonth.ToString() };
                var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.NVarChar) { Value = lastDayOfMonth.ToString() };

                var lstSearch = _unitOfWork.GetRepositoryInstance<VanDonDetail>().GetResultBySqlProcedure("SP_GetAllOrdersByParameter_New @RoleType,@OrderStatus,@Status,@Type,@ParentId,@OrderType,@UserId,@FromDate,@ToDate", sqlRoleType, sqlOrderStatus, sqlStatus, sqlType, sqlParentId, sqlOrderType, sqlUserId, sqlFromDate, sqlToDate).OrderByDescending(x => x.Id).ToList();
                foreach (var item in lstSearch)
                {
                    if (item.StatusDeleteOrder == 0 || item.StatusDeleteOrder == null)
                    {
                        OrdersViewPage o = new OrdersViewPage();
                        var lst_img = new List<string>();
                        o.CreateAt = item.CreateAt;
                        o.Created_By = item.Created_By;
                        o.DiemDenChiTiet = item.DiemDenChiTiet;
                        o.DiemDiChiTiet = item.DiemDiChiTiet;
                        o.DriverId = item.DriverId;
                        o.DriverName = item.DriverName;
                        o.Gia = item.Gia;
                        o.Id = item.Id;
                        o.LoaiThanhToan = item.LoaiThanhToan;
                        o.MaVanDon = item.MaVanDon;
                        o.TrongLuong = item.TrongLuong;
                        o.Note = item.Note;
                        o.Width = item.Width;
                        o.Height = item.Height;
                        o.Lenght = item.Lenght;
                        o.SoKmUocTinh = item.SoKmUocTinh;
                        if (item.OrderStatus == 1)
                        {
                            o.OrderStatus = "Đơn tự động";
                        }
                        else if (item.OrderStatus == 0)
                        {
                            o.OrderStatus = "Đơn thủ công";
                        }
                        o.ParentId = item.ParentId;
                        o.TenHang = item.TenHang;
                        o.ThoiGianDen = item.ThoiGianDen;
                        o.ThoiGianDi = item.ThoiGianDi;
                        o.TrackingStatus = item.TrackingStatus;
                        string cartype = "1.25 tấn";
                        var orderCar = _unitOfWork.GetRepositoryInstance<OrderCar>().GetFirstOrDefaultByParameter(x => x.OrderId == item.Id);
                        if (orderCar != null && orderCar.CarTypeId != null)
                        {
                            var car_detail = _unitOfWork.GetRepositoryInstance<CarType>().GetFirstOrDefaultByParameter(x => x.Id == orderCar.CarTypeId);
                            if (car_detail != null)
                                cartype = car_detail.Name;

                        }
                        o.CarType = cartype;

                        var lst_ordermedia = _unitOfWork.GetRepositoryInstance<OrderMedias>().GetListByParameter(x => x.OrderId == item.Id).ToList();
                        foreach (var img in lst_ordermedia)
                        {
                            lst_img.Add(img.FilePath);
                        }
                        o.Images = lst_img;

                        var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId == item.DriverId);
                        if (driver != null)
                        {
                            var drivercar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(x => x.DriverId == driver.Id);
                            if (drivercar != null)
                            {
                                var car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(x => x.Id == drivercar.CarId);
                                if (car != null)
                                {
                                    DriverInfo drInfo = new DriverInfo();
                                    drInfo.BienSoXe = car.License;
                                    drInfo.DiaChi = driver.Address;
                                    drInfo.GioiTinh = driver.Sex;
                                    drInfo.KichThuocXe = car.Leng_Size + "x" + car.Width_Size + "x" + car.Height_Size;
                                    drInfo.NgaySinh = driver.BirthDay;
                                    drInfo.SDT = driver.Phone;
                                    drInfo.TaiTrong = car.Payload.Value;
                                    drInfo.TenTaiXe = driver.Name;
                                    o.DriverInfo = drInfo;
                                }
                            }
                        }
                        lstResult.Add(o);
                    }
                }
                int count = lstResult.Count();

                // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
                int CurrentPage = pageNumber;

                // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
                int PageSize = 50;

                // Display TotalCount to Records to User  
                int TotalCount = count;

                // Calculating Totalpage by Dividing (No of Records / Pagesize)  
                int TotalPages = (int)Math.Ceiling(count / (double)PageSize);

                // Returns List of Customer after applying Paging   
                var items = lstResult.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

                // if CurrentPage is greater than 1 means it has previousPage  
                var previousPage = CurrentPage > 1 ? "Yes" : "No";

                // if TotalPages is greater than CurrentPage means it has nextPage  
                var nextPage = CurrentPage < TotalPages ? "Yes" : "No";

                var paginationMetadata = new
                {
                    totalCount = TotalCount,
                    pageSize = PageSize,
                    currentPage = CurrentPage,
                    totalPages = TotalPages,
                    previousPage,
                    nextPage
                };
                if (!string.IsNullOrEmpty(mavandon))
                {
                    items = items.Where(x => x.MaVanDon.Contains(mavandon)).ToList();
                }
                if (lstResult != null)
                {
                    response = new ApiResponse(HttpStatusCode.OK, items.OrderByDescending(o => o.CreateAt), "Successfully", TotalPages);
                    return Ok(response);
                }
                else
                {
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Main: FetchAllOrders " + ex.Message, log_carrier);
                return Ok(new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Fail " + ex.Message));
            }
        }
        public static string StatusToString(int status)
        {
            var strResult = string.Empty;
            switch (status)
            {
                case 0:
                    {
                        strResult = "Đang khớp lệnh";
                        break;
                    }
                case 1:
                    {
                        strResult = "Đã Khớp lệnh";
                        break;
                    }
                case 2:
                    {
                        strResult = "Tài xế hủy";
                        break;
                    }
                case 3:
                    {
                        strResult = "Chủ hàng hủy";
                        break;
                    }
                case 4:
                    {
                        strResult = "Đang vận chuyển";
                        break;
                    }
                case 5:
                    {
                        strResult = "Hoàn thành";
                        break;
                    }
                case 6:
                    {
                        strResult = "Gửi tài xế lỗi";
                        break;
                    }
                case 7:
                    {
                        strResult = "Đơn hết hạn";
                        break;
                    }

                default: break;
            }
            return strResult;
        }
    }
    public class MessageModel
    {
        public string deviceToken { get; set; }
        public string platform { get; set; }
        public string orderId { get; set; }
    }
    public class VersionModel
    {
        public string versionName { get; set; }
        public int versionCode { get; set; }
    }
    public class MessagePopup
    {
        public string title { get; set; }
        public string message { get; set; }
    }
}
