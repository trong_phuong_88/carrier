﻿using APIClient.Comon;
using APIClient.Helpers;
using Carrier.APIClient.Models;
using Carrier.Repository;
using Carrier.Utilities;
using GoogleMaps.LocationServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Carrier.APIClient.Controllers
{
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;
        public UsersController()
        {
        }

        public UsersController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [AllowAnonymous]
        [Route("UserRegister")]
        public async Task<IHttpActionResult> UserRegister(UserRegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                return Ok(response);
            }
            var user = new ApplicationUser
            {
                UserName = model.UserName,
                TwoFactorEnabled = true,
                PrivateKey = TimeSensitivePassCode.GeneratePresharedKey()
            };
            var checkUser = UserManager.FindByName(user.UserName);
            if (checkUser != null)
            {
                response = new ApiResponse(ApiResponseCode.ACCOUNT_EXIST_CODE, null, "Tài khoản đã được đăng ký!");
                return Ok(response);
            }
            user.PhoneNumber = model.MobilePhone;
            user.UsersInfo = new UserInfo();
            user.UsersInfo.FullName = model.FullName;
            user.UsersInfo.ApplicationUserID = user.Id;
            user.UsersInfo.Created_At = DateTime.Now;
            user.UsersInfo.Updated_At = DateTime.Now;
            user.UsersInfo.OrganzationId = 1163;
            user.UsersInfo.LastLogin = DateTime.Now;
            user.UsersInfo.Status = PublicConstant.STATUS_ACTIVE;
            user.UsersInfo.BirthDay = model.BirthDay;
            user.UsersInfo.MobilePhone = model.MobilePhone;
            user.UsersInfo.Address = model.Address;
            user.UsersInfo.CMND = model.CMND;
            string messageT = string.Empty;
            var point = GetLatLongByAddress(ref messageT, model.Address);
            if (point != null)
            {
                user.UsersInfo.Lat = point.Latitude;
                user.UsersInfo.Lng = point.Longitude;
            }
            else
            {
                user.UsersInfo.Lat = 0;
                user.UsersInfo.Lng = 0;
            }
            var result = await UserManager.CreateAsync(user, model.Password);
            result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
            if (result.Succeeded)
            {
                response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id } }, "Success");
                return Ok(response);
            }
            else
            {
                //result = await UserManager.SetLockoutEnabledAsync(user.Id, true);
                //result = await UserManager.SetLockoutEndDateAsync(user.Id, DateTime.MaxValue);
                if (result.Succeeded)
                {
                    response = new ApiResponse((int)HttpStatusCode.OK, new Dictionary<string, string> { { "PrivateKey", user.PrivateKey }, { "UserId", user.Id } }, "Success");
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.REGISTER_LOCK_FAILURE, null, result.Errors.FirstOrDefault<string>());
                }
                return Ok(response);
            }
        }
        [AllowAnonymous]
        [Route("UserLogin")]
        public IHttpActionResult UserLogin(UserLoginViewModel login)
        {
            try
            {
                var uName = login.UserName;
                var token = AuthenToken.getToken(uName, login.Password);
                if (token != null)
                {
                    var dataResponse = new APIUserLoginModel();
                    dataResponse.CurrentToken = token;
                    var user = UserManager.FindByName(uName);
                    var userInfo = new UsersInfo(user.UsersInfo);
                    userInfo.PrivateKey = user.PrivateKey;
                    dataResponse.Profile = userInfo;
                    response = new ApiResponse((int)HttpStatusCode.OK, dataResponse, "Success");
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.LOGIN_ERROR, null, "Lỗi không lấy được token");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Lỗi xảy ra khi đăng nhâp.Vui lòng đăng nhập lại sau ít phút.");
                return Ok(response);
            }
        }
        public MapPoint GetLatLongByAddress(ref string message, string address)
        {
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);
                return point;
                //var latitude = point.Latitude;
                //var longitude = point.Longitude;
                //result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
                return null;
            }
        }
    }
}
