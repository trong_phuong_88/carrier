﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using APIClient.Providers;
using Carrier.Repository;
using APIClient.Helpers;
using Microsoft.AspNet.Identity;
using Carrier.Utilities;
using System.Web.Helpers;
using Carrier.Models.Entities;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Carrier.APIClient.Controllers
{
    [RoutePrefix("api/Upload")]
    public class UploadController : ApiController
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;
        private string log_carrier = "C:\\Users\\data\\Log\\Log_Carrier.txt";
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        [Route("Evidence")]
        [Authorize]
        public async Task<IHttpActionResult> UploadImage(int orderId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var userId = User.Identity.GetUserId();
                var orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(userId));
                var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(d => d.UserId.Equals(userId));
                var orderMedia = new OrderMedias();
                if (orderTracking != null)
                {
                    //foreach (string file in httpRequest.Files)
                    for (int i = 0; i < httpRequest.Files.Count; i++)
                    {

                        //var postedFile = httpRequest.Files[file];
                        var postedFile = httpRequest.Files[i];
                        if (postedFile != null && postedFile.ContentLength > 0)
                        {

                            //int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  
                            //int MaxContentLength = 1024 * 1024 * 250; //Size = 10 MB  

                            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                            var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                            var extension = ext.ToLower();
                            if (!AllowedFileExtensions.Contains(extension))
                            {

                                var message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                                dict.Add("error", message);
                                response = new ApiResponse(-1, null, message);
                                Ultilities.SaveTextToFile("Upload: UploadImage - orderid " + orderId + " " + response, log_carrier);
                                return Ok(response);
                            }
                            //else if (postedFile.ContentLength > MaxContentLength)
                            //{

                            //    var message = string.Format("Dung lượng file vượt quá 3 mb.");

                            //    dict.Add("error", message);
                            //    response = new ApiResponse(-1, null, message);
                            //    return Ok(response);
                            //}
                            else
                            {

                                HttpPostedFileBase filebase = new HttpPostedFileWrapper(postedFile);
                                string message = "";
                                string filename = userId + "_" + orderId.ToString() + "_" + filebase.FileName;
                                UploadImage(ref message, filebase, filename, PublicConstant.USER_ORIGINALS, PublicConstant.USER_THUMBNAILS, null);
                                string publicRootPath = WebConfigurationManager.AppSettings["ImageUsers"].ToString();

                                orderTracking.EvidencePath = publicRootPath + filename;

                                orderMedia.FilePath = publicRootPath + filename;
                                orderMedia.Created_At = DateTime.Now;
                                orderMedia.UserId = userId;
                                orderMedia.IsOrderConfirm = true;
                                orderMedia.OrderId = orderTracking.OrderId;
                                orderMedia.FileType = ext;
                                orderMedia.MediaType = 0;
                                orderMedia.Update_At = DateTime.Now;
                                _unitOfWork.GetRepositoryInstance<OrderMedias>().Add(orderMedia);
                                _unitOfWork.SaveChanges();
                            }
                        }

                        //   return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                    }
                    orderTracking.Status = PublicConstant.ORDER_FINISHED;
                    orderTracking.Updated_At = DateTime.Now;

                    driver.Status = 0;
                    _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(orderTracking);
                    _unitOfWork.SaveChanges();
                    var message1 = string.Format("Image Updated Successfully.");
                    response = new ApiResponse(200, message1, "Successfully");
                    return Ok(response);
                }
                else
                {
                    var err = string.Format("Không tìm thấy dữ liệu đơn hàng trên hệ thống.Vui lòng liên hệ hỗ trợ về đơn hàng này");
                    //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                    response = new ApiResponse(-1, null, err);
                    Ultilities.SaveTextToFile("Upload: uploadimage - orderid " + orderId + " " + response, log_carrier);
                    return Ok(response);
                }
                var res = string.Format("Bạn chưa chọn ảnh hóa đơn.");
                dict.Add("error", res);
                //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                response = new ApiResponse(-1, null, res);
                return Ok(response);
            }
            catch (Exception ex)
            {
                var res = string.Format("Lỗi xảy ra khi hoàn thành đơn hàng.Vui lòng liên hệ hỗ trợ hoặc thử lại sau. ");
                dict.Add("error", res);
                response = new ApiResponse(-1, null, res);
                Ultilities.SaveTextToFile("Upload: uploadimage - orderid " + orderId + " " + response, log_carrier);
                return Ok(response);
            }
        }

        [Route("DriverPhoto")]
        [HttpPost]
        //[Authorize]
        public async Task<IHttpActionResult> UploadCMD(string userId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            bool bBack = false;
            bool bFront = false;
            string fileBack = string.Empty;
            string fileFront = string.Empty;
            try
            {
                var httpRequest = HttpContext.Current.Request;
                for (int i = 0; i < httpRequest.Files.Count; i++)
                {
                    var postedFile = httpRequest.Files[i];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                            dict.Add("error", message);
                            response = new ApiResponse(-1, message, "Failure");
                            Ultilities.SaveTextToFile("Upload: UploadCMD - userid " + userId + " " + response, log_carrier);
                            return Ok(response);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Dung lượng file vượt quá 1 mb.");

                            dict.Add("error", message);
                            response = new ApiResponse(-1, message, "Failure");
                            Ultilities.SaveTextToFile("Upload: UploadCMD - userid " + userId + " " + response, log_carrier);
                            return Ok(response);
                        }
                        else
                        {
                            HttpPostedFileBase filebase = new HttpPostedFileWrapper(postedFile);
                            string message = "";
                            string filename = UserId + "_" + filebase.FileName;
                            UploadImage(ref message, filebase, filename, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                            string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();

                            var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                            driver.Updated_At = DateTime.Now;
                            if (filebase.FileName.Contains("image_front"))
                            {
                                fileFront = publicRootPath + filename;
                                driver.Front_Image = fileFront;
                                bFront = true;
                            }
                            else if (filebase.FileName.Contains("image_back"))
                            {
                                fileBack = publicRootPath + filename;
                                driver.Back_Image = fileBack;
                                bBack = true;
                            }
                            _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                            _unitOfWork.SaveChanges();
                        }
                    }
                    //   return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                if (bBack || bFront)
                {
                    var message1 = string.Format("Image Updated Successfully.");
                    var dicPath = new Dictionary<string, string> { { "image_front", fileFront }, { "image_back", fileBack } };
                    response = new ApiResponse(200, dicPath, "Successfully");
                    return Ok(response);
                }

                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                response = new ApiResponse(-1, res, "Failure");
                Ultilities.SaveTextToFile("Upload: UploadCMD - userid " + userId + " " + response, log_carrier);
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Upload: UploadCMD - userid " + userId + " " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }

        [Route("SOSPhoto")]
        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> UploadSOS(int orderId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var userId = User.Identity.GetUserId();
                var orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(userId) && x.Status == 1);
                var orderMedia = new OrderMedias();
                if (orderTracking != null)
                {
                    //foreach (string file in httpRequest.Files)
                    for (int i = 0; i < httpRequest.Files.Count; i++)
                    {

                        var postedFile = httpRequest.Files[i];
                        if (postedFile != null && postedFile.ContentLength > 0)
                        {

                            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                            var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                            var extension = ext.ToLower();
                            if (!AllowedFileExtensions.Contains(extension))
                            {

                                var message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                                dict.Add("error", message);
                                response = new ApiResponse(-1, null, message);
                                Ultilities.SaveTextToFile("Upload: UploadSOS - orderId " + orderId + " " + response, log_carrier);
                                return Ok(response);
                            }
                            else
                            {

                                HttpPostedFileBase filebase = new HttpPostedFileWrapper(postedFile);
                                string message = "";
                                string filename = userId + "_" + orderId.ToString() + "_" + filebase.FileName;
                                UploadImage(ref message, filebase, filename, PublicConstant.USER_ORIGINALS, PublicConstant.USER_THUMBNAILS, null);
                                string publicRootPath = WebConfigurationManager.AppSettings["ImageUsers"].ToString();

                                orderMedia.FilePath = publicRootPath + filename;
                                orderMedia.Created_At = DateTime.Now;
                                orderMedia.UserId = userId;
                                orderMedia.IsOrderConfirm = true;
                                orderMedia.OrderId = orderTracking.OrderId;
                                orderMedia.FileType = ext;
                                orderMedia.MediaType = 1;
                                orderMedia.Update_At = DateTime.Now;
                                _unitOfWork.GetRepositoryInstance<OrderMedias>().Add(orderMedia);
                                _unitOfWork.SaveChanges();
                                var message1 = string.Format("Image Updated Successfully.");
                                response = new ApiResponse(200, message1, "Successfully");
                            }
                        }

                        //   return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                    }
                    Ultilities.SaveTextToFile("Upload: UploadSOS - orderId " + orderId + " " + response, log_carrier);
                    return Ok(response);
                }
                else
                {
                    var err = string.Format("Không tìm thấy dữ liệu đơn hàng trên hệ thống.Vui lòng liên hệ hỗ trợ về đơn hàng này");
                    //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                    response = new ApiResponse(-1, null, err);
                    Ultilities.SaveTextToFile("Upload: UploadSOS - orderId " + orderId + " " + response, log_carrier);
                    return Ok(response);
                }
                //var res = string.Format("Bạn chưa chọn ảnh hóa đơn.");
                //dict.Add("error", res);
                ////return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                //response = new ApiResponse(-1, null, res);
                //return Ok(response);
            }
            catch (Exception ex)
            {
                var res = string.Format("Lỗi xảy ra khi đăng ảnh.Vui lòng liên hệ hỗ trợ hoặc thử lại sau. ");
                dict.Add("error", res);
                response = new ApiResponse(-1, null, res);
                Ultilities.SaveTextToFile("Upload: UploadCMD - orderId " + orderId + " " + response, log_carrier);
                return Ok(response);
            }
        }

        [Route("DriverCarPhoto")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadCarPhoto(string userId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            bool bBack = false, bFront = false, dkBack = false, dkFront = false;
            string fileBack = string.Empty, fileFront = string.Empty, dk_fileFront = string.Empty, dk_fileBack = string.Empty;
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count < 4)
                {
                    response = new ApiResponse(-1, null, "Upload chưa đủ số ảnh");
                }
                else
                {
                    for (int i = 0; i < httpRequest.Files.Count; i++)
                    {
                        var postedFile = httpRequest.Files[i];
                        if (postedFile != null && postedFile.ContentLength > 0)
                        {

                            int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  

                            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                            var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                            var extension = ext.ToLower();
                            if (!AllowedFileExtensions.Contains(extension))
                            {

                                var message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                                dict.Add("error", message);
                                response = new ApiResponse(-1, message, "Failure");
                                Ultilities.SaveTextToFile("Upload: UploadCarPhoto - userid " + userId + " " + response, log_carrier);
                                return Ok(response);
                            }
                            else if (postedFile.ContentLength > MaxContentLength)
                            {

                                var message = string.Format("Dung lượng file vượt quá 1 mb.");

                                dict.Add("error", message);
                                response = new ApiResponse(-1, message, "Failure");
                                Ultilities.SaveTextToFile("Upload: UploadCarPhoto - userid " + userId + " " + response, log_carrier);
                                return Ok(response);
                            }
                            else
                            {
                                HttpPostedFileBase filebase = new HttpPostedFileWrapper(postedFile);
                                string message = "";
                                string filename = UserId + "_" + filebase.FileName;
                                UploadImage(ref message, filebase, filename, PublicConstant.CAR_IMAGES, PublicConstant.CAR_IMAGES, null);
                                string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                                string publicRootPathCar = WebConfigurationManager.AppSettings["ImageCars"].ToString();

                                var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                                if (driver != null)
                                {
                                    driver.Updated_At = DateTime.Now;
                                    if (filebase.FileName.Contains("image_front"))
                                    {
                                        fileFront = publicRootPath + filename;
                                        driver.Front_Image = fileFront;
                                        bFront = true;
                                    }
                                    else if (filebase.FileName.Contains("image_back"))
                                    {
                                        fileBack = publicRootPath + filename;
                                        driver.Back_Image = fileBack;
                                        bBack = true;
                                    }
                                    _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                                    var driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(d => d.DriverId == driver.Id);
                                    if (driverCar != null)
                                    {
                                        var carId = driverCar.CarId;
                                        var car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(x => x.Id == carId);
                                        if (car != null)
                                        {
                                            car.Updated_At = DateTime.Now;
                                            if (filebase.FileName.Contains("dk_image_front"))
                                            {
                                                dk_fileFront = publicRootPathCar + filename;
                                                car.Front_Image_Registry = dk_fileFront;
                                                dkFront = true;
                                            }
                                            else if (filebase.FileName.Contains("dk_image_back"))
                                            {
                                                dk_fileBack = publicRootPathCar + filename;
                                                car.Back_Image_Registry = dk_fileBack;
                                                dkBack = true;
                                            }
                                            _unitOfWork.GetRepositoryInstance<Car>().Update(car);

                                        }
                                        else
                                        {
                                            response = new ApiResponse(-1, null, "Khong tim thay thong tin xe!");
                                            Ultilities.SaveTextToFile("Upload: UploadCarPhoto - userid " + userId + " " + response, log_carrier);
                                        }
                                    }
                                    else
                                    {
                                        //Không tìm thấy xe của tài xế
                                        response = new ApiResponse(-1, null, "Khong tim thay thong tin xe cua tai xe!");
                                    }
                                    _unitOfWork.SaveChanges();
                                }
                                else
                                {
                                    //Không tìm thấy tài xế
                                    response = new ApiResponse(-1, null, "Khong tim thay thong tin tai xe!");
                                }
                            }
                        }
                    }
                    if (bBack && bFront && dkBack && dkFront)
                    {
                        var message1 = string.Format("Image Updated Successfully.");
                        var dicPath = new Dictionary<string, string> { { "image_front", fileFront }, { "image_back", fileBack }, { "dk_image_front", dk_fileFront }, { "dk_image_back", dk_fileBack } };
                        response = new ApiResponse(200, dicPath, "Successfully");
                        //return Ok(response);
                    }
                    else
                    {
                        var res = string.Format("Please Upload a image.");
                        dict.Add("error", res);
                        response = new ApiResponse(-1, res, "Failure");
                    }
                }
                Ultilities.SaveTextToFile("Upload: UploadCarPhoto - userid " + userId + " " + response, log_carrier);
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                Ultilities.SaveTextToFile("Upload: UploadCarPhoto - userid " + userId + " " + response, log_carrier);
                return Ok(response);
            }
        }
        [Route("CarPhoto")]
        [HttpPost]
        public async Task<IHttpActionResult> UpCarPhoto(string userId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            bool dkBack = false, dkFront = false;
            string fileBack = string.Empty, fileFront = string.Empty, dk_fileFront = string.Empty, dk_fileBack = string.Empty;
            try
            {
                var httpRequest = HttpContext.Current.Request;
                for (int i = 0; i < httpRequest.Files.Count; i++)
                {
                    var postedFile = httpRequest.Files[i];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                            dict.Add("error", message);
                            response = new ApiResponse(-1, message, "Failure");
                            return Ok(response);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Dung lượng file vượt quá 1 mb.");

                            dict.Add("error", message);
                            response = new ApiResponse(-1, message, "Failure");
                            return Ok(response);
                        }
                        else
                        {
                            HttpPostedFileBase filebase = new HttpPostedFileWrapper(postedFile);
                            string message = "";
                            string filename = UserId + "_" + filebase.FileName;
                            UploadImage(ref message, filebase, filename, PublicConstant.CAR_IMAGES, PublicConstant.CAR_IMAGES, null);
                            string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                            string publicRootPathCar = WebConfigurationManager.AppSettings["ImageCars"].ToString();

                            var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                            if (driver != null)
                            {
                                var driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(d => d.DriverId == driver.Id);
                                if (driverCar != null)
                                {
                                    var carId = driverCar.CarId;
                                    var car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(x => x.Id == carId);
                                    if (car != null)
                                    {
                                        car.Updated_At = DateTime.Now;
                                        if (filebase.FileName.Contains("dk_image_front"))
                                        {
                                            dk_fileFront = publicRootPathCar + filename;
                                            car.Front_Image_Registry = dk_fileFront;
                                            dkFront = true;
                                        }
                                        else if (filebase.FileName.Contains("dk_image_back"))
                                        {
                                            dk_fileBack = publicRootPathCar + filename;
                                            car.Back_Image_Registry = dk_fileBack;
                                            dkBack = true;
                                        }
                                        _unitOfWork.GetRepositoryInstance<Car>().Update(car);
                                    }
                                    else
                                    {
                                        response = new ApiResponse(-1, null, "Khong tim thay thong tin xe!");
                                    }
                                }
                                else
                                {
                                    //Không tìm thấy xe của tài xế
                                    response = new ApiResponse(-1, null, "Khong tim thay thong tin xe cua tai xe!");
                                }
                                _unitOfWork.SaveChanges();
                            }
                            else
                            {
                                //Không tìm thấy tài xế
                                response = new ApiResponse(-1, null, "Khong tim thay thong tin tai xe!");
                            }
                        }
                    }
                }
                if (dkBack || dkFront)
                {
                    var message1 = string.Format("Image Updated Successfully.");
                    var dicPath = new Dictionary<string, string> { { "dk_image_front", dk_fileFront }, { "dk_image_back", dk_fileBack } };
                    response = new ApiResponse(200, dicPath, "Successfully");
                    //return Ok(response);
                }
                else
                {
                    var res = string.Format("Please Upload a image.");
                    dict.Add("error", res);
                    response = new ApiResponse(-1, res, "Failure");
                }
                Ultilities.SaveTextToFile("Upload: UpCarPhoto - userid " + userId + " " + response, log_carrier);
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }

        //[HttpPost]
        //[Route("MediaUpload")]
        //public async Task<HttpResponseMessage> MediaUpload()
        //{
        //    // Check if the request contains multipart/form-data.  
        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
        //    //access form data  
        //    NameValueCollection formData = provider.FormData;
        //    //access files  
        //    IList<HttpContent> files = provider.Files;

        //    HttpContent file1 = files[0];
        //    var thisFileName = file1.Headers.ContentDisposition.FileName.Trim('\"');

        //    ////-------------------------------------For testing----------------------------------  
        //    //to append any text in filename.  
        //    //var thisFileName = file1.Headers.ContentDisposition.FileName.Trim('\"') + DateTime.Now.ToString("yyyyMMddHHmmssfff"); //ToDo: Uncomment this after UAT as per Jeeevan  

        //    //List<string> tempFileName = thisFileName.Split('.').ToList();  
        //    //int counter = 0;  
        //    //foreach (var f in tempFileName)  
        //    //{  
        //    //    if (counter == 0)  
        //    //        thisFileName = f;  

        //    //    if (counter > 0)  
        //    //    {  
        //    //        thisFileName = thisFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + f;  
        //    //    }  
        //    //    counter++;  
        //    //}  

        //    ////-------------------------------------For testing----------------------------------  

        //    string filename = String.Empty;
        //    Stream input = await file1.ReadAsStreamAsync();
        //    string directoryName = String.Empty;
        //    string URL = String.Empty;
        //    string tempDocUrl = WebConfigurationManager.AppSettings["DocsUrl"];

        //    if (formData["ClientDocs"] == "ClientDocs")
        //    {
        //        var path = HttpRuntime.AppDomainAppPath;
        //        directoryName = Path.Combine(path, "ClientDocument");
        //        filename = Path.Combine(directoryName, thisFileName);

        //        //Deletion exists file  
        //        if (File.Exists(filename))
        //        {
        //            File.Delete(filename);
        //        }

        //        string DocsPath = tempDocUrl + "/" + "ClientDocument" + "/";
        //        URL = DocsPath + thisFileName;

        //    }


        //    //Directory.CreateDirectory(@directoryName);  
        //    using (Stream file = File.OpenWrite(filename))
        //    {
        //        input.CopyTo(file);
        //        //close file  
        //        file.Close();
        //    }

        //    var response = Request.CreateResponse(HttpStatusCode.OK);
        //    response.Headers.Add("DocsUrl", URL);
        //    return response;
        //}
        //[HttpPost]
        //[Route("MediaUpload")]
        //public async Task<HttpResponseMessage> MediaUpload()
        //{
        //    // Check if the request contains multipart/form-data.  
        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
        //    //access form data  
        //    NameValueCollection formData = provider.FormData;
        //    //access files  
        //    IList<HttpContent> files = provider.Files;

        //    HttpContent file1 = files[0];
        //    var thisFileName = file1.Headers.ContentDisposition.FileName.Trim('\"');

        //    ////-------------------------------------For testing----------------------------------  
        //    //to append any text in filename.  
        //    //var thisFileName = file1.Headers.ContentDisposition.FileName.Trim('\"') + DateTime.Now.ToString("yyyyMMddHHmmssfff"); //ToDo: Uncomment this after UAT as per Jeeevan  

        //    //List<string> tempFileName = thisFileName.Split('.').ToList();  
        //    //int counter = 0;  
        //    //foreach (var f in tempFileName)  
        //    //{  
        //    //    if (counter == 0)  
        //    //        thisFileName = f;  

        //    //    if (counter > 0)  
        //    //    {  
        //    //        thisFileName = thisFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + f;  
        //    //    }  
        //    //    counter++;  
        //    //}  

        //    ////-------------------------------------For testing----------------------------------  

        //    string filename = String.Empty;
        //    Stream input = await file1.ReadAsStreamAsync();
        //    string directoryName = String.Empty;
        //    string URL = String.Empty;
        //    string tempDocUrl = WebConfigurationManager.AppSettings["DocsUrl"];

        //    if (formData["ClientDocs"] == "ClientDocs")
        //    {
        //        var path = HttpRuntime.AppDomainAppPath;
        //        directoryName = Path.Combine(path, "ClientDocument");
        //        filename = Path.Combine(directoryName, thisFileName);

        //        //Deletion exists file  
        //        if (File.Exists(filename))
        //        {
        //            File.Delete(filename);
        //        }

        //        string DocsPath = tempDocUrl + "/" + "ClientDocument" + "/";
        //        URL = DocsPath + thisFileName;

        //    }


        //    //Directory.CreateDirectory(@directoryName);  
        //    using (Stream file = File.OpenWrite(filename))
        //    {
        //        input.CopyTo(file);
        //        //close file  
        //        file.Close();
        //    }

        //    var response = Request.CreateResponse(HttpStatusCode.OK);
        //    response.Headers.Add("DocsUrl", URL);
        //    return response;
        //}

        /// <summary>
        /// Uploading and resizing an image, Currently it is used to upload member profile pic, provider service banner image and category image
        /// </summary>
        /// <param name="message"></param>
        /// <param name="originalImage"></param>
        /// <param name="imageName"></param>
        /// <param name="rootPathOriginal"></param>
        /// <param name="rootPathThumbNail"></param>
        /// <param name="server"></param>
        private void UploadImage(ref string message, HttpPostedFileBase originalImage, string imageName, string rootPathOriginal, string rootPathThumbNail, HttpServerUtilityBase server)
        {
            try
            {
                bool existsOriginal = Directory.Exists(PublicConstant.IMAGE_PAHT_TEMP);
                if (!existsOriginal)
                {
                    Directory.CreateDirectory(PublicConstant.IMAGE_PAHT_TEMP);
                }
                originalImage.SaveAs(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                WebImage img1 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                //img1.Resize(849, 320);
                bool exists = Directory.Exists(rootPathOriginal);
                if (!exists)
                    Directory.CreateDirectory(rootPathOriginal);
                img1.Save(Path.Combine(rootPathOriginal + "/" + imageName));

                WebImage img2 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                //img2.Resize(274, 175);
                bool exists2 = Directory.Exists(rootPathThumbNail);
                if (!exists2)
                    Directory.CreateDirectory(rootPathThumbNail);
                img2.Save(Path.Combine(rootPathThumbNail + "/" + imageName));

                File.Delete(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                #region save
                //bool existsOriginal = System.IO.Directory.Exists(server.MapPath(Constant.IMAGE_PAHT_TEMP));
                //if (!existsOriginal)
                //{
                //    System.IO.Directory.CreateDirectory(server.MapPath(Constant.IMAGE_PAHT_TEMP));
                //}
                //originalImage.SaveAs(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));

                //WebImage img1 = new WebImage(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                //img1.Resize(849, 320);
                //bool exists = System.IO.Directory.Exists(server.MapPath(rootPathOriginal));
                //if (!exists)
                //    System.IO.Directory.CreateDirectory(server.MapPath(rootPathOriginal));
                //img1.Save(Path.Combine(server.MapPath(rootPathOriginal + "/" + imageName)));

                //WebImage img2 = new WebImage(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                //img2.Resize(274, 175);
                //bool exists2 = System.IO.Directory.Exists(server.MapPath(rootPathThumbNail));
                //if (!exists2)
                //    System.IO.Directory.CreateDirectory(server.MapPath(rootPathThumbNail));
                //img2.Save(Path.Combine(server.MapPath(rootPathThumbNail + "/" + imageName)));

                //System.IO.File.Delete(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                #endregion
            }
            catch (Exception ex)
            {
                message = "Lỗi upload ảnh: " + ex.Message;
            }
        }
        //public static Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
        //{
        //    int newWidth;
        //    int newHeight;
        //    if (preserveAspectRatio)
        //    {
        //        int originalWidth = image.Width;
        //        int originalHeight = image.Height;
        //        float percentWidth = (float)size.Width / (float)originalWidth;
        //        float percentHeight = (float)size.Height / (float)originalHeight;
        //        float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
        //        newWidth = (int)(originalWidth * percent);
        //        newHeight = (int)(originalHeight * percent);
        //    }
        //    else
        //    {
        //        newWidth = size.Width;
        //        newHeight = size.Height;
        //    }
        //    Image newImage = new Bitmap(newWidth, newHeight);
        //    using (Graphics graphicsHandle = Graphics.FromImage(newImage))
        //    {
        //        graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //        graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
        //    }
        //    return newImage;
        //}
    }
}
