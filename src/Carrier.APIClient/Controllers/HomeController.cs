﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.APIClient.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.Identity;
using APIClient.Comon;

namespace Carrier.APIClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";


            //IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<CarrierMonitorHub>();
            //hubContext.Clients.All.ReceivedData("Insured Report");
         //   PushOrdersScheduler.Start();
            return View();
        }
    }
}
