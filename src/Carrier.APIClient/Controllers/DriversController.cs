﻿using APIClient.Helpers;
using APIClient.Models;
using Carrier.APIClient.Comon;
using Carrier.APIClient.Models;
using Carrier.APIClient.SignalR;
using Carrier.Models.Entities;
using Carrier.PushNotification;
using Carrier.Repository;
using Carrier.Utilities;
using GoogleMaps.LocationServices;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Carrier.APIClient.Controllers
{
    [Authorize]
    [RoutePrefix("api/Driver")]
    public class DriversController : SignalRBase<CarrierMonitorHub>
    {
        #region Property
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;

        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        private string userId
        {
            get { return User.Identity.GetUserId(); }
        }
        private string ParrentUserId
        {
            get
            {
                var userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId));
                var objParrent = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefault(userInfo.OrganzationId);
                return objParrent.ApplicationUserID;
            }
        }
        public string parrentUserId
        {
            get
            {
                var userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId));
                var objParrent = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefault(userInfo.OrganzationId);
                return objParrent.ApplicationUserID;
            }
        }
        #endregion
        private string log_carrier = "C:\\Users\\data\\Log\\Log_Carrier.txt";
        [Route("CheckIn")]
        [HttpPut]
        public async Task<IHttpActionResult> CheckInLocation(CheckInLocationBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response); ;
                }
                var currentItem = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
                if (currentItem != null)
                {
                    currentItem.Updated_At = DateTime.Now;
                    currentItem.Lat = model.Latitude.Value;
                    currentItem.Lng = model.Longitude.Value;
                    _unitOfWork.GetRepositoryInstance<Driver>().Update(currentItem);
                    _unitOfWork.SaveChanges();
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                    return Ok(response);
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.LOGGIC_CODE, null, "Failure");
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Drivers: CheckInLocation " + ex.Message, log_carrier);
                return Ok(response);
            }

        }
        [HttpPost]
        [Route("AcceptOrder")]
        public async Task<IHttpActionResult> AcceptOrder(int id)
        {
            try
            {
                var walletuser = new WalletInfo();
                var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(id);
                if (order.Status == 0)
                {
                    // Don noi bo
                    var obj = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.DriverId.Equals(userId));
                    obj.Updated_At = DateTime.Now;
                    obj.Status = PublicConstant.ORDER_ACCEPT;
                    var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(d => d.UserId.Equals(userId));
                    driver.Status = 1;

                    walletuser = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                    if (walletuser != null)
                    {
                        TransactionOrder transaction = new TransactionOrder();
                        walletuser.Updated_At = DateTime.Now;
                        var VAT = order.Gia * 10 / 100;
                        //var VAT = order.Gia;
                        if (walletuser.Commission + walletuser.Balance < VAT)
                        {
                            response = new ApiResponse(ApiResponseCode.BALANCE_USER_FAIL, null, "Số dư không đủ. Nạp thêm tiền để được nhận đơn");
                            return Ok(response);
                        }
                        else
                        {
                            if (walletuser.Commission >= VAT)
                            {
                                walletuser.Commission -= VAT;
                                transaction.CommissionChange = VAT;
                                transaction.BalanceChange = 0;
                            }
                            else
                            {
                                walletuser.Balance = walletuser.Balance + walletuser.Commission - VAT;
                                walletuser.Commission = 0;
                                transaction.CommissionChange = walletuser.Commission;
                                transaction.BalanceChange = VAT - walletuser.Commission;
                            }
                            _unitOfWork.GetRepositoryInstance<WalletInfo>().Update(walletuser);

                            transaction.Created_At = DateTime.Now;
                            transaction.Created_By = userId;
                            transaction.Description = $"Trừ tiền : {VAT} (VNĐ) nhận đơn thành công với mã đơn hàng {order.MaVanDon}";
                            transaction.OrderId = order.Id;
                            transaction.Price = VAT;
                            transaction.Status = (int)Tran_Status.accept;
                            transaction.ToUser = userId;
                            transaction.Update_By = "CARRIER";
                            transaction.UpdatedAt = DateTime.Now;
                            transaction.TransactionType = (int)Tran_Type.tru_tien;
                            transaction.MaVanDon = order.MaVanDon;
                            transaction.CurrentBalanceWallet = walletuser.Balance + walletuser.Commission;
                            _unitOfWork.GetRepositoryInstance<TransactionOrder>().Add(transaction);
                        }
                    }
                    else
                    {
                        response = new ApiResponse(ApiResponseCode.WALLET_INVALID, null, "Không tìm thấy thông tin ví. Liên hệ hotline để được hỗ trợ");
                        return Ok(response);
                    }
                    _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(obj);
                    _unitOfWork.SaveChanges();
                    await Task.Run(() => Hub.Clients.Group(ParrentUserId).AcceptOrderId(id));
                    await Task.Run(() => Hub.Clients.Group(obj.OwnerId).AcceptOrderId(id));


                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                    return Ok(response);
                }
                else
                {
                    // Don push tu dong
                    var oldTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.Status == 1);
                    if (oldTracking != null)
                    {
                        response = new ApiResponse(ApiResponseCode.VAN_DON_DA_DUOCNHAN, null, "Bạn không thể nhận được vận đơn này,vận đơn đã được nhận bởi tài xế khác.");
                        return Ok(response);
                    }
                    else
                    {
                        var obj = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.DriverId.Equals(userId));
                        if (obj != null)
                        {
                            obj.Updated_At = DateTime.Now;
                            obj.Status = PublicConstant.ORDER_ACCEPT;
                            _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(obj);
                        }
                        else
                        {
                            obj = new OrderTracking();
                            obj.OrderId = order.Id;
                            obj.OwnerId = order.Created_By;
                            obj.DriverId = userId;
                            obj.Created_At = DateTime.Now;
                            obj.Status = PublicConstant.ORDER_ACCEPT;
                            obj.Updated_At = DateTime.Now;
                            _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                        }

                        walletuser = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                        if (walletuser != null)
                        {
                            walletuser.Updated_At = DateTime.Now;
                            TransactionOrder transaction = new TransactionOrder();
                            var VAT = order.Gia * 10 / 100;
                            //var VAT = order.Gia;
                            if (walletuser.Commission + walletuser.Balance < VAT)
                            {
                                response = new ApiResponse(ApiResponseCode.BALANCE_USER_FAIL, null, "Sô dư không đủ. Nạp thêm tiền để được nhận đơn");
                                return Ok(response);
                            }
                            else
                            {
                                if (walletuser.Commission >= VAT)
                                {
                                    walletuser.Commission -= VAT;
                                    transaction.CommissionChange = VAT;
                                    transaction.BalanceChange = 0;
                                }
                                else
                                {
                                    walletuser.Balance = walletuser.Balance + walletuser.Commission - VAT;
                                    walletuser.Commission = 0;
                                    transaction.CommissionChange = walletuser.Commission;
                                    transaction.BalanceChange = VAT - walletuser.Commission;
                                }
                                _unitOfWork.GetRepositoryInstance<WalletInfo>().Update(walletuser);


                                transaction.Created_At = DateTime.Now;
                                transaction.Created_By = userId;
                                transaction.Description = $"Trừ tiền : {VAT} (VNĐ) nhận đơn thành công với mã đơn hàng {order.MaVanDon}";
                                transaction.OrderId = order.Id;
                                transaction.Price = VAT;
                                transaction.Status = (int)Tran_Status.accept;
                                transaction.ToUser = userId;
                                transaction.Update_By = "CARRIER";
                                transaction.UpdatedAt = DateTime.Now;
                                transaction.TransactionType = (int)Tran_Type.tru_tien;
                                transaction.MaVanDon = order.MaVanDon;
                                transaction.CurrentBalanceWallet = walletuser.Balance + walletuser.Commission;
                                _unitOfWork.GetRepositoryInstance<TransactionOrder>().Add(transaction);
                            }
                        }
                        else
                        {
                            response = new ApiResponse(ApiResponseCode.WALLET_INVALID, null, "Không tìm thấy thông tin ví. Liên hệ hotline để được hỗ trợ");
                            return Ok(response);
                        }

                        _unitOfWork.SaveChanges();
                        var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
                        //await Task.Run(() => Hub.Clients.Group(obj.OwnerId).SendData(order.Created_By, driver));
                        await Task.Run(() => Hub.Clients.Group(ParrentUserId).AcceptOrderId(id));
                        await Task.Run(() => Hub.Clients.Group(obj.OwnerId).AcceptOrderId(id));
                        response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                        return Ok(response);
                    }
                }
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Drivers: AcceptOrder " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        [HttpPost]
        [Route("CancelOrder")]
        public async Task<IHttpActionResult> CancelOrder(int id)
        {
            try
            {
                var orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.DriverId.Equals(userId));
                if (orderTracking != null)
                {
                    orderTracking.Status = PublicConstant.ORDER_CANCEL_BY_DRIVER;
                    orderTracking.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(orderTracking);
                    _unitOfWork.SaveChanges();
                }
                response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                var orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id && x.DriverId.Equals(userId));
                if (orderTracking != null)
                {
                    orderTracking.Status = PublicConstant.ORDER_CANCEL_BY_DRIVER;
                    orderTracking.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(orderTracking);
                    _unitOfWork.SaveChanges();
                }
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Successfully");
                Ultilities.SaveTextToFile("Drivers: CancelOrder " + ex.Message, log_carrier);
                return Ok(response);
            }
        }
        [HttpPost]
        [Route("ForwardOrder")]
        public async Task<IHttpActionResult> ForwardOrder(int orderId)
        {
            try
            {
                var nearDriver = GetDriverNearOrder(orderId, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(orderId);
                CarrierHubClient client = new CarrierHubClient();
                if (client.isOnline(nearDriver.UserId))
                {
                    await client.SendMessage(User.Identity.GetUserId(), nearDriver.UserId, order);
                }
                else
                {
                    PushMessageForDriver(nearDriver.UserId, order);
                }
                var orderTracking = new OrderTracking();
                orderTracking.OrderId = (int)order.Id;
                orderTracking.DriverId = nearDriver.UserId;
                orderTracking.OwnerId = order.Created_By;
                orderTracking.Created_By = User.Identity.GetUserId();
                orderTracking.Status = PublicConstant.ORDER_PENDING;
                orderTracking.Updated_At = DateTime.Now;
                orderTracking.Created_At = DateTime.Now;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(orderTracking);
                _unitOfWork.SaveChanges();
                response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: ForwardOrder " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }
        [HttpPost]
        [Route("UpdateStatusOrder")]
        public async Task<IHttpActionResult> UpdatedStatusOrder(int id, int status, string parentId)
        {
            try
            {
                var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(id);
                var obj = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OwnerId.Equals(order.Created_By) && x.DriverId.Equals(UserId));
                obj.Updated_At = DateTime.Now;
                obj.Status = status;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(obj);
                _unitOfWork.SaveChanges();
                response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
                await Task.Run(() => Hub.Clients.Group(obj.OwnerId).SendData(order.Created_By, order.Id));
                await Task.Run(() => Hub.Clients.Group(parentId).SendData(order.Created_By, order.Id));

                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: UpdatedStatusOrder " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                return Ok(response);
            }
        }

        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
            var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
            var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId,@distance", sqlOrderId, sqlDistance).ToList();
            return lstSearch.FirstOrDefault();
        }
        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="order"></param>
        private void PushMessageForDriver(string driverId, Order order)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + order.Id + "\"}";
            DevicesPush device = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, order.Id.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + order.Id + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, order.Id.ToString(), callback);
            }
        }
        private void PushMessageForOwner(string UserId, string jsonMessage)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;

            //string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + order.Id + "\"}";
            DevicesPush device = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, string.Empty, callback);
            }
            else
            {
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, string.Empty, callback);
            }
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            if (result.Equals("Success"))
            {

            }
            else
            {
                // Show message 
            }
        }

        [HttpGet]
        [Route("GetStatus")]
        public async Task<IHttpActionResult> GetStatus()
        {
            try
            {
                var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                response = new ApiResponse(HttpStatusCode.OK, driver, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetStatus " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                return Ok(response);
            }

        }
        [HttpPost]
        [Route("UpdateStatus")]
        public async Task<IHttpActionResult> UpdateStatus(int status, string parrentId)
        {
            try
            {
                var orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.DriverId.Equals(UserId) && x.Status == 1);
                if (orderTracking != null)
                {
                    response = new ApiResponse(ApiResponseCode.LOGGIC_CODE, null, "You need finish current order before update status");
                    return Ok(response);
                }
                var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
                driver.Updated_At = DateTime.Now;
                driver.Status = status;
                _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                _unitOfWork.SaveChanges();
                await Task.Run(() => Hub.Clients.Group("9e3c2c01-0427-43f1-9f06-e90e64b25440").ReceivedData(driver));
                await Task.Run(() => Hub.Clients.Group(parrentId).ReceivedData(driver));
                response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                Ultilities.SaveTextToFile("Drivers: UpdateStatus " + ex.Message, log_carrier);
                return Ok(response); ;
            }
        }

        [Route("History")]
        [HttpGet]
        public async Task<IHttpActionResult> GetHistoryOrder(int status)
        {
            try
            {
                var result = new List<OrderSuccessDetailViewModel>();
                var sqlUser = new SqlParameter("userId", System.Data.SqlDbType.NVarChar) { Value = userId };
                var sqlStatus = new SqlParameter("status", System.Data.SqlDbType.Int) { Value = status };
                var lstOrderHistory = _unitOfWork.GetRepositoryInstance<SP_DriversHistory_Result>().GetResultBySqlProcedure("SP_DriversHistory @status,@userId", sqlStatus, sqlUser).ToList();
                foreach (var item in lstOrderHistory)
                {
                    var obj = new OrderSuccessDetailViewModel(item);
                    var lst_img = new List<string>();
                    var list_image = _unitOfWork.GetRepositoryInstance<OrderMedias>().GetListByParameter(x => x.OrderId == item.OrderId && x.UserId == userId).ToList();
                    foreach (var im in list_image)
                    {
                        lst_img.Add(im.FilePath);
                    }
                    obj.Images = lst_img;
                    var ordertracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == item.OrderId && x.Status == status);
                    if (ordertracking != null)
                    {
                        var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId == ordertracking.DriverId);
                        if (driver != null)
                        {
                            var drivercar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(x => x.DriverId == driver.Id);
                            if (drivercar != null)
                            {
                                var car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(x => x.Id == drivercar.CarId);
                                if (car != null)
                                {
                                    DriverInfo drInfo = new DriverInfo();
                                    drInfo.BienSoXe = car.License;
                                    drInfo.DiaChi = driver.Address;
                                    drInfo.GioiTinh = driver.Sex;
                                    drInfo.KichThuocXe = car.Leng_Size + "x" + car.Width_Size + "x" + car.Height_Size;
                                    drInfo.NgaySinh = driver.BirthDay;
                                    drInfo.SDT = driver.Phone;
                                    drInfo.TaiTrong = car.Payload.Value;
                                    drInfo.TenTaiXe = driver.Name;
                                    obj.DriverInfo = drInfo;
                                }
                            }
                        }
                    }
                    result.Add(obj);
                }
                response = new ApiResponse(HttpStatusCode.OK, result, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                Ultilities.SaveTextToFile("Drivers: GetHistoryOrder " + ex.Message, log_carrier);
                return Ok(response);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("SearchOrderByCode")]
        public async Task<IHttpActionResult> SearchOrderByCode(string orderCode = "")
        {
            try
            {
                //if (string.IsNullOrEmpty(orderCode))
                //{

                //}
                //else
                //{
                //    var sqlOrderCode = new SqlParameter("@code", System.Data.SqlDbType.NVarChar) { Value = orderCode };
                //    var lstSearch = _unitOfWork.GetRepositoryInstance<Search_OrderByCode_Result>().GetResultBySqlProcedure("Search_OrderByCode @code", sqlOrderCode).ToList();
                //    response = new ApiResponse(HttpStatusCode.OK, lstSearch.FirstOrDefault(), "Successfully");
                //}
                //return Ok(response);
                var sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = "" };
                var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
                var sqlBsxCode = new SqlParameter("@bsxCode", System.Data.SqlDbType.NVarChar) { Value = "" };
                var lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId_Enterprise_New @userId,@orderCode,@bsxCode",
                     sqlParentId, sqlOrderCode, sqlBsxCode).ToList();
                var result = new SP_GetAllDriverTrackingByUserId_Result();
                if (lstSearchOrderTracking.Count > 0)
                {
                    result = lstSearchOrderTracking.FirstOrDefault();
                    return Ok(new ApiResponse(HttpStatusCode.OK, result, "Successfully"));
                }
                else
                {
                    sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = "" };
                    sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = "" };
                    sqlBsxCode = new SqlParameter("@bsxCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
                    lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId_Enterprise_New @userId,@orderCode,@bsxCode",
                         sqlParentId, sqlOrderCode, sqlBsxCode).ToList();
                    if (lstSearchOrderTracking.Count > 0)
                    {
                        result = lstSearchOrderTracking.FirstOrDefault();
                        return Ok(new ApiResponse(HttpStatusCode.OK, result, "Successfully"));
                    }
                    else
                    {
                        return Ok(new ApiResponse(ApiResponseCode.MAVANDON_INVALID, null, "Mã vận đơn hoặc biển số xe không tồn tại!"));
                    }
                }
                //var dictData = new Dictionary<string, object>() { { "count", lstSearchOrderTracking.Count() }, { "drivers", lstSearchOrderTracking } };

            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: SearchOrderByCode " + ex.Message, log_carrier);
                return Ok(new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Lỗi tìm mã vận đơn!"));
            }
        }
        [AllowAnonymous]
        [Route("UpdateCar")]
        public async Task<IHttpActionResult> UpdateCarInfo(CarInfo car, string userId)
        {
            try
            {
                var sqlDriverId = new SqlParameter("@driverId", System.Data.SqlDbType.NVarChar) { Value = userId };
                var obj = _unitOfWork.GetRepositoryInstance<Car>().GetResultBySqlProcedure("SP_GetCar_By_DriverId @driverId", sqlDriverId).FirstOrDefault();
                if (obj != null)
                {
                    obj.Updated_At = DateTime.Now;
                    if (car.Payload != null)
                    {
                        obj.Payload = car.Payload;
                    }
                    if (car.Width_Size != null)
                    {
                        obj.Width_Size = car.Width_Size;
                    }
                    if (car.Leng_Size != null)
                    {
                        obj.Leng_Size = car.Leng_Size;
                    }
                    if (car.Height_Size != null)
                    {
                        obj.Height_Size = car.Height_Size;
                    }
                    if (string.IsNullOrEmpty(car.License))
                    {
                        obj.License = car.License;
                    }
                    if (string.IsNullOrEmpty(car.Description))
                    {
                        obj.Description = car.Description;
                    }
                    if (string.IsNullOrEmpty(car.Description))
                    {
                        obj.Description = car.Description;
                    }
                    if (car.Lat != null)
                    {
                        obj.Lat = car.Lat;
                    }
                    if (car.Lng != null)
                    {
                        obj.Lng = car.Lng;
                    }
                    obj.Address = car.Address;
                    _unitOfWork.GetRepositoryInstance<Car>().Update(obj);
                }
                else
                {
                    var carNew = new Car();
                    carNew.Created_At = DateTime.Now;
                    carNew.Created_By = userId;
                    carNew.OrganizationId = 1163;
                    carNew.Payload = car.Payload;
                    carNew.Width_Size = car.Width_Size;
                    carNew.Updated_At = DateTime.Now;
                    carNew.Leng_Size = car.Leng_Size;
                    carNew.Height_Size = car.Height_Size;
                    carNew.License = car.License;
                    carNew.Description = car.Description;
                    carNew.Lat = car.Lat;
                    carNew.Lng = car.Lng;
                    carNew.Address = car.Address;
                    _unitOfWork.GetRepositoryInstance<Car>().Add(carNew);

                    var driverInfo = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                    var drivercar = new DriverCar();
                    drivercar.CarId = carNew.Id;
                    drivercar.DriverId = driverInfo.Id;
                    _unitOfWork.GetRepositoryInstance<DriverCar>().Add(drivercar);

                }
                _unitOfWork.SaveChanges();
                response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: UpdateCarInfo " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }

        }
        [Route("GetCar")]
        public async Task<IHttpActionResult> GetCarInfo()
        {
            try
            {
                var sqlDriverId = new SqlParameter("@driverId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var objCar = _unitOfWork.GetRepositoryInstance<Car>().GetResultBySqlProcedure("SP_GetCar @driverId", sqlDriverId).FirstOrDefault();
                response = new ApiResponse(HttpStatusCode.OK, objCar, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetCarInfo " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }
        [HttpPost]
        [Route("AddCar")]
        public async Task<IHttpActionResult> AddCarInfo([FromBody]CarInfo car)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response);
                }
                var checkcar = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(c => c.License.Equals(car.License));
                if (checkcar != null)
                {
                    //check xe đã có trong db chưa 
                    response = new ApiResponse(ApiResponseCode.REGISTER_FAILURE, null, "Xe đã tồn tại trên hệ thống! Kiểm tra lại");
                    return Ok(response);
                }
                else
                {
                    //chưa có thì thêm xe vào trong db và đồng thời lấy add vào cả bảng drivercar
                    checkcar = new Car();
                    checkcar.Address = car.Address;
                    checkcar.License = car.License;
                    checkcar.Leng_Size = car.Leng_Size;
                    checkcar.Height_Size = car.Height_Size;
                    checkcar.Width_Size = car.Width_Size;
                    checkcar.Payload = car.Payload;
                    string messageT = string.Empty;
                    var point = GetLatLongByAddress(ref messageT, car.Address);
                    if (point != null)
                    {
                        checkcar.Lat = point.Latitude;
                        checkcar.Lng = point.Longitude;
                    }
                    else
                    {
                        checkcar.Lat = 0;
                        checkcar.Lng = 0;
                    }
                    checkcar.OrganizationId = 1163;
                    checkcar.Created_At = DateTime.Now;
                    checkcar.Created_By = userId;
                    checkcar.Updated_At = DateTime.Now;
                    checkcar.Updated_By = userId;
                    checkcar.Description = car.Description;
                    _unitOfWork.GetRepositoryInstance<Car>().Add(checkcar);
                    _unitOfWork.SaveChanges();
                    //Add bang drivercar
                    var findcar = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(c => c.License.Equals(car.License));
                    if (findcar != null)
                    {
                        var drive = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(d => d.UserId.Equals(userId));
                        if (drive != null)
                        {
                            var iddriver = drive.Id;
                            var driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(c => c.DriverId == iddriver);
                            if (driverCar != null)
                            {
                                driverCar.CarId = findcar.Id;
                                _unitOfWork.GetRepositoryInstance<DriverCar>().Update(driverCar);
                                _unitOfWork.SaveChanges();
                            }
                            else
                            {
                                driverCar = new DriverCar();
                                driverCar.CarId = findcar.Id;
                                driverCar.DriverId = iddriver;
                                _unitOfWork.GetRepositoryInstance<DriverCar>().Add(driverCar);
                                _unitOfWork.SaveChanges();
                            }
                        }
                    }
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: AddCarInfo " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }
        [Route("GetProfit")]
        public async Task<IHttpActionResult> GetProfit()
        {
            try
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                var sqlDriverId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var sqlFromdate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = startDate };
                var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = endDate };
                var objProfit = _unitOfWork.GetRepositoryInstance<SP_Driver_Profit_Info_Result>().GetResultBySqlProcedure("SP_Driver_Profit_Info @UserId,@FromDate,@ToDate", sqlDriverId, sqlFromdate, sqlToDate).FirstOrDefault();
                response = new ApiResponse(HttpStatusCode.OK, objProfit, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetProfit " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }

        [Route("GetBalance")]
        public async Task<IHttpActionResult> GetBalance()
        {
            try
            {
                var obj = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                if (obj != null)
                {
                    //balance: số dư khả dụng trong tài khoản
                    //commission: số dư bonnus admin. Khi hiện lên app thì số dư hiện tại = commis+balance
                    obj.Commission = obj.Commission + obj.Balance;
                    response = new ApiResponse(HttpStatusCode.OK, obj, "Successfully");
                }
                else
                {
                    string mss = "Account chưa được cấp ví";
                    response = new ApiResponse(ApiResponseCode.BALANCE_USER_FAIL, null, mss);
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetBalance " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }
        [Route("GetCurrentOrderInfo")]
        public async Task<IHttpActionResult> GetCurrentOrderInfo()
        {
            try
            {
                var sqlDriverId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var obj = _unitOfWork.GetRepositoryInstance<SP_GetCurrentOrderInfo_Result>().GetResultBySqlProcedure("SP_GetCurrentOrderInfo @userId", sqlDriverId).FirstOrDefault();
                if (obj != null)
                {
                    obj.Gia = obj.Gia.Value;
                    var OrderViewModel = new OrderBindingModel();
                    var orderid = obj.Id;
                    var lstDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
                    OrderViewModel.Id = obj.Id;
                    OrderViewModel.Gia = obj.Gia;
                    OrderViewModel.Status = obj.Status;
                    OrderViewModel.Note = obj.Note;
                    OrderViewModel.SoKmUocTinh = obj.SoKmUocTinh;
                    OrderViewModel.TenHang = obj.TenHang;
                    OrderViewModel.DiemDiChiTiet = obj.DiemDiChiTiet;
                    OrderViewModel.DiemDenChiTiet = obj.DiemDenChiTiet;
                    OrderViewModel.ThoiGianDi = obj.ThoiGianDi;
                    OrderViewModel.ThoiGianDen = obj.ThoiGianDen;
                    OrderViewModel.UserName = obj.UserName;
                    OrderViewModel.LoaiThanhToan = obj.LoaiThanhToan;
                    OrderViewModel.MaVanDon = obj.MaVanDon;
                    OrderViewModel.CreateAt = obj.CreateAt;
                    OrderViewModel.UpdateAt = obj.UpdateAt;
                    OrderViewModel.FromLat = obj.FromLat;
                    OrderViewModel.ToLat = obj.ToLat;
                    OrderViewModel.FromLng = obj.FromLng;
                    OrderViewModel.ToLng = obj.ToLng;
                    OrderViewModel.Height = obj.Height;
                    OrderViewModel.Width = obj.Width;
                    OrderViewModel.Lenght = obj.Lenght;
                    OrderViewModel.VAT = obj.VAT;
                    OrderViewModel.TrongLuong = obj.TrongLuong;
                    OrderViewModel.Created_By = obj.Created_By;
                    OrderViewModel.ParentId = obj.ParentId;
                    OrderViewModel.NguoiLienHe = obj.NguoiLienHe;
                    OrderViewModel.DienThoaiLienHe = obj.DienThoaiLienHe;
                    OrderViewModel.TrackingStatus = obj.TrackingStatus;
                    var dth = String.Empty;
                    var lstTH = new List<string>();
                    foreach (var item in lstDiemTraHang)
                    {
                        dth = item.FormatAddress;
                        lstTH.Add(dth);
                    }
                    OrderViewModel.DiemTraHang = lstTH;
                    string name = string.Empty, phone = string.Empty;
                    var lstSettings = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetListByParameter(o => o.UserId.Equals(obj.Created_By)).ToList();
                    if (lstSettings.Count > 0)
                    {
                        name = lstSettings.Where(x => x.SettingKey.Equals("TenDieuPhoiVien")).FirstOrDefault().SettingValue;
                        phone = lstSettings.Where(x => x.SettingKey.Equals("DienThoaiDieuPhoiVien")).FirstOrDefault().SettingValue;
                    }
                    var dieuvanInfo = new Dictionary<string, string> { { "Phone", phone }, { "Name", name } };
                    response = new ApiResponse(HttpStatusCode.OK, new Dictionary<string, object> { { "Order", OrderViewModel }, { "SubPerson", dieuvanInfo } }, "Successfully");
                }
                else
                {
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetCurrentOrderInfo " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, ex.Message);
                return Ok(response);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        [Route("ReportSOS")]
        public async Task<IHttpActionResult> ReportSOS(DriverSOSBindingModel SoSModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response); ;
                }
                var order = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.DriverId == UserId && x.Status == 1);
                if (order != null)
                {
                    var obj = new ReportSOS();
                    var odId = order.OrderId;
                    var orderReport = _unitOfWork.GetRepositoryInstance<ReportSOS>().GetFirstOrDefaultByParameter(o => o.OrderId == odId);
                    if (orderReport != null)
                    {
                        orderReport.Reason = SoSModel.ReportDescription;
                        orderReport.Lat = SoSModel.Latitude;
                        orderReport.Lng = SoSModel.Longitude;
                        _unitOfWork.GetRepositoryInstance<ReportSOS>().Update(orderReport);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                        obj.UserId = parrentUserId.ToString();
                        //obj.UserId = "";
                        obj.Created_At = DateTime.Now;
                        obj.ReportTypeId = 0;
                        obj.Reason = SoSModel.ReportDescription;
                        obj.DriverId = UserId;
                        obj.OrderId = order.OrderId;
                        obj.Lat = SoSModel.Latitude.Value;
                        obj.Lng = SoSModel.Longitude.Value;
                        _unitOfWork.GetRepositoryInstance<ReportSOS>().Add(obj);
                        _unitOfWork.SaveChanges();
                    }
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                }
                else
                {
                    response = new ApiResponse(HttpStatusCode.NotFound, null, "Bạn chưa có đơn hàng để thông báo hỏng xe");
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: reportSOS " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                return Ok(response);
            }
        }

        [HttpGet]
        [Route("FindDriver")]
        public async Task<IHttpActionResult> FindDriver(long OrderId)
        {
            try
            {
                var driverFind = new DriverFind();

                var sqlOrderId = new SqlParameter("@OrderId", System.Data.SqlDbType.NVarChar) { Value = OrderId };
                var obj = _unitOfWork.GetRepositoryInstance<SP_GetDriverByOrderId_Result>().GetResultBySqlProcedure("SP_GetDriverByOrderId @OrderId", sqlOrderId).FirstOrDefault();
                if (obj == null)
                {
                    //sqlOrderId = new SqlParameter("@OrderId", System.Data.SqlDbType.NVarChar) { Value = 20539 };
                    //obj = _unitOfWork.GetRepositoryInstance<SP_GetDriverByOrderId_Result>().GetResultBySqlProcedure("SP_GetDriverByOrderId @OrderId", sqlOrderId).FirstOrDefault();
                    response = new ApiResponse(ApiResponseCode.FINDDRIVER_FAIL, obj, "Không tìm thấy tài xế phù hợp");
                    return Ok(response);
                }

                response = new ApiResponse(HttpStatusCode.OK, obj, "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: FinDriver " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Failure");
                return Ok(response);
            }
        }
        public MapPoint GetLatLongByAddress(ref string message, string address)
        {
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);
                return point;
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
                return null;
            }
        }
        [HttpPost]
        [Route("WithdrawalRequest")]
        public async Task<IHttpActionResult> RequestWithdraw([FromBody]WithdrawalModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response);
                }
                else
                {
                    TransactionWithDrawRequest drawRequest = new TransactionWithDrawRequest();
                    drawRequest.ApplicationUserId = userId;
                    drawRequest.BankName = model.TenNganHang;
                    drawRequest.ChiNhanhNganHang = model.ChiNhanhNganHang;
                    drawRequest.Created_At = DateTime.Now;
                    drawRequest.Created_Update = DateTime.Now;
                    drawRequest.FullName = model.FullName;
                    drawRequest.SoDienThoai = User.Identity.Name;
                    drawRequest.SoTaiKhoanNganHang = model.SoTaiKhoanNganHang;
                    drawRequest.SoTien = model.SoTienRut;
                    drawRequest.UserNamePay = null;
                    drawRequest.Status = PublicConstant.ChuaChuyen;
                    drawRequest.TransactionType = PublicConstant.RutTien;
                    _unitOfWork.GetRepositoryInstance<TransactionWithDrawRequest>().Add(drawRequest);
                    _unitOfWork.SaveChanges();
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: Requestwithdraw " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Exception");
                return Ok(response);
            }
        }

        [HttpGet]
        [Route("GetListWithdraw")]
        public async Task<IHttpActionResult> GetListRutTien(int transactionType)
        {
            try
            {
                var lstResult = _unitOfWork.GetRepositoryInstance<TransactionWithDrawRequest>().GetListByParameter(x => x.ApplicationUserId == userId && x.TransactionType == transactionType).ToList();
                response = new ApiResponse(HttpStatusCode.OK, lstResult.OrderByDescending(x => x.Id), "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetListRutTien " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Exception");
                return Ok(response);
            }
        }

        [HttpGet]
        [Route("GetListTransactionOrder")]
        public async Task<IHttpActionResult> GetListTransactionOrder()
        {
            try
            {
                var lstResult = _unitOfWork.GetRepositoryInstance<TransactionOrder>().GetListByParameter(x => x.ToUser.Equals(UserId)).Take(100).ToList();
                response = new ApiResponse(HttpStatusCode.OK, lstResult.OrderByDescending(x => x.Id), "Successfully");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Ultilities.SaveTextToFile("Drivers: GetListTransactionOrder " + ex.Message, log_carrier);
                response = new ApiResponse(ApiResponseCode.EXCEPTION_CODE, null, "Exception");
                return Ok(response);
            }
        }
    }
    public class HistoryInfo
    {
        public long OrderId { get; set; }
        public int TrackingId { get; set; }
        public string OrderCode { get; set; }
        public string OrderDate { get; set; }
        public double OrderPrice { get; set; }
    }
    public class AccountInfo
    {
        public string TotalMoney { get; set; }
        public float PercentSuccess { get; set; }
        public float PercentCancel { get; set; }
        public int Rate { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
    }
    public class CarInfo
    {
        public string License { get; set; }
        public Nullable<double> Payload { get; set; }
        public Nullable<double> Width_Size { get; set; }
        public Nullable<double> Height_Size { get; set; }
        public Nullable<double> Leng_Size { get; set; }
        public string Description { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lng { get; set; }
        public string Address { get; set; }
    }
    public class DriverFind
    {
        public string DriverName { get; set; }
        public string Lincense { get; set; }
        public string PayLoad { get; set; }
        public string Phone { get; set; }
        public double SoKmUocTinh { get; set; }
        public double Gia { get; set; }

    }
}