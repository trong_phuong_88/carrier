﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Carrier.APIClient.SignalR;
using APIClient.Models;
using APIClient.Helpers;
using Carrier.Models.Entities;
using System.Web.Http.ModelBinding;
using System.Threading.Tasks;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace Carrier.APIClient.Controllers
{

    [RoutePrefix("api/Monitor")]
    [System.Web.Http.Authorize]
    public class MonitorController : SignalRBase<CarrierMonitorHub>
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApiResponse response;

        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }

        [Route("LookingNewOrder")]
        [HttpPost]
        public async Task<IHttpActionResult> CheckIn(CheckInLocationBindingModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = ModelState.Values.FirstOrDefault<ModelState>().Errors.FirstOrDefault<ModelError>().ErrorMessage;
                    response = new ApiResponse(ApiResponseCode.VALIDATOR_CODE, null, message);
                    return Ok(response); ;
                }
                var currentItem = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(UserId));
                if (currentItem != null)
                {
                    currentItem.Updated_At = DateTime.Now;
                    currentItem.Lat = model.Latitude.Value;
                    currentItem.Lng = model.Longitude.Value;
                    _unitOfWork.GetRepositoryInstance<Driver>().Update(currentItem);
                    _unitOfWork.SaveChanges();
                    await Task.Run(() => Hub.Clients.Group(UserId).ReceivedData("Group"));
                    response = new ApiResponse(HttpStatusCode.OK, null, "Successfully");
                    return Ok(response);
                }
                else
                {
                    response = new ApiResponse(ApiResponseCode.LOGGIC_CODE, null, "Failure");
                    return Ok(response); ;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
