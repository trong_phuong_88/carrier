﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIClient.Models
{
    public class VanDonBindingModel
    {                                         
        public double Gia { get; set; } 
        public string Note { get; set; }
        public double SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public string CarType { get; set; }
        public long ThoiGianDi { get; set; }
        public long ThoiGianDen { get; set; }  
        public int LoaiThanhToan { get; set; } 
        public double FromLongitude { get; set; }
        public double FromLatitude { get; set; }
        public double ToLatitude { get; set; }
        public double ToLongitude { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Lenght { get; set; }
        public int VAT { get; set; }
        public double TrongLuong { get; set; }
        public string Phone { get; set; }
        public string NguoiLienHe { get; set; }
    }
}