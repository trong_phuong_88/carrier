﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using APIClient.Comon;

namespace Carrier.APIClient.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Tên đầy đủ không quá 100 ký tự.", MinimumLength = 6)]
        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }
        public bool IsDriver { get; set; }
        public string MobilePhone { get; set; }
        public string CMND { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public int Gender { get; set; }
    }
    public class RegisterDriverBindingModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Tên đầy đủ không quá 100 ký tự.", MinimumLength = 6)]
        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }
        public bool IsDriver { get; set; }
        public string MobilePhone { get; set; }
        public string CMND { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public int Gender { get; set; }
        [Required(ErrorMessage = "Chưa nhập biển số xe")]
        [Display(Name = "Biển số xe")]
        public string License { get; set; }
        [Required(ErrorMessage = "Chưa nhập trọng tải xe")]
        public Nullable<double> Payload { get; set; }
        [Required(ErrorMessage = "Chưa nhập đủ kích thước xe")]
        public Nullable<double> Width_Size { get; set; }
        [Required(ErrorMessage = "Chưa nhập đủ kích thước xe")]
        public Nullable<double> Height_Size { get; set; }
        [Required(ErrorMessage = "Chưa nhập đủ kích thước xe")]
        public Nullable<double> Leng_Size { get; set; }
        public string Description { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lng { get; set; }
        public string AddressCar { get; set; }


    }
    public class SignUpBindingModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Tên đầy đủ không quá 100 ký tự.", MinimumLength = 6)]
        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }
        public string MobilePhone { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public int Gender { get; set; }
    }
    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    public class LoginBindingModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ParentCode { get; set; }
    }
    public class SignInBindingModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class APILoginModel
    {
        public object Profile { get; set; }
        public object DriverInfo { get; set; }
        public Token CurrentToken { get; set; }
    }
}
