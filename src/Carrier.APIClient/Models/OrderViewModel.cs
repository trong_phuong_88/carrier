﻿using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.APIClient.Models
{
    public class OrderViewModel
    {
    }
    public class OrderDto
    {
        public long Id { get; set; }
        public Nullable<double> Gia { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public Nullable<double> SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public string UserName { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
        public string MaVanDon { get; set; }
        public Nullable<System.DateTime> CreateAt { get; set; }
        public Nullable<System.DateTime> UpdateAt { get; set; }
        public Nullable<double> FromLat { get; set; }
        public Nullable<double> FromLng { get; set; }
        public Nullable<double> ToLat { get; set; }
        public Nullable<double> ToLng { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
        public Nullable<double> Lenght { get; set; }
        public Nullable<bool> VAT { get; set; }
        public Nullable<double> TrongLuong { get; set; }
        public string DienThoaiLienHe { get; set; }
        public string Created_By { get; set; }
        public string NguoiLienHe { get; set; }
        public Nullable<long> ParentId { get; set; }

        public OrderCarDto OrderCarDto { get; set; }
        public List<OrderPlacesDto> ListOrderPlaces = new List<OrderPlacesDto>();

        public OrderDto()
        {

        }
        public OrderDto(Order o, List<OrderPlaces> orderPlaces = null, OrderCar orderCar = null)
        {
            if (o != null)
            {
                Id = o.Id;
                Gia = o.Gia;
                Status = o.Status;
                Note = o.Note;
                SoKmUocTinh = o.SoKmUocTinh;
                TenHang = o.TenHang;
                DiemDenChiTiet = o.DiemDenChiTiet;
                DiemDiChiTiet = o.DiemDiChiTiet;
                ThoiGianDen = o.ThoiGianDen;
                ThoiGianDi = o.ThoiGianDi;
                UserName = o.UserName;
                LoaiThanhToan = o.LoaiThanhToan;
                MaVanDon = o.MaVanDon;
                CreateAt = o.CreateAt;
                UpdateAt = o.UpdateAt;
                FromLat = o.FromLat;
                FromLng = o.FromLng;
                ToLat = o.ToLat;
                ToLng = o.ToLng;
                Width = o.Width;
                Height = o.Height;
                Lenght = o.Lenght;
                VAT = o.VAT;
                TrongLuong = o.TrongLuong;
                DienThoaiLienHe = o.DienThoaiLienHe;
                Created_By = o.Created_By;
                NguoiLienHe = o.NguoiLienHe;
                ParentId = o.ParentId;
            }
            if (orderPlaces != null)
            {
                foreach (var v in orderPlaces)
                {
                    OrderPlacesDto v1 = new OrderPlacesDto
                    {
                        Created_At = v.Created_At,
                        FormatAddress = v.FormatAddress,
                        Id = v.Id,
                        OrderId = v.OrderId,
                        PlaceLat = v.PlaceLat,
                        PlaceLng = v.PlaceLng,
                        SortDesc = v.SortDesc,
                        Updated_At = v.Updated_At
                    };
                    this.ListOrderPlaces.Add(v1);
                }
            }
            if (orderCar != null)
            {
                this.OrderCarDto = new OrderCarDto
                {
                    CarTypeId = orderCar.CarTypeId,
                    Id = orderCar.Id,
                    Number = orderCar.Number,
                    OrderId = orderCar.OrderId
                };


            }
        }
    }
    public class OrderCarDto
    {
        public long Id { get; set; }
        public Nullable<int> CarTypeId { get; set; }
        public Nullable<long> OrderId { get; set; }
        public Nullable<int> Number { get; set; }
    }
    public class OrderPlacesDto
    {
        public long Id { get; set; }
        public Nullable<long> OrderId { get; set; }
        public Nullable<double> PlaceLat { get; set; }
        public Nullable<double> PlaceLng { get; set; }
        public string FormatAddress { get; set; }
        public Nullable<int> SortDesc { get; set; }
        public Nullable<System.DateTime> Created_At { get; set; }
        public Nullable<System.DateTime> Updated_At { get; set; }
    }
}