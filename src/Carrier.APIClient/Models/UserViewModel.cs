﻿using APIClient.Comon;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Carrier.APIClient.Models
{
    public class UserViewModel
    {
    }
    public class UserLoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class APIUserLoginModel
    {
        public object Profile { get; set; }
        public Token CurrentToken { get; set; }
    }

    public class UserRegisterBindingModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Tên đầy đủ không quá 100 ký tự.", MinimumLength = 6)]
        [Display(Name = "Tên đầy đủ")]
        public string FullName { get; set; }
        public string MobilePhone { get; set; }
        public string CMND { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public int Gender { get; set; }
    }
}