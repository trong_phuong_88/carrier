﻿using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.APIClient.Models
{
    public class OrderSuccessDetailViewModel
    {
        public OrderSuccessDetailViewModel(SP_DriversHistory_Result item)
        {
            OrderId = item.OrderId;
            TenHang = item.TenHang;
            Gia = item.Gia;
            SoKmUocTinh = item.SoKmUocTinh;
            Note = item.Note;
            ThoiGianDi = item.ThoiGianDi;
            ThoiGianDen = item.ThoiGianDen;
            Created_By = item.Created_By;
            DiemDiChiTiet = item.DiemDiChiTiet;
            DiemDenChiTiet = item.DiemDenChiTiet;
            DienThoaiLienHe = item.DienThoaiLienHe;
            TrongLuong = item.TrongLuong;
            Height = item.Height;
            Width = item.Width;
            Lenght = item.Lenght;
            LoaiThanhToan = item.LoaiThanhToan;
            FromLat = item.FromLat;
            FromLng = item.FromLng;
            ToLat = item.ToLat;
            ToLng = item.ToLng;
            VAT = item.VAT;
            MaVanDon = item.MaVanDon;
            OrderTrackingId = item.OrderTrackingId;
        }
        public long OrderId { get; set; }
        public string TenHang { get; set; }
        public Nullable<double> Gia { get; set; }
        public Nullable<double> SoKmUocTinh { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public string Created_By { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public string DienThoaiLienHe { get; set; }
        public Nullable<double> TrongLuong { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
        public Nullable<double> Lenght { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
        public Nullable<double> FromLat { get; set; }
        public Nullable<double> FromLng { get; set; }
        public Nullable<double> ToLat { get; set; }
        public Nullable<double> ToLng { get; set; }
        public Nullable<bool> VAT { get; set; }
        public string MaVanDon { get; set; }
        public int OrderTrackingId { get; set; }
        public List<string> Images { get; set; }
        public DriverInfo DriverInfo { get; set; }
    }

}