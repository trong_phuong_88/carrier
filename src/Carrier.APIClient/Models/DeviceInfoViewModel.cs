﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
namespace APIClient.Models
{
    public class DeviceInfoViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string DeviceToken { get; set; }
        public string SystemVersion { get; set; }
        public string AppVersion { get; set; }
        [Required]
        public string Platform { get; set; }
        public Nullable<int> Status { get; set; }
    }
}