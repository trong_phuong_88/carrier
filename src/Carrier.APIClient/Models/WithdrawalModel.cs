﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.APIClient.Models
{
    public class WithdrawalModel
    {
        public string FullName { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string TenNganHang { get; set; }
        public string ChiNhanhNganHang { get; set; }
        public double SoTienRut { get; set; }
    }
}