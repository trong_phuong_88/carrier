﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APIClient.Models
{
    public class OrderTest
    {
        [Required]
        [Display(Name = "name")]
        public string name { get; set; }
    }
    public class OrderBindingModel
    {
        [Required]
        [Display(Name = "name")]
        public string name { get; set; }

        public long createAt { get; set; }
        public long updateAt { get; set; }
        public int timeEnd { get; set; }

        [Required]
        [Display(Name = "timeStart")]
        public int timeStart { get; set; }
        [Required]
        [Display(Name = "cityFrom")]
        public string cityFrom { get; set; }
        [Required]
        [Display(Name = "cityTo")]
        public string cityTo { get; set; }
        [Required]
        [Display(Name = "districtFrom")]
        public string districtFrom { get; set; }
        [Required]
        [Display(Name = "districtTo")]
        public string districtTo { get; set; }
        [Required]
        [Display(Name = "wardFrom")]

        public string wardFrom { get; set; }
        [Required]
        [Display(Name = "wardTo")]
        public string wardTo { get; set; }
        // get from api calculator when insert
        [Required]
        [Display(Name = "price")]
        public float price { get; set; }
        public string username { get; set; }
        public int status { get; set; }
        [Required]
        [Display(Name = "payType")]
        public int payType { get; set; }
        [Required]
        [Display(Name = "weight")]
        public float weight { get; set; }
        // 1: KH do,2:KH xep,3:Nha xe do,4:Nha xe xep
        [Required]
        [Display(Name = "porterType")]
        public int porterType { get; set; }
        // thoi gian do hang
        [Required]
        [Display(Name = "timeUnload")]
        public int timeUnload { get; set; }
        // thoi gian xep hang
        [Required]
        [Display(Name = "timeLoad")]
        public int timeLoad { get; set; }
        [Required]
        [Display(Name = "mobilePhone")]
        public string mobilePhone { get; set; }
        public string tellPhone { get; set; }
        // don vi khoi luong
        [Required]
        [Display(Name = "klType")]
        public string klType { get; set; }
        // don vi trong luong hoac the tich
        [Required]
        [Display(Name = "slType")]
        public string slType { get; set; }

        public string note { get; set; }
        public string addressFrom { get; set; }

        public string addressTo { get; set; }
        [Required]
        [Display(Name = "distance")]
        public float distance { get; set; }
    }
}