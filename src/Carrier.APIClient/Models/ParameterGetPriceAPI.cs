﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.APIClient.Models
{
    public class ParameterGetPriceAPI
    {
        public string addresssug { get; set; }
        public string formataddress { get; set; }

        //public string addressApi { get; set; }
        //public string address { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string cartype { get; set; }
        public double distance { get; set; }
        //public string origirndistance { get; set; }
        //public string newtime { get; set; }
        //public string origirntime { get; set; }
        //public string arrayDistance { get; set; }
    }
}