﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APIClient.Models
{
    public class CheckInLocationBindingModel
    {
        [Required]
        public Nullable<double> Latitude { get; set; }
        [Required]
        public Nullable<double> Longitude { get; set; }
    }
}