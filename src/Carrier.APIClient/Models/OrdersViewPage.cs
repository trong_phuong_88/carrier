﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Carrier.Models.Entities;
namespace Carrier.APIClient.Models
{
    public class OrdersViewPage
    {
        public long Id { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string MaVanDon { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public Nullable<double> Gia { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
        public Nullable<System.DateTime> CreateAt { get; set; }
        public string Created_By { get; set; }
        public string OrderStatus { get; set; }
        public Nullable<int> TrackingStatus { get; set; }
        public string DriverId { get; set; }
        public string DriverName { get; set; }
        public string CarType { get; set; }
        public List<string> Images { get; set; }
        public double TrongLuong { get; set; }
        public string DienThoaiLienHe { get; set; }
        public string Note { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Lenght { get; set; }
        public double SoKmUocTinh { get; set; }
        public DriverInfo DriverInfo { get; set; }
    }
    public class DriverInfo
    {
        public string BienSoXe { get; set; }
        public string TenTaiXe { get; set; }
        public string SDT { get; set; }
        public string KichThuocXe { get; set; }
        public double TaiTrong { get; set; }
        public string GioiTinh { get; set; }
        public string DiaChi { get; set; }
        public string NgaySinh { get; set; }
    }
}