﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using Carrier.Algorithm.Entity;
using EntityFramework.Utilities;
using System.Text.RegularExpressions;

namespace ImportExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create COM Objects.
            Application excelApp = new Application();


            if (excelApp == null)
            {
                Console.WriteLine("Excel is not installed!!");
                return;
            }
            Console.WriteLine("Drag and drop file excel here...");
            var filePath = Console.ReadLine();
            Workbook excelBook = excelApp.Workbooks.Open(filePath.Substring(1, filePath.Length - 2));
            _Worksheet excelSheet = excelBook.Sheets[3];
            Range excelRange = excelSheet.UsedRange;

            int rows = excelRange.Rows.Count;
            int cols = excelRange.Columns.Count;

            List<BangGiaTheoTuyen> lstTuyen = new List<BangGiaTheoTuyen>();
            for (int i = 2; i <= rows; i++)
            {
                //create new line
                var objTuyen = new BangGiaTheoTuyen();
                var strFrom = new List<string>();
                var strTo = new List<string>();

                for (int j = 1; j <= cols; j++)
                {

                    //write the console
                    if (excelRange.Cells[i, j] != null && excelRange.Cells[i, j].Value2 != null)
                    {
                        if (j == 2)
                        {
                            objTuyen.DiemDi = excelRange.Cells[i, j].Value2.ToString();
                        }
                        else if (j == 3)
                        {
                            objTuyen.DiemDen = excelRange.Cells[i, j].Value2.ToString();
                        }
                        if (j == 4)
                        {
                            //objTuyen.TinhThanh = excelRange.Cells[i, j].Value2.ToString();
                        }

                        if (j == 5)
                        {
                            objTuyen.TypeCar = "1.25";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty))*1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }
                        }
                        if (j == 6)
                        {
                            objTuyen.TypeCar = "1.4";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                        if (j == 7)
                        {
                            objTuyen.TypeCar = "2.5";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                        if (j == 8)
                        {
                            objTuyen.TypeCar = "3.5";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                        if (j == 9)
                        {
                            objTuyen.TypeCar = "5";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }


                        }
                        if (j == 10)
                        {
                            objTuyen.TypeCar = "8";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }


                        }
                        if (j == 11)
                        {
                            objTuyen.TypeCar = "10";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                        if (j == 12)
                        {
                            objTuyen.TypeCar = "15";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                        if (j == 13)
                        {
                            objTuyen.TypeCar = "18";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                        if (j == 14)
                        {
                            objTuyen.TypeCar = "20";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }


                        }
                        if (j == 15)
                        {
                            objTuyen.TypeCar = "40";
                            string strPrice = excelRange.Cells[i, j].Value2.ToString();
                            objTuyen.Price = double.Parse(strPrice.Replace(".", string.Empty).Replace(",", string.Empty)) * 1000;
                            if (objTuyen.Price > 0)
                            {
                                lstTuyen.Add(objTuyen);
                            }

                        }
                    }
                }
            }
            Console.WriteLine($"Tong so tuyen: {lstTuyen.Count()}");

            using(var db = new LogisticDataEntities())
            {
                EFBatchOperation.For(db, db.BangGiaTheoTuyen).InsertAll(lstTuyen);
                db.SaveChanges();
            }

            //after reading, relaase the excel project
            excelApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            Console.ReadLine();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source">Ngô Quyền, Hải Phòng, Việt Nam</param>
        /// <param name="target">An Lão, Hải Phòng, Việt Nam</param>
        public static void InsertCopy(string source,string target)
        {
            using(var db = new LogisticDataEntities())
            {
                var lstSource = db.BangGiaTheoTuyen.Where(x => x.DiemDi.Contains(source)).ToList();
                var lstTuyen = new List<BangGiaTheoTuyen>();
                foreach(var i in lstSource)
                {
                    i.DiemDi = target;
                    i.DiemDiSearch = RemoveUnicodeCharactor(target.Replace(",",string.Empty).Replace(" ",string.Empty));
                    i.Updated_At = DateTime.Now.ToString() ;
                    var isExist = db.BangGiaTheoTuyen.Where(x => x.DiemDiSearch.Equals(i.DiemDiSearch) && x.DiemDenSearch.Equals(i.DiemDenSearch)).FirstOrDefault() != null ? false : true;
                    if (!isExist)
                    {
                        lstTuyen.Add(i);
                    }
                }

                EFBatchOperation.For(db, db.BangGiaTheoTuyen).InsertAll(lstTuyen);
                db.SaveChanges();
            }
        }
        public static string RemoveUnicodeCharactor(string text)
        {
            string result = "";
            try
            {
                for (int i = 33; i < 48; i++)
                {
                    if (i != 44)
                    {
                        text = text.Replace(((char)i).ToString(), "");
                    }
                }
                for (int i = 58; i < 65; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 91; i < 97; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 123; i < 127; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
                string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
                result = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace("(", "").Replace(")", "").Replace("\"", "").Replace("/", "").Replace("\\", "").Replace("'", "").Replace("“", "").Replace("”", "");
            }
            catch (Exception)
            {

            }
            return result;
        }
    }
}
