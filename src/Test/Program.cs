﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        public static string RemoveUnicodeCharactor(string text)
        {
            string result = "";
            try
            {
                for (int i = 33; i < 48; i++)
                {
                    if (i != 44)
                    {
                        text = text.Replace(((char)i).ToString(), "");
                    }
                }
                for (int i = 58; i < 65; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 91; i < 97; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 123; i < 127; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
                string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
                result = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace("(", "").Replace(")", "").Replace("\"", "").Replace("/", "").Replace("\\", "").Replace("'", "").Replace("“", "").Replace("”", "");
            }
            catch (Exception)
            {

            }
            return result;
        }

        static void Main(string[] args)
        {
            var str = "Hồ Chí Minh,Đà Nẵng,Quảng Nam,Quảng Ngãi,Bình Định,Phú Yên,Khánh Hòa,Ninh Thuận,Bình Thuận,Kon Tum,Gia Lai,Đắk Lắk,Đắk Nông,Lâm Đồng,Bà Rịa Vũng Tàu,Bình Dương,Bình Phước,Đồng Nai,Tây Ninh,An Giang,Bạc Liêu,Bến Tre,Cà Mau,Cần Thơ,Đồng Tháp,Hậu Giang,Kiên Giang,Long An,Sóc Trăng,Tiền Giang,Trà Vinh,Vĩnh Long";
            Console.WriteLine(RemoveUnicodeCharactor(str).Replace(" ",string.Empty).ToLower());
            // Configuration (NOTE: .pfx can also be used here)
            var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox,
                @"C:\Users\Data\1dev.p12", "");

            // Create a new broker
            var apnsBroker = new ApnsServiceBroker(config);

            // Wire up events
            apnsBroker.OnNotificationFailed += (notification, aggregateEx) => {

                aggregateEx.Handle(ex => {

                    // See what kind of exception it was to further diagnose
                    if (ex is ApnsNotificationException notificationException)
                    {

                        // Deal with the failed notification
                        var apnsNotification = notificationException.Notification;
                        var statusCode = notificationException.ErrorStatusCode;

                        Console.WriteLine($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}");

                    }
                    else
                    {
                        // Inner exception might hold more useful information like an ApnsConnectionException			
                        Console.WriteLine($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                    }

                    // Mark it as handled
                    return true;
                });
            };

            apnsBroker.OnNotificationSucceeded += (notification) => {
                Console.WriteLine("Apple Notification Sent!");
            };

            // Start the broker
            apnsBroker.Start();

            //foreach (var deviceToken in MY_DEVICE_TOKENS)
            //{
                // Queue a notification to send
                apnsBroker.QueueNotification(new ApnsNotification
                {
                    DeviceToken = "898D32304CFB2F5D6AF892F635F479652981FC4701684CCFD627F140DF708F83",
                    Payload = JObject.Parse("{\"aps\":{\"badge\":7}}")
                });
            //}

            // Stop the broker, wait for it to finish   
            // This isn't done after every message, but after you're
            // done with the broker
            apnsBroker.Stop();
            Console.ReadLine();
        }
        
    }
}
