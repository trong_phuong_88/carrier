﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using Carrier.Models.Entities;
using Newtonsoft.Json.Linq;
using Carrier.Repository;

namespace Carrier.CoreServices
{
    public class CarrierHubClient
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        private HubConnection hubConnection;
        public async System.Threading.Tasks.Task<bool> SendMessage(string fromUserId, string toUserId, object data)
        {
            try
            {
                hubConnection = new HubConnection("http://api.vantaitoiuu.com/", new Dictionary<string, string>{
                         { "UserId", fromUserId }});
                var hubProxy = hubConnection.CreateHubProxy("CarrierMonitor");
                await hubConnection.Start();
                await hubProxy.Invoke("SendData", toUserId, data);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async void ReceivedMessage(string userId, Func<object> callBack)
        {
            try
            {
                hubConnection = new HubConnection("http://api.vantaitoiuu.com/", new Dictionary<string, string>{
                         { "UserId", userId }});
                var hubProxy = hubConnection.CreateHubProxy("CarrierMonitor");
                hubProxy.On("ReceivedData", (str) =>
                {
                    var jsobject = str as JObject;
                    dynamic objResult = jsobject.ToObject<Order>();
                    callBack();
                });
                await hubConnection.Start();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool isOnline(string userId)
        {
            try
            {
                var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userId));
                if (userOnline != null)
                {
                    return userOnline.IsOnline.Value;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}