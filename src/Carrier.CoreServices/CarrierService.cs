﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.PushNotification;
using System.Data.SqlClient;
using Carrier.Utilities;
using System.Data.Entity;
using System.Threading;
using EntityFramework.Utilities;

namespace Carrier.CoreServices
{
    public partial class CarrierService : ServiceBase
    {
        //private System.Timers.Timer timer;
        PushOrdersScheduler jobScheduler;
        public CarrierService()
        {
            InitializeComponent();
            //Setup Service
            this.ServiceName = "CarrierService";
            this.CanStop = true;
            this.CanPauseAndContinue = true;

            //Setup logging
            this.AutoLog = false;

            ((ISupportInitialize)this.EventLog).BeginInit();
            if (!EventLog.SourceExists(this.ServiceName))
            {
                EventLog.CreateEventSource(this.ServiceName, "Application");
            }
            ((ISupportInitialize)this.EventLog).EndInit();

            this.EventLog.Source = this.ServiceName;
            this.EventLog.Log = "Application";
        }

        protected override void OnStart(string[] args)
        {
            this.EventLog.WriteEntry("In OnStart");
            //this.timer = new System.Timers.Timer(30000D);  // 30000 milliseconds = 30 seconds
            //this.timer.AutoReset = true;
            //this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
            //this.timer.Start();
            jobScheduler = new PushOrdersScheduler();
            jobScheduler.Start();
        }

        protected override void OnStop()
        {
            this.EventLog.WriteEntry("In OnStop");
            //this.timer.Stop();
            //this.timer = null;
            if (jobScheduler != null)
            {
                jobScheduler.StopScheduler();
            }
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        //private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    var autoTask = new SearchEngine();
        //    autoTask.MatchingOrders();
        //}
    }
    [Quartz.DisallowConcurrentExecutionAttribute()]
    public class SearchEngine : IJob
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public void Execute(IJobExecutionContext context)
        {
            object execution_lock = new object();
            if (!Monitor.TryEnter(execution_lock, 1))
            {
                return;
            }
            MatchingOrders();
            Monitor.Exit(execution_lock);
        }
        public void MatchingOrders()
        {
            AutoMatchingNewOrders();
        }
        public void AutoMatchingNewOrders()
        {
            try
            {
                using (var db = new Carrier3Entities())
                {
                    var lstOrderPending = db.Database.SqlQuery<OrderTracking>("SP_GetUpdateOrderTracking").ToList();
                    foreach (var item in lstOrderPending)
                    {
                        // Không tìm thấy tài xế sẽ update trạng thái tài xế cancel đơn
                        item.Status = 2;  //Change to Driver cancel
                        item.Updated_At = DateTime.Now;
                    }
                    EFBatchOperation.For(db, db.OrderTracking).UpdateAll(lstOrderPending, x => x.ColumnsToUpdate(c => c.Status, c => c.Updated_At));
                    var lstOrderExpired = db.Database.SqlQuery<OrderTracking>("SP_GetAllOrderExpired").ToList();
                    foreach (var item in lstOrderExpired)
                    {
                        // Không tìm thấy tài xế sẽ update trạng thái tài xế cancel đơn
                        item.Status = PublicConstant.ORDER_EXPIRED;  //Change to Driver cancel
                        item.Updated_At = DateTime.Now;
                    }
                    EFBatchOperation.For(db, db.OrderTracking).UpdateAll(lstOrderExpired, x => x.ColumnsToUpdate(c => c.Status, c => c.Updated_At));
                    db.SaveChanges();
                    var lstNewOrders = db.Database.SqlQuery<SP_GetAllOrders_NeedMatching_Result>("SP_GetAllOrders_NeedMatching").ToList();
                    foreach (var item in lstNewOrders)
                    {
                        var userId = item.Created_By;
                        MapOrder userMapOrder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefaultByParameter(o => o.ToUser.Equals(userId));
                        if (userMapOrder == null)
                        {
                            var nearDriver = GetDriverNearOrder(item.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                            if (nearDriver != null)
                            {

                                PushMessageForDriver(nearDriver.UserId, item.Id);
                            }
                        }
                        //var nearDriver = GetDriverNearOrder(item.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                        //if (nearDriver != null)
                        //{
                        //    //var obj = new OrderTracking();
                        //    //obj.Status = 0;
                        //    //obj.Updated_At = DateTime.Now;
                        //    //obj.Created_At = DateTime.Now;
                        //    //obj.DriverId = nearDriver.UserId;
                        //    //obj.OwnerId = item.Created_By;
                        //    //obj.OrderId = item.Id;
                        //    //_unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                        //    //_unitOfWork.SaveChanges();
                        //    PushMessageForDriver(nearDriver.UserId, item.Id);

                        //    //CarrierHubClient client = new CarrierHubClient();
                        //    //if (client.isOnline(nearDriver.UserId))
                        //    //{
                        //    //    Task.Run(() => client.SendMessage(order.Created_By, nearDriver.UserId, order.Id));
                        //    //}
                        //    //else
                        //    //{
                        //    //    PushMessageForDriver(nearDriver.UserId, item.Id);
                        //    //}

                        //}
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="orderId"></param>
        public void PushMessageForDriver(string driverId, long orderId)
        {
            try
            {
                PushCompletedCallBack callback = PushNotificationCallBack;

                string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
                DevicesPush device = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
                if (device == null)
                {
                    var objOrder = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
                    var obj = new OrderTracking();
                    obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                    obj.Updated_At = DateTime.Now;
                    obj.Created_At = DateTime.Now;
                    obj.DriverId = driverId;
                    obj.OwnerId = objOrder.Created_By;
                    obj.OrderId = orderId;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                    _unitOfWork.SaveChanges();
                    return;
                }
                if (device.Platform.ToLower().Equals("ios"))
                {
                    PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                    PushServices.SetupPushAPN(true);
                    PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
                }
                else
                {
                    jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                    PushServices.GcmKey = PublicConstant.GCM_KEY;
                    PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                    PushServices.SetupPushGCM();
                    PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
                }
            }
            catch (Exception ex)
            {
                var objOrder = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
                var obj = new OrderTracking();
                obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = driverId;
                obj.OwnerId = objOrder.Created_By;
                obj.OrderId = orderId;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            long orderId = long.Parse(obj2);
            //var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
            if (result.Equals("Success") == false)
            {
                // Cap nhat lai trang thai don hang gui loi
                var obj = new OrderTracking();
                obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
            else
            {
                var obj = new OrderTracking();
                obj.Status = 0;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
        }

        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            try
            {
                var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
                //var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
                var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId", sqlOrderId).FirstOrDefault();
                return lstSearch;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private static void WriteEventLogEntry(string message)
        {
            // Create an instance of EventLog
            System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog();

            // Check if the event source exists. If not create it.
            if (!System.Diagnostics.EventLog.SourceExists("CarrierService"))
            {
                System.Diagnostics.EventLog.CreateEventSource("CarrierService", "Application");
            }

            // Set the source name for writing log entries.
            eventLog.Source = "CarrierService";

            // Create an event ID to add to the event log
            int eventID = 8;

            // Write an entry to the event log.
            eventLog.WriteEntry(message,
                                System.Diagnostics.EventLogEntryType.Error,
                                eventID);

            // Close the Event Log
            eventLog.Close();
        }
    }

    public class PushOrdersScheduler
    {
        public void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            IJobDetail job = JobBuilder.Create<SearchEngine>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInSeconds(60)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
                  )
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
        public void StopScheduler()
        {
            StdSchedulerFactory.GetDefaultScheduler().Shutdown();
        }
    }
}
