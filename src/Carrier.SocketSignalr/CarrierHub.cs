﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrier.SocketSignalr
{
    [HubName("carrierhub")]
    class CarrierHub : Hub
    {
        static IHubContext context = GlobalHost.ConnectionManager.GetHubContext<CarrierHub>();
        #region Persitence connection
        public override Task OnConnected()
        {
            string userId = Context.QueryString["userid"];
            Groups.Add(Context.ConnectionId, userId);
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            string userId = Context.QueryString["userid"];
            Groups.Remove(Context.ConnectionId, userId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            string userId = Context.QueryString["userid"];
            Groups.Add(Context.ConnectionId, userId);
            return base.OnReconnected();
        }
        #endregion
        public static void UpdateUser(long id, string type)
        {
            context.Clients.All.updateUser(id, type);
        }
        public static void UpdateDriver(long id, string type)
        {
            context.Clients.All.updateDriver(id, type);
        }
        public static void UpdateOrder(long id, string type)
        {
            context.Clients.All.updateOrder(id, type);
        }
    }
}
public interface IUserIdProvider
{
    string GetUserId(IRequest request);
}