﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableDependency.SqlClient;
using Carrier.Models.Entities;
using Microsoft.Owin.Hosting;
using System.Configuration;
using TableDependency.SqlClient.Base;
using TableDependency.SqlClient.Base.EventArgs;

namespace Carrier.SocketSignalr
{
    class Program
    {
        private static SqlTableDependency<UserInfoes> _tableDependencyUser;
        private static SqlTableDependency<Driver> _tableDependencyDriver;
        private static SqlTableDependency<Order> _tableDependencyOrder;
        static void Main(string[] args)
        {
            try
            {
                var options = new StartOptions();
                var uri = string.Format("http://*:{0}/", ConfigurationManager.AppSettings["Port"]);
                //var uri = string.Format("http://localhost:8888");

                options.Urls.Add(uri);
                using (WebApp.Start<Startup>(uri))
                {
                    //Console.WriteLine(string.Format("Server running at {0}", options.ToString()));
                    Console.WriteLine(string.Format("Server running at {0}", uri.ToString()));

                    //mapper users
                    var mapper = new ModelToTableMapper<UserInfoes>();
                    mapper.AddMapping(s => s.Id, "Id");
                    mapper.AddMapping(s => s.Updated_At, "Updated_At");
                    mapper.AddMapping(s => s.Status, "Status");

                    //mapper driver
                    var mapperdriver = new ModelToTableMapper<Driver>();
                    mapperdriver.AddMapping(s => s.Id, "Id");
                    mapperdriver.AddMapping(s => s.Updated_At, "Updated_At");
                    mapperdriver.AddMapping(s => s.Status, "Status");

                    //mapper order
                    var mapperorder = new ModelToTableMapper<Order>();
                    mapperorder.AddMapping(s => s.Id, "Id");
                    //mapperorder.AddMapping(s => s.UpdateAt, "UpdatedAt");
                    mapperorder.AddMapping(s => s.Status, "Status");

                    _tableDependencyUser = new SqlTableDependency<UserInfoes>(ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString, "UserInfoes", null, mapper);
                    _tableDependencyUser.OnChanged += SqlTableDependencyUser_Changed;
                    _tableDependencyUser.OnError += SqlTableDependency_OnError;
                    _tableDependencyUser.Start();

                    _tableDependencyDriver = new SqlTableDependency<Driver>(ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString, "Driver", null, mapperdriver);
                    _tableDependencyDriver.OnChanged += SqlTableDependencyDriver_Changed;
                    _tableDependencyDriver.OnError += SqlTableDependency_OnError;
                    _tableDependencyDriver.Start();

                    _tableDependencyOrder = new SqlTableDependency<Order>(ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString, "Order", null, mapperorder);
                    _tableDependencyOrder.OnChanged += SqlTableDependencyOrder_Changed;
                    _tableDependencyOrder.OnError += SqlTableDependency_OnError;
                    _tableDependencyOrder.Start();

                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
        static void SqlTableDependency_OnError(object sender, TableDependency.SqlClient.Base.EventArgs.ErrorEventArgs e)
        {
            throw e.Error;
        }
        static void SqlTableDependencyUser_Changed(object sender, RecordChangedEventArgs<UserInfoes> e)
        {
            var changedEntity = e.Entity;
            switch (e.ChangeType)
            {
                case TableDependency.SqlClient.Base.Enums.ChangeType.Insert:
                    {
                        CarrierHub.UpdateUser(e.Entity.Id, "ins");
                    }
                    break;
                case TableDependency.SqlClient.Base.Enums.ChangeType.Delete:
                    {
                        CarrierHub.UpdateUser(e.Entity.Id, "del");
                    }
                    break;

                case TableDependency.SqlClient.Base.Enums.ChangeType.Update:
                    {
                        CarrierHub.UpdateUser(e.Entity.Id, "upd");
                    }
                    break;
                default: break;
            }
        }
        static void SqlTableDependencyDriver_Changed(object sender, RecordChangedEventArgs<Driver> e)
        {
            var changedEntity = e.Entity;
            switch (e.ChangeType)
            {
                case TableDependency.SqlClient.Base.Enums.ChangeType.Insert:
                    {
                        CarrierHub.UpdateDriver(e.Entity.Id, "ins");
                    }
                    break;
                case TableDependency.SqlClient.Base.Enums.ChangeType.Delete:
                    {
                        CarrierHub.UpdateDriver(e.Entity.Id, "del");
                    }
                    break;

                case TableDependency.SqlClient.Base.Enums.ChangeType.Update:
                    {
                        CarrierHub.UpdateDriver(e.Entity.Id, "upd");
                    }
                    break;
                default: break;
            }
        }
        static void SqlTableDependencyOrder_Changed(object sender, RecordChangedEventArgs<Order> e)
        {
            var changedEntity = e.Entity;
            switch (e.ChangeType)
            {
                case TableDependency.SqlClient.Base.Enums.ChangeType.Insert:
                    {
                        CarrierHub.UpdateOrder(e.Entity.Id, "ins");
                    }
                    break;
                case TableDependency.SqlClient.Base.Enums.ChangeType.Delete:
                    {
                        CarrierHub.UpdateOrder(e.Entity.Id, "del");
                    }
                    break;

                case TableDependency.SqlClient.Base.Enums.ChangeType.Update:
                    {
                        CarrierHub.UpdateOrder(e.Entity.Id, "upd");
                    }
                    break;
                default: break;
            }
        }
    }
}
