//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Carrier.Models.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrderPlaces
    {
        public long Id { get; set; }
        public Nullable<long> OrderId { get; set; }
        public Nullable<double> PlaceLat { get; set; }
        public Nullable<double> PlaceLng { get; set; }
        public string FormatAddress { get; set; }
        public Nullable<int> SortDesc { get; set; }
        public Nullable<System.DateTime> Created_At { get; set; }
        public Nullable<System.DateTime> Updated_At { get; set; }
    }
}
