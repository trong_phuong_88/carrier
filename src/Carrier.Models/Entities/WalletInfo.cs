//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Carrier.Models.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class WalletInfo
    {
        public int Id { get; set; }
        public Nullable<double> Balance { get; set; }
        public Nullable<double> Commission { get; set; }
        public Nullable<System.DateTime> Updated_At { get; set; }
        public string UserId { get; set; }
    }
}
