//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Carrier.Models.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string WebSite { get; set; }
        public string Updated_At { get; set; }
        public string Phone { get; set; }
        public string UserId { get; set; }
        public string SubPhone { get; set; }
        public string DispatcherName { get; set; }
        public string DispatcherPhone { get; set; }
    }
}
