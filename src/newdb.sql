USE [Carrier]
GO
/****** Object:  Table [dbo].[Route]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Route]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Route](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Route] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Route] ON
INSERT [dbo].[Route] ([id], [Name]) VALUES (0, N'Hà nội- Hải Dương')
INSERT [dbo].[Route] ([id], [Name]) VALUES (1, N'Hà Nội - Bắc Ninh')
INSERT [dbo].[Route] ([id], [Name]) VALUES (2, N'Hà Nội - Hải Phòng')
INSERT [dbo].[Route] ([id], [Name]) VALUES (3, N'Hà Nội - Hà Nam')
INSERT [dbo].[Route] ([id], [Name]) VALUES (4, N'Hải Dương - Hà Nội')
INSERT [dbo].[Route] ([id], [Name]) VALUES (5, N'Bắc Ninh - Hà Nội')
INSERT [dbo].[Route] ([id], [Name]) VALUES (6, N'Hải Phòng - Hà Nội')
INSERT [dbo].[Route] ([id], [Name]) VALUES (7, N'Hà Nam - Hà Nội')
INSERT [dbo].[Route] ([id], [Name]) VALUES (8, N'Thái Nguyên - Hà Nội')
INSERT [dbo].[Route] ([id], [Name]) VALUES (9, N'Hà Nội - Thái Nguyên')
SET IDENTITY_INSERT [dbo].[Route] OFF
/****** Object:  Table [dbo].[TINH]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TINH]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TINH](
	[TINH_ID] [int] IDENTITY(1,1) NOT NULL,
	[MATINH] [nvarchar](3) NULL,
	[TENTINH] [nvarchar](50) NULL,
 CONSTRAINT [PK_TINH] PRIMARY KEY CLUSTERED 
(
	[TINH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[TINH] ON
INSERT [dbo].[TINH] ([TINH_ID], [MATINH], [TENTINH]) VALUES (1, N'HN', N'Hà Nội')
INSERT [dbo].[TINH] ([TINH_ID], [MATINH], [TENTINH]) VALUES (2, N'HCM', N'TP Hồ Chí Minh')
INSERT [dbo].[TINH] ([TINH_ID], [MATINH], [TENTINH]) VALUES (4, N'HNA', N'Hà Nam')
INSERT [dbo].[TINH] ([TINH_ID], [MATINH], [TENTINH]) VALUES (5, N'DNA', N'Đà Nẵng')
INSERT [dbo].[TINH] ([TINH_ID], [MATINH], [TENTINH]) VALUES (6, N'HP', N'Hải Phòng')
INSERT [dbo].[TINH] ([TINH_ID], [MATINH], [TENTINH]) VALUES (7, N'QN', N'Quảng Ninh')
SET IDENTITY_INSERT [dbo].[TINH] OFF
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'905dd5aa-6059-44a3-8b37-2a0a77b31f53', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3', N'Khách')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'fbcd03df-33e2-4384-83a7-46d86ca1ea57', N'Tài xế')
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201602250811585_InitialCreate', N'IdentitySamp.Migrations.Configuration', 0x1F8B0800000000000400DD5CDB6EE436127D5F60FF41D05376E1B47CD919CC1AED044EDBCE1A3BBE60DA13E46DC096D86D61244A9128C7C6225F96877C527E618B1275E14D976EB9BB1D0408A645F254B17848168B45FFF9FB1FD3EF9FC3C07AC249EA47E4CC3E9A1CDA16266EE4F96475666774F9ED07FBFBEFFEFEB7E9A5173E5B3F95F54E583D6849D233FB91D2F8D47152F71187289D84BE9B4469B4A413370A1DE445CEF1E1E1BF9DA3230703840D589635FD9411EA8738FF013F671171714C3314DC441E0E52FE1D4AE639AA758B429CC6C8C567F6B587A12D7D99A3300EF0A468605BE7818F4099390E96B685088928A2A0EAE9E714CF691291D53C860F2878788931D45BA220C5BC0BA775F5BEBD393C66BD71EA8625949BA5340A07021E9D70F33872F3B58C6C57E603035EE6C662BDCE8D58DBEF531480016481A7B3206195CFEC9B4AC4791ADF623A291B4E0AC8AB04E07E8D92AF9326E281D5BBDD4145A7E3C921FBEFC09A6501CD127C46704613141C58F7D922F0DDFFE29787E82B266727478BE5C98777EF9177F2FE5FF8E45DB3A7D057A8277C804FF74914E30474C3CBAAFFB6E588ED1CB961D5ACD1A6B00A700966866DDDA0E78F98ACE823CC99E30FB675E53F63AFFCC2C9F599F83091A0114D32F8799B05015A04B82A775A65B2FFB7483D7EF77E14A9B7E8C95FE5432FC9878993C0BCFA8483BC347DF4E3627A09E3FD8557BB4AA290FD16F955947E994759E2B2CE44C62A0F2859612A6A37756AF2F6A234831A9FD625EAFE539B69AAD25B5B9575689D99508AD8F66C28F57D5DB9BD19771EC7307839B59845DA08A7DDAF2612C081D5AC56D3E7A82F7D0874EBAFBC1A5E86C80F46580E7B48016764E92721AE7AF94304E44364B0CEF7284D6135F0FE83D2C716D5E19F23A83EC76E9630FE5020D0AB4BBB7F8C08BECDC205E3FEF6648D36340FBF4657C8A551724958AB8DF13E46EED728A397C4BB40147FA66E09C87E3EF8617F8051D439775D9CA6574066ECCD22F0B54BC06B424F8E07C3B1156AD7EEC82C407EA8F747A4B5F44B59B5F649F43514BFC4504DE79BB4A9FA315AF9A49FAA6555B3AA458D4E5579B5A1AA32B07E9AF29A6645F30A9D7A16B546F3F6F2111ADFDDCB61F7DFDFDB6CF336AD050D33CE6185C43F62821358C6BC7B44294E483D027DD68D5D380BF9F031A1AFBE37E5927E424136B6A8B56643BE088C3F1B72D8FD9F0DB99AF0F9C9F79857D2E310545606F85EF5F5E7ABEE392769B6EDE9207473DBC2B7B30698A6CB799A46AE9FCF024DF88B072F44FDC187B3BA2319456FE46808740C88EEB32D0FBE40DF6C995477E402079862EBDC2DC2833394BAC853CD081DF2062856EEA81AC5EAA888A8DC3F1599C0749CB046881D825298A93EA1EAB4F089EBC728E8B492D4B2E716C6FA5EC9904B2E708C0913D869893EC2F54110A64025471A942E0B4D9D06E3DA8968F05A4D63DEE5C2D6E3AEC426B6C2C90EDFD9C04BEEBFBD0A31DB2DB60572B69BA48F02C680DE2E08CACF2A7D09201F5CF68DA0D289C94050EE526D85A0A2C5764050D1246F8EA0C511B5EFF84BE7D57DA3A77850DEFEB6DE6AAE1D7053B0C79E51B3F03DA10D85163851E979B16085F8996A0E67A0273F9FA5DCD59529925F0B602A866C6A7F57EB873AED203289DA006BA27580F2AB400548995003942B6379ADDA712F62006C19776B85E56BBF04DBE0808ADDBC126D54345F9CCAE4EC75FAA87A56B1412179AFC3420347430879F1123BDEC328A6B8AC6A983EBEF0106FB8D1313E182D06EAF05C0D462A3B33BA954A6A765B49E7900D71C936B292E43E19AC547666742B718E761B49E3140C700B363291B8858F34D9CA4847B5DB546553A74896E21FA68E21AB6A7A83E2D827AB469615FF62CD8B14ABD9B7F3E189476181E1B8A926FFA8D2B69244A304ADB0540AA241D32B3F49E905A26881589C67E6854A35EDDE6A58FE4B91CDED531DC4721F286BB37F8B6BBB78812F6CB7AA3FC261AEA09321736AF248BA8602FAE6164B7C43014A34C1FB5914642131FB58E6D6C5155EB37DF14545983A92FE8A0FA5184CF17445EBF71A1B755E8C374E9517B3FE5899214C162F7DD0A6CD4D7EA919A50C5335514CA1AB9D8D9DC99D193A5EB2B3387CB83A115E6776F10C952600FF3410A391E4A08035CAFAA38A79284D4CB1A43FA2946CD284948A0668D94C2911946C16AC8567B0A8BE467F096A1249135D2DED8FAC492769426B8AD7C0D6E82C97F547D5649C348135C5FDB1EBF413791DDDE3FDCB7884D964032B0EBA9BED60068CD75914C7D9001BF7F94DA0C6E78158FCC65E01E3DFF79250C6D3DE26842A421C9B11CA80615E7F84CB7071F969BDC137630A37DCC212DF76C36FC61B46DB57258772DE93AB54D2AB739F74BE9BF2B356F7D31AE5F05554B1ADD28CB0BDBFA4148713566132FF2598053E668B7959E106117F89535A6475D8C78747C7D2D39CFD7926E3A4A91768CEAAA6B732E2986D21418B3CA1C47D44899A2EB1C153921A5489445F130F3F9FD9FFCB5B9DE6410DF6AFFCF381759D7E26FE2F19143C2419B67E53D33FC749AD6F3F6DEDE94388FE56BDFEF94BD1F4C0BA4B60C69C5A87922DD71961F179C4206D8AA61B68B3F6A389B73BA184D7085A546942ACFFF860E1D3511E1E945A7E13A2E77F0C554DFBB8602344CD0382B1F04631A1E981C03A58C6C7011EFCA4F9E380619DD53F16584735E343019F0C07939F09F45F86CA963BDC6A34C7A26D2C49B99D3BD3AC37CAB9DCF5DEA464636F34D1D58CEB01701B6455AFC18C3796903CDAEEA8C9371E0D7B97D47EF524E37DC92BAE333E769B4EBCCD0CE296FBA1BF54E2F01EA4BA695277769F1EBC6DAE9942B97B9E63392C0978CFC8C613BA769FEABB6DB299C2BC7B4EB64109BD7BC6B55DED9F3B665AEF2D74E7E9B96AA691E14A46170BEE4ABF2D02E770C25F444082C2A32C5E4DEAF3BD4CC26AB21805D655CC42CD8966B26065E22872951AED6287F5956FF8AD9DE575DAC51AD233DB64F3F5BF5536AFD32EDB90F4B88BC4616DDAA12E99BB631D6BCB867A4B89C2424F3AF2D2BB7CD6D6FBF5B794173C8A5184D963B8237E3B69C0A39864CCA93320ED57BDEE85BDB3F1F71661FF4EFD550DC1FEFA22C1AEB06B5675AEC9322A376F49A3B28A14A1B9C11479B0A59E27D45F229742318B31E7CFBEF3B81DBBE95860EF9ADC6534CE287419878B4008783127A04D7E9EDB2CEA3CBD8BF3BF60324617404D9FC5E6EFC80F991F7895DE579A9890018279173CA2CBC692B2C8EEEAA542BA8D484F206EBECA297AC0611C00587A47E6E809AFA31BD0EF235E21F7A58E009A40BA074234FBF4C247AB048529C7A8DBC34FE0B0173E7FF77FDC672B4276540000, N'6.1.0-30225')
/****** Object:  Table [dbo].[Comment]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Comment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Star] [nchar](10) NULL,
	[Detail] [nvarchar](max) NULL,
	[FromUser] [nvarchar](max) NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CarDriver]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CarDriver]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CarDriver](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CarID] [int] NULL,
	[DriverID] [int] NULL,
 CONSTRAINT [PK_CarDriver] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Car]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Car]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Car](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[license] [nvarchar](50) NOT NULL,
	[Payload] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Owner] [nvarchar](50) NULL,
	[Size] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Car] ON
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (0, N'huyndai', N'18', N'29xa', N'tài xế 1', N'2x6x4', N'thùng kín')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (1, N'Xe Thaco Olin', N'14', N'99x1-1123', N'Nguyễn Văn B', N'0.8,1.2,1.6', N'Thùng bạt')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (2, N'Xe Huyndai HD05', N'8', N'29A-4589', N'Nguyễn Văn B', NULL, N'Thùng bửng')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (3, N'HD120', N'2', N'29X7-7854', N'Nguyễn Văn Mạnh', N'6.250 x 2.280 x  400', N'Thùng Kín')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (4, N'Kia 165S', N'2.4', N'29Y7-897', N'Doanh nghiệp Vận Tải Thành Công', N'3.500x1.670x2.380', N'MBK')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (5, N'Thaco HD 72', N'3.5', N'29Y7-899', N'Doanh nghiệp Vận Tải Thành Công', N'4.880 x 2.030 x 2.390', N'MBK')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (6, N'Thac Olin 800A', N'8', N'29Y7-989', N'Doanh nghiệp Vận Tải Thành Công', N'6.900, 2.290,0.600', N'MBK')
INSERT [dbo].[Car] ([id], [license], [Payload], [Description], [Owner], [Size], [Type]) VALUES (7, N'HINO XZU720L', N'3.7', N'29T8-5656', N'Trần Trung Kiên', N'5.200 x 2.050 x 640/1.890', N'MBB')
SET IDENTITY_INSERT [dbo].[Car] OFF
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'09a4e418-42ef-46bd-b373-dc74aaea58db', NULL, 0, N'AJxnZZVP6ozbrARW5+BDHCwu4AfCPkd/9SFTpbbXOtiN+mj+K8DqYqLAkHj9W28RJw==', N'fa4776c3-ccb9-434f-ab2c-c2a4bf72e8fd', N'04333969999', 0, 0, NULL, 1, 0, N'Doanh nghiệp Vận Tải Thành Công')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1521e99c-e2b6-4ec3-902d-16323fb85a69', NULL, 0, N'AD+BaS5vqLB6LXN1X5xBxntGpKheE0uwzC8be6Jw6l5OAAl1IoN5V+siaNo4c3/xlw==', N'b1ab295a-ddee-482a-b172-7db0d4339d56', N'0169879899', 0, 0, NULL, 1, 0, N'Trần Đức Cường')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1a75663b-d0d9-46e7-b4a1-6543c21af2e6', NULL, 0, N'APSHW/CTUCOqmoECKFXDdNVOHkFFw20bVeW94VXRWh+tVqt+WdHib+n0ae4fd/ZDoA==', N'03dc56df-450f-49b4-95a2-da18af5803ba', N'0938177703', 0, 0, NULL, 1, 0, N'Trần Trọng Luân')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1ed9b151-3f24-4779-b128-fae639da17b1', NULL, 0, N'AOuFdc14hOAE9kXrIWWCPaoX9TEwvWP+d7zhXW/2QMnWD0rv7bAo57Js892tmhBf3A==', N'06ea1aed-39db-4eca-92a2-1d5a55e3a9f0', N'0968999666', 0, 0, NULL, 1, 0, N'Công ty vận tải Hợp Nhất')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'23c1f69d-f475-4afc-b824-5df11e4facb8', NULL, 0, N'ACG2lKp6iqU8/CUIM3DYTfE86zYj2jeRQJnsctc031vzJ7y5gc7u1lxgVDqcE8SsxA==', N'3a2e6e83-8da7-4d93-9c86-fa5c1d3f14e4', N'0989887866', 0, 0, NULL, 1, 0, N'Trần Trung Kiên')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'25e4d823-b690-4fa8-a6ee-7b54855dc2f3', N'emalaes@gmail.com', 0, N'AMW5YKnQnIbuz3lbGTKuw6vuF7fS8umTjOs2V/Aca6RFKV6RlfLy77l7l/nGIGP4dw==', N'5f97c13f-b040-4a08-b0af-0922c48cb1cc', NULL, 0, 0, NULL, 1, 0, N'emalaes@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2ba194f9-beaf-43f7-949c-a76348665972', NULL, 0, N'APfjU28/9MTZFQaxMDIIjpy0MNbOd1pnvWKbUCdukRgmJCMrMuSgURUBJFunz2IZZg==', N'd07e2d64-046c-4eff-b0d0-9cff788df3f0', N'0989701404', 0, 0, NULL, 1, 0, N'Nguyễn Văn B')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4053cf2c-b3e3-4131-a629-a4cefb25b7b7', NULL, 0, N'AA9w8t8JzXoRj5dAbRkvKSGZRWlTF9A5BowK10k2vGOcOHySa2PS3sSf55ot5YdW3A==', N'80c8186f-21e7-4c6d-bfb2-2406d4e6217d', N'0913196318', 0, 0, NULL, 1, 0, N'traigahoanglan')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'41f668c5-0330-418e-b1ec-05ed08e41c67', NULL, 1, N'ADdfDSHsOFSlRb7/acICE3c1txh01/R3v1ca3jEwwY0IiHz7d3x1iX2mjFZN7m8yDQ==', N'09b72bfc-ae75-4889-b50e-fd61fc34f1fb', NULL, 0, 0, NULL, 1, 0, N'nguyenvana')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'511463e8-9a69-4790-aa5f-9ba5b190b37e', NULL, 0, N'ADpiT4SN+GU/LF9ggfCTqs9djN58YzQR0kpRXwIYMoXZ0x+e8ef+ndNNYjCtuSHT3w==', N'e00d7052-7092-44b5-9814-6409aa6e86f8', N'0986548956', 0, 0, NULL, 1, 0, N'tài xế 1')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'59ef2cfb-b787-438f-804d-da890df07aa9', NULL, 1, N'AErLTONZ6dveP5i4w5VeVe3BlpZdxKzEMGWXAgbU+kyLgXs6ItsdlDWTy6zyQRIS1w==', N'8edf830a-33d8-434a-a140-f409c5fa6fc2', NULL, 0, 0, NULL, 1, 0, N'nguyen van a')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6458d89a-db98-474f-b3ac-ed6f4206b84a', NULL, 0, N'ACWlWbUCDoJ9Ge9eTE0yE/2xW0LH6qPNylMjndzSXZNCm69L8IE9trTRr5+ZlIliWw==', N'1ce8f65a-ae66-4a81-a9b2-9c0a301a4e9a', N'08401687997093', 0, 0, NULL, 1, 0, N'testdriver2')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'68d05988-d36c-47a6-8b06-0be82303d02f', NULL, 0, N'AKkm0iakJdp9aa9AM4m0FJtOjPcMtzjOY1vU7F1p96Gyk3NV2FbUkLXNrrYaqT1sMw==', N'3b6cd741-0009-4671-8c21-0a42e0fa5362', N'09845675543', 0, 0, NULL, 1, 0, N'Kien')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7547f2ac-d8d3-4232-93cd-3d9f3f81c42e', NULL, 0, N'APyBb/ciXnmmjF2ii+S3FIi9Pj9sduhD8ZfOcaPBwCLOG8Ufl0Wb0PQ1qt6Ob8/3rw==', N'bfabb32f-24b8-44a4-8cb9-cd609580c155', N'01629333839', 0, 0, NULL, 1, 0, N'mrkhanh42')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7edaac1f-2b19-4199-8365-09c28b4507af', NULL, 0, N'AP/jMTowtszxrEmt956EC1Jua0nMKlArceQ9O9JzK9Z0/iH1x+3U1YxXvvFEUFDtgQ==', N'7c409c83-17e5-48ee-b1aa-c44747763379', N'08401687997093', 0, 0, NULL, 1, 0, N'anotherdriver')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'800f4774-3ee5-45a7-9ca0-572c969639c4', NULL, 0, N'AIp1SfeU20kZvm9eer8MZnk40fjHq9W4MpAaC38yOKrdBn0pAeydsrWPC+Uxbgx1fA==', N'cac7e031-c0bb-4ee2-a7d8-7ae3bc3bf1ea', N'08401687997093', 0, 0, NULL, 1, 0, N'hoangvanb')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8756d314-c672-4bd7-8d0a-7473896e69d4', NULL, 0, N'ADajQ8o+gzcXgCyl/h2ppoy/ENxl83tcIL6Mym5oWM6G3AJEijhlfu3I6zz5O8jMog==', N'6867f9e2-6034-4232-9b7c-35291c7d42d9', N'08401687997093', 0, 0, NULL, 1, 0, N'testdriver')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8e56f17d-40f8-4217-a00c-baf4fa42899f', NULL, 0, N'AGKQ9SADNhUxeeGdu6HeHQ8xClpaSeIKllbch6VXse3A/kfm9kK5Eb2ZFkIh/fzvpg==', N'3288fb5d-065e-4964-8e7b-310d6274657c', N'545555566556', 0, 0, NULL, 1, 0, N'sanghapharma')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8f2dcb77-57bc-4f1e-9ebd-c90316311bf3', NULL, 0, N'AIemfHvPHGTPLTet052NlgQ/dbhXkMx8SM/AKvLa42j3YsriL3kXFt+kUygVwIwVNA==', N'e46885f8-2a03-4da8-855f-97e7a8f41160', N'0966898999', 0, 0, NULL, 1, 0, N'Doanh nghiệp Đức Nam')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a6391262-9194-447d-8944-c8d0edc099d1', NULL, 0, N'AN049WTOCEhiyEpaa18nOxTTMCVzqTuJ67yXU3mL8fg6Lh1TNSWCX0W0DmnLapLkuw==', N'a034447b-b247-4c30-a9ca-274d17142711', N'08401687997093', 0, 0, NULL, 1, 0, N'newdriver')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'aca99136-48eb-4a20-87d1-7ec09e4a150f', N'aedede123@gmail.com', 1, N'AKMsZcHYnqEmvWfq+G6CoIJImP9p2gfrxxzKLmrDxezffq+QnFLzHlbKVUebYHB2ew==', N'f28b0e06-9798-4d50-b375-295900a65ece', NULL, 0, 0, NULL, 1, 0, N'aedede123@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ae73375f-0b0b-4b4c-9d1b-fe93314ccc8e', NULL, 0, N'APq89CYT5LkyaqasRJTG0dtgRFfBSgtO0hETO2lXzna/8kuSrxHBECRurZFqSqZY6w==', N'47b9a840-e89d-4033-969b-b457948f5a97', N'0912642911', 0, 0, NULL, 1, 0, N'admin')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b603b73a-f566-4770-8216-8ab30814e295', NULL, 0, N'AMcAVEYhGVnts+h03E6NYuOS6KSnjj/Qc4N2yC0PAV4BFVW2EvoOX8959wZe1zAmnQ==', N'1c63471a-ec30-466d-a3e1-d0251ddcfe35', N'08401687997093', 0, 0, NULL, 1, 0, N'newdriver2')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b8807de7-43fb-42ec-84e0-4fc5ff3761b7', NULL, 0, N'AKYcAZDAK6lK9MSlyH/85oQ35zcWIvBTpGXBFTWlc1+kOYlZf+xpE6oO0APQ9YiLKQ==', N'803d09ff-0855-49b8-98f5-38e8fb85c1f9', N'090000000', 0, 0, NULL, 1, 0, N'test')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'bc70df9b-6003-4391-9c52-8df7e70e3aba', N'admin@hh.com', 0, N'AGE7On6Gplarq5P31ybjI8buaBoqGxFdUnLk3t8FmbzNArSgwFn8ePta4EkX+Pg1pg==', N'9480e673-4313-48d2-82ca-8d0c1cd8a4e3', NULL, 0, 0, NULL, 1, 0, N'admin@hh.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c216adbf-d967-4809-9e4c-aa29f43c43ea', NULL, 0, N'AAvHW99KNBQMC90FYVtjdRf5cy0unW3qI9A9v3L0WUkmiTnriW7dGOw+b5e9fkwj3w==', N'b1aa07f5-6323-41db-8bb7-97321980a1f2', N'0902221404', 0, 0, NULL, 1, 0, N'hiennguyen')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'cab1e5ca-e2f2-4375-bd9b-657e7ed58cdb', NULL, 0, N'AEEwb++KqepBqLT6oR6YleXBlWMIWkakEaB8T4Jxx3+KPSqLtR/1RDbCB2fRvNtBiw==', N'b3ab4573-bedd-48f7-af6b-17772f953c50', N'01687997093', 0, 0, NULL, 1, 0, N'webtest')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'cbd5eeb2-af46-414b-b151-a7c842f76c98', NULL, 0, N'ACk6ZMeDOYG4PMJzFM2DkKPxTn9S6QNmnILBd2/dD/DLLubC0/EBTs4w54WUZLCkJQ==', N'94dd5814-95b2-4dfc-a2d3-c52809744c6c', N'0969895742', 0, 0, NULL, 1, 0, N'Nguyễn Văn Mạnh')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd5fe3f5c-34d2-41c6-a6e3-318e29353f35', NULL, 0, N'AKHtvWyBK5Pvm8R/fw4GNF1IqBlnxNJtsyRgmvjp1MfJmUXfBViMQyQnZSnhyTm8CQ==', N'ae72f245-eba5-4784-9871-74904503f390', NULL, 0, 0, NULL, 1, 0, N'test123')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'df1db208-9f46-416d-a9a6-66d8f3226d86', NULL, 1, N'AB6BUXKD/q6kLAUvGjW4fqS9SfEsPjwWgzcUHJ5Y32RCqzzu2mMAHFzFIz6a4hox9g==', N'5335a2a1-34ca-4e7f-8649-f25638b37726', N'8401687997093', 1, 0, NULL, 1, 0, N'sonhoang')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e01be8de-f12d-4d4d-95a3-e05af49546ba', NULL, 0, N'ADYvoqn7WPzKS5VFVk9v99NAEaVLK7D/ln91GJu8G1LJNgDCCrSXuVGnecTovVTndg==', N'd66b8870-2e7a-4ded-b3fc-2e3e6b371ffc', N'01687997093', 0, 0, NULL, 1, 0, N'testweb')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e2e4cfa3-c46a-465e-90cd-d1151e7d9eb7', NULL, 0, N'ADIYc+Czchkn4Wtyf0azlhe4sKlYrG+HmBX77r8DHXqunrT6C2MBijQXRR9zCULHmA==', N'2e172ccc-3d89-43a0-8fe7-b92bc80f3ab6', N'01687997093', 0, 0, NULL, 1, 0, N'testweb1')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e8d69232-0b88-4192-8f92-0d9fa1f47889', NULL, 0, N'AFXtS/WIlLeeKJf5tUhToUTi2qMV/m2a9+gIstDLuGWSQRsvIGmRMO9MC+C6omqh/w==', N'f5198d66-bb93-4709-8ad0-8da6feb5b7ef', N'0989701404', 0, 0, NULL, 1, 0, N'Nguyễn Văn A')
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'09a4e418-42ef-46bd-b373-dc74aaea58db', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1521e99c-e2b6-4ec3-902d-16323fb85a69', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1a75663b-d0d9-46e7-b4a1-6543c21af2e6', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1ed9b151-3f24-4779-b128-fae639da17b1', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'23c1f69d-f475-4afc-b824-5df11e4facb8', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2ba194f9-beaf-43f7-949c-a76348665972', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4053cf2c-b3e3-4131-a629-a4cefb25b7b7', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'511463e8-9a69-4790-aa5f-9ba5b190b37e', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6458d89a-db98-474f-b3ac-ed6f4206b84a', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'68d05988-d36c-47a6-8b06-0be82303d02f', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7547f2ac-d8d3-4232-93cd-3d9f3f81c42e', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7edaac1f-2b19-4199-8365-09c28b4507af', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8756d314-c672-4bd7-8d0a-7473896e69d4', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8e56f17d-40f8-4217-a00c-baf4fa42899f', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8f2dcb77-57bc-4f1e-9ebd-c90316311bf3', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a6391262-9194-447d-8944-c8d0edc099d1', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ae73375f-0b0b-4b4c-9d1b-fe93314ccc8e', N'905dd5aa-6059-44a3-8b37-2a0a77b31f53')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b603b73a-f566-4770-8216-8ab30814e295', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b8807de7-43fb-42ec-84e0-4fc5ff3761b7', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'bc70df9b-6003-4391-9c52-8df7e70e3aba', N'905dd5aa-6059-44a3-8b37-2a0a77b31f53')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c216adbf-d967-4809-9e4c-aa29f43c43ea', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cab1e5ca-e2f2-4375-bd9b-657e7ed58cdb', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cbd5eeb2-af46-414b-b151-a7c842f76c98', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'df1db208-9f46-416d-a9a6-66d8f3226d86', N'fbcd03df-33e2-4384-83a7-46d86ca1ea57')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e01be8de-f12d-4d4d-95a3-e05af49546ba', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e2e4cfa3-c46a-465e-90cd-d1151e7d9eb7', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e8d69232-0b88-4192-8f92-0d9fa1f47889', N'0D9BD5B7-EE73-4A21-B61B-0A100FDF17E3')
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Driver]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Driver]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Driver](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Information] [nvarchar](max) NULL,
	[Rank] [int] NULL,
	[Comments] [int] NULL,
	[CarList] [int] NOT NULL,
 CONSTRAINT [PK_Driver] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Driver] ON
INSERT [dbo].[Driver] ([id], [Name], [Information], [Rank], [Comments], [CarList]) VALUES (0, N'tài xế 1', N'Công ty a>HÀ nỘI>NGLSDJFIDUO', NULL, NULL, 0)
INSERT [dbo].[Driver] ([id], [Name], [Information], [Rank], [Comments], [CarList]) VALUES (1, N'Nguyễn Văn B', N'Công Ty TNHH Vận Tải Việt>Đống Đa -Hà Nội>nguyenvanb@gmail.com', NULL, NULL, 1)
INSERT [dbo].[Driver] ([id], [Name], [Information], [Rank], [Comments], [CarList]) VALUES (2, N'Nguyễn Văn Mạnh', N'>Hà Đông>nguyenvanmanh@gmail.com', NULL, NULL, 3)
INSERT [dbo].[Driver] ([id], [Name], [Information], [Rank], [Comments], [CarList]) VALUES (3, N'Doanh nghiệp Vận Tải Thành Công', N'Doanh nghiệp Vận Tải Thành Công>Phú nghĩa - Chương Mỹ - Hà Nội>vtthanhcong@mgmail.com', NULL, NULL, 4)
INSERT [dbo].[Driver] ([id], [Name], [Information], [Rank], [Comments], [CarList]) VALUES (4, N'Trần Trung Kiên', N'>>', NULL, NULL, 7)
SET IDENTITY_INSERT [dbo].[Driver] OFF
/****** Object:  Table [dbo].[DriverUser]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DriverUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DriverUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DriverID] [int] NULL,
	[UserName] [nvarchar](150) NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_DriverUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Schedule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Schedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Driverid] [int] NULL,
	[Carid] [int] NULL,
	[Time] [nvarchar](50) NULL,
	[Routeid] [int] NULL,
	[Loaded] [nvarchar](50) NULL,
	[Code] [nvarchar](50) NULL,
	[Location] [nvarchar](50) NULL,
	[NgayDi] [datetime] NULL,
	[NgayDen] [datetime] NULL,
	[DiemDi] [nvarchar](250) NULL,
	[DiemDen] [nvarchar](250) NULL,
	[GioDi] [nvarchar](5) NULL,
	[GioDen] [nvarchar](5) NULL,
	[TaiTrong] [float] NULL,
	[DaCho] [float] NULL,
	[ConTrong] [float] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Schedule] ON
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (0, 0, 0, N'6-9-14/06/2016', NULL, N'12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (1, 2, 3, N'6-9-19/08/2016', 0, N'1', N'69-190816KiHnHP', N'Nhân Chính, Thanh Xuân, Hà Nội, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (2, 3, 4, N'7-9-28/08/2016', 1, N'0', N'79-280816KiHNBN', N'Thanh Nhàn, Hai Bà Trưng, Hà Nội, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (3, 3, 4, N'11-13-28/08/2016', 5, N'2', N'1113-280816KiBNHN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (4, 3, 5, N'7-10-29/08/2016', 2, N'3.3', N'710-290816ThHNHP', N'Đồng Tâm, Hai Bà Trưng, Hà Nội, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (5, 3, 5, N'12-16-28/08/2016', 6, N'0', N'1216-280816ThHPHN', N'Đông Hải, Hải An, Hải Phòng, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (6, 2, 3, N'6-9-28/08/2016', 2, N'0', N'69-280816HDHNHP', N'Thanh Trì, Hoàng Mai, Hà Nội, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (7, 2, 3, N'11-17-28/08/2016', 6, N'3.5', N'1117-280816HDHPHN', N'Nam Hải, Hải An, Hải Phòng, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Schedule] ([id], [Driverid], [Carid], [Time], [Routeid], [Loaded], [Code], [Location], [NgayDi], [NgayDen], [DiemDi], [DiemDen], [GioDi], [GioDen], [TaiTrong], [DaCho], [ConTrong]) VALUES (8, 2, 3, N'6-8-29/08/2016', 8, N'0', N'68-290816HDTNHN', N'Bách Quang, tp. Sông Công, Thái Nguyên, Việt Nam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Schedule] OFF
/****** Object:  Table [dbo].[Order]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Order](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Customer] [nvarchar](50) NOT NULL,
	[Driver] [int] NULL,
	[Start] [nvarchar](max) NULL,
	[To] [nvarchar](max) NULL,
	[Route] [int] NULL,
	[Price] [real] NULL,
	[Time] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
	[Package] [nvarchar](50) NULL,
	[Weight] [nvarchar](50) NULL,
	[Number] [nvarchar](50) NULL,
	[Load] [nvarchar](50) NULL,
	[Deploy] [nchar](10) NULL,
	[TArrive] [nvarchar](50) NULL,
	[Scheduleid] [int] NULL,
	[LoadT] [nvarchar](50) NULL,
	[DepT] [nvarchar](50) NULL,
	[Payby] [nvarchar](50) NULL,
	[Size] [nvarchar](50) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Order] ON
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (0, N'Kien', NULL, N'28, 24 Nguyễn Công Hoan, Ngọc Khánh, Ba Đình, Hà Nội, Việt Nam>21.0271731,105.8154227>sadsa>sadsa', N'Tứ Kỳ, Hải Dương, Việt Nam>20.841486425909398,106.37649536132812>sadsa>sadsa', NULL, 1.45665E+07, N'2016-06-13T06:30', N'Đang chờ', N'jfdslkfjsdlkj', N'hàng khô', N'3', N'5', N'Khách chịu', N'Khách chịu', N'2016-06-14T12:30', NULL, N'10', NULL, N'Tiền mặt', N'6x6x9')
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (1, N'Nguyễn Văn A', NULL, N'Trung Văn, Từ Liêm, Hà Nội, Việt Nam>20.9886053,105.78758259999995>20.9886053,105.78758259999995', N'Tràng Cát, Hải An, Hải Phòng, Việt Nam>20.815173724636892,106.74041748046875>20.815173724636892,106.74041748046875', 0, 3240966, N'2016-06-14T06:00', N'Đang chờ', N'hàng dễ vỡ', N'Hàng nông sản', N'1', N'2', N'Khách chịu', N'Khách chịu', N'2016-06-14T00:00', NULL, N'0,30', N'0,30', N'Tiền mặt', N'0.6,1.2,2')
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (2, N'Trần Đức Cường', NULL, N'Hoàng Văn Thụ, Hoàng Mai, Hà Nội, Việt Nam>20.984360909484018,105.8602237701416>sadsa', N'Tân Hưng, tp. Hải Dương, Hải Dương, Việt Nam>20.90756876387173,106.336669921875>sadsa', 0, 1.655856E+07, N'2016-08-19T06:30', N'Đang chờ', N'Vận chuyển đúng giờ', N'Vải may công nghiệp ', N'10', N'2', N'default', N'default   ', N'2016-08-19T00:30', NULL, N'0.5', N'0.5', N'tiền mặt(mặc định)', N'2,2,1')
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (3, N'Trần Đức Cường', NULL, N'Bến Thành, Quận 1, Hồ Chí Minh, Việt Nam>10.7712478,106.68984469999998>sadsa', N'Mỹ Phước, tx. Bến Cát, Bình Dương, Việt Nam>11.146066375906868,106.600341796875>sadsa', 0, 1887480, N'2016-07-27T05:24', N'Đang chờ', NULL, N'Hải sản', N'3', N'1', N'default', N'default   ', N'2016-08-27T17:24', NULL, NULL, NULL, N'tiền mặt(mặc định)', NULL)
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (4, N'Trần Đức Cường', NULL, N'13, 25, 32, 34, 38, 50, 70, 209 Kim Mã, Ba Đình, Hà Nội, Việt Nam>21.03131453420615,105.82374572753906', N'Đình Bảng, tx. Từ Sơn, Bắc Ninh, Việt Nam>21.10692202327835,105.95008850097656', 1, 486720, N'2016-08-28T07:00', N'Đang chờ(đã sửa)', N'Vận chuyển đúng giờ', N'Bao Ngô', N'0.05', N'40', N'default', N'default   ', N'2016-08-28T10:00', NULL, N'0,30', N'0,30', N'tiền mặt(mặc định)', N'1,0.4,0.4')
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (5, N'Doanh nghiệp Đức Nam', NULL, N'Vĩnh Tuy, Hai Bà Trưng, Hà Nội, Việt Nam>20.99686195227438,105.87052345275879', N'Đông Hải, Hải An, Hải Phòng, Việt Nam>20.853036890926578,106.72874450683594', 2, 1434096, N'2016-08-29T08:00', N'Đang chờ(đã sửa)', N'Vận chuyển cẩn thận', N'Máy móc thiết bị', N'1', N'1', N'default', N'default   ', N'2016-08-29T11:00', NULL, N'0,20', N'0,20', N'tiền mặt(mặc định)', N'1,1,1')
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (6, N'Doanh nghiệp Đức Nam', NULL, N'Trần Nguyên Hãn, Lê Chân, Hải Phòng, Việt Nam>20.849828517369758,106.66900634765625', N'Mễ Trì, Hà Nội, Việt Nam>21.007599191785754,105.7708740234375', 6, 4723452, N'2016-08-29T00:00', N'Đang chờ(đã sửa)', NULL, N'Thép cuộn', N'1', N'3', N'Khách chịu', N'Khách chịu', N'2016-08-29T16:00', NULL, N'0,30', N'0,30', N'Chuyển khoản', N'1,1,0.5')
INSERT [dbo].[Order] ([id], [Customer], [Driver], [Start], [To], [Route], [Price], [Time], [Status], [Comment], [Package], [Weight], [Number], [Load], [Deploy], [TArrive], [Scheduleid], [LoadT], [DepT], [Payby], [Size]) VALUES (7, N'Trần Đức Cường', NULL, N'Mai Động, Hoàng Mai, Hà Nội, Việt Nam>20.98780681420909,105.85958003997803>20.98780681420909,105.85958003997803', N'Bình Hàn, tp. Hải Dương, Hải Dương, Việt Nam>20.942202249303275,106.3143539428711>20.942202249303275,106.3143539428711', 0, 8.58048E+08, N'2016-08-28T08:00', N'Đang chờ', N'Hàng dễ vỡ', N'Vải may mặc', N'500', N'2', N'Khách chịu', N'Khách chịu', N'2016-08-28T14:00', NULL, N'0,20', N'0,20', N'Chuyển khoản', N'2,0.5,1')
SET IDENTITY_INSERT [dbo].[Order] OFF
/****** Object:  Table [dbo].[Pricebid]    Script Date: 10/15/2016 09:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricebid]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Pricebid](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Orderid] [int] NOT NULL,
	[driverid] [int] NOT NULL,
	[price] [nvarchar](50) NULL,
	[comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_Pricebid] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_Driver_Car]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Driver_Car]') AND parent_object_id = OBJECT_ID(N'[dbo].[Driver]'))
ALTER TABLE [dbo].[Driver]  WITH CHECK ADD  CONSTRAINT [FK_Driver_Car] FOREIGN KEY([CarList])
REFERENCES [dbo].[Car] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Driver_Car]') AND parent_object_id = OBJECT_ID(N'[dbo].[Driver]'))
ALTER TABLE [dbo].[Driver] CHECK CONSTRAINT [FK_Driver_Car]
GO
/****** Object:  ForeignKey [FK_Driver_Comment]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Driver_Comment]') AND parent_object_id = OBJECT_ID(N'[dbo].[Driver]'))
ALTER TABLE [dbo].[Driver]  WITH CHECK ADD  CONSTRAINT [FK_Driver_Comment] FOREIGN KEY([Comments])
REFERENCES [dbo].[Comment] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Driver_Comment]') AND parent_object_id = OBJECT_ID(N'[dbo].[Driver]'))
ALTER TABLE [dbo].[Driver] CHECK CONSTRAINT [FK_Driver_Comment]
GO
/****** Object:  ForeignKey [FK_DriverUser_Driver]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DriverUser_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[DriverUser]'))
ALTER TABLE [dbo].[DriverUser]  WITH CHECK ADD  CONSTRAINT [FK_DriverUser_Driver] FOREIGN KEY([ID])
REFERENCES [dbo].[Driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DriverUser_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[DriverUser]'))
ALTER TABLE [dbo].[DriverUser] CHECK CONSTRAINT [FK_DriverUser_Driver]
GO
/****** Object:  ForeignKey [FK_Order_Driver]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Driver] FOREIGN KEY([Driver])
REFERENCES [dbo].[Driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Driver]
GO
/****** Object:  ForeignKey [FK_Order_Route]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Route]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Route] FOREIGN KEY([Route])
REFERENCES [dbo].[Route] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Route]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Route]
GO
/****** Object:  ForeignKey [FK_Order_Schedule]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Schedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Schedule] FOREIGN KEY([Scheduleid])
REFERENCES [dbo].[Schedule] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Schedule]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Schedule]
GO
/****** Object:  ForeignKey [FK_Pricebid_Driver]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid]  WITH CHECK ADD  CONSTRAINT [FK_Pricebid_Driver] FOREIGN KEY([id])
REFERENCES [dbo].[Driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid] CHECK CONSTRAINT [FK_Pricebid_Driver]
GO
/****** Object:  ForeignKey [FK_Pricebid_Driver1]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Driver1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid]  WITH CHECK ADD  CONSTRAINT [FK_Pricebid_Driver1] FOREIGN KEY([driverid])
REFERENCES [dbo].[Driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Driver1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid] CHECK CONSTRAINT [FK_Pricebid_Driver1]
GO
/****** Object:  ForeignKey [FK_Pricebid_Order]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Order]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid]  WITH CHECK ADD  CONSTRAINT [FK_Pricebid_Order] FOREIGN KEY([id])
REFERENCES [dbo].[Order] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Order]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid] CHECK CONSTRAINT [FK_Pricebid_Order]
GO
/****** Object:  ForeignKey [FK_Pricebid_Order1]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Order1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid]  WITH CHECK ADD  CONSTRAINT [FK_Pricebid_Order1] FOREIGN KEY([Orderid])
REFERENCES [dbo].[Order] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pricebid_Order1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pricebid]'))
ALTER TABLE [dbo].[Pricebid] CHECK CONSTRAINT [FK_Pricebid_Order1]
GO
/****** Object:  ForeignKey [FK_Schedule_Car]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Car]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Car] FOREIGN KEY([Carid])
REFERENCES [dbo].[Car] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Car]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Car]
GO
/****** Object:  ForeignKey [FK_Schedule_Driver]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Driver] FOREIGN KEY([Driverid])
REFERENCES [dbo].[Driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Driver]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Driver]
GO
/****** Object:  ForeignKey [FK_Schedule_Route]    Script Date: 10/15/2016 09:07:52 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Route]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Route] FOREIGN KEY([Routeid])
REFERENCES [dbo].[Route] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Route]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Route]
GO
