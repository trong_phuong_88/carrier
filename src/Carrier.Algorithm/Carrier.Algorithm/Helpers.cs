﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carrier.Algorithm.Entity.Geo;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;

namespace Carrier.Algorithm
{
    public class Helpers
    {
        public static double CalcDistance(double latFrom, double longFrom, double latTo, double longTo)
        {
            var R = 6371d; // Radius of the earth in km
            var dLat = DegreeToRadian(latTo - latFrom);  // deg2rad below
            var dLon = DegreeToRadian(longTo - longFrom);
            var a =
              Math.Sin(dLat / 2d) * Math.Sin(dLat / 2d) +
              Math.Cos(DegreeToRadian(latFrom)) * Math.Cos(DegreeToRadian(latTo)) *
              Math.Sin(dLon / 2d) * Math.Sin(dLon / 2d);
            var c = 2d * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1d - a));
            var d = R * c; // Distance in km
            return d;
        }
        public static double DegreeToRadian(double angle)
        {
            return angle * (Math.PI / 180d);
        }
        public static string RemoveUnicodeCharactor(string text)
        {
            string result = "";
            try
            {
                for (int i = 33; i < 48; i++)
                {
                    if(i!=44)
                    {
                        text = text.Replace(((char)i).ToString(), "");
                    }
                }
                for (int i = 58; i < 65; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 91; i < 97; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 123; i < 127; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
                string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
                result = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace("(", "").Replace(")", "").Replace("\"", "").Replace("/", "").Replace("\\", "").Replace("'", "").Replace("“", "").Replace("”", "");
            }
            catch (Exception)
            {

            }
            return result;
        }

        public static async Task<GeoResponse> FetchGeoData(string address)
        {
            var url = string.Format("{0}{1}", @"https://maps.googleapis.com/maps/api/geocode/json?&address=", address);
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));
            var httpClient = new HttpClient();
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<GeoResponse>(json);
        }

        /// <summary>
            /// Get Driving Distance In Miles based on Source and Destination.
            /// </summary>
            /// <param name="origin"></param>
            /// <param name="destination"></param>
            /// <returns></returns>
        public static Dictionary<string, double> GetDrivingDistanceInKm(string origin, string destination)
        {
            string url = "https://maps.googleapis.com/maps/api/distancematrix/xml?origins=" + origin + "&destinations=" + destination + "&mode=driving&sensor=false&language=en-EN&units=imperial";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader sreader = new StreamReader(dataStream);
            string responsereader = sreader.ReadToEnd();
            response.Close();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(responsereader);
            if (xmldoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
            {
                XmlNodeList distance = xmldoc.GetElementsByTagName("distance");
                XmlNodeList duration = xmldoc.GetElementsByTagName("duration");
                var dict = new Dictionary<string, double>{ {"duration",Convert.ToDouble(duration[0].ChildNodes[0].InnerText.ToString()) },
                    { "distance", Convert.ToDouble(duration[0].ChildNodes[0].InnerText.ToString()) } };
                //return Convert.ToDouble(distance[0].ChildNodes[1].InnerText.Replace(" mi", ""));
                return dict;
            }

            return null;
        }

        /// <summary>
            /// Get Location based on Latitude and Longitude.
            /// </summary>
            /// <param name="latitude"></param>
            /// <param name="longitude"></param>
            /// <returns></returns>
        public string GetLocation(double latitude, double longitude)
        {
            string url = "https://maps.googleapis.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader sreader = new StreamReader(dataStream);
            string responsereader = sreader.ReadToEnd();
            response.Close();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(responsereader);
            if (xmldoc.GetElementsByTagName("status")[0].ChildNodes[0].InnerText == "OK")
            {
                XmlNodeList location = xmldoc.GetElementsByTagName("distance");
                return xmldoc.GetElementsByTagName("formatted_address")[0].ChildNodes[0].InnerText;
            }

            return "";
        }
        public static string GetAddressFrom(int index, string[] arr)
        {
            var strResult = string.Empty;
            //for (int i = index; i < arr.Count(); i++)
            //{
            //    strResult += arr[i];
            //}
            strResult = arr[index];
            return strResult;
        }
    }
}
