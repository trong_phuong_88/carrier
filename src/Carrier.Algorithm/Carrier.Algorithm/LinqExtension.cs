﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Carrier.Algorithm.Entity;
namespace Carrier.Algorithm
{
    public static class LinqExtension
    {
        public static Expression<Func<BangGiaTheoTuyen, string>> StringFromCity1()
        {
            return x => x.DiemDiSearch.StringFromCity();
        }
        public static bool HasCity(this string value)
        {
            var arr = value.Trim().Split(',');
            if ( arr.Length > 2)
            {
                return true ;
            }
            return false;
        }
        public static bool HasDistrict(this string value)
        {
            var arr = value.Trim().Split(',');
            if (arr.Length > 2)
            {
                return true;
            }
            return false;
        }
        public static bool HasWard(this string value)
        {
            var arr = value.Trim().Split(',');
            if (arr.Length > 3)
            {
                return true;
            }
            return false;
        }
        public static string SubStringFromIndex(this string value, int number)
        {
            //Cang Cat Lai, Nguyen Thi Dinh, Cat Lai, Quan 2, Ho Chi Minh, Viet Nam
            var arr = value.Trim().Split(',');
            if (number +1 > arr.Length)
            {
                return value.Replace(",", "").Replace(" ", "").Trim();
            }
            var str = string.Empty;
            var index = arr.Length - (number + 1);
            for (int i = index; i < arr.Length; i++)
            {
                if (string.IsNullOrEmpty(str))
                {
                    str += arr[i].Trim();
                }
                else
                {
                    str = $"{str}{arr[i].Trim()}";
                }
            }
            return str.Replace(" ", string.Empty);
        }
        public static string StringFromCity(this string value)
        {
            return value.SubStringFromIndex(1);
        }
        public static string StringFromDistrict(this string value)
        {
            return value.SubStringFromIndex(2);
        }
        public static string StringFromWard(this string value)
        {
            return value.SubStringFromIndex(3);
        }
    }
}
