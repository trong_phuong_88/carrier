﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carrier.Algorithm.Entity;
using System.Data.SqlClient;
using GoogleMaps.LocationServices;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Linq.Expressions;

namespace Carrier.Algorithm
{
    public class SysLogisticValidate
    {
        private readonly float _degrees;
        private readonly Location _fromLocation;
        private readonly Location _toLocation;
        private readonly double _distance;
        private readonly string _fromAddress;
        private readonly string _toAddress;
        public SysLogisticValidate(float degrees, Location loc1, Location loc2)
        {
            _degrees = degrees;
            _fromLocation = loc1;
            _toLocation = loc2;
            _distance = Helpers.CalcDistance(loc1.Lat, loc1.Lng, loc2.Lat, loc2.Lng);
        }
        public SysLogisticValidate(float degrees, string from, string to)
        {
            _degrees = degrees;
            _fromAddress = from;
            _toAddress = to;
        }
        public bool InTheWay(Location lc)
        {
            bool bResult = false;
            return bResult;
        }
        public bool OutTheWay(Location lc)
        {
            bool bResult = false;
            return bResult;
        }
        public bool ApproximateTheWay(Location lc, float hs)
        {
            bool bResult = false;
            return bResult;
        }
        public bool ApproximateTheWay(string midAddress, float hs)
        {
            try
            {
                bool bResult = true;
                Dictionary<string, double> obj1 = Helpers.GetDrivingDistanceInKm(_fromAddress, midAddress);
                Dictionary<string, double> obj2 = Helpers.GetDrivingDistanceInKm(midAddress, _toAddress);
                Dictionary<string, double> obj3 = Helpers.GetDrivingDistanceInKm(_fromAddress, _toAddress);
                var AB = obj3["distance"];
                var ACB = obj1["distance"] + obj2["distance"];
                if (ACB - AB > hs)
                {
                    // the new route greater than old route 30km
                    return false;
                }
                else
                {
                    var TimeAB = obj3["duration"];
                    var TimeACB = obj1["duration"] + obj2["duration"];
                    if (TimeACB - TimeAB > 30)
                    {
                        return false;
                    }
                }
                return bResult;
            }
            catch (Exception)
            {

                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newDistance">The distance of routes cross all location from origirn address</param>
        /// <param name="origirnDistance">The distance from origirn address to destination address </param>
        /// <param name="newTime"></param>
        /// <param name="origirnTime"></param>
        /// <param name="lstLocation"></param>
        /// <param name="lstAddress"></param>
        /// <returns></returns>
        public static bool IsValid(float newDistance, float origirnDistance, long newTime, long origirnTime, List<Location> lstLocation, List<string> lstAddress)
        {
            bool bResult = true;
            if (newDistance - origirnDistance > 30 * 1000)
            {
                bResult = false;
            }
            if (newTime - origirnTime > 45 * 60 * 60)
            {
                bResult = false;
            }
            return bResult;
        }
    }
    public class SysLogisticRoute
    {
        private readonly Location _fromLocation;
        private readonly Location _toLocation;
        public SysLogisticRoute(Location lc1, Location lc2)
        {
            _fromLocation = lc1;
            _toLocation = lc2;
        }
        public bool CanDriver(out double distance)
        {
            bool bResult = false;
            double cDistance = 0;

            distance = cDistance;
            return bResult;
        }
    }

    public class SysLogistic
    {
        public static double GetPrice(string carType, double distance, List<string> lstAdd, string[] arrOrigirnAd, string[] arrApi, out string strMess)
        {
            if (string.IsNullOrEmpty(carType) || lstAdd == null)
            {
                strMess = "The parammeter must be not null.";
                return 0;
            }
            if (lstAdd.Count() < 2)
            {
                strMess = "The param lstAdd must greater than 2 value";
                return 0;
            }
            else
            {
                double price = 0;
                price = GetPriceByRoute(carType, arrApi[0], arrApi[1]);
                if (price == 0)
                {
                    var area = GetArea(arrApi[0], arrApi[1]);
                    price = GetPriceByKm(carType, distance, area);
                }
                strMess = string.Empty;
                return price;
            }
        }
        public static Area GetArea(string fromAdd, string toAdd)
        {
            var arrCityFrom = fromAdd.Split(',').ToArray();
            var cityFrom = arrCityFrom[arrCityFrom.Count() - 2].Trim();
            var arrCityTo = toAdd.Split(',').ToArray();
            var cityTo = arrCityTo[arrCityTo.Count() - 2].Trim();
            using (var db = new LogisticDataEntities())
            {
                var queryFrom = $"SELECT * FROM dbo.AREA WHERE [data] LIKE N'%{cityFrom}%' OR [data] LIKE N'%{Helpers.RemoveUnicodeCharactor(cityFrom)}%' OR [data] LIKE N'%{Helpers.RemoveUnicodeCharactor(cityFrom).ToLower()}%'";
                var queryTo = $"SELECT * FROM dbo.AREA WHERE [data] LIKE N'%{cityTo}%' OR [data] LIKE N'%{Helpers.RemoveUnicodeCharactor(cityTo)}%' OR [data] LIKE N'%{Helpers.RemoveUnicodeCharactor(cityTo).ToLower()}%'";

                var areaF = db.Database.SqlQuery<Area>(queryFrom).FirstOrDefault();

                var areaT = db.Database.SqlQuery<Area>(queryTo).FirstOrDefault();
                if (areaF != null && areaF.ID == 1)
                {
                    return areaF;
                }
                else if (areaT != null && areaT.ID == 1)
                {
                    return areaT;
                }
                if (areaF != null) { return areaF; }
                if (areaT != null) { return areaT; }
                return null;
            }
        }
        public static double GetPriceForEnterprise(string uid, string carType, double distance, List<string> lstAdd)
        {
            if (string.IsNullOrEmpty(carType) || lstAdd == null)
            {
                return 0;
            }
            if (lstAdd.Count() < 2)
            {
                return 0;
            }
            else
            {
                double price = GetPriceForEnterprise(uid, carType, distance, lstAdd.First(), lstAdd.Last());
                return price;
            }
        }
        public static double GetPriceForEnterprise(string uid, string carType, double distance, string fromAddress, string toAddress)
        {

            return GetPriceByRouteForEnterprise(uid, carType, fromAddress, toAddress);
        }
        public static double GetPriceByRouteForEnterprise(string uid, string carType, string fromAddress, string toAddress)
        {
            try
            {
                using (var db = new LogisticDataEntities())
                {
                    var strFromAdd = Helpers.RemoveUnicodeCharactor(fromAddress);
                    var strToAdd = Helpers.RemoveUnicodeCharactor(toAddress);

                    var newTuyen = new E_BangGiaTheoTuyen();
                    var objTuyen = db.E_BangGiaTheoTuyen.Where(o => o.user_id == uid && o.TypeCar == carType
                                    && ((o.DiemDiSearch.ToLower().Equals(strToAdd.ToLower()) && o.DiemDenSearch.ToLower().Contains(strFromAdd.ToLower()))
                                    || (o.DiemDiSearch.ToLower().Equals(strFromAdd.ToLower()) && o.DiemDenSearch.ToLower().Contains(strToAdd.ToLower())))).FirstOrDefault();
                    if (objTuyen != null)
                    {
                        return objTuyen.Price.Value;
                    }
                    else
                    {
                        var arrFrom = strFromAdd.Split(',');
                        var arrTo = strToAdd.Split(',');
                        int countF = arrFrom.Count();
                        int countT = arrTo.Count();
                        var _fromQuery = "";
                        var _toQuery = "";
                        //lay dia chia den huyen
                        for (int i = countF - 1; i > countF - 4 && i >= 0; i--)
                        {
                            _fromQuery = arrFrom[i].Trim() + ", " + _fromQuery;
                        }
                        for (int i = countT - 1; i > countT - 4 && i >= 0; i--)
                        {
                            _toQuery = arrTo[i].Trim() + ", " + _toQuery;
                        }
                        _fromQuery = _fromQuery.Substring(0, _fromQuery.Length - 2);
                        _toQuery = _toQuery.Substring(0, _toQuery.Length - 2);
                        var di = arrFrom[countF - 2].ToLower().Trim();
                        if (strFromAdd.Trim().ToLower().Contains("hai phong"))
                        {
                            di = "hai phong";
                        }
                        if (strFromAdd.Trim().ToLower().Contains("noi bai"))
                        {
                            di = "noi bai";
                        }
                        if (fromAddress.Equals("Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || fromAddress.Equals("Cảng hàng không quốc tế Nội Bài (HAN), Phú Minh, Sóc Sơn, Hà Nội, Việt Nam"))
                        {
                            di = "noi bai";
                        }
                        if (fromAddress.Contains("Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || fromAddress.Contains("Cảng hàng không quốc tế Nội Bài (HAN), Phú Minh, Sóc Sơn, Hà Nội, Việt Nam"))
                        {
                            di = "noi bai";
                        }
                        if (fromAddress.Contains("Phú Cường, Sóc Sơn, Hà Nội, Việt Nam"))
                        {
                            di = "noi bai";
                        }
                        var den = _toQuery.ToLower().Trim();
                        // Chieu thuan 
                        objTuyen = db.E_BangGiaTheoTuyen.Where(o => o.user_id == uid && o.TypeCar == carType
                                   && ((o.DiemDiSearch.ToLower().Equals(di) && o.DiemDenSearch.ToLower().Contains(den))
                                   )).FirstOrDefault();
                        if (objTuyen != null)
                        {
                            return objTuyen.Price.Value;
                        }
                        // Chieu nghịch
                        di = arrTo[countT - 2].ToLower().Trim();
                        if (strToAdd.Trim().ToLower().Contains("hai phong"))
                        {
                            di = "hai phong";
                        }
                        if (strToAdd.Trim().ToLower().Contains("noi bai"))
                        {
                            di = "noi bai";
                        }
                        if (toAddress.Equals("Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || toAddress.Equals("Cảng hàng không quốc tế Nội Bài (HAN), Phú Minh, Sóc Sơn, Hà Nội, Việt Nam"))
                        {
                            di = "noi bai";
                        }
                        if (toAddress.Contains("Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || toAddress.Contains("Cảng hàng không quốc tế Nội Bài (HAN), Phú Minh, Sóc Sơn, Hà Nội, Việt Nam"))
                        {
                            di = "noi bai";
                        }
                        if (toAddress.Contains("Phú Cường, Sóc Sơn, Hà Nội, Việt Nam"))
                        {
                            di = "noi bai";
                        }
                        den = _fromQuery.ToLower().Trim();
                        objTuyen = db.E_BangGiaTheoTuyen.Where(o => o.user_id == uid && o.TypeCar == carType
                                   && ((o.DiemDiSearch.ToLower().Equals(di) && o.DiemDenSearch.ToLower().Contains(den))
                                   )).FirstOrDefault();
                        if (objTuyen != null)
                        {
                            return objTuyen.Price.Value;
                        }


                    }
                    #region Update  Bang Gia
                    newTuyen.DiemDi = fromAddress;
                    newTuyen.DiemDen = toAddress;
                    newTuyen.DiemDiSearch = strFromAdd;
                    newTuyen.DiemDenSearch = strToAdd;
                    newTuyen.IsActive = false;
                    newTuyen.Price = 0;
                    newTuyen.Updated_At = DateTime.Now.ToString("dd/MM/yyyy");
                    newTuyen.Version = 1;
                    db.E_BangGiaTheoTuyen.Add(newTuyen);
                    db.SaveChangesAsync();
                    #endregion
                    return 0;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }

        }

        public static string StringFromDistrict(string str)
        {
            return str.SubStringFromIndex(2);
        }
        public static double GetPriceByRoute(string carType, string fromAddress, string toAddress)
        {
            try
            {
                // Lay đến xã trước
                var wardfrom = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromWard().Trim()).ToLower();
                var wardto = Helpers.RemoveUnicodeCharactor(toAddress.StringFromWard()).ToLower();
                var districtFrom = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromDistrict()).ToLower();
                var districtTo = Helpers.RemoveUnicodeCharactor(toAddress.StringFromDistrict()).ToLower();
                var cityFrom = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromCity()).ToLower();
                var cityTo = Helpers.RemoveUnicodeCharactor(toAddress.StringFromCity()).ToLower();
    
                double price = 0;
                using (var db = new LogisticDataEntities())
                {
                    if (fromAddress.Contains("Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || fromAddress.Contains("Sóc Sơn Hà Nội, Vietnam")
   || fromAddress.Contains("Cảng hàng không quốc tế Nội Bài (HAN), Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || fromAddress.Contains("Phú Cường, Sóc Sơn, Hà Nội, Việt Nam")
   || fromAddress.Contains("Cảng hàng không quốc tế Nội Bài (HAN), Sóc Sơn, Hà Nội, Việt Nam") || fromAddress.Equals("Cty A76 Nội Bài, Sân Bay Nội Bài, Vietnam") || districtFrom.Equals("SocSonHaNoiVietnam")
   || fromAddress.Contains("Sóc Sơn, Hanoi, Vietnam"))
                    {
                        wardfrom = "SocSonHaNoiVietnam".ToLower();
                        districtFrom = "SocSonHaNoiVietnam".ToLower();
                        cityFrom = "SocSonHaNoiVietnam".ToLower();
                    }
                    if (toAddress.Contains("Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || toAddress.Contains("Sóc Sơn Hà Nội, Vietnam")
   || toAddress.Contains("Cảng hàng không quốc tế Nội Bài (HAN), Phú Minh, Sóc Sơn, Hà Nội, Việt Nam") || toAddress.Contains("Phú Cường, Sóc Sơn, Hà Nội, Việt Nam")
   || toAddress.Contains("Cảng hàng không quốc tế Nội Bài (HAN), Sóc Sơn, Hà Nội, Việt Nam") || toAddress.Equals("Cty A76 Nội Bài, Sân Bay Nội Bài, Vietnam") || districtTo.Equals("SocSonHaNoiVietnam")
   || toAddress.Contains("Sóc Sơn, Hanoi, Vietnam"))
                    {
                        wardto = "SocSonHaNoiVietnam".ToLower();
                        districtTo = "SocSonHaNoiVietnam".ToLower();
                        cityTo = "SocSonHaNoiVietnam".ToLower();
                    }
                    var strFromAdd = Helpers.RemoveUnicodeCharactor(fromAddress.Trim());
                    var strToAdd = Helpers.RemoveUnicodeCharactor(toAddress.Trim());
                    strToAdd = strToAdd.Replace(",", string.Empty).Replace(" ", string.Empty).Trim().ToLower();
                    strFromAdd = strFromAdd.Replace(",", string.Empty).Replace(" ", string.Empty).Trim().ToLower();
                    var newTuyen = new BangGiaTheoTuyen();

                    var query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{strFromAdd}' = DiemDiSearch " +
                        $"AND '{strToAdd}' = DiemDenSearch) OR ('{strFromAdd}' = DiemDenSearch AND '{strToAdd}' = DiemDiSearch))";
                    var objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                    if (objTuyen == null)
                    {
                        if (fromAddress.HasWard() && toAddress.HasWard())
                        {
                            // Xã-Xã
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{wardfrom}'  = XaDiSearch" +
            $"AND '{wardto}' = XaDenSearch) OR ('{wardfrom}' = XaDenSearch AND '{wardto}' = XaDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                            // Xã- Huyện
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{wardfrom}'  = XaDiSearch" +
            $"AND '{districtTo}' = HuyenDenSearch) OR ('{wardfrom}' = HuyenDenSearch AND '{districtTo}' = XaDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                            // Huyện- Huyện
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{districtFrom}'  = HuyenDiSearch " +
            $"AND '{districtTo}' = HuyenDenSearch) OR ('{districtFrom}' = HuyenDenSearch AND '{districtTo}' = HuyenDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                        }
                        else if (fromAddress.HasWard() && toAddress.HasDistrict())
                        {
                            // Xã- Huyện
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{wardfrom}'  = XaDiSearch " +
            $"AND '{districtTo}' = HuyenDenSearch) OR ('{wardfrom}' = HuyenDenSearch AND '{districtTo}' = XaDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                            // Huyện- Huyện
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{districtFrom}'  = HuyenDiSearch " +
            $"AND '{districtTo}' = HuyenDenSearch) OR ('{districtFrom}' = HuyenDenSearch AND '{districtTo}' = HuyenDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                        }
                        else if (fromAddress.HasDistrict() && toAddress.HasWard())
                        {
                            // Huyện- Xã
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{districtFrom}'  = HuyenDiSearch " +
            $"AND '{wardto}' = XaDenSearch) OR ('{wardto}' = HuyenDiSearch AND '{districtFrom}' = XaDenSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                            // Huyện- Huyện
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{districtFrom}'  = HuyenDiSearch " +
            $"AND '{districtTo}' = HuyenDenSearch) OR ('{districtFrom}' = HuyenDenSearch AND '{districtTo}' = HuyenDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                        }
                        else if (fromAddress.HasDistrict() && toAddress.HasDistrict())
                        {
                            // Huyện- Huyện
                            if (objTuyen == null)
                            {
                                query = $"SELECT * FROM dbo.BangGiaTheoTuyen WHERE IsActive = 1 AND TypeCar = '{carType}' AND (('{districtFrom}'  = HuyenDiSearch " +
            $"AND '{districtTo}' = HuyenDenSearch) OR ('{districtFrom}' = HuyenDenSearch AND '{districtTo}' = HuyenDiSearch))";
                                objTuyen = db.Database.SqlQuery<BangGiaTheoTuyen>(query).FirstOrDefault();
                            }
                        }
                        else if (fromAddress.HasCity() || toAddress.HasCity())
                        {
                            // Điểm đi và điểm đến chỉ nhập tỉnh thành,báo lỗi,yêu cầu nhập địa điểm đi chính xác đến huyện
                            price = -1;
                        }
                    }
                    if (objTuyen != null)
                    {
                        price = objTuyen.Price.Value;
                    }
                    return price;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }

        }
        public static double GetPriceByKm(string carType, double distance, Area area)
        {
            try
            {
                double price = 0;
                //double distance = Helpers.CalcDistance(lc1.Lat, lc1.Lng, lc2.Lat, lc2.Lng);
                distance = Math.Round(distance, 2);
                BangGiaTheoKM bangGiaKM = null;
                using (var db = new LogisticDataEntities())
                {
                    var id_area = area.ID;
                    var query = $" select * from BangGiaTheoKM where CONVERT(float, typecar) = CONVERT(float, '{carType}')  and [Rank] < {distance.ToString().Replace(",",".")} and AreaId = {id_area} order by [Rank] desc";
                    bangGiaKM = db.Database.SqlQuery<BangGiaTheoKM>(query).FirstOrDefault();
                    if (bangGiaKM != null)
                    {
                        var priceFixed = double.Parse(bangGiaKM.Price.ToString());
                        price = priceFixed * distance; // giá = đơn giá * km
                        if (price < bangGiaKM.Min)
                        {
                            price = (double)bangGiaKM.Min;
                        }
                    }
                    else
                    {
                        // Điểm đi và điểm đến không thuộc khu vực nào trên hệ thống hoặc hệ thống chưa hỗ trợ giá cho trường hợp này
                        bangGiaKM = db.BangGiaTheoKM.Where(o => o.TypeCar == carType && o.Rank < distance).OrderByDescending(o => o.Rank).FirstOrDefault();
                        var priceFixed = double.Parse(bangGiaKM.Price.ToString());
                        price = priceFixed * distance; // giá = đơn giá * km
                        if (price < bangGiaKM.Min)
                        {
                            price = (double)bangGiaKM.Min;
                        }
                    }
                }
                return Math.Round(price / 1000) * 1000;
            }
            catch (Exception ex)
            {
                var t = ex.Message;
                return 0;
            }
        }
    }
    public class BangGiaTheoTuyenSearch
    {
        public BangGiaTheoTuyen obj { get; set; }
        public string searchTo { get; set; }
    }
}