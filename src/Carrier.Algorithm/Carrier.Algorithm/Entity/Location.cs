﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrier.Algorithm.Entity
{
    public class Location
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
