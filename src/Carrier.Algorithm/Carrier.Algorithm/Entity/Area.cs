//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Carrier.Algorithm.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Area
    {
        public long ID { get; set; }
        public string area_name { get; set; }
        public Nullable<double> lat_center { get; set; }
        public Nullable<double> lng_center { get; set; }
        public Nullable<double> lat_boudary { get; set; }
        public Nullable<double> lng_boudary { get; set; }
        public Nullable<long> parent_id { get; set; }
        public string data { get; set; }
        public Nullable<int> config_id { get; set; }
        public Nullable<bool> is_center { get; set; }
    }
}
