﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrier.Algorithm.Entity
{
    public class Triangle
    {
        public double LengthA { get; set; }
        public double LengthB { get; set; }
        public double LengthC { get; set; }
        public Triangle(Location a,Location b,Location c)
        {
            LengthA = 0;
            LengthB = 0;
            LengthC = 0;
        }
    }
}
