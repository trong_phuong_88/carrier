﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrier.Utilities
{
    public class SecureConstant
    {
        public const decimal BalanceDefault = 10000000;
        // Xe container
        public const float GIA_XE_DAUKEO = 30000;
        public const float GIA_MOCUA_DAUKEO = 300000;

        //Giá tối thiểu 300.000 đồng
        public const float GIA_MOCUA_XETAI = 300000;

        // Dưới 4km đầu : 1500đ/1km/1tấn
        public const float GIA_XETAI_KM0_4 = 15000;
        //Từ km thứ 4 trở đi: 1200đ/1km/1tấn    
        public const float GIA_XETAI_KM4 = 12000;

        //HE SO X1
        //0-100 km
        public const float km0100 = 1;
        //101-150 km
        public const float km101150 = 0.8f;
        //151-250 km
        public const float km151250 = 0.7f;
        //251-450 km
        public const float km251450 = 0.6f;
        //501- 1000 km 
        public const float km5011000 = 0.55f;
        //1001-1500km  
        public const float km10011500 = 0.5f;
        //1501-2000km 
        public const float km15012000 = 0.45f;
        //2001-2500 km 
        public const float km20012500 = 0.4f;
        //Trên 2500 km
        public const float km2500 = 0.35f;

        // He so X2
        public static string[] X2_15 => new string[] { "Lào Cai", "Tuyên Quang", "Yên Bái", "Bắc Cạn", "Cao Bằng", "Sơn La", "Lai Châu", "Điện Biên" };
        public static string[] X2_135 => new string[] { "Nam Định", "Thái Bình", "Hòa Bình", "Đắk Lắk", "Đắk Nông", "Cà Mau", "Kon Tum", "Điện Biên" };
    }

}
