﻿
namespace Carrier.Utilities
{
    public class UIConstant
    {
        public const string COMBOBOX_ALL_VALUE = "[Tất cả]"; // gia tri tat ca
        public const string COMBOBOX_CHOOSE_VALUE = "[Lựa chọn]"; // gia tri lua chon
        //Result Action
        public const string RESULT_DELETE = "RESULT_DELETE";

        //Indicator Group
        public const string LIST_INDICATOR_GROUP = "LIST_INDICATOR_GROUP"; // danh sach nhom tieu chi
        public const string LIST_INDICATOR = "LIST_INDICATOR"; // danh sach tieu chi
        public const string COMBOBOX_STATUS = "LIST_STATUS"; // danh sach trang thai
        public const string COMBOBOX_SINGLE_STATUS = "COMBOBOX_SINGLE_STATUS"; // danh sach trang thai khong co tat ca
        public const string COMBOBOX_INDICATOR = "COMBOBOX_INDICATOR"; // danh sach tieu chi

        //Indicator 
        public const string CREATE_OR_EDIT_INDICATOR = "CREATE_OR_EDIT_INDICATOR"; // tieu chi

        //Active

        public const string COMBOBOX_STATUS_ITEM2_VALUE = "Hiệu lực"; // trang thai hieu luc
        public const string COMBOBOX_STATUS_ITEM3_VALUE = "Hết hiệu lực"; // trang thai het hieu luc

        public const string COMBOBOX_STATUS_ITEM1_NAME = "-1"; // trang thai tat ca
        public const string COMBOBOX_STATUS_ITEM2_NAME = "1"; // trang thai hieu luc
        public const string COMBOBOX_STATUS_ITEM3_NAME = "0"; // trang thai het hieu luc

        //App param
        public const string APP_PARAM_NAME = "APP_PARAM_NAME"; //Ten session AppParam
        public const int APP_TYPE_UNIT = 1; //Đơn vị tính UNIT_ID
        public const int APP_TYPE_VALUE_TYPE = 3; //Kiểu dữ liệu VALUE_TYPE
        public const int APP_TYPE_INPUT_TYPE = 4; //Nhập liệu INPUT_TYPE

        //Name of AppType
        public const string APP_TYPE_NAME_UNIT = "UNIT_ID"; //Đơn vị tính UNIT_ID
        public const string APP_TYPE_NAME_VALUE_TYPE = "VALUE_TYPE"; //Kiểu dữ liệu VALUE_TYPE
        public const string APP_TYPE_NAME_INPUT_TYPE = "INPUT_TYPE"; //Nhập liệu INPUT_TYPE

        //Period
        public const string PERIOD_NAME = "PERIOD_NAME"; //Ten viewdata PERIOD_NAME
        public const string DEPT_NAME = "DEPT_NAME";
        public const string PERIOD_ID = "PERIOD_ID"; //Ten viewdata PERIOD_ID

        //District
        public const string MA_QUANHUYEN = "MA_QUANHUYEN";
        public const string TEN_QUANHUYEN = "TEN_QUANHUYEN";


        //Dia diem thi
        public const string DM_DIADIEMTHI_ID = "DM_DIADIEMTHI_ID";
        public const string TEN_DIADIEMTHI = "TEN_DIADIEMTHI";

        //Commune
        public const string MA_XAPHUONG = "MA_XAPHUONG";
        public const string TEN_XAPHUONG = "TEN_XAPHUONG";

        //NhomUT
        public const string MA_NHOMUT = "MA_NHOMUT";
        public const string TEN_NHOMUT = "TEN_NHOMUT";

        //Đơn vị quản lý
        public const string MA_DVQL = "MA_DVQL";
        public const string TEN_DVQL = "TEN_DVQL";

        //Province
        public const string MA_TINH = "MA_TINH";
        public const string TEN_TINH = "TEN_TINH";

        //Đối tượng ưu tiên
        public const string MA_DTUT = "MA_DTUT";
        public const string TEN_DTUT = "TEN_DTUT";

        //Template
        public const string TEMPLATE_ID = "TEMPLATE_ID"; //Ten viewdata TEMPLATE_ID
        public const string IS_LOAD_ALL = "IS_LOAD_ALL"; //Ten viewdata IS_LOAD_ALL

        //Data version detail 
        public const string DATA_VERSION_ID = "DATA_VERSION_ID"; //Ten viewdata DATA_VERSION_ID
        public const string DATA_VERSION_STATUS = "DATA_VERSION_STATUS"; //Ten viewdata trang thai DATA_VERSION
        public const string TEMPLATE_TREE = "TEMPLATE_TREE"; //Ten viewdata cay bieu mau
        public const string TEMPLATE_DETAIL = "TEMPLATE_DETAIL"; //Ten  viewdata chi tiet bieu mau
        public const string MAPPING_INDICATOR = "MAPPING_INDICATOR"; //MappingObject viewdata dung de luu du lieu mapping indicator va gia tri
        public const string VISIBLE_BUTTON = "VISIBLE_BUTTON";//dung de an hien cac button luu thanh ban khac, chot phien ban, xuat excel

        //Xem du lieu
        public const string Viewdata = "Xem dữ liệu";
        public const string STATUS_NOTSEND = "Chưa gửi dữ liệu";
        public const string STATUS_SEND = "Đã gửi dữ liệu";
        public const string STATUS_APROVED = "Đã chốt dữ liệu";
        public const string STATUS_REFUSE = "Yêu cầu tổng hợp lại";

        //Reporting Template
        public const string REPORTING_TEMPLATE_TREE_AREA = "REPORTING_TEMPLATE_TREE_AREA"; // Ten du lieu tree table khu vuc
        public const string REPORTING_TEMPLATE_GRID_PERIOD = "REPORTING_TEMPLATE_GRID_PERIOD"; // Ten du lieu gridview dot
        public const string REPORTING_TEMPLATE_ERROR_ATTACH = "REPORTING_TEMPLATE_ERROR_ATTACH"; // Ten du lieu file loi load len gridview
        public const string REPORTING_TEMPLATE_TEMPLATE_NAME = "REPORTING_TEMPLATE_TEMPLATE_NAME"; // Ten du lieu report template load combobox

        //SUPERVISING_DEPT
        public const string EMULATION_FLAG_VALUE1 = "1";
        public const string EMULATION_FLAG_VALUE2 = "2";
        public const string EMULATION_FLAG_NAME1 = "Chính phủ";
        public const string EMULATION_FLAG_NAME2 = "Bộ GD-ĐT";


        //GLOBAL Message
        public const string LIST_AREA_1 = "LOGIN";
        public const string LIST_AREA_2 = "HOME";
        public const string LIST_AREA_3 = "LOGIN/HOME";

        public const string LIST_AREA_VALUE_1 = "1";
        public const string LIST_AREA_VALUE_2 = "2";
        public const string LIST_AREA_VALUE_3 = "3";

        //dung cho ghi log
        public const string WILD_LOG = "*#*";
        public const string ACTION_AUDIT_OLD_JSON = "old json object";
        public const string ACTION_AUDIT_NEW_JSON = "new json object";
        public const string ACTION_AUDIT_OBJECTID = "id cua data modify";
        public const string ACTION_AUDIT_DESCRIPTION = "description";


        //So thang trong nam
        public const int THANG = 12;


        public const string FORGET_PASS_CAPTCHA = "forgetpassword";
        public const string MESSAGE_CONFIMATION_EMPTY = "Bạn chưa nhập mã xác nhận";
        public const string MESSAGE_CONFIMATION_NOT_CORRECT = "Mã xác nhận không đúng";
        public const string MESSAGE_DUPLICATE_EMAIL = "Email đã tồn tại!";
        public const string MESSAGE_CREATE_NEW_USER = "Tạo mới người dùng thành công!";
        public const string MESSAGE_UPDATE_USER = "Sửa người dùng thành công!";
        public const string MESSAGE_EMAIL_NOT_EXIST = "Email không tồn tại!";

        public const string HUONG_DAN_PDF = "~/TemplateFile/HUONG_DAN_SU_DUNG0408.pdf";

        public const string NAME_HUONG_DAN_PDF = "HUONG_DAN_SU_DUNG.pdf";
        public const string BO_Y_TE = "Bộ Y Tế Dự Phòng";
        public const string CUC_Y_TE = "Cục Y Tế Dự Phòng";
    }
    public class CAPTINH_HUYEN_XA
    {
        /// <summary>
        /// CẤP TỈNH
        /// </summary>
        public const int CAPTINH = 1;
        /// <summary>
        /// CẤP HUYỆN
        /// </summary>
        public const int CAPHUYEN = 2;
        /// <summary>
        /// CẤP XÃ
        /// </summary>
        public const int CAPXA = 3;
    }
    public class MENU_POSITON
    {
        public const int LEFT = 0;

        public const int RIGHT = 1;
        public const int TOP = 2;
        public const int BOTTOM = 3;
    }
}