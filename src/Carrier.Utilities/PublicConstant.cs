﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrier.Utilities
{
    public class PublicConstant
    {
        public const float DEFAULT_DISTANCE_SEARCH = 100;

        //trai thái lich trinh
        public const int ORDER_PENDING = 0;          // Hệ thống push cho user nhưng chưa có phản hồi lại
        public const int ORDER_ACCEPT = 1;           // Tài xế đồng ý nhận đơn
        public const int ORDER_CANCEL_BY_DRIVER = 2; // Tài xế hủy đơn hàng
        public const int ORDER_CANCEL_BY_OWNER = 3;  // Chủ đơn hàng hủy đơn hàng
        public const int ORDER_CARRYING = 4;         // Đang vận chuyển hàng
        public const int ORDER_FINISHED = 5;         // Kết thúc đơn hàng
        public const int ORDER_CANNOT_SEND = 6;         // Gửi lỗi
        public const int ORDER_EXPIRED = 7;         // Hết hạn
        public const int ORDER_SEND_SUCCESS = 8;         // Gửi cho tài xế thành công

        // trạn thai

        public const int STATUS_PENDING = 0;
        public const int STATUS_ACTIVE = 1;
        public const int STATUS_DEACTIVE = 2;
        public const int STATUS_DELETE = 3;

        // Trnag thai Tai Xe
        public const int DRIVER_STATUS_PENDING = 0; //Dang cho nhan don
        public const int DRIVER_STATUS_ACTIVE = 1;

        public const int DRIVER_ON = 1; //Dang cho nhan don
        public const int DRIVER_OFF = 0;

        // Da khop lenh
        public const int STATUS_ACCEPT = 4;

        // Role constant
        public const string ROLE_SUPPER_ADMIN = "SupperAdmin";
        public const string ROLE_ADMIN = "Admin";
        public const string ROLE_USERS = "Users";
        public const string ROLE_ENTERPRISE = "Enterprise";
        public const string ROLE_FORWARDER = "Forwarder";
        public const string ROLE_DRIVERS = "Drivers";

        public const int NHAXE_BOCHANG_DIEMDEN = 4;
        public const int NHAXE_BOCHANG_DIEMDI = 3;
        public const int KHACH_BOCHANG_DIEMDI = 1;
        public const int KHACH_BOCHANG_DIEMDEN = 2;

        public const int CODE_VANDON = 1;
        public const int CODE_LICHTRINH = 2;

        public const int CAR_TYPE_CONTAINER = 1;
        public const int CAR_TYPE_CHUYENDUNG = 2;
        public const int CAR_TYPE_XETAI = 3;

        public const string DONVI_SOLUONG_THUNG = "Thùng";
        public const string DONVI_SOLUONG_BAO = "Bao";
        public const string DONVI_SOLUONG_HOP = "Hộp";
        public const string DONVI_SOLUONG_KIEN = "Kiện hàng";

        public const string DONVI_KHOILUONG_TAN = "Tấn";
        public const string DONVI_KHOILUONG_TA = "Tạ";
        public const string DONVI_KHOILUONG_YEN = "Yến";
        public const string DONVI_KHOILUONG_KG = "Kg";

        public const string DONVI_KHOILUONG_THETICH = "m3";
        public const string DONVI_KHOILUONG_DIENTICH = "m2";

        // Tiền mặt
        public const int PAYMENT_TYPE_DEFAULT = 0;
        // thẻ visa
        public const int PAYMENT_TYPE_VISACARD = 1;
        // Chuyển khoản ngân hàng nội địa
        public const int PAYMENT_TYPE_BANKING = 2;

        public const string FORMAT_DATEVN = "dd/MM/yyyy";

        public const int MATCH_EXPECT = 0;
        public const int MATCH_DIADIEM = 1;
        public const int MATCH_XE_TAITRONG = 2;
        public const int MATCH_THOIGIAN = 3;
        public const int MATCH_VITRI = 4;

        // Loai Xe Tai
        public const float LOAIXE15T = 1.5F;
        public const float LOAIXE25T = 2.5F;
        public const float LOAIXE35T = 3.5F;
        public const float LOAIXE5T = 5;
        public const float LOAIXE8T = 8;
        public const float LOAIXE10T = 10;
        public const float LOAIXE3CHAN = 16;
        public const float LOAIXE4CHAN = 19;

        public const string IMAGE_PAHT_TEMP = "C:/Users/Data/CarrierMedia/TempImage";
        public const string PRODUCT_THUMBNAILS = "C:/Users/Data/CarrierMedia/Products/Thumbnails";
        public const string PRODUCT_ORIGINALS = "C:/Users/Data/CarrierMedia/Products/Originals";

        public const string USER_ORIGINALS = "C:/Users/Data/CarrierMedia/Users/Originals";
        public const string USER_THUMBNAILS = "C:/Users/Data/CarrierMedia/Users/Thumbnails";
        public const string ARTICLE_ORIGINALS = "C:/Users/Data/CarrierMedia/Articles/Originals";
        public const string ARTICLE_THUMBNAILS = "C:/Users/Data/CarrierMedia/Articles/Thumbnails";
        public const string BANNER_ORIGINALS = "C:/Users/Data/CarrierMedia/Banners/Originals";
        public const string BANNER_THUMBNAILS = "C:/Users/Data/CarrierMedia/Banners/Thumbnails";
        public const string PROVIDER_ORIGINALS = "C:/Users/Data/CarrierMedia/Providers/Originals";
        public const string PROVIDER_THUMBNAILS = "C:/Users/Data/CarrierMedia/Providers/Thumbnails";
        public const string PROVIDER_IMAGES = "C:/Users/Data/CarrierMedia/Providers";
        public const string DRIVER_IMAGES = "C:/Users/Data/CarrierMedia/Drivers";
        public const string CAR_IMAGES = "C:/Users/Data/CarrierMedia/Cars";

        public const string GCM_KEY = "AAAAWr1db04:APA91bH_jYeA_hNNorWhdzY15Mf4I_4hGjgHj2isjnaWpOJ-pPl31TT2GT8hKNRo0Bt7_ATsYyc7g_th9XtyrCiL4Ba_B1qSid0E8On1uwVV-GWsYiNfXRP8DyJ4YnDWtR-zG2zulJ9B";
        public const string GCM_KEY_LACE = "AIzaSyDJgOQhr7dzyIpLknFHuEnf7QEt5I_aaCk";
        public const string GCM_SENDER = "389724073806";
        public const string IOS_CERTIFICATE_PATH = "C:\\Users\\Data\\CarrierMedia\\PushDevDriver.p12";
        public const string IOS_CERTIFICATE_PATH_PRODUCT = "C:\\Users\\Data\\CarrierMedia\\PushProductDriver.p12";

        public const int WAYPOINTS = 5;

        // trang thái rút tiền
        public const int ChuaChuyen = 0;
        public const int DaChuyen = 1;
        public const int KhongChuyenDuoc = 2;

        // type rút tiền hay cộng tiền vào tài khoản
        public const int RutTien = 0;
        public const int CongTienAdmin = 1;
    }
}
