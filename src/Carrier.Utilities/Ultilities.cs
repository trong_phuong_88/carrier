﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
namespace Carrier.Utilities
{
    public static class Ultilities
    {
        public static string CurrentDate()
        {
            return DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            //return string.Format("{dd/MM/yyyy}", DateTime.Now);
        }
        public static string ConvertToCurrency(double number)
        {
            // number.ToString("N"); //12.345.00
            //return string.Format("{0:N}", number); // 12,345.00
            return (number.ToString("#,###"));
        }
        /// <summary>
        /// Mã lịch trình: giờ đi đến-ngày tháng năm 2 ký tự đầu tên xe lộ trình
        /// </summary>
        /// <param name="fromHour"></param>
        /// <param name="toHour"></param>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="preName"></param>
        /// <returns></returns>
        public static string GenerateOrderCode(string fromDateTime,string userName)
        {
            return string.Format("{0}-{1}", fromDateTime.Replace(":", string.Empty).Replace("/", string.Empty), userName.ToUpper());
        }

        public static string RandomNumber(int size)
        {
            StringBuilder builder = new StringBuilder();

            for (int index = 0; index < size; index++)
            {
                var number = random.NextDouble().ToString("0.000000000000").Substring(2, 1);
                builder.Append(number);
            }
            return builder.ToString();
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public static string GenerateSheduleCode(string fromHour, string fromDate, string preName)
        {
            return string.Format("{0}-{1}-{2}", fromHour.Replace(":", string.Empty), fromDate.Substring(0, 5).Replace("/", string.Empty), preName.ToUpper());
        }
        public static Random random = new Random((int)DateTime.Now.Ticks);
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static string RandomStrings(DateTime date, int coso_id)
        {
            string format = "{0}{1}{2}{3}{4}{5}";
            return string.Format(format, date.Year.ToString().Substring(2, 2), date.Month.ToString("00"), date.Day.ToString("00"), date.Hour.ToString("00"), date.Minute.ToString("00"), coso_id.ToString("0000"));
        }
        public static DateTime? ToDataTime(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                var date = obj.Split('/');
                if (date != null)
                {
                    if (!string.IsNullOrEmpty(date[0]) && !string.IsNullOrEmpty(date[1]) && !string.IsNullOrEmpty(date[2]))
                    {
                        var day = int.Parse(date[0]).ToString("00");
                        var month = int.Parse(date[1]).ToString("00");
                        var year = int.Parse(date[2]).ToString("0000");
                        return DateTime.ParseExact(string.Format("{0}/{1}/{2}", day, month, year), "dd/MM/yyyy", null);
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }
        public static DateTime? ToDateTime(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                var date = obj.Split('/');
                if (date != null)
                {
                    if (!string.IsNullOrEmpty(date[0]) && !string.IsNullOrEmpty(date[1]) && !string.IsNullOrEmpty(date[2]))
                    {
                        var day = int.Parse(date[0]).ToString("00");
                        var month = int.Parse(date[1]).ToString("00");
                        var year = int.Parse(date[2]).ToString("0000");
                        return DateTime.ParseExact(string.Format("{0}/{1}/{2}", day, month, year), "dd/MM/yyyy", null);
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }
        public static short? ToShortOrNULL(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                return short.Parse(obj);
            }
            else
            {
                return null;
            }
        }

        public static int? ToIntOrNULL(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                return int.Parse(obj);
            }
            else
            {
                return null;
            }
        }
        public static long? ToLongOrNULL(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                return long.Parse(obj);
            }
            else
            {
                return null;
            }
        }
        public static short ToShortOrZero(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                return short.Parse(obj);
            }
            else
            {
                return 0;
            }
        }

        public static int ToIntOrZero(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                return int.Parse(obj);
            }
            else
            {
                return 0;
            }
        }
        public static long ToLongOrZero(this string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                return long.Parse(obj);
            }
            else
            {
                return 0;
            }
        }
        public static List<long> ToListLong(this string obj, char split_key)
        {
            List<long> listLong = new List<long>();
            if (!string.IsNullOrEmpty(obj))
            {
                var list = obj.Split(split_key);
                if (list != null)
                {
                    foreach (var item in list)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            listLong.Add(long.Parse(item));
                        }
                    }
                }
            }
            return listLong;
        }
        public static List<int> ToListInt(this string obj, char split_key)
        {
            List<int> listInt = new List<int>();
            if (!string.IsNullOrEmpty(obj))
            {
                var list = obj.Split(split_key);
                if (list != null)
                {
                    foreach (var item in list)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            listInt.Add(int.Parse(item));
                        }
                    }
                }
            }
            return listInt;
        }

        public static List<short> ToListShort(this string obj, char split_key)
        {
            List<short> listInt = new List<short>();
            if (!string.IsNullOrEmpty(obj))
            {
                var list = obj.Split(split_key);
                if (list != null)
                {
                    foreach (var item in list)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            listInt.Add(short.Parse(item));
                        }
                    }
                }
            }
            return listInt;
        }

        public static string CollapseSpaces(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return Regex.Replace(value, @"\s+", " ");
            }
            return string.Empty;
        }

        public static string HtmlEncoder(this string value)
        {
            StringBuilder sb = new StringBuilder(HttpUtility.HtmlEncode(value));
            // Selectively allow  and <i>
            sb.Replace("&lt;b&gt;", "<b>");
            sb.Replace("&lt;/b&gt;", "");
            sb.Replace("&lt;i&gt;", "<i>");
            sb.Replace("&lt;/i&gt;", "");
            return sb.ToString();
        }

        public static string HtmlDecoder(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return HttpUtility.HtmlDecode(value);
                String myEncodedString;
                // Encode the string.
                myEncodedString = HttpUtility.HtmlEncode(value);

                StringWriter myWriter = new StringWriter();
                // Decode the encoded string.
                HttpUtility.HtmlDecode(myEncodedString, myWriter);
                return myWriter.ToString();
                //return HttpUtility.HtmlDecode(value);
            }
            return value;
        }
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static DateTime FirstDateOfWeek(int year, int weekOfYear, System.Globalization.CultureInfo ci)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int)ci.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;
            DateTime firstWeekDay = jan1.AddDays(daysOffset);
            int firstWeek = ci.Calendar.GetWeekOfYear(jan1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if (firstWeek <= 1 || firstWeek > 50)
            {
                weekOfYear -= 1;
            }
            return firstWeekDay.AddDays(weekOfYear * 7);
        }
        public static DateTime GetStartOfWeek(int year, int month, int weekofmonth)
        {
            //lấy ngày bắt đầu của tuần trong tháng
            var day = weekofmonth * 7 - 6;
            var StartDate = new DateTime(year, month, day);
            var weekOfYear = GetIso8601WeekOfYear(StartDate);
            return FirstDateOfWeek(year, weekOfYear, CultureInfo.CurrentCulture);
        }

        public static DateTime GetEndOfWeek(DateTime startOfWeek)
        {
            return startOfWeek.AddDays(6);
        }

        public static List<WeekMapDate> GetWeeks(int iYear, int iMonth)
        {
            //first
            int countDays = DateTime.DaysInMonth(iYear, iMonth);
            List<WeekMapDate> arrWeeks = new List<WeekMapDate>();
            var j = 0;
            for (int i = 1; i <= countDays; i = i + 7)
            {
                j++;
                WeekMapDate mapdate = new WeekMapDate();
                mapdate.Week = j;
                mapdate.StartDate = new DateTime(iYear, iMonth, i);
                mapdate.EndDate = mapdate.StartDate.AddDays(7);
                arrWeeks.Add(mapdate);
            }
            //zero-based array
            return arrWeeks;
        }
        private static double DaysInYear(this int iYear)
        {
            var startDate = new DateTime(iYear, 1, 1);
            var enddate = new DateTime(iYear, 12, 31);
            var totalDate = enddate - startDate;
            return totalDate.TotalDays;
        }

        /// <summary>
        /// Trả ra số tuần trong 1 năm
        /// </summary>
        /// <param name="iYear">Năm cần tính số tuần</param>
        /// <returns></returns>
        public static List<int> ListWeekOfYear(int iYear)
        {
            //lấy tổng số ngày trong năm
            double countDays = iYear.DaysInYear();
            List<int> arrWeeks = new List<int>();
            var j = 0;
            for (int i = 1; i <= countDays; i = i + 7)
            {
                j++;
                arrWeeks.Add(j);
            }
            //trả ra danh sách tuần theo năm
            return arrWeeks;
        }


        /// <summary>
        /// Trả ra ngày bắt đầu và ngày kết thúc của tuần theo năm
        /// </summary>
        /// <param name="iWeek">Tuần</param>
        /// <param name="iYear">Năm</param>
        /// <returns></returns>
        public static WeekMapDate GetStartAndEndOfWeek(int iWeek, int iYear)
        {
            DateTime startOfYear = new DateTime(iYear, 1, 1);
            WeekMapDate mapdate = new WeekMapDate();
            mapdate.Week = iWeek;
            mapdate.StartDate = startOfYear.AddDays((iWeek - 1) * 7);
            mapdate.EndDate = mapdate.StartDate.AddDays(7);
            return mapdate;
        }


        static readonly string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ", "DA", "DB", "DC", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DM", "DN", "DO", "DP", "DQ", "DR", "DS", "DT", "DU", "DV", "DW", "DX", "DY", "DZ" };
        public static string ToIndexToColumn(this int index)
        {
            if (index <= 0)
                throw new IndexOutOfRangeException("index must be a positive number");

            return Columns[index - 1];
        }

        public static string Truncate(string input, int length)
        {
            if (input.Length <= length)
            {
                return input;
            }
            else
            {
                return input.Substring(0, length) + "...";
            }
        }

        #region Built full text search
        public static string ConvertToFTS(this string s)
        {
            try
            {
                var fulltext = HtmlUtilities.ConvertToPlainText(s);
                return fulltext.ConvertToVN();
            }
            catch
            {
                return string.Empty;
            }

        }
        public static string ConvertToVN(this string chucodau)
        {
            const string FindText = "áàảãạâấầẩẫậăắằẳẵặđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶĐÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴ";
            const string ReplText = "aaaaaaaaaaaaaaaaadeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAADEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYY";
            int index = -1;
            char[] arrChar = FindText.ToCharArray();
            while ((index = chucodau.IndexOfAny(arrChar)) != -1)
            {
                int index2 = FindText.IndexOf(chucodau[index]);
                chucodau = chucodau.Replace(chucodau[index], ReplText[index2]);
            }
            return chucodau;
        }
        #endregion End Built full text search

        public static int unixTimestamp(DateTime dt)
        {
            int unixTimeStamp;
            DateTime zuluTime = dt.ToUniversalTime();
            DateTime unixEpoch = new DateTime(1970, 1, 1);
            unixTimeStamp = (int)(zuluTime.Subtract(unixEpoch)).TotalSeconds;
            return unixTimeStamp;
        }
        public static void SaveTextToFile(string strData, string FullPath)
        {
            try
            {
                if (File.Exists(FullPath))
                {
                    System.IO.File.AppendAllText(FullPath, DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " " + strData + Environment.NewLine);
                }
                else
                {
                    System.IO.File.Create(FullPath).Close();
                    System.IO.File.AppendAllText(FullPath, DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") + " " + strData + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}