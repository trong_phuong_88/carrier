﻿using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushSharp.Google;
using PushSharp.Core;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Carrier.PushNotification
{
    public delegate void PushCompletedCallBack(string result, string obj1, string obj2);

    public class PushServices
    {
        public static GcmConfiguration gcmConfig;
        public static ApnsConfiguration apnConfig;
        public static GcmServiceBroker gcmBroker;
        public static ApnsServiceBroker apnsBroker;

        public static string strFileP12;
        public static string GcmSenderId;
        public static string GcmKey;

        public static void SetupPushAPN(bool isPr)
        {
            // Initialize
            if (isPr)
            {
                apnConfig = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, strFileP12, "123456");
                apnsBroker = new ApnsServiceBroker(apnConfig);
            }
            else
            {
                apnConfig = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, strFileP12, string.Empty);
                apnsBroker = new ApnsServiceBroker(apnConfig);
            }
        }
        public static void SetupPushGCM()
        {
            // Configuration
            gcmConfig = new GcmConfiguration(GcmSenderId, GcmKey, null);
            gcmConfig.OverrideUrl("https://fcm.googleapis.com/fcm/send");
            //// Create a new broker
            gcmBroker = new GcmServiceBroker(gcmConfig);
        }
        public static void PushAPNMessage(string jsonMessage, List<string> lstDevice, string obj1, string obj2, PushCompletedCallBack callback)
        {
            // Wire up events
            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
            {
                aggregateEx.Handle(ex =>
                {
                    // See what kind of exception it was to further diagnose
                    if (ex is ApnsNotificationException)
                    {
                        var notificationException = (ApnsNotificationException)ex;
                        // Deal with the failed notification
                        var apnsNotification = notificationException.Notification;
                        var statusCode = notificationException.ErrorStatusCode;
                        callback("Failure iOS 1", obj1, obj2);
                        Console.WriteLine($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}");
                    }
                    else
                    {
                        callback("Failure iOS 2", obj1, obj2);
                        // Inner exception might hold more useful information like an ApnsConnectionException			
                        Console.WriteLine($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                    }

                    // Mark it as handled
                    return true;
                });
            };

            apnsBroker.OnNotificationSucceeded += (notification) =>
            {
                callback("Success", obj1, obj2);
                Console.WriteLine("Apple Notification Sent!");
            };

            // Start the broker
            apnsBroker.Start();

            foreach (var deviceToken in lstDevice)
            {
                // Queue a notification to send
                apnsBroker.QueueNotification(new ApnsNotification
                {
                    DeviceToken = deviceToken,
                    //"{\"aps\":{\"badge\":7}}"
                    Payload = JObject.Parse(jsonMessage)
                });
            }

            // Stop the broker, wait for it to finish   
            // This isn't done after every message, but after you're
            // done with the broker
            apnsBroker.Stop();
        }
        public static void PushGCMMessage(string jsonMessage, List<string> lstDevice, string obj1, string obj2, PushCompletedCallBack callback)
        {
            // Wire up events
            gcmBroker.OnNotificationFailed += (notification, aggregateEx) =>
            {
                aggregateEx.Handle(ex =>
                {

                    // See what kind of exception it was to further diagnose
                    if (ex is GcmNotificationException)
                    {
                        var notificationException = (GcmNotificationException)ex;

                        // Deal with the failed notification
                        var gcmNotification = notificationException.Notification;
                        var description = notificationException.Description;
                        callback("Failure Android 1", obj1, obj2);

                        Console.WriteLine($"GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                    }
                    else if (ex is GcmMulticastResultException)
                    {
                        var multicastException = (GcmMulticastResultException)ex;

                        foreach (var succeededNotification in multicastException.Succeeded)
                        {
                            Console.WriteLine($"GCM Notification Succeeded: ID={succeededNotification.MessageId}");
                        }

                        foreach (var failedKvp in multicastException.Failed)
                        {
                            var n = failedKvp.Key;
                            var e = failedKvp.Value;

                            Console.WriteLine($"GCM Notification Failed: ID={n.MessageId}, Desc={e.InnerException.Message}");
                        }
                        callback("Failure Android 2", obj1, obj2);
                    }
                    else if (ex is DeviceSubscriptionExpiredException)
                    {
                        var expiredException = (DeviceSubscriptionExpiredException)ex;

                        var oldId = expiredException.OldSubscriptionId;
                        var newId = expiredException.NewSubscriptionId;

                        Console.WriteLine($"Device RegistrationId Expired: {oldId}");

                        if (!string.IsNullOrWhiteSpace(newId))
                        {
                            // If this value isn't null, our subscription changed and we should update our database
                            callback("Failure Android 3", obj1, obj2);

                            Console.WriteLine($"Device RegistrationId Changed To: {newId}");
                        }
                        else
                        {
                            callback("Failure Android 4", obj1, obj2);
                        }
                    }
                    else if (ex is RetryAfterException)
                    {
                        var retryException = (RetryAfterException)ex;
                        // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                        callback("Failure Android 5", obj1, obj2);

                        Console.WriteLine($"GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                    }
                    else
                    {
                        callback("Failure Android 6", obj1, obj2);

                        Console.WriteLine("GCM Notification Failed for some unknown reason");
                    }

                    // Mark it as handled
                    return true;
                });
            };

            gcmBroker.OnNotificationSucceeded += (notification) =>
            {
                callback("Success", obj1, obj2);
                Console.WriteLine("GCM Notification Sent!");
            };

            // Start the broker
            gcmBroker.Start();

            foreach (var regId in lstDevice)
            {
                // Queue a notification to send
                gcmBroker.QueueNotification(new GcmNotification
                {
                    RegistrationIds = new List<string> {
            regId
        },
                    //"{ \"somekey\" : \"somevalue\" }"
                    Data = JObject.Parse(jsonMessage)
                });
            }

            // Stop the broker, wait for it to finish   
            // This isn't done after every message, but after you're
            // done with the broker
            gcmBroker.Stop();
        }

        public static void SendNotificationFromFirebaseCloud(string jsonMessage, List<string> lstDevice)
        {
            try
            {
                foreach (string str in lstDevice)
                {
                    SendMessage(jsonMessage, str);
                }
            }
            catch (Exception ex)
            {
                //  Response.Write(ex.Message);
            }
        }

        public static void SendMessage(string message, string deviceToken)
        {
            string serverKey = "AAAAWr1db04:APA91bH_jYeA_hNNorWhdzY15Mf4I_4hGjgHj2isjnaWpOJ-pPl31TT2GT8hKNRo0Bt7_ATsYyc7g_th9XtyrCiL4Ba_B1qSid0E8On1uwVV-GWsYiNfXRP8DyJ4YnDWtR-zG2zulJ9B";
            string legacy_server_key = "AIzaSyDJgOQhr7dzyIpLknFHuEnf7QEt5I_aaCk";
            try
            {
                //var result = "-1";
                var webAddr = "https://fcm.googleapis.com/fcm/send";
                HttpRequestMessage request = new HttpRequestMessage();

                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var str = "key=" + legacy_server_key;
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", str);
                    var notification = new
                    Dictionary<string, string>{
                        { "body" , message}, { "title", "CARRIER"}
                    };
                    var json_noti = JsonConvert.SerializeObject(notification);
                    var param = new Dictionary<string, object> { { "to", deviceToken }, { "notification", notification } };
                    string json = JsonConvert.SerializeObject(param);
                    var body = new StringContent(json, Encoding.UTF8, "application/json");
                    var result = client.PostAsync(new Uri(webAddr), body).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var response = result.Content.ReadAsStringAsync().Result;
                    }
                    else
                    {
                    }
                }
                // return result;
            }
            catch (Exception ex)
            {
                //  Response.Write(ex.Message);
            }
        }
    }

}
