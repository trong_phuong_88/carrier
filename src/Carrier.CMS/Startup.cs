//[assembly: Microsoft.Owin.OwinStartup("",typeof(CMS.Carrier.Startup))]
using Microsoft.Owin;

[assembly: OwinStartupAttribute(typeof(Carrier.CMS.Startup))]

namespace Carrier.CMS
{
    using System.Configuration;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;

    using Owin;
    public partial class Startup
    {
        public void Configuration(IAppBuilder builder)
        {
            builder.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                AuthenticationMode = AuthenticationMode.Active
            });
            ConfigureAuth(builder);

            builder.MapSignalR();
        }     
    }
}
