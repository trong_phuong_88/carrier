﻿using Carrier.CMS.Hubs;
using Carrier.Models.Entities;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CMS.Carrier
{
    public class NotificationComponents
    {
        public void RegisterNotification(DateTime currentTime)
        {
            string conString = ConfigurationManager.ConnectionStrings["Carrier3Entities"].ToString();
            string command = "select * From Message";
            using (SqlConnection con = new SqlConnection(conString))
            {
                SqlCommand cmd = new SqlCommand(command, con);
                cmd.Parameters.AddWithValue("", currentTime);
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                cmd.Notification = null;

                SqlDependency slqDep = new SqlDependency();
                slqDep.OnChange += SlqDep_OnChange;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {

                }
            }
        }

        private void SlqDep_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                SqlDependency dep = new SqlDependency();
                dep.OnChange -= SlqDep_OnChange;

                var notificationHub = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                notificationHub.Clients.All.notify("add");
                RegisterNotification(DateTime.Now);

            }
        }

        public List<Messages> GetNitification(DateTime afterDate)
        {
            using (Carrier3Entities db = new Carrier3Entities())
            {
                return db.Messages.Where(o => o.Created_At == afterDate.ToString()).ToList();
            }
        }
    }
}