﻿using Carrier.CMS.Models;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Carrier.CMS
{
    // Note: For instructions on enabling IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=301868
    public class MvcApplication : System.Web.HttpApplication
    {
        string con = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SqlDependency.Start(con);
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var loadbalancerReceivedSslRequest = string.Equals(Request.Headers["X-Forwarded-Proto"], "https");
            var serverReceivedSslRequest = Request.IsSecureConnection;

            if (loadbalancerReceivedSslRequest || serverReceivedSslRequest) return;

            UriBuilder uri = new UriBuilder(Context.Request.Url);
            if (!uri.Host.Equals("localhost"))
            {
                uri.Port = 443;
                uri.Scheme = "https";
                Response.Redirect(uri.ToString());
            }
            Context.Response.AppendHeader("Access-Control-Allow-Credentials", "true");
            var referrer = Request.UrlReferrer;
            if (Context.Request.Path.Contains("signalr/") && referrer != null)
            {
                Context.Response.AppendHeader("Access-Control-Allow-Origin", referrer.Scheme + "://" + referrer.Authority);
            }
        }
        protected void Application_End()
        {
            SqlDependency.Stop(con);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            string path = Request.AppRelativeCurrentExecutionFilePath;
            if (path.Contains("Enterprise"))
            {
                Exception exception = Server.GetLastError();
                Response.Clear();
                HttpException httpException = exception as HttpException;
                Response.TrySkipIisCustomErrors = true;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        Response.StatusCode = 404;
                        Response.Redirect("~/Enterprise/Error");
                        break;
                    case 500:
                    default:
                        Response.StatusCode = 500;
                        Response.Redirect("~/Enterprise/Error/Maintenance");
                        break;
                }
                Response.Redirect("~/Enterprise/Error/Maintenance");
                Server.ClearError();
            }
        }
    }
}
