namespace Carrier.CMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserInfoes", "Lat", c => c.Single(nullable: false));
            AddColumn("dbo.UserInfoes", "Lng", c => c.Single(nullable: false));
            DropColumn("dbo.UserInfoes", "Location");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserInfoes", "Location", c => c.Geography());
            DropColumn("dbo.UserInfoes", "Lng");
            DropColumn("dbo.UserInfoes", "Lat");
        }
    }
}
