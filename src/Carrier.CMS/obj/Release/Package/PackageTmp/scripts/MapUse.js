﻿var latlng;
var cur;
var loc;
var locid;
var price;
//map
var mcenter = new google.maps.LatLng(20.99684, 105.84426);
var map;
var marker;
var markerList = []; 
var markName;
var geocoder = new google.maps.Geocoder();


function initialize() {
    var mapProp = {
        center: mcenter,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    directionsDisplay.setMap(map);
    google.maps.event.addListener(map, 'click', function (event) {
        placeMarker(event.latLng);

    });

    function calPrice() {
        if (!isNaN((document).getElementById('Number').value) && !isNaN((document).getElementById('Weight').value)) {
            // console.log(true);
            var x = (document).getElementById('Number').value;
            var y = (document).getElementById('Weight').value;
             console.log(x+" "+y);
            // (document).getElementById('Price').value ;
             var w = $('#distance').text();
             console.log(w);
            var z;
            if (w <= 4) {
                z = (x * y) * w * 15000;
            } else {
                z = (x * y) * 60000 + (w - 4) * 12000 * (x * y);

            }
            (document).getElementById('Price').value = Math.round(z);
        }
    }

    //Buttons
   
    $(document).ready(function () {
        //--Maps
        $(document).on('click', '.maptog', function () {
            loc = $(this).parent().siblings('input')[0];
            latlng = $(this).siblings('div')[0];
            markName = $(this).siblings('div')[1].innerHTML;
            //console.log(latlng.innerHTML);
            locid = $(this).parent().siblings('input')[0].id;
            //console.log(locid);
            if (loc.value == null || loc.value == "") {
                //map.setCenter(initialLocation);
                //placeMarker(initialLocation);
            } else {
                if(latlng.innerHTML=="sadsa")
                    geocodeAddress(geocoder, map);
                else geocodeLatLng(geocoder,map,latlng.innerHTML)
            }
            $('#form').fadeOut(0);
            $('#googleMap').height('380px');
            $('#googleMap').width('350px');
            google.maps.event.trigger(map, 'resize');

            $('#locsel').show();

            //console.log(loc.value);
        })
        $('#locsel').click(function () {
            $('#googleMap').height('1px');
            $('#googleMap').width('1px');
            $('#form').fadeIn(1);
            $(this).hide();
            //console.log(latlng.innerHTMl);
            latlng.innerHTML = markerList[markName].getPosition().lat() + "," + markerList[markName].getPosition().lng();
            geocodeLatLng(geocoder, map, latlng.innerHTML);
            if (markerList[0] && markerList[1]) {
                //console.log(document.getElementById('stlatlng').innerHTML);
                //console.log(document.getElementById('endlatlng').innerHTML);
                var org = document.getElementById('stlatlng').innerHTML;
                var end = document.getElementById('endlatlng').innerHTML;
                displayRoute(org,end,directionsService, directionsDisplay);
            }

        })
        //select current location
        $(document).on('click', '.here', function () {
            loc = $(this).parent().siblings('input')[0];
            latlng = $(this).siblings('div')[0];
            markName = $(this).siblings('div')[1].innerHTML;
            //console.log(latlng.innerHTML);
            locid = $(this).parent().siblings('input')[0].id;
            //console.log(initialLocation.lat());
            //if (loc.value == null || loc.value == "") {
            //    //map.setCenter(initialLocation);
            //    //placeMarker(initialLocation);
            //} else {
            //    if (latlng.innerHTML == "sadsa")
            //        geocodeAddress(geocoder, map);
            //    else geocodeLatLng(geocoder, map, latlng.innerHTML)
            //}
            var c = initialLocation.lat() + "," + initialLocation.lng();
            latlng.innerHTML = c;
            geocodeLatLng(geocoder, map, c)
            geocodeLatLng(geocoder, map, latlng.innerHTML);
            placeMarker(initialLocation)
            if (markerList[0] && markerList[1]) {
                //console.log(document.getElementById('stlatlng').innerHTML);
                //console.log(document.getElementById('endlatlng').innerHTML);
                var org = document.getElementById('stlatlng').innerHTML;
                var end = document.getElementById('endlatlng').innerHTML;
                displayRoute(org, end, directionsService, directionsDisplay);
            }
            //console.log(loc.value);
        })
        //--Prices
        $('#Number, #Weight').on('change paste keyup', function () {

            //console.log((this).value)
            calPrice();
            if (!document.getElementById('distance').innerHTML)
            {
                
            }
            else {

            }
            //var y = (document).getElementById('Weight').value

        })

        
        //--form pre-submit
        $(document).on('click', '.mode', function () {
            $(this).siblings('input')[0].value=$(this).val();
        })
        $('#createform').submit(function () {
            if (document.getElementById('Start').value.length!=0 && (document.getElementById('To').value.length!=0 )){
                var x = document.getElementById('Start').value + ">" + document.getElementById('stlatlng').innerHTML;
                var y = document.getElementById('To').value + ">" + document.getElementById('endlatlng').innerHTML;
                //$('#Start').value.concat($('#stlatlng').innerHTML);
                console.log(x);
                document.getElementById('Start').value = x;
                document.getElementById('To').value = y;
            }   
            return true;
        })

    })
    //currenlocation
    if (navigator.geolocation) {
        browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function (position) {
            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            placeMarker(initialLocation);
            map.setCenter(initialLocation);

        }, function () {
            alert("Geolocation service failed.");
            initialLocation = new google.maps.LatLng(20.99684, 105.84426);
            placeMarker(initialLocation);
            map.setCenter(initialLocation);
        });
    }
    //Place marker
    function placeMarker(location) {
        if (markerList[markName]) {
            markerList[markName].setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                label: markName,
                map: map,
            });
            markerList.push(marker);
        }
        //document.getElementById('Start').value = location.lat() +"," + location.lng();
        //console.log(startloc);
    }
    //autolocation

    //geocoding
    function geocodeAddress(geocoder, resultsMap) {
        var address = loc.value;
        if (address != null || adress != "") {
            //console.log(address)
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    resultsMap.setCenter(results[0].geometry.location);
                    placeMarker(results[0].geometry.location);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

    }
    //Reverse geocoding
    function geocodeLatLng(geocoder, map, latlng) {
        //var input = document.getElementById('latlng').value;
        var latlngStr = latlng.split(',', 2);
        var pos = { lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1]) };
        geocoder.geocode({ 'location': pos }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    map.setZoom(11);
                    //var marker = new google.maps.Marker({
                    //    position: latlng,
                    //    map: map
                    //});
                    document.getElementById(locid).value = results[1].formatted_address;
                    //loctext = 
                    //console.log(loctext)

                    //infowindow.open(map, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
    //distance matrix
    function displayRoute(origin, destination, service, display) {
        console.log(origin+" "+destination)
        service.route({
            origin: origin,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                display.setDirections(response);
                computeTotalDistance(directionsDisplay.getDirections());
            } else {
                alert('Could not display directions due to: ' + status);
            }
        });
    }

    function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        //total = total / 1000;
        //console.log(total);

        document.getElementById('distance').innerHTML = "";
        document.getElementById('distance').innerHTML = total / 1000;
        //var temp = total / 1000;
        //var price;
        //if (temp <= 4.000) {
        //    price = 15000 * temp;
        //} else {
        //    price = Math.round((temp - 4.000) * 12000 + 60000);       
        //}
        ////console.log(price);
        //document.getElementById('Price').value = price;
        calPrice();
    }


}



google.maps.event.addDomListener(window, 'load', initialize);