﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Configuration;

namespace Carrier.CMS.Hubs
{
    public class NotificationHub: Hub
    {
        private static string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        [HubMethodName("sendMessages")]
        public static void SendMessages(string action)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

            // admin
            context.Clients.All.updateMessagesAccount(action);
            context.Clients.All.updateMessagesOrderTracking(action);

            // doanh nghiệp
            context.Clients.All.updateMessagesOrder(action);
            context.Clients.All.updateMessagesNotification(action);
            context.Clients.All.updateReportSOS(action);
            context.Clients.All.updatePoupMessOrderNew(action);
            context.Clients.All.updateMessListOrderNewFromFowarder(action);

            // Fowader
            context.Clients.All.updateMessagesOrderNewRun(action);
            context.Clients.All.updateMessagesOrderNewFinish(action);
            context.Clients.All.updateMessagesOrderNewExpired(action);
            //context.Clients.All.updateMessagesNotification(action);
        }
        //[HubMethodName("reportSOS")]
        //public static void reportSOS(string action)
        //{
        //    IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

        //    // doanh nghiệp
        //    context.Clients.All.updateReportSOS(action);

        //}
        [HubMethodName("updateMatchOrder")]
        public static void UpdateMatchOrder()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            context.Clients.All.updateOrderTracking();
        }
    }
}