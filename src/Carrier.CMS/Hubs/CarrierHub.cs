﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace CMS.Carrier.Hubs
{
    // Using Sigle-User group
    [HubName("carrierhub")]
    [Authorize]
    public class CarrierHub : Hub
    {
        static IHubContext context = GlobalHost.ConnectionManager.GetHubContext<CarrierHub>();
        public static void UpdateWallet(string userId)
        {
            context.Clients.Group(userId).updateWallet();
            context.Clients.Group(userId).updateWalletClient();

            context.Clients.Group(userId).updateCard();

            context.Clients.Group(userId).checkCardNew();
        }
        public static void CheckCardNew(string userId)
        {
            context.Clients.Group(userId).checkCardNew();
        }
    }

    // Using IUserID provider
    public interface IUserIdProvider
    {
        string GetUserId(IRequest request);
    }
}