﻿using Carrier.CMS.Common;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;
using Carrier.CMS;
using CMS.Carrier.Models;

namespace CMS.Carrier.Areas.User.Controllers
{
    [Roles(PublicConstant.ROLE_USERS)]
    public class UMessageController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();

        public UMessageController()
        {

        }

        public string _UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public UMessageController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Enterprise/EMessage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListMessageOrder()
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            var CurrentUsertId = GetCurrentOrganizatinId(User.Identity.GetUserId());
            DateTime dateCompare = DateTime.Now.AddDays(-1);
            result = (from o in db.Order
                      join t in db.OrderTracking on o.Id equals t.OrderId
                      join a in db.UserInfoes on o.Created_By equals a.ApplicationUserID
                      where a.OrganzationId == CurrentUsertId
                      && o.CreateAt > dateCompare
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = o.UserName,
                          OrganizationId = a.OrganzationId,
                          CreateAt = o.CreateAt,
                          Created_By = o.Created_By
                      }).OrderByDescending(o => o.Id).ToList();

            return View(result);
        }
        public async Task<ActionResult> ListMessageUser()
        {
            return View();
        }
        public ActionResult ListMessageNotification()
        {
            var notificationView = new MessageNotificationViewModel();
            var lstNotificationRecords = new List<MessageNotificationModel>();
            List<Messages> lstMessage = db.Messages.Where(o => o.ToUserId == User.Identity.GetUserId()).OrderByDescending(o => o.Id).ToList();
            MessageNotificationModel item;
            foreach (Messages message in lstMessage)
            {
                item = new MessageNotificationModel();
                item.Id = message.Id.ToString();
                item.Message = message.Message;
                item.FromUserId = message.FromUserId;
                item.ToUserId = message.ToUserId;
                item.Created_At = DateTime.Parse(message.Created_At);
                lstNotificationRecords.Add(item);
            }

            lstNotificationRecords = lstNotificationRecords.Where(o => o.ToUserId == User.Identity.GetUserId() && o.Created_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();

            notificationView.Quanlity = lstNotificationRecords.Count();
            notificationView.listMessageNotification = lstNotificationRecords.ToList();

            return View(notificationView);
        }
        public ActionResult ListMessageOrderTracking()
        {
            return View();
        }

        public int GetCurrentOrganizatinId(string userId)
        {
            int organizationId = 0;
            try
            {
                organizationId = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault().Id;
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}