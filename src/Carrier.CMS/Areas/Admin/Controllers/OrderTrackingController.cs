﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]

    public class OrderTrackingController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: OrderTracking
        public ActionResult Index()
        {
            //if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            //{
            //    //List<Order> listOrder = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(o=>o.order)

            //    string[] s = ("").Split(',').ToArray();
            //    List<Order> listOrder = _unitOfWork.GetRepositoryInstance<Order>().GetResultBySqlProcedure("", s).ToList();
            //}
            //else
            //{
            //    List<Order> listOrder = _unitOfWork.GetRepositoryInstance<Order>().GetAllRecords().ToList();
            //}
            return View();
        }

        public JsonResult SearchOrder()
        {
            List<OrderTracking> listOrder;
            try
            {
                string userName = Request.Form["UserName"];
                int orderId = Request.Form["OrderId"] != null ? int.Parse(Request.Form["OrderId"].ToString()) : 0;
                if (userName.Length > 0 && orderId > 0)
                {
                    listOrder = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetListByParameter(o => o.OrderId == orderId).ToList();
                }
                else
                {
                    if ( orderId > 0)
                    {
                        listOrder = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetListByParameter(o => o.OrderId == orderId).ToList();
                    }
                    else
                    {

                    }
                }

                string strListOrder = "['Xe đạp', 33.890542, 151.274856, 'HẠ Long'],['xe máy', 33.923036, 151.259052, 'address 2']";
                return Json(strListOrder, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}