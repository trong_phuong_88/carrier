﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.Repository;
using PagedList;
using Carrier.Utilities;
using Carrier.CMS.Common;
using System.Data.Entity.Validation;


namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class CarController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Enterprise/Driver
        public ActionResult Index(string currentFilter, string searchString, int? page, int? pagsiz)
        {
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                List<Organization> listOrganization = new List<Organization>();
                listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();
                IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listOrganizationSelect;
            }

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<Car> result = _unitOfWork.GetRepositoryInstance<Car>().GetAllRecords().Where(o => o.Created_By == User.Identity.Name).OrderByDescending(o => o.License).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                result = result.Where(o => o.License.Contains(searchString)).OrderByDescending(o => o.License).ToList();
            }

            int pageSize = (pagsiz ?? 5);
            ViewBag.PageSize = pageSize;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/Car/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // GET: Admin/Car/Create
        public ActionResult Create()
        {
            //if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            //{
            //    List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //    ViewBag.ListOrganization = listOrganization;
            //}

            return View();
        }

        // POST: Admin/Car/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            Car car = new Car();
            if (ModelState.IsValid)
            {
                try
                {
                    car.License = form.Get("License") != null ? form.Get("License") : "";
                    car.Width_Size = form.Get("txtWidth") != null ? double.Parse(form.Get("txtWidth").ToString()) : 0;
                    car.Height_Size = form.Get("txtHeight") != null ? double.Parse(form.Get("txtHeight").ToString()) : 0;
                    car.Leng_Size = form.Get("txtLenght") != null ? double.Parse(form.Get("txtLenght").ToString()) : 0;
                    var payload = form.Get("Payload") != null ? form.Get("Payload") : "";
                    if (payload.Contains("1.5"))
                    {
                        car.Payload = PublicConstant.LOAIXE15T;
                    }
                    else if (payload.Contains("2.5"))
                    {
                        car.Payload = PublicConstant.LOAIXE25T;
                    }
                    else if (payload.Contains("3.5"))
                    {
                        car.Payload = PublicConstant.LOAIXE35T;
                    }
                    else if (payload.Contains("5"))
                    {
                        car.Payload = PublicConstant.LOAIXE5T;
                    }
                    else if (payload.Contains("8"))
                    {
                        car.Payload = PublicConstant.LOAIXE8T;
                    }
                    else if (payload.Contains("10"))
                    {
                        car.Payload = PublicConstant.LOAIXE10T;
                    }
                    else if (payload.Contains("3"))
                    {
                        car.Payload = PublicConstant.LOAIXE3CHAN;
                    }
                    else if (payload.Contains("4"))
                    {
                        car.Payload = PublicConstant.LOAIXE4CHAN;
                    }
                    car.Description = form.Get("Description") != null ? form.Get("Description") : "";
                    car.Created_At = DateTime.Now;
                    car.Created_By = User.Identity.Name;
                    car.Updated_At = DateTime.Now;
                    car.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                    _unitOfWork.GetRepositoryInstance<Car>().Add(car);
                    _unitOfWork.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                return RedirectToAction("Index");
            }

            return View(car);
        }

        // GET: Admin/Car/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (car == null)
            {
                return HttpNotFound();
            }
            //List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //ViewBag.ListOrganization = listOrganization;
            return View(car);
        }

        // POST: Admin/Car/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (ModelState.IsValid)
            {
                car.License = form.Get("License") != null ? form.Get("License") : "";
                car.Width_Size = form.Get("txtWidth") != null ? double.Parse(form.Get("txtWidth").ToString()) : 0;
                car.Height_Size = form.Get("txtHeight") != null ? double.Parse(form.Get("txtHeight").ToString()) : 0;
                car.Leng_Size = form.Get("txtLenght") != null ? double.Parse(form.Get("txtLenght").ToString()) : 0;
                var payload = form.Get("Payload") != null ? form.Get("Payload") : "";
                //car.ContainerSize = form.Get("ContainerSize") != null ? form.Get("ContainerSize") : "";
                car.Description = form.Get("Description") != null ? form.Get("Description") : "";
                if (payload.Contains("1.5"))
                {
                    car.Payload = PublicConstant.LOAIXE15T;
                }
                else if (payload.Contains("2.5"))
                {
                    car.Payload = PublicConstant.LOAIXE25T;
                }
                else if (payload.Contains("3.5"))
                {
                    car.Payload = PublicConstant.LOAIXE35T;
                }
                else if (payload.Contains("5"))
                {
                    car.Payload = PublicConstant.LOAIXE5T;
                }
                else if (payload.Contains("8"))
                {
                    car.Payload = PublicConstant.LOAIXE8T;
                }
                else if (payload.Contains("10"))
                {
                    car.Payload = PublicConstant.LOAIXE10T;
                }
                else if (payload.Contains("3"))
                {
                    car.Payload = PublicConstant.LOAIXE3CHAN;
                }
                else if (payload.Contains("4"))
                {
                    car.Payload = PublicConstant.LOAIXE4CHAN;
                }
                car.Created_At = car.Created_At;
                car.Created_By = car.Created_By;
                car.Updated_At = DateTime.Now;
                car.Updated_By = User.Identity.Name;
                _unitOfWork.SaveChanges();

                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(car);
        }

        [HttpPost]
        public JsonResult DeleteCar(string listId)
        {
            Car car;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefault(int.Parse(id));
                    _unitOfWork.GetRepositoryInstance<Car>().Remove(car);
                    _unitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public string GetOrganizationName(int? id)
        {
            string name = "";
            if (id != null)
            {
                Organization product = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefault(int.Parse(id.ToString()));
                if (product != null)
                {
                    name = product.Name;
                }
            }

            return name;
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
