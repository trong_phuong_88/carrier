﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.PushNotification;
using Carrier.Repository;
using Carrier.Utilities;
using CMS.Carrier.Areas.Admin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class DevicesPushController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //GET: DevicesPush
        //public ActionResult Index(string userName, string content)
        public ActionResult Index(int OrganizationId = 0)
        {
            //if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(content))
            //{

            //}
            //return View();

            List<DriverViewModel> resultView = new List<DriverViewModel>();
            List<Driver> result;
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                List<UserInfoes> listOrganization = new List<UserInfoes>();
                listOrganization = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetListByParameter(x => x.OrganzationId == 0).ToList();
                IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.FullName.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listOrganizationSelect;
            }
            if (OrganizationId > 0)
            {
                result = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == OrganizationId).OrderByDescending(o => o.Name).ToList();
            }
            else
            {
                result = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().OrderByDescending(o => o.Name).ToList();
            }

            foreach (var item in result)
            {
                var viewModel = new DriverViewModel();
                var user = UserManager.FindById(item.UserId);
                viewModel.Id = item.Id;
                viewModel.Name = item.Name;
                viewModel.BirthDay = item.BirthDay;
                viewModel.OrganizationId = item.OrganizationId;
                viewModel.Phone = item.Phone;
                viewModel.Sex = item.Sex;
                viewModel.Status = item.Status;
                viewModel.UserId = item.UserId;
                viewModel.Updated_At = item.Updated_At;
                if (user != null)
                {
                    viewModel.User_Name = user.UserName;
                    viewModel.LockoutEnabled = user.LockoutEnabled;
                }

                List<Messages> listMessage = _unitOfWork.GetRepositoryInstance<Messages>().GetAllRecords().OrderBy(o => o.Message).ToList();
                if (listMessage.Count > 0)
                {
                    ViewBag.ListMessage = listMessage;
                }

                resultView.Add(viewModel);
            }

            return View(resultView);
        }

        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
            var devicePushList = (from user in db.AspNetUsers
                                  join dv in db.Driver on user.Id equals dv.UserId
                                  where user.UserName.StartsWith(prefix)
                                  select new
                                  {
                                      label = user.UserName,
                                      val = user.Id
                                  }).ToList();

            return Json(devicePushList);
        }

        //[HttpPost]
        //public ActionResult Index(string CustomerName, string CustomerId)
        //{
        //    ViewBag.Message = "CustomerName: " + CustomerName + " CustomerId: " + CustomerId;
        //    return View();
        //}
        // public void Push(DevicesPush item, string message)
        //{
        //if (item.Platform.Equals("iOS"))
        //{
        //    string strCertificate = string.Empty;
        //    // Check push cho User or Driver
        //    if (Roles.IsUserInRole(item.UserName, "Tài Xế"))
        //    {
        //        strCertificate = "\\Content\\Certificate\\PushDriverDev.p12";
        //    }
        //    else
        //    {
        //        strCertificate = "\\Content\\Certificate\\PushUserDev.p12";
        //    }
        //    PushServices.strFileP12 = strCertificate;
        //    PushServices.SetupPushAPN();
        //    string jsonData = "{\"aps\":{\"badge\":1,\"alert\":\"" + message + "\",\"sound\":\"default\"}}";
        //    List<string> lstDevice = new List<string>();
        //    //lstDevice.Add("0D74269EC851EE90A4A3AA429FD954CBC0C62AE0EB2976EF287BACCDF10ED801");
        //    lstDevice.Add(item.TokenDevice);
        //    PushServices.PushAPNMessage(jsonData, lstDevice);
        //}
        //else
        //{
        //    string gcmKey = string.Empty;
        //    // Check push cho User or Driver
        //    if (Roles.IsUserInRole(item.UserName, "Tài Xế"))
        //    {
        //        PushServices.GcmSenderId = "";
        //        PushServices.GcmKey = "";
        //    }
        //    else
        //    {
        //        PushServices.GcmSenderId = "";
        //        PushServices.GcmKey = "";
        //    }
        //    PushServices.SetupPushAPN();
        //    PushServices.PushGCMMessage(string.Empty, null);
        //}
        //}

        //public JsonResult PustMessage(string userId, string message)
        //{
        //    DevicesPush devicePush = db.DevicesPush.Where(o => o.UserId == userId).Single();
        //    try
        //    {
        //        if (devicePush != null)
        //        {
        //            Push(devicePush, message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json("Eror:" + ex.Message);
        //    }

        //    return Json("ok");
        //}

        public JsonResult PustMessage(string strUserId, string message)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;

            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"" + message + "\",\"sound\":\"default\"}}";
            try
            {
                string[] listUserId = strUserId.TrimEnd(',').Split(',').ToArray();
                DevicesPush device;
                bool check = false;
                foreach (var userId in listUserId)
                {
                    device = db.DevicesPush.Where(x => x.UserId.Equals(strUserId)).FirstOrDefault();
                    if (device != null)
                    {
                        if (device.Platform.ToLower().Equals("ios"))
                        {
                            PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                            PushServices.SetupPushAPN(true);
                            PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, string.Empty, callback);
                        }
                        else
                        {
                            jsonMessage = "{\"message\":\"" + message + "\"}";
                            PushServices.GcmKey = PublicConstant.GCM_KEY;
                            PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                            PushServices.SetupPushGCM();
                            PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, string.Empty, string.Empty, callback);
                        }
                        //Insert Messages
                        Messages suMessages = new Messages();
                        suMessages.Message = message;
                        suMessages.ToUserId = userId;
                        var user = UserManager.FindByName(User.Identity.Name);
                        string fromUserId = user.Id;
                        suMessages.FromUserId = fromUserId;
                        suMessages.Created_At = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                        _unitOfWork.GetRepositoryInstance<Messages>().Add(suMessages);
                        _unitOfWork.SaveChanges();

                        check = true;
                    }
                }
                if (check)
                {
                    return Json("ok");
                }
                else
                {
                    return Json("error");
                }
            }
            catch (Exception ex)
            {
                return Json("Eror:" + ex.Message);
            }

        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            if (result.Equals("Success"))
            {
                // Push success
            }
            else
            {
                // Show message 
            }
        }

    }
}