﻿using Carrier.CMS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections.Generic;
using Carrier.Utilities;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using Carrier.Repository;
using GoogleMaps.LocationServices;
using System;
using System.Data.SqlClient;
using Carrier.PushNotification;
using System.IO;
using System.Web.Configuration;
using System.Web.Helpers;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class UsersAdminController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public UsersAdminController()
        {
        }

        public UsersAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }
        #region Index cu 2802
        //public async Task<ActionResult> Index(string ddlRole)
        //{
        //    ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");

        //    var lstUser = await UserManager.Users.Include(u => u.Roles).ToListAsync();
        //    var lstUserViewModel = new List<UserViewModel>();
        //    foreach (ApplicationUser item in lstUser)
        //    {
        //        if (item.UsersInfo != null)
        //        {
        //            var newItem = new UserViewModel();
        //            newItem.Address = item.UsersInfo.Address;
        //            newItem.FullName = item.UsersInfo.FullName;
        //            newItem.MobilePhone = item.UsersInfo.MobilePhone;
        //            newItem.UserName = item.UserName;
        //            newItem.LastLogin = item.UsersInfo.LastLogin;
        //            var roles = new List<string>();
        //            foreach (var role in UserManager.GetRoles(item.Id))
        //            {
        //                role.ToString();
        //                roles.Add(role.ToString());
        //            }
        //            newItem.Email = item.Email;
        //            if (ddlRole == null)
        //            {
        //                if (roles.Contains("Admin"))
        //                {
        //                    newItem.Roles = null;
        //                }
        //                else
        //                {
        //                    newItem.Roles = roles;
        //                }
        //                if (item.LockoutEnabled)
        //                {
        //                    newItem.Status = 0;
        //                }
        //                else
        //                {
        //                    newItem.Status = 1;

        //                }
        //                newItem.Id = item.UsersInfo.Id;
        //                newItem.Created_At = item.UsersInfo.Created_At;
        //                lstUserViewModel.Add(newItem);
        //            }
        //            else
        //            {
        //                if (roles.Contains(ddlRole))
        //                {
        //                    if (roles.Contains("Admin"))
        //                    {
        //                        newItem.Roles = null;
        //                    }
        //                    else
        //                    {
        //                        newItem.Roles = roles;
        //                    }
        //                    if (item.LockoutEnabled)
        //                    {
        //                        newItem.Status = 0;
        //                    }
        //                    else
        //                    {
        //                        newItem.Status = 1;

        //                    }
        //                    newItem.Id = item.UsersInfo.Id;
        //                    newItem.Created_At = item.UsersInfo.Created_At;
        //                    lstUserViewModel.Add(newItem);
        //                }
        //            }
        //        }
        //    }
        //    //if (!String.IsNullOrEmpty(ddlRole))
        //    //{
        //    //    if (ddlRole == "Admin")
        //    //    {
        //    //        lstUserViewModel = lstUserViewModel.Where(o => o.Roles.Contains("Admin")).OrderByDescending(o => o.Created_At).ToList();
        //    //    }
        //    //    else 
        //    //    {
        //    //        lstUserViewModel = lstUserViewModel.Where(o => o.Roles.Contains("Enterprise")).OrderByDescending(o => o.Created_At).ToList();
        //    //    }
        //    //}

        //    UserInfoes user = new UserInfoes();
        //    foreach (UserViewModel Item in lstUserViewModel)
        //    {
        //        user = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID == Item.ApplicationUserID);
        //        if (user != null)
        //        {
        //            _unitOfWork.SaveChanges();
        //        }
        //    }

        //    return View(lstUserViewModel.OrderByDescending(x => x.Created_At));
        //}
        #endregion

        public async Task<ActionResult> ListUser(string ddlRole)
        {
            try
            {
                //ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                if (ddlRole == null)
                {
                    ddlRole = "4";
                }

                List<SP_GetAllUserByRoleName_Result> result = new List<SP_GetAllUserByRoleName_Result>();

                var RoleId = new SqlParameter("@RoleId", System.Data.SqlDbType.NVarChar) { Value = ddlRole };

                result = _unitOfWork.GetRepositoryInstance<SP_GetAllUserByRoleName_Result>().GetResultBySqlProcedure("SP_GetAllUserByRoleName @RoleId", RoleId).ToList();

                return PartialView("_ListViewUser", result);

            }
            catch (Exception ex)
            {
                string mss = ex.Message;
                throw;
            }

        }
        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);

            return View(user);
        }

        //
        // GET: /Users/Create
        public async Task<ActionResult> Create()
        {
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(UserViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                List<AspNetUsers> aspUser = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetListByParameter(o => o.UserName == userViewModel.UserName).ToList();
                if (aspUser.Count > 0)
                {
                    TempData["error"] = "Tên đăng nhập đã tồn tại vui lòng chọn tên đăng nhập khác";
                    return View();
                }
                var user = new ApplicationUser { UserName = userViewModel.UserName, Email = userViewModel.Email, LockoutEnabled = true };

                UserInfo userInfo = new UserInfo();
                userInfo.Address = userViewModel.Address;
                userInfo.MobilePhone = userViewModel.MobilePhone;
                userInfo.FullName = userViewModel.FullName;
                userInfo.ApplicationUserID = user.Id;
                userInfo.Sex = false;
                userInfo.Status = PublicConstant.STATUS_ACTIVE;
                userInfo.Updated_At = System.DateTime.Now;
                userInfo.LastLogin = System.DateTime.Now;
                userInfo.Created_At = System.DateTime.Now;
                //userInfo.IsView = false;
                string message = "";
                if (userViewModel.Address.Length > 0)
                {
                    string latLongFrom = GetLatLongByAddress(ref message, userViewModel.Address);
                    if (message.Length == 0)
                    {
                        userInfo.Lat = double.Parse(latLongFrom.Split(':')[0]);
                        userInfo.Lng = double.Parse(latLongFrom.Split(':')[1]);
                    }
                }
                user.UsersInfo = userInfo;

                var result = await UserManager.CreateAsync(user, userViewModel.PassWord);
                if (result.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                    }
                    else
                    {
                        result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
                    }
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "UsersAdmin");
                    }
                }
            }

            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");

            return View();
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (System.Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }

        //
        // GET: /Users/Edit/1
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            UserInfoes UserAdmin = db.UserInfoes.Find(id);
            var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await UserManager.GetRolesAsync(user.Id);

            return View(new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })
            });
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(FormCollection form, params string[] selectedRole)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (userInfo != null)
            {
                var user = await UserManager.FindByIdAsync(userInfo.ApplicationUserID);

                if (ModelState.IsValid)
                {
                    if (user == null)
                    {
                        return HttpNotFound();
                    }

                    var userRoles = await UserManager.GetRolesAsync(user.Id);

                    selectedRole = selectedRole ?? new string[] { };

                    var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First());
                        return View();
                    }
                    result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First());
                        return View();
                    }
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "Something failed.");
            }
            return View();
        }

        // GET: /Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            UserInfoes UserAdmin = db.UserInfoes.Find(id);
            var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                //var user = await UserManager.FindByIdAsync(id);
                UserInfoes UserAdmin = db.UserInfoes.Find(id);
                var drId = UserAdmin.ApplicationUserID;
                //Driver dr = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId == drId);
                var dr = db.Driver.Where(x => x.UserId == drId).FirstOrDefault();
                var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                else
                {
                    db.UserInfoes.Remove(UserAdmin);
                    if (dr != null)
                    {
                        db.Driver.Remove(dr);
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        //
        // GET: /Users/ChangeLock/5
        public async Task<ActionResult> ChangeLock(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            UserInfoes UserAdmin = db.UserInfoes.Find(id);
            var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/ChangeLock/5
        [HttpPost, ActionName("ChangeLock")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeLockConfirmed(int? id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                //var user = await UserManager.FindByIdAsync(id);
                UserInfoes UserAdmin = db.UserInfoes.Find(id);
                var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
                if (user == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    if (user.LockoutEnabled == true)
                    {
                        user.LockoutEnabled = false;
                    }
                    else
                    {
                        user.LockoutEnabled = true;
                    }
                    var result = await UserManager.UpdateAsync(user);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First());
                        return View();
                    }
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        [HttpPost]
        public JsonResult SetPassword(string id, string newpass)
        {
            var token = UserManager.GeneratePasswordResetToken(id);
            UserManager.ResetPassword(id, token, newpass);

            return Json(new { success = true });
        }

        [HttpPost]
        public JsonResult lockUser(string id)
        {

            MembershipUser u = Membership.GetUser(id);
            if (u.IsLockedOut)
            {
                u.UnlockUser();
                return Json(new { success = false });
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    Membership.ValidateUser(id, "wellthisistheverywrongpassword");
                }

            }
            return Json(new { success = true });
        }

        [HttpPost]
        public async Task<JsonResult> ChangeLockout(int id)
        {
            UserInfoes userInfo;
            userInfo = db.UserInfoes.Find(id);
            var user = await UserManager.FindByIdAsync(userInfo.ApplicationUserID);
            if (user == null)
            {
                return Json(new { success = false });
            }
            else
            {
                if (user.LockoutEnabled == true)
                {
                    user.LockoutEnabled = false;
                }
                else
                {
                    user.LockoutEnabled = true;
                }
                UserManager.Update(user);
                return Json(new { success = true });
            }
        }

        public ActionResult NapTien(string userid)
        {
            WalletInfo wallet = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userid));
            if (wallet != null)
            {
                var user = UserManager.FindById(userid);
                if (user != null)
                {
                    var us = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(userid));
                    if (us != null)
                    {
                        TempData["FULLNAMe"] = us.FullName;
                        TempData["USERNAME"] = user.UserName;
                    }
                }
            }
            ViewBag.userid = userid;
            return View();
        }
        [HttpPost]
        public ActionResult NapTien(FormCollection form)
        {
            try
            {
                //string content = form["txtnoidungnapien"] != null ? form["txtnoidungnapien"].ToString() : "";
                string userid = form["userid"] != null ? form["userid"].ToString() : "";
                double money = double.Parse(form["txtmonney"] != null ? form["txtmonney"].ToString() : "0");
                var type = form["hf-Type"] != null ? form["hf-Type"].ToString() : string.Empty;
                double price_change = Math.Abs(money);
                if (money < 0)
                {
                    //TempData["error"] = "Số tiền không được âm!";
                    //return View();
                    // Tiền âm là trừ tiền
                    WalletInfo wallet = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(o => o.UserId == userid);
                    if (wallet != null) // update balence
                    {
                        TransactionWithDrawRequest transaction = new TransactionWithDrawRequest();
                        transaction.ApplicationUserId = userid;
                        transaction.FullName = "ADMIN";
                        transaction.SoTien = price_change;
                        transaction.Status = 0;
                        transaction.Created_At = DateTime.Now;
                        transaction.Created_Update = DateTime.Now;
                        transaction.TransactionType = 2; //trừ tiền 
                        if (type.Equals("1")) // trừ tiền vào tài khoản chính
                        {
                            if (wallet.Balance < price_change)
                            {
                                TempData["error"] = "Tài khoản chính không đủ tiền để trừ";
                                return View();
                            }
                            wallet.Balance = wallet.Balance - price_change;
                            transaction.Description = "Trừ tiền từ Admin Carrier vào tài khoản chính: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else if (type.Equals("0"))
                        {
                            if (wallet.Commission < price_change)
                            {
                                TempData["error"] = "Tài khoản khuyến mại không đủ tiền để trừ";
                                return View();
                            }
                            wallet.Commission = wallet.Commission - price_change;
                            transaction.Description = "Trừ tiền từ Admin Carrier vào tài khoản khuyến mại: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else
                        {
                            TempData["error"] = "Chưa chọn hình thức nạp tiền";
                            return View();
                        }
                        transaction.CurrentBalanceWallet = wallet.Balance + wallet.Commission;
                        _unitOfWork.GetRepositoryInstance<TransactionWithDrawRequest>().Add(transaction);
                        wallet.Updated_At = DateTime.Now;

                        _unitOfWork.GetRepositoryInstance<WalletInfo>().Update(wallet);
                        _unitOfWork.SaveChanges();
                    }
                    else // insert new
                    {
                        // GenericUnitOfWork _unitOfWorkInsert = new GenericUnitOfWork();
                        wallet = new WalletInfo();
                        wallet.Balance = money;
                        wallet.UserId = userid;
                        wallet.Updated_At = DateTime.Now;
                        _unitOfWork.GetRepositoryInstance<WalletInfo>().Add(wallet);

                        // lưu lịch sử
                        TransactionWithDrawRequest transaction = new TransactionWithDrawRequest();
                        transaction.ApplicationUserId = userid;
                        transaction.FullName = "ADMIN";
                        transaction.SoTien = money;
                        transaction.Status = 0;
                        transaction.Created_At = DateTime.Now;
                        transaction.Created_Update = DateTime.Now;
                        transaction.TransactionType = 2; //trừ tiền 
                        if (type.Equals("1")) // cộng tiền vào tài khoản chính
                        {
                            wallet.Balance = price_change + wallet.Balance;
                            transaction.Description = "Cộng tiền từ Admin Carrier vào tài khoản chính: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else if (type.Equals("0"))
                        {
                            wallet.Commission = price_change + wallet.Commission;
                            transaction.Description = "Cộng tiền từ Admin Carrier vào tài khoản thưởng: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else
                        {
                            TempData["error"] = "Chưa chọn hình thức nạp tiền";
                            return View();
                        }
                        transaction.CurrentBalanceWallet = wallet.Balance + wallet.Commission;
                        _unitOfWork.GetRepositoryInstance<TransactionWithDrawRequest>().Add(transaction);
                        wallet.Updated_At = DateTime.Now;

                        _unitOfWork.SaveChanges();
                        TempData["messages"] = "Cộng tiền thành công";
                    }
                    TempData["messages"] = "Cộng tiền thành công";
                    PushMessage(userid, "Bạn vừa được cộng tiền từ Admin Carrier " + Ultilities.ConvertToCurrency(money) + " VND");
                }
                else
                {
                    WalletInfo wallet = _unitOfWork.GetRepositoryInstance<WalletInfo>().GetFirstOrDefaultByParameter(o => o.UserId == userid);
                    if (wallet != null) // update balence
                    {
                        TransactionWithDrawRequest transaction = new TransactionWithDrawRequest();
                        transaction.ApplicationUserId = userid;
                        transaction.FullName = "ADMIN";
                        transaction.SoTien = money;
                        transaction.Status = 0;
                        transaction.Created_At = DateTime.Now;
                        transaction.Created_Update = DateTime.Now;
                        transaction.TransactionType = 1; //Cộng tiền 
                        if (type.Equals("1")) // cộng tiền vào tài khoản chính
                        {
                            wallet.Balance = money + wallet.Balance;
                            transaction.Description = "Cộng tiền từ Admin Carrier vào tài khoản chính: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else if (type.Equals("0"))
                        {
                            wallet.Commission = money + wallet.Commission;
                            transaction.Description = "Cộng tiền từ Admin Carrier vào tài khoản khuyến mại: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else
                        {
                            TempData["error"] = "Chưa chọn hình thức nạp tiền";
                            return View();
                        }
                        transaction.CurrentBalanceWallet = wallet.Balance + wallet.Commission;
                        _unitOfWork.GetRepositoryInstance<TransactionWithDrawRequest>().Add(transaction);
                        wallet.Updated_At = DateTime.Now;

                        _unitOfWork.GetRepositoryInstance<WalletInfo>().Update(wallet);
                        _unitOfWork.SaveChanges();
                    }
                    else // insert new
                    {
                        // GenericUnitOfWork _unitOfWorkInsert = new GenericUnitOfWork();
                        wallet = new WalletInfo();
                        wallet.Balance = money;
                        wallet.UserId = userid;
                        wallet.Updated_At = DateTime.Now;
                        _unitOfWork.GetRepositoryInstance<WalletInfo>().Add(wallet);

                        // lưu lịch sử
                        TransactionWithDrawRequest transaction = new TransactionWithDrawRequest();
                        transaction.ApplicationUserId = userid;
                        transaction.FullName = "ADMIN";
                        transaction.SoTien = money;
                        transaction.Status = 0;
                        transaction.Created_At = DateTime.Now;
                        transaction.Created_Update = DateTime.Now;
                        transaction.TransactionType = 1; //Cộng tiền 
                        if (type.Equals("1")) // cộng tiền vào tài khoản chính
                        {
                            wallet.Balance = money + wallet.Balance;
                            transaction.Description = "Cộng tiền từ Admin Carrier vào tài khoản chính: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else if (type.Equals("0"))
                        {
                            wallet.Commission = money + wallet.Commission;
                            transaction.Description = "Cộng tiền từ Admin Carrier vào tài khoản thưởng: " + Ultilities.ConvertToCurrency(money) + " VND";
                        }
                        else
                        {
                            TempData["error"] = "Chưa chọn hình thức nạp tiền";
                            return View();
                        }
                        transaction.CurrentBalanceWallet = wallet.Balance + wallet.Commission;
                        _unitOfWork.GetRepositoryInstance<TransactionWithDrawRequest>().Add(transaction);
                        wallet.Updated_At = DateTime.Now;

                        _unitOfWork.SaveChanges();
                        TempData["messages"] = "Cộng tiền thành công";
                    }
                    TempData["messages"] = "Cộng tiền thành công";
                    PushMessage(userid, "Bạn vừa được cộng tiền từ Admin Carrier " + Ultilities.ConvertToCurrency(money) + " VND");
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi: " + ex.Message;
                return View();
            }
            return RedirectToAction("Index", "UsersAdmin");
        }
        public void PushMessage(string strUserId, string message)
        {
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"" + message + "\",\"sound\":\"default\"}}";
            try
            {
                var device = db.DevicesPush.Where(x => x.UserId.Equals(strUserId)).FirstOrDefault();
                if (device != null)
                {
                    if (device.Platform.ToLower().Equals("ios"))
                    {
                        PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                        PushServices.SetupPushAPN(true);
                        PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, null, null, null);
                    }
                    else
                    {
                        jsonMessage = "{\"message\":\"" + message + "\"}";
                        PushServices.GcmKey = PublicConstant.GCM_KEY;
                        PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                        PushServices.SetupPushGCM();
                        PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, null, null, null);
                    }
                    //Insert Messages
                    Messages suMessages = new Messages();
                    suMessages.Message = message;
                    suMessages.ToUserId = strUserId;
                    var user = UserManager.FindByName(User.Identity.Name);
                    string fromUserId = user.Id;
                    suMessages.FromUserId = fromUserId;
                    suMessages.Created_At = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    _unitOfWork.GetRepositoryInstance<Messages>().Add(suMessages);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }

        }
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(FormCollection fm)
        {
            string userid = string.Empty;
            if (Request.QueryString.Get("id") != null)
            {
                userid = Request.QueryString.Get("id");
                Session["USERID"] = userid;
            }
            var appUserId = Session["USERID"].ToString();
            if (!string.IsNullOrEmpty(appUserId))
            {
                if (fm["Newpass"] != null)
                {
                    string code = await UserManager.GeneratePasswordResetTokenAsync(appUserId);
                    IdentityResult result = await UserManager.ResetPasswordAsync(appUserId, code, fm["Newpass"].ToString());

                    if (result.Succeeded)
                    {
                        var userReset = UserManager.FindById(appUserId);
                        //Common.CommonUtils.SendSms(userReset.UserName, fm["Newpass"].ToString());
                        TempData["messages"] = "Thay đổi mật khẩu thành công";
                        return RedirectToAction("Index", "UsersAdmin");
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            else
            {
                TempData["messages"] = "Không tìm thấy thông tin tài khoản! Vui lòng thử lại";
                return RedirectToAction("Index", "UsersAdmin");
            }

        }
        public JsonResult setPassword()
        {
            //var newPass = Utitlity.Utility.RandomString(pass);
            var newPass = Ultilities.CreatePassword(6);
            return Json(newPass, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ProfileDriver(string userid)
        {
            if (userid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //var bangGiaTheoTuyen = _unitOfWork.GetRepositoryInstance<BangGiaTheoTuyen>().GetFirstOrDefaultByParameter(x => x.Id == id);
            var driver = db.Driver.Where(x => x.UserId.Equals(userid)).FirstOrDefault();

            if (driver == null)
            {
                return HttpNotFound();
            }
            return PartialView("ProfileDriver", driver);
        }
        public JsonResult EditProfileDriver([Bind(Include = "Name,Address,Phone,Sex,BirthDay")] Driver driver, IEnumerable<HttpPostedFileBase> files_front_img)
        {
            if (ModelState.IsValid)
            {
                driver.Name = driver.Name;
                driver.Address = driver.Address;
                driver.Phone = driver.Phone;
                driver.Sex = driver.Sex;
                driver.BirthDay = driver.BirthDay;
                var fileName = string.Empty;
                if (files_front_img != null)
                {
                    foreach (var file in files_front_img)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            try
                            {
                                //kiem tra file extension lan nua
                                List<string> excelExtension = new List<string>();
                                excelExtension.Add(".png");
                                excelExtension.Add(".jpg");
                                excelExtension.Add(".gif");
                                var extension = Path.GetExtension(file.FileName);
                                if (!excelExtension.Contains(extension.ToLower()))
                                {
                                    TempData["error"] = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png";
                                    return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                                }
                                //kiem tra dung luong file
                                var fileSize = file.ContentLength;
                                var fileMB = (fileSize / 1024f) / 1024f;
                                if (fileMB > 5)
                                {
                                    TempData["error"] = "Dung lượng ảnh không được lớn hơn 5MB";
                                    return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                                }

                                if (file.FileName != null && file.FileName.Length > 0)
                                {
                                    // luu ra dia
                                    fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                    //fileName = fileName + extension;
                                    fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
                                }

                                var physicalPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                                try
                                {
                                    file.SaveAs(physicalPath);
                                    //System.IO.File.Delete(Server.MapPath("~/Uploads/TinTuc/" + old.ThumbURL));
                                }
                                catch (Exception)
                                {
                                }
                                //articles.articleInfo.ThumbURL = fileName;
                                //old.ThumbURL = articles.articleInfo.ThumbURL;
                            }
                            catch (Exception ex)
                            {
                                TempData["error"] = "Lỗi upload ảnh bài viết: " + ex.Message;
                            }
                        }
                    }
                }

                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return Json("OK", JsonRequestBehavior.AllowGet);
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }
        private void UploadImage(ref string message, HttpPostedFileBase originalImage, string imageName, string rootPathOriginal, string rootPathThumbNail, HttpServerUtilityBase server)
        {
            try
            {
                bool existsOriginal = Directory.Exists(PublicConstant.IMAGE_PAHT_TEMP);
                if (!existsOriginal)
                {
                    Directory.CreateDirectory(PublicConstant.IMAGE_PAHT_TEMP);
                }
                originalImage.SaveAs(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                WebImage img1 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                //img1.Resize(849, 320);
                bool exists = Directory.Exists(rootPathOriginal);
                if (!exists)
                    Directory.CreateDirectory(rootPathOriginal);
                img1.Save(Path.Combine(rootPathOriginal + "/" + imageName));

                WebImage img2 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                //img2.Resize(274, 175);
                bool exists2 = Directory.Exists(rootPathThumbNail);
                if (!exists2)
                    Directory.CreateDirectory(rootPathThumbNail);
                img2.Save(Path.Combine(rootPathThumbNail + "/" + imageName));

                System.IO.File.Delete(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
            }
            catch (Exception ex)
            {
                message = "Lỗi upload ảnh: " + ex.Message;
            }
        }

        //
        // GET: /Users/Edit/1
        public async Task<ActionResult> EditDriver(string userid)
        {
            if (string.IsNullOrEmpty(userid))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            var driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(userid));
            if (driver == null)
            {
                return HttpNotFound();
            }

            return View(driver);
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDriver(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (driver != null)
            {

                string message = "";
                driver.Name = form.Get("Name") != null ? form.Get("Name") : "";
                driver.Address = form.Get("Address") != null ? form.Get("Address") : "";
                driver.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                driver.Sex = form.Get("Sex") != null ? form.Get("Sex") : "";
                driver.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";
                var fileName = string.Empty;
                var front_Image = "";
                var back_Image = "";
                if (Request.Files.Count > 0)
                {
                    string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                    HttpPostedFileBase fileTruoc = Request.Files[0];
                    HttpPostedFileBase fileSau = Request.Files[1];
                    string filename1 = "image_front" + fileTruoc.FileName;
                    string filename2 = "image_back" + fileSau.FileName;

                    if (fileTruoc.ContentLength > 0)
                    {
                        front_Image = publicRootPath + filename1;
                        UploadImage(ref message, fileTruoc, filename1, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                        driver.Front_Image = front_Image;
                    }


                    if (message.Length > 0)
                    {
                        TempData["error"] = "Lỗi upload ảnh mặt trước";
                        return RedirectToAction("Index", "UsersAdmin");
                    }

                    if (fileSau.ContentLength > 0)
                    {
                        back_Image = publicRootPath + filename2;
                        UploadImage(ref message, fileSau, filename2, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                        driver.Back_Image = back_Image;
                    }
                    if (message.Length > 0)
                    {
                        TempData["error"] = "Lỗi upload ảnh mặt sau";
                        return RedirectToAction("Index", "UsersAdmin");
                    }
                }
                TempData["messages"] = "Sửa thông tin tài xế thành công";
                _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                _unitOfWork.SaveChanges();
            }
            return RedirectToAction("Index", "UsersAdmin");
        }
        //
        // GET: /Users/Edit/1
        public async Task<ActionResult> EditUser(string userid)
        {
            if (string.IsNullOrEmpty(userid))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            var driver = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(userid));
            if (driver == null)
            {
                return HttpNotFound();
            }

            return View(driver);
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUser(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            UserInfoes userinfoes = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (userinfoes != null)
            {
                userinfoes.FullName = form.Get("Name") != null ? form.Get("Name") : "";
                userinfoes.Address = form.Get("Address") != null ? form.Get("Address") : "";
                userinfoes.MobilePhone = form.Get("Phone") != null ? form.Get("Phone") : "";
                if (form.Get("Sex").Equals("Nam"))
                {
                    userinfoes.Sex = true;
                }
                else
                {
                    userinfoes.Sex = false;
                }
                userinfoes.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";
                var fileName = string.Empty;
                TempData["messages"] = "Sửa thông tin User thành công";
                _unitOfWork.GetRepositoryInstance<UserInfoes>().Update(userinfoes);
                _unitOfWork.SaveChanges();
            }
            return RedirectToAction("Index", "UsersAdmin");
        }
        public ActionResult ProfileUser(string userid)
        {
            if (userid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //var bangGiaTheoTuyen = _unitOfWork.GetRepositoryInstance<BangGiaTheoTuyen>().GetFirstOrDefaultByParameter(x => x.Id == id);
            var driver = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(userid));

            if (driver == null)
            {
                return HttpNotFound();
            }
            return PartialView("ProfileUser", driver);
        }
        public JsonResult EditProfileUser([Bind(Include = "Id,FullName,Address,MobilePhone,Sex,BirthDay")] UserInfoes userInfoes)
        {
            if (ModelState.IsValid)
            {
                userInfoes.FullName = userInfoes.FullName;
                userInfoes.Address = userInfoes.Address;
                userInfoes.MobilePhone = userInfoes.MobilePhone;

                userInfoes.Sex = userInfoes.Sex;
                //userInfoes.BirthDay = userInfoes.BirthDay;

                _unitOfWork.GetRepositoryInstance<UserInfoes>().Update(userInfoes);
                _unitOfWork.SaveChanges();
                return Json("OK", JsonRequestBehavior.AllowGet);
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }
    }
}
