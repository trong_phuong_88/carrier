﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.CMS.Common;
using Carrier.Utilities;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class CarTypesController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/CarTypes
        public ActionResult Index()
        {
            var lstCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            return View(lstCarType);
        }

        // GET: Admin/CarTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarType carType = _unitOfWork.GetRepositoryInstance<CarType>().GetFirstOrDefault(id.Value);
            if (carType == null)
            {
                return HttpNotFound();
            }
            return View(carType);
        }

        // GET: Admin/CarTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/CarTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Payload,Value")] CarType carType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.GetRepositoryInstance<CarType>().Add(carType);
                _unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(carType);
        }

        // GET: Admin/CarTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarType carType = _unitOfWork.GetRepositoryInstance<CarType>().GetFirstOrDefault(id.Value);
            if (carType == null)
            {
                return HttpNotFound();
            }
            return View(carType);
        }

        // POST: Admin/CarTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Payload,Value")] CarType carType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.GetRepositoryInstance<CarType>().Update(carType);
                _unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(carType);
        }

        // GET: Admin/CarTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarType carType = _unitOfWork.GetRepositoryInstance<CarType>().GetFirstOrDefault(id.Value);
            if (carType == null)
            {
                return HttpNotFound();
            }
            return View(carType);
        }

        // POST: Admin/CarTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarType carType = _unitOfWork.GetRepositoryInstance<CarType>().GetFirstOrDefault(id);
            _unitOfWork.GetRepositoryInstance<CarType>().Remove(carType);
            _unitOfWork.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
