﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_ADMIN, PublicConstant.ROLE_SUPPER_ADMIN)]
    public class ThongKeCuocVanChuyenTaiXeController : Controller
    {
        // GET: Admin/ThongKeCuocVanChuyenTaiXe
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: OrderStatic
        public ActionResult Index()
        {
            var userId ="";
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };

            var lstdata = _unitOfWork.GetRepositoryInstance<SP_ThongKe_CuocVanChuyen_Result>().GetResultBySqlProcedure("SP_ThongKe_CuocVanChuyen @userId", sqlUserId).ToList();
            return View(lstdata);
        }
    }
}