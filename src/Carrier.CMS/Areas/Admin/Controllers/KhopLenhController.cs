﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.CMS.Models;
using PagedList;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Common;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class KhopLenhController : Controller
    {
        // GetMapSchedule
        // type = 1 with order
        // type = 0 with schedule
        private Carrier3Entities db = new Carrier3Entities();
        // GET: KhopLenh
        [Authorize]
        public ActionResult Index(int? page, int? pagsize, FormCollection fc, string MaVanDon = "", string UserName = "")
        {
            List<HistoryModel> listData = new List<HistoryModel>();
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            if (!string.IsNullOrEmpty(UserName) || MaVanDon.Length > 0)
            {
                listData = (from tracking in db.OrderTracking
                            join info in db.UserInfoes on tracking.DriverId equals info.ApplicationUserID
                            join order in db.Order on new { Id = (long)tracking.OrderId } equals new { order.Id }
                            where (tracking.Status == PublicConstant.ORDER_ACCEPT) && (order.UserName == UserName || order.MaVanDon == MaVanDon)
                            && info.OrganzationId == organizationId
                            select new HistoryModel
                            {
                                Id = tracking.ID,
                                OrderId = tracking.OrderId,
                                MaVanDon = order.MaVanDon,
                                UserName = order.UserName,
                                FullName = info.FullName,
                                Phone = info.MobilePhone,
                                Status = tracking.Status.ToString()
                            }).OrderByDescending(o => o.Id).ToList();
            }
            else
            {
                listData = (from tracking in db.OrderTracking
                            join info in db.UserInfoes on tracking.DriverId equals info.ApplicationUserID
                            join order in db.Order on new { Id = (long)tracking.OrderId } equals new { order.Id }
                            where (tracking.Status == PublicConstant.ORDER_ACCEPT)
                            && info.OrganzationId == organizationId
                            select new HistoryModel
                            {
                                Id = tracking.ID,
                                OrderId = tracking.OrderId,
                                MaVanDon = order.MaVanDon,
                                UserName = order.UserName,
                                FullName = info.FullName,
                                Phone = info.MobilePhone,
                                Status = tracking.Status.ToString()
                            }).OrderByDescending(o => o.Id).ToList();
            }

            return View(listData);
        }

        public ActionResult OrderDetail(int Id)
        {
            Order order = db.Order.Where(o => o.Id == Id).Single();
            return PartialView("_OrderDetail", order);
        }
        public ActionResult UserInfoDetail(int DriverId)
        {
            Driver userInfo = db.Driver.Where(o => o.Id == DriverId).Single();
            return PartialView("_UserInfo", userInfo);
        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        //[ChildActionOnly]
        //public MvcHtmlString DiaDiem(string cityCode, string districtCode, string wardCode)
        //{
        //    var city = db.City.Where(x => x.CityCode.Equals(cityCode)).FirstOrDefault();
        //    var district = db.District.Where(x => x.DistrictCode.Equals(districtCode)).FirstOrDefault();
        //    var ward = db.Ward.Where(x => x.WardCode.Equals(wardCode)).FirstOrDefault();
        //    var result = (ward != null ? ward.Name : string.Empty) + "-" + (district != null ? district.Name : string.Empty) + "-" + (city != null ? city.Name : string.Empty);
        //    return new MvcHtmlString(result);
        //}
        //public ActionResult SartMapping()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public JsonResult SearchTinh(string textSearch)
        //{
        //    var lstTinh = db.City.ToList();
        //    if (textSearch.Trim() != "")
        //    {
        //        lstTinh = db.City.Where(x => x.Name.StartsWith(textSearch)).ToList();
        //    }
        //    return Json(lstTinh, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public JsonResult GetMapSuggestSchedule(int scheduleId)
        //{
        //    List<ScheduleSuggetViewModel> scheduleSuggetViewModels = new List<ScheduleSuggetViewModel>();
        //    List<ScheduleOrder> scheduleMapSchedules = db.ScheduleOrder.Where(x => x.ScheduleJoinId == scheduleId).ToList();
        //    //List<ScheduleOrder> scheduleMapOrders = db.ScheduleOrder.Where(x => x.OrderJoinId == orderId).ToList();
        //    foreach (var item in scheduleMapSchedules)
        //    {
        //        if (item.OrderJoinId != null)
        //        {
        //            Order order = db.Order.Where(x => x.Id == item.OrderJoinId).FirstOrDefault();

        //            string diemdi = db.City.Where(x => x.CityCode == order.DiemDiTinh).FirstOrDefault().Name;
        //            string diemden = db.City.Where(x => x.CityCode == order.DiemDenTinh).FirstOrDefault().Name;
        //            //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //            //{
        //            //    id = order.Id,
        //            //    Time = order.ThoiGianDi,
        //            //    Route = diemdi + " - " + diemden,
        //            //    Type = 1,
        //            //    Status = ((MapOrerScheduleStatus)item.Status).ToString(),
        //            //};
        //            //scheduleSuggetViewModels.Add(scheduleSuggetViewModel);
        //        }
        //        if (item.ScheduleId != null)
        //        {
        //            Schedule schedule = db.Schedule.Where(x => x.Id == item.ScheduleId).FirstOrDefault();

        //            string diemdi = db.City.Where(x => x.CityCode == schedule.DiemDiTinh).FirstOrDefault().Name;
        //            string diemden = db.City.Where(x => x.CityCode == schedule.DiemDenTinh).FirstOrDefault().Name;
        //            //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //            //{
        //            //    id = schedule.Id,
        //            //    Time = schedule.ThoiGianDi,
        //            //    Route = diemdi + " - " + diemden,
        //            //    Type = 0,
        //            //    Status = ((MapOrerScheduleStatus)item.Status).ToString(),
        //            //};
        //            //scheduleSuggetViewModels.Add(scheduleSuggetViewModel);
        //        }
        //    }
        //    return Json(scheduleSuggetViewModels, JsonRequestBehavior.AllowGet);
        //}
        //[HttpGet]
        //public JsonResult GetMapSchedule(int scheduleId)
        //{
        //    Schedule schedule = db.Schedule.Where(x => x.Id == scheduleId).FirstOrDefault();
        //    string diemdi = db.City.Where(x => x.CityCode == schedule.DiemDiTinh).FirstOrDefault().Name;
        //    string diemden = db.City.Where(x => x.CityCode == schedule.DiemDenTinh).FirstOrDefault().Name;
        //    // type = 1 with order
        //    // type = 0 with schedule
        //    List<ScheduleSuggetViewModel> scheduleMap = findSchedule(schedule.DiemDiTinh, schedule.DiemDiHuyen, schedule.DiemDiXa, schedule.DiemDenTinh, schedule.DiemDenHuyen, schedule.DiemDenXa, schedule.ThoiGianDen, schedule.ThoiGianDi);
        //    List<ScheduleSuggetViewModel> orderMap = findScheduleOnOrder(schedule.DiemDiTinh, schedule.DiemDiHuyen, schedule.DiemDiXa, schedule.DiemDenTinh, schedule.DiemDenHuyen, schedule.DiemDenXa, schedule.ThoiGianDi);
        //    scheduleMap.AddRange(orderMap);
        //    return Json(scheduleMap, JsonRequestBehavior.AllowGet);
        //    //return Json(scheduleMap, JsonRequestBehavior.AllowGet);
        //}

        //// schedule : type = 0
        //// order : type = 1
        //[HttpPost]
        //public JsonResult MapScheduleOrder(int scheduleId, int scheduleOrderId, int type)
        //{
        //    ScheduleOrder scheduleOrder = new ScheduleOrder()
        //    {
        //        ScheduleId = scheduleId,
        //        Status = (int)MapOrerScheduleStatus.Wait,
        //        UpdateTime = DateTime.Now,
        //        UserName = User.Identity.Name,
        //        Type = type
        //    };

        //    if (type == 0)
        //    {
        //        scheduleOrder.ScheduleJoinId = scheduleOrderId;
        //        Schedule schedule = db.Schedule.Where(x => x.Id == scheduleOrderId).FirstOrDefault();
        //        if (schedule != null)
        //        {
        //            scheduleOrder.UserName = schedule.UserName;
        //        }
        //    }
        //    else
        //    {
        //        scheduleOrder.OrderJoinId = scheduleOrderId;
        //        Order order = db.Order.Where(x => x.Id == scheduleOrderId).FirstOrDefault();
        //        if (order != null)
        //        {
        //            scheduleOrder.UserName = order.UserName;
        //        }
        //    }

        //    db.ScheduleOrder.Add(scheduleOrder);
        //    db.SaveChanges();
        //    return Json("OK", JsonRequestBehavior.AllowGet);
        //}

        //private List<ScheduleSuggetViewModel> findSchedule(string DiemDiTinh, string DiemDiHuyen, string DiemDiXa, string DiemDenTinh, string DiemDenHuyen, string DiemDenXa, string timeStart, string timeEnd)
        //{
        //    // tìm list schedule
        //    List<Schedule> schedules = db.Schedule.Where(x => x.DiemDenTinh == DiemDiTinh && x.DiemDenHuyen == DiemDiHuyen && x.DiemDenXa == DiemDiXa).ToList();
        //    List<ScheduleSuggetViewModel> schedulesSelect = new List<ScheduleSuggetViewModel>();
        //    foreach (var schedule in schedules)
        //    {

        //        string date = "";

        //        if (GlobalCommon.LessThanOrEqualHour(timeEnd, schedule.ThoiGianDi) == true)
        //        {
        //            string diemdi = db.City.Where(x => x.CityCode == DiemDiTinh).FirstOrDefault().Name;
        //            string diemden = db.City.Where(x => x.CityCode == DiemDenTinh).FirstOrDefault().Name;

        //            //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //            //{
        //            //    id = schedule.Id,
        //            //    Time = schedule.ThoiGianDi + " - " + schedule.ThoiGianDen,
        //            //    Route = diemdi + " - " + diemden,
        //            //    Type = 0
        //            //};
        //            //schedulesSelect.Add(scheduleSuggetViewModel);
        //        }

        //    }
        //    return schedulesSelect;
        //}

        //private List<ScheduleSuggetViewModel> findScheduleOnOrder(string DiemDiTinh, string DiemDiHuyen, string DiemDiXa, string DiemDenTinh, string DiemDenHuyen, string DiemDenXa, string time)
        //{
        //    int locationStart2 = -1;
        //    int locationEnd2 = -1;
        //    int routeIdSelect = -1;
        //    List<ScheduleSuggetViewModel> schedulesSelect = new List<ScheduleSuggetViewModel>();
        //    List<Order> allOrder = db.Order.ToList();
        //    // tìm route chính xác.
        //    foreach (var item in allOrder)
        //    {
        //        if (item.DiemDiTinh != null && item.DiemDenTinh != null)
        //        {
        //            if (item.DiemDiTinh == DiemDenTinh && item.DiemDenTinh == DiemDiTinh && item.DiemDiHuyen == DiemDenHuyen && item.DiemDenHuyen == DiemDiHuyen && item.DiemDiXa == DiemDenXa && item.DiemDenXa == DiemDiXa)
        //            {
        //                City tinhDi = db.City.Where(x => x.CityCode == DiemDiTinh).FirstOrDefault();
        //                City tinhDen = db.City.Where(x => x.CityCode == DiemDenTinh).FirstOrDefault();
        //                //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //                //{
        //                //    id = item.Id,
        //                //    Time = item.ThoiGianDi,
        //                //    Route = tinhDen.Name + " -> " + tinhDi.Name,
        //                //    Type = 1
        //                //};
        //                //schedulesSelect.Add(scheduleSuggetViewModel);
        //            }
        //        }
        //    }
        //    return schedulesSelect;
        //}

        //public List<ScheduleSuggetViewModel> SearchAllData(int? pageNumber, int? pageSize)
        //{
        //    var lstData = new List<ScheduleSuggetViewModel>();
        //    var lstSchedule = db.Schedule.Where(x => x.ScheduleOrder.Where(y=>y.ScheduleId == x.Id && x.Status == PublicConstant.STATUS_ACTIVE).Count() == 0).ToList();
        //    var lstOrder = db.Order.Where(x => x.ScheduleOrder.Where(y=>y.OrderJoinId == x.Id && x.Status == PublicConstant.STATUS_ACTIVE).Count() == 0).ToList();
        //    foreach(Schedule schedule in lstSchedule)                                                       
        //    {                                            
        //        foreach (Order order in lstOrder)
        //        {
        //            var condition = GlobalCommon.MatchSheduleWithOrder(schedule,order);
        //            var isMatch1 = condition.MatchTinh && condition.MatchHuyen && condition.MatchXa && condition.MatchDateTimeDen
        //                && (condition.MatchTrongTai1 || condition.MatchTrongTai2);
        //            var isMatch2 = condition.MatchTinh && condition.MatchHuyen && condition.MatchXa && condition.MatchDateTimeDen && condition.MatchDateTimeDi
        //                && (condition.MatchTrongTai1 || condition.MatchTrongTai2);
        //            if (isMatch1 || isMatch2)
        //            {
        //                var sugguets = new ScheduleSuggetViewModel();
        //                sugguets.Type = 0;
        //                sugguets.ItemId = string.Format("{0}-{1}-{2}", schedule.Id, order.Id,sugguets.Type);
        //                sugguets.Order = order;
        //                sugguets.Schedule1 = schedule;
        //                sugguets.Gia = order.Gia.Value;
        //                sugguets.Condition = condition;
        //                lstData.Add(sugguets);
        //            }
        //        }
        //    }
        //    return lstData;
        //}
    }
    enum MapOrerScheduleStatus
    {
        Wait = 0,
        Appcept = 1,
        Cancel = 2
    }
}