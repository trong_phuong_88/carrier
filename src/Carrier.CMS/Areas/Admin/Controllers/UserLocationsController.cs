﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Models;
using System.Xml.Linq;
using System.IO;
using Carrier.Utilities;
using Carrier.CMS.Common;
using Carrier.Algorithm.Entity;
namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class UserLocationsController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private LogisticDataEntities dbLogictis = new LogisticDataEntities();

        // GET: UserLocations
        public ActionResult Index(string DriverName, string City)
        {
            List<City> listCity = dbLogictis.City.OrderBy(o => o.Name).ToList();
            ViewBag.CityList = new SelectList(listCity, "Name", "Name");
            List<UserLocationViewModel> listUserLocation = new List<UserLocationViewModel>();
            if (!string.IsNullOrEmpty(DriverName) || !string.IsNullOrEmpty(City))
            {
                listUserLocation = (from location in db.Driver join info in db.UserInfoes on location.UserId equals info.ApplicationUserID
                                    select new UserLocationViewModel {
                                        Id = location.Id,
                                        FullName = info.FullName,
                                        CurrentLocation = "",
                                        GPS = (location.Lat + "," + location.Lng),
                                        LastUpdate = location.Updated_At.Value}).ToList();
            }
            else
            {
                listUserLocation = (from location in db.Driver join info in db.UserInfoes on location.UserId equals info.ApplicationUserID
                                    select new UserLocationViewModel {
                                        Id = location.Id,
                                        FullName = info.FullName,
                                        CurrentLocation = "",
                                        GPS = (location.Lat + "," + location.Lng),
                                        LastUpdate = location.Updated_At.Value
                                    }).ToList();
            }

            foreach (UserLocationViewModel item in listUserLocation)
            {
                item.CurrentLocation = GeocoderLocation(item.Lat, item.Lng);
            }

            return View(listUserLocation);
        }

        // GET: UserLocations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver userLocation = db.Driver.Find(id);
            if (userLocation == null)
            {
                return HttpNotFound();
            }
            return View(userLocation);
        }

        // GET: UserLocations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserLocations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserName,Latitude,Longitude,CityName,DistrictName,WardName,Location,CreatedAt,UpdateAt")] Driver userLocation)
        {
            if (ModelState.IsValid)
            {
                db.Driver.Add(userLocation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userLocation);
        }

        // GET: UserLocations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver userLocation = db.Driver.Find(id);
            if (userLocation == null)
            {
                return HttpNotFound();
            }
            return View(userLocation);
        }

        // POST: UserLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,Latitude,Longitude,CityName,DistrictName,WardName,Location,CreatedAt,UpdateAt")] Driver userLocation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userLocation).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userLocation);
        }


        // GET: UserLocations/Edit/5
        public ActionResult ChangeStatus(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver userLocation = db.Driver.Find(id);
            if (userLocation == null)
            {
                return HttpNotFound();
            }
            return View(userLocation);
        }

        // POST: UserLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //        public ActionResult ChangeStatus([Bind(Include = "Id,Status")] UserLocation userLocation)
        public ActionResult ChangeStatus(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                int id = int.Parse(Request.Form["Id"]);
                int status = int.Parse(Request.Form["Status"]);
                var userLocation = db.Driver.Where(o => o.Id == id).Single();
                userLocation.Status = status;
                //db.Entry(userLocation).State = EntityState.Modified;
                db.SaveChanges();
            }
            // return View(userLocation);
            return RedirectToAction("Index");
        }

        // GET: UserLocations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userLocation = db.Driver.Find(id);
            if (userLocation == null)
            {
                return HttpNotFound();
            }
            return View(userLocation);
        }

        // POST: UserLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var userLocation = db.Driver.Find(id);
            db.Driver.Remove(userLocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Get Full Address from google api
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns>string</returns>
        public string GeocoderLocation(string lat, string lng)
        {
            string address = "";
            string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false", lat, lng);
            WebRequest request = WebRequest.Create(requestUri);

            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    XDocument document = XDocument.Load(new StreamReader(stream));

                    XElement adDressElement = document.Descendants("formatted_address").FirstOrDefault();

                    if (adDressElement != null)
                    {
                        address = adDressElement.Value;
                    }
                }
            }

            return address;
        }

        public ActionResult UserLocationDetail(int Id)
        {
            try
            {
                List<UserLocationViewModel> listUserLocation = new List<UserLocationViewModel>();// db.UserLocation.Where(o => o.Id == Id).Single();

                //listUserLocation = (from location in db.UserLocation join info in db.UserInfo on location.UserName equals info.UserName select new UserLocationViewModel { Id = location.Id, UserName = location.UserName, FullName = info.FullName, LoaiXe = info.CarType, TaiTrong = location.TaiTrong.ToString(), CurrentLocation = "", GPS = (location.Latitude + "," + location.Longitude), LastUpdate = location.UpdateAt, Status = location.Status.ToString(), Lat = location.Latitude.ToString(), Lng = location.Longitude.ToString() }).Where(o => o.Id == Id).Take(1).ToList();

                //foreach (UserLocationViewModel item in listUserLocation)
                //{
                //    item.CurrentLocation = GeocoderLocation(item.Lat, item.Lng);
                //}

                //ViewBag.Lat = listUserLocation[0].Lat;
                //ViewBag.Lng = listUserLocation[0].Lng;
                //ViewBag.Currentlocation = listUserLocation[0].CurrentLocation;
                return PartialView("UserLocationDetail", listUserLocation);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
