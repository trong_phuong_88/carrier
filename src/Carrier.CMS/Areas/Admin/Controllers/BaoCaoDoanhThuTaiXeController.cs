﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using Carrier.Repository;
using Carrier.Utilities;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_ADMIN,PublicConstant.ROLE_SUPPER_ADMIN)]
    public class BaoCaoDoanhThuTaiXeController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/BaoCaoDoanhThuTaiXe
        public ActionResult Index()
        {
            string txtFromDate = null;
            string txtToDate = null;
            if (Request.Form["txtFromDate"]!=null && Request.Form["txtFromDate"].Length>0)
            {
                txtFromDate = Request.Form["txtFromDate"];
            }
            if (Request.Form["txtToDate"] != null && Request.Form["txtToDate"].Length > 0)
            {
                txtToDate = Request.Form["txtToDate"];
            }
            if (txtFromDate==null)
            {
                txtFromDate = "2017-01-01 00:00:00";
            }
            if (txtToDate == null)
            {
                txtToDate = DateTime.Now.ToString();
            }

            List<SP_Report_Profit_For_Drivers_Admin_Result> model = new List<SP_Report_Profit_For_Drivers_Admin_Result>();
            var drivers = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords();
            foreach(var d in drivers)
            {
                var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = d.UserId };
                var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = txtFromDate };
                var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = txtToDate };
                var profit = _unitOfWork.GetRepositoryInstance<SP_Report_Profit_For_Drivers_Admin_Result>().GetResultBySqlProcedure("SP_Report_Profit_For_Drivers_Admin @UserId,@FromDate, @ToDate", sqlUser, sqlFromDate, sqlToDate).FirstOrDefault();
                model.Add(profit);
            }
            return View(model);
        }
    }
}