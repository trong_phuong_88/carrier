﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class DriverCarController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/DriverCar
        public ActionResult Index(int DriverId = 0, int OrganizationId = 0)
        {
            if (DriverId == 0)
            {
                List<Organization> listOrganization = new List<Organization>();
                listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();
                IEnumerable<SelectListItem> listCOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listCOrganizationSelect;
            }

            if (DriverId > 0)
            {
                List<SP_GetCarByDriverId_Result> listCar = new List<SP_GetCarByDriverId_Result>();
                int driverId = DriverId;
                listCar = _unitOfWork.GetRepositoryInstance<SP_GetCarByDriverId_Result>().GetResultBySqlProcedure("SP_GetAllCarByDriverId @OrganizationId,@DriverId", OrganizationId, driverId).ToList();

                return View(listCar);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult GetListDriverByOrganization(int Id)
        {
            List<Driver> listDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(o => o.OrganizationId == Id).OrderBy(o => o.Name).ToList();
            SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
            return Json(listDriverReturn);
        }

        //[HttpPost]
        //public ActionResult GetListCarByDriverId(int Id)
        //{
        //    List<DriverCar> listDriver = _unitOfWork.GetRepositoryInstance<DriverCar>().GetListByParameter(o => o.DriverId == Id).ToList();
        //    SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
        //    return Json(listDriverReturn);
        //}

        //[HttpPost]
        //public ActionResult GetListDriverByCarId(int Id)
        //{
        //    List<DriverCar> listDriver = _unitOfWork.GetRepositoryInstance<DriverCar>().GetListByParameter(o => o.CarId == Id).ToList();
        //    SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);

        //    return Json(listDriverReturn);
        //}

        [HttpPost]
        public JsonResult UpdateDriverCar(string listId, int driverId)
        {
            DriverCar driverCar = new DriverCar();
            // xóa tất cả các bản ghi cũ đã gán cho tài xế đó -> update lại
            if (_unitOfWork.GetRepositoryInstance<DriverCar>().GetListByParameter(o => o.DriverId == driverId).Count() > 0)
            {
                _unitOfWork.GetRepositoryInstance<DriverCar>().RemoveByWhereClause(o => o.DriverId == driverId);
                _unitOfWork.SaveChanges();
            }

            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            bool isExits = false;
            foreach (string id in liststrId)
            {
                if (id.Length > 0)
                {
                    try
                    {
                        int carId = int.Parse(id);
                        driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(o => o.CarId == carId && o.DriverId != driverId);
                        if (driverCar == null)
                        {
                            driverCar = new DriverCar();
                            driverCar.CarId = carId;
                            driverCar.DriverId = driverId;
                            _unitOfWork.GetRepositoryInstance<DriverCar>().Add(driverCar);
                            //_unitOfWork.SaveChanges();
                        }
                        else
                        {
                            isExits = true;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            if (isExits == true)
            {
                Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id != driverCar.DriverId);
                Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id != driverCar.CarId);
                if (driver != null && car != null)
                {
                    TempData["error"] = "xe " + car.License + " đã được gán cho tài xế " + driver.Name + " vui lòng chọn xe khác!";
                }
                else
                {
                    TempData["error"] = "gán xe thất bại 1 trong só xe đã được gán cho tài xế khác vui lòng chọn xe khác!";
                }

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            _unitOfWork.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckTaiXeMoi()
        {
            int count = 0;
            try
            {
                count = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(x => x.Status == PublicConstant.DRIVER_STATUS_PENDING).Count();
            }
            catch (Exception)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }

            return Json(count, JsonRequestBehavior.AllowGet);
        }
    }
}
