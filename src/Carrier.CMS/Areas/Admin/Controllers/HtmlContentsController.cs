﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using Carrier.Utilities;
using Carrier.Repository;
using Carrier.CMS.Models;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class HtmlContentsController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/HtmlContents
        public ActionResult Index()
        {
            var lstData = db.HtmlContent.ToList();
            List<HtmlContentsViewModel> lstResult = new List<HtmlContentsViewModel>();
            foreach (var item in lstData)
            {
                var viewModel = new HtmlContentsViewModel();
                viewModel.Description = item.Description;
                viewModel.PageId = item.PageId.Value;
                viewModel.Id = item.Id;

                lstResult.Add(viewModel);
            }
            return View(lstResult.ToList());
        }

        // GET: Admin/HtmlContents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            if (htmlContent == null)
            {
                return HttpNotFound();
            }
            return View(htmlContent);
        }

        // GET: Admin/HtmlContents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/HtmlContents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HtmlContentsViewModel htmlContent)
        {
            if (ModelState.IsValid)
            {
                var obj = new HtmlContent();
                obj.Id = htmlContent.Id;
                obj.PageId = htmlContent.PageId;
                obj.Description = htmlContent.Description;
                db.HtmlContent.Add(obj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(htmlContent);
        }

        // GET: Admin/HtmlContents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            if (htmlContent == null)
            {
                return HttpNotFound();
            }
            HtmlContentsViewModel viewModel = new HtmlContentsViewModel();
            viewModel.PageId = htmlContent.PageId.Value;
            viewModel.Id = htmlContent.Id;
            viewModel.Description = htmlContent.Description;
            return View(viewModel);
        }

        // POST: Admin/HtmlContents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HtmlContentsViewModel htmlContent)
        {
            if (ModelState.IsValid)
            {
                var old = db.HtmlContent.Find(htmlContent.Id);
                old.Description = htmlContent.Description;
                old.PageId = htmlContent.PageId;
                db.Entry(old).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(htmlContent);
        }

        // GET: Admin/HtmlContents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            if (htmlContent == null)
            {
                return HttpNotFound();
            }
            return View(htmlContent);
        }

        // POST: Admin/HtmlContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HtmlContent htmlContent = db.HtmlContent.Find(id);
            db.HtmlContent.Remove(htmlContent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
