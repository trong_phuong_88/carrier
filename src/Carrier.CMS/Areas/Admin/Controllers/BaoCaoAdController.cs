﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class BaoCaoAdController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/BaoCaoAd
        public ActionResult BaoCaoDoanhThuAd(FormCollection collection)
        {
            List<SP_GetAll_Enterprise_Result> totalView = new List<SP_GetAll_Enterprise_Result>();
            totalView = _unitOfWork.GetRepositoryInstance<SP_GetAll_Enterprise_Result>().GetResultBySqlProcedure("SP_GetAll_Enterprise").ToList();
            if (collection["txtFromDate"] != null && collection["txtToDate"] != null)
            {
                ViewBag.FromDate = collection["txtFromDate"];
                ViewBag.ToDate = collection["txtToDate"];
            }
            return View(totalView);
        }
        public JsonResult ProfitByUserId(string id, string fromDate = "", string toDate = "")
        {
            UserInfoes userInfoes = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(id));
            if (userInfoes != null)
            {
                var userId = userInfoes.ApplicationUserID;
                SP_Profit_By_Ad_Result profitUser = new SP_Profit_By_Ad_Result();
                var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
                if (fromDate.Length > 0 && toDate.Length > 0)
                {
                    var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Parse(fromDate, DateTimeFormatInfo.InvariantInfo) };
                    var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Parse(toDate, DateTimeFormatInfo.InvariantInfo) };
                    profitUser = _unitOfWork.GetRepositoryInstance<SP_Profit_By_Ad_Result>().GetResultBySqlProcedure("SP_Profit_By_Ad  @UserId, @FromDate, @ToDate", sqlUserId, sqlFromDate, sqlToDate).FirstOrDefault();
                }
                else
                {
                    var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
                    var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
                    profitUser = _unitOfWork.GetRepositoryInstance<SP_Profit_By_Ad_Result>().GetResultBySqlProcedure("SP_Profit_By_Ad  @UserId, @FromDate, @ToDate", sqlUserId, sqlFromDate, sqlToDate).FirstOrDefault();
                }
                return Json(profitUser, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }
    }
}