﻿using Carrier.CMS.Common;
using Carrier.CMS.Hubs;
using Carrier.Models.Entities;
using Carrier.PushNotification;
using Carrier.Repository;
using Carrier.Utilities;
using CMS.Carrier.Areas.Enterprise.Controllers;
using CMS.Carrier.Common;
using EntityFramework.Utilities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class ChoKhopLenhController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/ChoKhopLenh
        [Authorize]
        public ActionResult Index()
        {
            SQLDependencyInit();
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">1: Khop lenh tu dong,0: Khop lenh bang tay</param>
        /// <returns></returns>
        public ActionResult ListOrderTracking(int type)
        {
            var userId = User.Identity.GetUserId();
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = "" };
            var sqlType = new SqlParameter("@Type", System.Data.SqlDbType.NVarChar) { Value = type };

            var listData = _unitOfWork.GetRepositoryInstance<SP_GetAllDataTracking_Result>().GetResultBySqlProcedure("SP_GetAllDataTracking @UserId,@Type", sqlUserId, sqlType);
            return PartialView("_ListViewTrackingOrder", listData);
        }
        public void SQLDependencyInit()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;

            string listenQuery = @"SELECT [OrderId] ,[Updated_At],[Status],[OwnerId] ,[DriverId] ,[EvidencePath] FROM [dbo].[OrderTracking]";
            string listQerydontay = @"SELECT [Id],[Status],[CreateAt] FROM [dbo].[Order]";
            // Create instance of the DB Listener
            DatabaseChangeListener changeListener = new DatabaseChangeListener(connectionString);

            // Define what to do when changes were detected
            changeListener.OnChange += () =>
            {
                //ChatHub.SendMessages();
                NotificationHub.UpdateMatchOrder();
                // Reattach listener event - DO NOT TOUCH!
                changeListener.Start(listenQuery);
                changeListener.Start(listQerydontay);
            };

            // Start listening for changes 
            changeListener.Start(listenQuery);
            changeListener.Start(listQerydontay);
        }
        public ActionResult OrderDetail(int Id)
        {
            Order order = db.Order.Where(o => o.Id == Id).Single();
            return PartialView("_OrderDetail", order);
        }
        public ActionResult UserInfoDetail(string DriverId)
        {
            Driver userInfo = db.Driver.Where(o => o.UserId == DriverId).Single();
            return PartialView("_UserInfo", userInfo);
        }

        public ActionResult DeleteOrder(int id)
        {
            using (var db = new Carrier3Entities())
            {
                OrderTracking ot = db.OrderTracking.Find(id);
                if (ot != null)
                {
                    EFBatchOperation.For(db, db.OrderTracking).Where(b => b.OrderId == id).Delete();
                    var obj = db.Order.Where(x => x.Id == ot.OrderId).FirstOrDefault();
                    db.AttachAndModify(obj).Set(x => x.Status, 2);
                    db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            }
        }

        [HttpPost]
        public JsonResult DeleteOrderId(string listId)
        {
            OrderTracking order;
            //Order orderView;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    order = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefault(int.Parse(id));
                    if (order != null)
                    {
                        order.Status = 3;
                        _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(order);
                        //_unitOfWork.GetRepositoryInstance<OrderTracking>().Remove(order);
                        _unitOfWork.SaveChanges();
                        return Json("OK", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public List<ListDriver> GetListDriver(string keysearch = "")
        {
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            List<ListDriver> lstDriver = new List<ListDriver>();
            //var listCT = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == organizationId);
            List<Driver> listCT = new List<Driver>();
            if (keysearch != "")
            {
                listCT = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(o => (o.Name.Contains(keysearch) || o.Phone.Contains(keysearch))).ToList();
            }
            else
            {
                listCT = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Take(20).ToList();
            }
            foreach (var item in listCT)
            {
                lstDriver.Add(new ListDriver() { Id = item.Id, Name = item.Name, Phone = item.Phone, UserId = item.UserId });
            }
            return lstDriver;
        }

        public void PushMessageForDriver(string driverId, long orderId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            if (result.Equals("Success"))
            {
                long orderId = long.Parse(obj2);
                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
                if (old != null)
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                    old.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                }
                else
                {
                    var obj = new OrderTracking();
                    obj.OrderId = orderId;
                    obj.DriverId = obj1;
                    obj.OwnerId = User.Identity.GetUserId();
                    obj.Created_At = DateTime.Now;
                    obj.Updated_At = DateTime.Now;
                    obj.Created_By = User.Identity.GetUserId();
                    obj.Status = PublicConstant.ORDER_PENDING;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                }
                _unitOfWork.SaveChanges();
            }
            else
            {
                // Show message 
            }
        }
        public JsonResult LoadListDriver(int id, string keysearch)
        {
            string strCode = "";
            List<ListDriver> lstDriver = GetListDriver(keysearch);
            foreach (var item in lstDriver)
            {
                strCode += "<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-4\"><button type = \"button\" style=\"border-color:white; width:200px;text-align:left \" class =\"btn btn-default col-md-3 btn-primary-pay\" data-value=\"" + item.Phone + "\"  class =\"btn btn-default\" id=\"" + item.Id + "\"  onClick =\"ClickDriver('" + item.Id + "')\" value=\"" + item.UserId + "\"><i style=\"color:#f7e60a\" class=\"fa fa-car\" aria-hidden=\"true\"></i>&nbsp;" + item.Name + "</button>" + "<br /></div>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SendNewOrderToDriver(long orderId, string driverId)
        {
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault((int)orderId);
            var car = (from c in db.Car
                       join dc in db.DriverCar on c.Id equals dc.CarId
                       join d in db.Driver on dc.DriverId equals d.Id
                       where d.UserId == driverId
                       select new
                       {
                           c.Id
                       }).FirstOrDefault();
            if (car == null)
            {
                return Json("Tài xế chưa có xe", JsonRequestBehavior.AllowGet);
            }
            else
            {
                PushMessageForDriver(driverId, orderId);

                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(driverId));
                if (old != null)
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                    old.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                }
                else
                {
                    var obj = new OrderTracking();
                    obj.OrderId = orderId;
                    obj.DriverId = driverId;
                    obj.OwnerId = order.Created_By;
                    obj.Created_At = DateTime.Now;
                    obj.Updated_At = DateTime.Now;
                    obj.Created_By = User.Identity.GetUserId();
                    obj.Status = PublicConstant.ORDER_PENDING;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                }
                _unitOfWork.SaveChanges();
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}