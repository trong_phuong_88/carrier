﻿using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Data.Entity;
using Carrier.CMS.Common;
using Carrier.Utilities;
using Carrier.Models.Entities;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using CMS.Carrier.Common;
using Carrier.Algorithm.Entity;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class HomeBackendController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private LogisticDataEntities db = new LogisticDataEntities();
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public UserInfoes UserInfo
        {
            get { return _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId)); }
        }
        // GET: Admin/HomeBackend
        public ActionResult Index()
        {
            var lstCity = db.City.ToList();
            List<SelectListItem> ls = new List<SelectListItem>();
            foreach (var temp in lstCity)
            {
                ls.Add(new SelectListItem() { Text = (temp as City).Name, Value = (temp as City).ItemID.ToString() });
            }
            ViewBag.ListCity = new SelectList(ls, "Id", "Name");
            ViewBag.Lat = "20.9978737";
            ViewBag.Lng = "105.795412";
            return View();
        }

        public ActionResult IndexBackend()
        {
            int totalUser = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetAllRecordsCount();
            int TotalOrder = _unitOfWork.GetRepositoryInstance<Order>().GetAllRecordsCount();
            int TotalDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecordsCount();
            int TotalOrderRunning = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(o => o.Status == PublicConstant.STATUS_PENDING).Count();
            int TotalOrderSuccess = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(o => o.Status == PublicConstant.STATUS_ACTIVE).Count();

            ViewBag.totalUser = totalUser;
            ViewBag.TotalOrder = TotalOrder;
            ViewBag.TotalDriver = TotalDriver;
            ViewBag.TotalOrderRunning = TotalOrderRunning;
            ViewBag.TotalOrderSuccess = TotalOrderSuccess;


            return View();
        }

        public List<ListCity> GetListCity()
        {
            List<ListCity> lstCity = new List<ListCity>();
            var listCT = db.City.ToList();
            foreach (var item in listCT)
            {
                lstCity.Add(new ListCity() { Name = item.Name, CityCode = item.CityCode });
            }
            return lstCity;
        }
        public JsonResult LoadListCity()
        {
            string strCode = "";
            List<ListCity> lstCity = GetListCity();
            foreach (var item in lstCity)
            {
                strCode += "<option value = \"" + item.Name + "\" id=\"" + item.CityCode + "\">" + item.Name + "</option>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }

        #region Fetch data for Map
        public JsonResult SearchDriver(string orderCode)
        {
            var sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = "" };
            var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };

            var lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId_Admin @userId,@orderCode",
                 sqlParentId, sqlOrderCode).ToList();
            var dictData = new Dictionary<string, object>() { { "count", lstSearchOrderTracking.Count() }, { "drivers", lstSearchOrderTracking } };
            return Json(dictData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchOrderTracking(string orderCode)
        {
            var sqlLat = new SqlParameter("@lat", System.Data.SqlDbType.Float) { Value = DBNull.Value };
            var sqlLng = new SqlParameter("@long", System.Data.SqlDbType.Float) { Value = DBNull.Value };
            var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = DBNull.Value };
            var sqlStatus = new SqlParameter("@status", System.Data.SqlDbType.Int) { Value = DBNull.Value };
            var sqlParentId = new SqlParameter("@parrentId", System.Data.SqlDbType.Int) { Value = -1 };
            var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
            var orderTracking = _unitOfWork.GetRepositoryInstance<SearchOrderTrackingByDistance_Result>().GetResultBySqlProcedure("SearchOrderTrackingByDistance @lat,@long,@distance,@status,@parrentId,@orderCode",
                sqlLat, sqlLng, sqlDistance, sqlStatus, sqlParentId, sqlOrderCode).FirstOrDefault();
            return Json(orderTracking, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchOrder(int distance, int type)
        {
            var lstOrders = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(x => x.Status == PublicConstant.STATUS_ACTIVE);
            var dictData = new Dictionary<string, object>() { { "count", lstOrders.Count() }, { "Data", lstOrders } };
            return Json(dictData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        ///// <summary>
        ///// lấy thông báo đẩy về client
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult GetMessageOrder()
        //{
        //    MessageRespository messageService = new MessageRespository();
        //    return PartialView("~/Views/Shared/_MessagesListOrder.cshtml", messageService.GetListMessageOrder(User.Identity.GetUserId(), PublicConstant.ROLE_ADMIN));
        //}

        /// <summary>
        /// lấy thông báo đẩy về client
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageAccount()
        {
            MessageRespository messageService = new MessageRespository();
            return PartialView("~/Views/Shared/_MessagesListAccount.cshtml", messageService.GetListMessageAccount(User.Identity.GetUserId(), PublicConstant.ROLE_ADMIN));
        }

        public ActionResult GetMessageOrderTracking()
        {
            MessageRespository orderService = new MessageRespository();
            return PartialView("~/Views/Shared/_MessageListOrdertracking.cshtml", orderService.GetListMessageOrderTracking(User.Identity.GetUserId(), PublicConstant.ROLE_ADMIN));
        }
    }
    public class ListCity
    {
        public string Name { get; set; }
        public string CityCode { get; set; }
    }
}