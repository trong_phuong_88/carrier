﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using System.IO;
using Carrier.CMS.Models;
using System.Web.Http;
using Carrier.Utilities;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class ArticlesController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        
        // GET: Articles
        public ActionResult Index()
        {
            var articles = db.Articles.Include(a => a.Category).OrderByDescending(o => o.Id);
            return View(articles.ToList());
        }

        // GET: Articles/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            return View(articles);
        }

        // GET: Articles/Create
        public ActionResult Create()
        {
            ViewBag.Status = Utitlitys.StatusToList();

            List<Category> listITem = new List<Category>();
            List<Category> listCateParent;
            List<Category> listCateChild;
            Category dopDowItem;
            listCateParent = db.Category.Where(o => o.ParentId == null).OrderBy(o => o.Id).ToList();

            foreach (Category item in listCateParent)
            {
                dopDowItem = new Category();
                listCateChild = db.Category.Where(o => o.ParentId == item.Id).ToList();
                if (listCateChild.Count > 0)
                {
                    listITem.Add(item);
                    foreach (Category itemCchild in listCateChild)
                    {
                        itemCchild.Name = "---- " + itemCchild.Name;
                        listITem.Add(itemCchild);
                    }
                }
                else
                {
                    listITem.Add(item);
                }
            }

            ViewBag.CategoryId = new SelectList(listITem, "Id", "Name");
            return View();
        }

        // POST: Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(ArticleViewModel articles, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var fileName = string.Empty;
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            if (file != null && file.ContentLength > 0)
                            {
                                //kiem tra file extension lan nua
                                List<string> excelExtension = new List<string>();
                                excelExtension.Add(".png");
                                excelExtension.Add(".jpg");
                                excelExtension.Add(".gif");
                                var extension = Path.GetExtension(file.FileName);
                                if (!excelExtension.Contains(extension.ToLower()))
                                {
                                    return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                                }
                                //kiem tra dung luong file
                                var fileSize = file.ContentLength;
                                var fileMB = (fileSize / 1024f) / 1024f;
                                if (fileMB > 5)
                                {
                                    return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                                }
                                // luu ra dia
                                fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                //fileName = fileName + "_" + user.UserID + extension;
                                fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
                                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/TinTuc"), fileName);
                                file.SaveAs(physicalPath);
                                articles.articleInfo.ThumbURL = fileName;
                            }
                        }
                    }
                    articles.articleInfo.Summary = articles.Summary;
                    articles.articleInfo.Description = articles.Description;
                    articles.articleInfo.Summary_EN = articles.Summary_EN;
                    articles.articleInfo.Description_EN = articles.Description_EN;
                    articles.articleInfo.CreateAt = DateTime.Now;
                    articles.articleInfo.CreateBy = User.Identity.Name;
                    articles.articleInfo.Category = db.Category.Find(articles.articleInfo.CategoryId);
                    db.Articles.Add(articles.articleInfo);
                    db.SaveChanges();
                    TempData["info"] = "Thêm bài viết thành công";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi thêm bài viết: " + ex.Message;
                }
            }

            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", articles.articleInfo.CategoryId);
            return View(articles);
        }

        // GET: Articles/Edit/5
        public ActionResult Edit(long? id)
        {
            ViewBag.Status = Utitlitys.StatusToList();
            //ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }

            List<Category> listITem = new List<Category>();
            List<Category> listCateParent;
            List<Category> listCateChild;
            Category dopDowItem;
            listCateParent = db.Category.Where(o => o.ParentId == null).OrderBy(o => o.Id).ToList();

            foreach (Category item in listCateParent)
            {
                dopDowItem = new Category();
                listCateChild = db.Category.Where(o => o.ParentId == item.Id).ToList();
                if (listCateChild.Count > 0)
                {
                    listITem.Add(item);
                    foreach (Category itemCchild in listCateChild)
                    {
                        itemCchild.Name = "---- " + itemCchild.Name;
                        listITem.Add(itemCchild);
                    }
                }
                else
                {
                    listITem.Add(item);
                }
            }

            ViewBag.CategoryId = new SelectList(listITem, "Id", "Name", articles.CategoryId);
            ArticleViewModel viewmodel = new ArticleViewModel();
            viewmodel.articleInfo = articles;
            viewmodel.Description = articles.Description;
            viewmodel.Summary = articles.Summary;
            viewmodel.Description_EN = articles.Description_EN;
            viewmodel.Summary_EN = articles.Summary_EN;
            return View(viewmodel);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(ArticleViewModel articles, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var old = db.Articles.Find(articles.articleInfo.Id);
                    var fileName = string.Empty;
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            if (file != null && file.ContentLength > 0)
                            {
                                try
                                {
                                    //kiem tra file extension lan nua
                                    List<string> excelExtension = new List<string>();
                                    excelExtension.Add(".png");
                                    excelExtension.Add(".jpg");
                                    excelExtension.Add(".gif");
                                    var extension = Path.GetExtension(file.FileName);
                                    if (!excelExtension.Contains(extension.ToLower()))
                                    {
                                        TempData["error"] = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png";
                                        return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                                    }
                                    //kiem tra dung luong file
                                    var fileSize = file.ContentLength;
                                    var fileMB = (fileSize / 1024f) / 1024f;
                                    if (fileMB > 5)
                                    {
                                        TempData["error"] = "Dung lượng ảnh không được lớn hơn 5MB";
                                        return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                                    }

                                    if (file.FileName != null && file.FileName.Length > 0)
                                    {
                                        // luu ra dia
                                        fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                        //fileName = fileName + extension;
                                        fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
                                    }

                                    var physicalPath = Path.Combine(Server.MapPath("~/Uploads/TinTuc"), fileName);
                                    try
                                    {
                                        file.SaveAs(physicalPath);
                                        System.IO.File.Delete(Server.MapPath("~/Uploads/TinTuc/" + old.ThumbURL));
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    articles.articleInfo.ThumbURL = fileName;
                                    old.ThumbURL = articles.articleInfo.ThumbURL;
                                }
                                catch (Exception ex)
                                {
                                    TempData["error"] = "Lỗi upload ảnh bài viết: " + ex.Message;
                                }
                            }
                        }
                    }
                    articles.articleInfo.Category = db.Category.Find(articles.articleInfo.CategoryId);
                    old.Category = articles.articleInfo.Category;
                    old.Status = articles.articleInfo.Status;
                    old.Title = articles.articleInfo.Title;
                    old.Title_EN = articles.articleInfo.Title_EN;
                    old.UpdateAt = DateTime.Now;
                    old.UpdateBy = User.Identity.Name;
                    old.Summary = articles.Summary;
                    old.Description = articles.Description;
                    old.Summary_EN = articles.Summary_EN;
                    old.Description_EN = articles.Description_EN;
                    db.Entry(old).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    TempData["info"] = "Cập nhật vài viết thành công ";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi thêm bài viết: " + ex.Message;
                }
            }
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", articles.articleInfo.CategoryId);
            return View(articles);
        }

        // GET: Articles/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            return View(articles);
        }

        // POST: Articles/Delete/5
        [System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Articles articles = db.Articles.Find(id);
            db.Articles.Remove(articles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
