﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.Repository;
using PagedList;
using Carrier.CMS.Common;
using Carrier.Utilities;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_ADMIN, PublicConstant.ROLE_SUPPER_ADMIN)]
    public class CustomersController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Enterprise/Customers
        public ActionResult Index(string currentFilter, string searchString, int? page, int? pagsiz)
        {
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                List<Organization> listOrganization = new List<Organization>();
                listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();
                IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listOrganizationSelect;
            }

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<Customer> result = _unitOfWork.GetRepositoryInstance<Customer>().GetAllRecords().Where(o => o.Created_By == User.Identity.Name).OrderByDescending(o => o.Name).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                result = result.Where(o => o.Name.Contains(searchString) || o.Phone.Contains(searchString)).OrderByDescending(o => o.Name).ToList();
            }

            int pageSize = (pagsiz ?? 5);
            ViewBag.PageSize = pageSize;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: Enterprise/Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = _unitOfWork.GetRepositoryInstance<Customer>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Enterprise/Customers/Create
        public ActionResult Create()
        {
            //List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //ViewBag.ListOrganization = listOrganization;

            return View();
        }

        // POST: Enterprise/Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            Customer customer = new Customer();
            if (ModelState.IsValid)
            {
                customer.Name = form.Get("Name") != null ? form.Get("Name") : "";
                customer.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                customer.Sex = form.Get("Sex") == "1" ? true : false;
                customer.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";
                customer.Note = form.Get("Note") != null ? form.Get("Note") : "";
                customer.Created_At = DateTime.Now;
                customer.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                customer.Created_By = User.Identity.Name;

                _unitOfWork.GetRepositoryInstance<Customer>().Add(customer);
                _unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: Enterprise/Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = _unitOfWork.GetRepositoryInstance<Customer>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            //List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //ViewBag.ListOrganization = listOrganization;

            return View(customer);
        }

        // POST: Enterprise/Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Customer customer = _unitOfWork.GetRepositoryInstance<Customer>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (ModelState.IsValid)
            {
                if (customer != null)
                {
                    customer.Name = form.Get("Name") != null ? form.Get("Name") : "";
                    customer.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                    customer.Sex = form.Get("Sex") == "1" ? true : false;
                    customer.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";
                    customer.Note = form.Get("Note") != null ? form.Get("Note") : "";
                    customer.Updated_At = DateTime.Now;
                    customer.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                    customer.Updated_By = User.Identity.Name;

                    //_unitOfWork.GetRepositoryInstance<Customer>().Update(customer);
                    _unitOfWork.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(customer);
        }

        [HttpPost]
        public JsonResult DeleteCustomers(string listId)
        {
            Customer customers;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    customers = _unitOfWork.GetRepositoryInstance<Customer>().GetFirstOrDefault(int.Parse(id));
                    _unitOfWork.GetRepositoryInstance<Customer>().Remove(customers);
                    _unitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public string GetOrganizationName(int? id)
        {
            string name = "";
            if (id != null)
            {
                Organization product = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefault(int.Parse(id.ToString()));
                if (product != null)
                {
                    name = product.Name;
                }
            }

            return name;
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult GetDriverByOrganizationId(int Id)
        {
            List<Driver> listDriver = new List<Driver>();
            listDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(m => m.OrganizationId == Id).ToList();
            SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
            return Json(listDriverReturn);
        }
    }
}
