﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Common;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN,PublicConstant.ROLE_ADMIN)]
    public class MessagesTemplateController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/MessagesTemplate
        public ActionResult Index()
        {
            var msgt = _unitOfWork.GetRepositoryInstance<MessagesTemplates>().GetAllRecords().ToList();
            return View(msgt.OrderByDescending(x=>x.Id));
        }

        //// GET: Admin/MessagesTemplate/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    MessagesTemplates messagesTemplates = db.MessagesTemplates.Find(id);
        //    if (messagesTemplates == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(messagesTemplates);
        //}
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessagesTemplates messagesTemplates = _unitOfWork.GetRepositoryInstance<MessagesTemplates>().GetFirstOrDefaultByParameter(x => x.Id == id);
            if (messagesTemplates == null)
            {
                return HttpNotFound();
            }
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ViewBag.CreateBy = userManager.FindById(messagesTemplates.Created_By).UserName;
            if (messagesTemplates.Updated_By != null)
            {
                ViewBag.UpdateBy = userManager.FindById(messagesTemplates.Updated_By).UserName;
            }
            return View(messagesTemplates);
        }

        // GET: Admin/MessagesTemplate/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/MessagesTemplate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,TemplateType,TemplateContent,Created_At,Updated_At,Created_By,Updated_By")] MessagesTemplates messagesTemplates)
        //{
        //    if (ModelState.IsValid)
        //    {               
        //        _unitOfWork.GetRepositoryInstance<MessagesTemplates>().Add(messagesTemplates);
        //        _unitOfWork.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(messagesTemplates);
        //}
       
        [ValidateInput(false)]
        public ActionResult CreateTinNhanMau([Bind(Include ="TemplateType,TemplateContent")] MessagesTemplates messagesTemplates)
        {
            if(ModelState.IsValid)
            {
                messagesTemplates.Created_At = DateTime.Now;
                messagesTemplates.Created_By = User.Identity.GetUserId();
                _unitOfWork.GetRepositoryInstance<MessagesTemplates>().Add(messagesTemplates);
                _unitOfWork.SaveChanges();
                return Json("OK",JsonRequestBehavior.AllowGet);
            }
            return Json("error", JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/MessagesTemplate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessagesTemplates messagesTemplates = _unitOfWork.GetRepositoryInstance<MessagesTemplates>().GetFirstOrDefaultByParameter(x=>x.Id==id);
            if (messagesTemplates == null)
            {
                return HttpNotFound();
            }
            return View(messagesTemplates);
        }

        // POST: Admin/MessagesTemplate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,TemplateType,TemplateContent,Created_At,Updated_At,Created_By,Updated_By")] MessagesTemplates messagesTemplates)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(messagesTemplates).State = System.Data.Entity.EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(messagesTemplates);
        //}
        [ValidateInput(false)]
        public JsonResult EditTinNhanMau([Bind(Include = "Id,TemplateType,TemplateContent")] MessagesTemplates messagesTemplates)
        {
            if(ModelState.IsValid)
            {
                MessagesTemplates messagesTemplates1 = _unitOfWork.GetRepositoryInstance<MessagesTemplates>().GetFirstOrDefaultByParameter(x => x.Id == messagesTemplates.Id);
                messagesTemplates1.TemplateType = messagesTemplates.TemplateType;
                messagesTemplates1.TemplateContent = messagesTemplates.TemplateContent;
                messagesTemplates1.Updated_At = DateTime.Now;
                messagesTemplates1.Updated_By = User.Identity.GetUserId();
                _unitOfWork.GetRepositoryInstance<MessagesTemplates>().Update(messagesTemplates1);
                _unitOfWork.SaveChanges();
                return Json("OK",JsonRequestBehavior.AllowGet);
            }
            return Json("error", JsonRequestBehavior.AllowGet);
        }
        //// GET: Admin/MessagesTemplate/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    MessagesTemplates messagesTemplates = db.MessagesTemplates.Find(id);
        //    if (messagesTemplates == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(messagesTemplates);
        //}

        //// POST: Admin/MessagesTemplate/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    MessagesTemplates messagesTemplates = db.MessagesTemplates.Find(id);
        //    db.MessagesTemplates.Remove(messagesTemplates);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        public ActionResult DeleteTinNhanMau(string listId)
        {
            MessagesTemplates result;
            //UserInfo userInfo;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    result = _unitOfWork.GetRepositoryInstance<MessagesTemplates>().GetFirstOrDefault(int.Parse(id));
                    if (result != null)
                    {
                        _unitOfWork.GetRepositoryInstance<MessagesTemplates>().Remove(result);
                        _unitOfWork.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
