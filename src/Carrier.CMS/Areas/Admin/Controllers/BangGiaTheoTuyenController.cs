﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.Repository;
using System.Data.SqlClient;
using System.Globalization;
using Carrier.Algorithm.Entity;
using Carrier.Algorithm;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using Carrier.Algorithm.Entity.Geo;
using PagedList;
using EntityFramework.Utilities;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class BangGiaTheoTuyenController : Controller
    {
        private LogisticDataEntities db = new LogisticDataEntities();

        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/BangGiaTheoTuyen
        [HttpPost]
        public ActionResult Index(int? Page_No)
        {
            var listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var car_type in listCarType)
            {
                var item = new SelectListItem { Value = car_type.Value, Text = car_type.Name };
                list.Add(item);
            }
            var listTT = db.City.ToList();
            List<SelectListItem> list1 = new List<SelectListItem>();
            foreach (var c in listTT)
            {
                var item = new SelectListItem { Value = c.CityCode, Text = c.Name };
                list1.Add(item);
            }
            ViewBag.listCarType = list;
            ViewBag.listCT = list1;


            List<Carrier.Algorithm.Entity.BangGiaTheoTuyen> result = new List<Carrier.Algorithm.Entity.BangGiaTheoTuyen>();
            var diemDi = Request.Form["txtDiemDi"] != null ? Request.Form["txtDiemDi"].ToString() : "";
            var diemDen = Request.Form["txtDiemDen"] != null ? Request.Form["txtDiemDen"].ToString() : "";
            var tinhthanh = Request.Form["txtTinhThanh"] != null ? Request.Form["txtTinhThanh"].ToString() : "";
            var cartype = Request.Form["txtCarType"] != null ? Request.Form["txtCarType"].ToString() : "";

            var sqldiemdi = new SqlParameter("@diemdi", SqlDbType.NVarChar) { Value = diemDi };
            var sqldiemden = new SqlParameter("@diemden", SqlDbType.NVarChar) { Value = diemDen };
            var sqlTinhThanh = new SqlParameter("@tinhthanh", SqlDbType.NVarChar) { Value = tinhthanh };
            var sqlCarType = new SqlParameter("@cartype", SqlDbType.NVarChar) { Value = cartype };
            result = db.Database.SqlQuery<Carrier.Algorithm.Entity.BangGiaTheoTuyen>("SP_GetAllBangGiaTheoTuyenByParameter @diemdi,@diemden,@tinhthanh,@cartype", sqldiemdi, sqldiemden, sqlTinhThanh, sqlCarType).ToList();
            //return View(result.OrderByDescending(x => x.Id));

            ViewBag.txtDiemDen = diemDen;
            ViewBag.txtTinhThanh = tinhthanh;
            ViewBag.txtDiemDi = diemDi;
            ViewBag.txtCarType = cartype;
            int Size_Of_Page = 100;
            int No_Of_Page = (Page_No ?? 1);
            var result1 = result.ToPagedList(No_Of_Page, Size_Of_Page);
            return View(result1);
        }

        [HttpGet]
        public ActionResult Index(int? Page_No, string txtDiemDi, string txtDiemDen, string txtTinhThanh, string txtCarType)
        {
            var listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var car_type in listCarType)
            {
                var item = new SelectListItem { Value = car_type.Value, Text = car_type.Name };
                list.Add(item);
            }
            var listTT = db.City.ToList();
            List<SelectListItem> list1 = new List<SelectListItem>();
            foreach (var c in listTT)
            {
                var item = new SelectListItem { Value = c.CityCode, Text = c.Name };
                list1.Add(item);
            }
            ViewBag.listCarType = list;
            ViewBag.listCT = list1;

            List<Carrier.Algorithm.Entity.BangGiaTheoTuyen> result = new List<Carrier.Algorithm.Entity.BangGiaTheoTuyen>();
            //var diemDi = Request.QueryString["txtDiemDi"] != null ? Request.Form["txtDiemDi"].ToString() : "";
            //var diemDen = Request.QueryString["txtDiemDen"] != null ? Request.Form["txtDiemDen"].ToString() : "";
            //var tinhthanh = Request.QueryString["txtTinhThanh"] != null ? Request.Form["txtTinhThanh"].ToString() : "";
            //var cartype = Request.QueryString["txtCarType"] != null ? Request.Form["txtCarType"].ToString() : "";
            if (txtDiemDi == null) { txtDiemDi = ""; }
            if (txtDiemDen == null) { txtDiemDen = ""; }
            if (txtCarType == null) { txtCarType = ""; }
            if (txtTinhThanh == null) { txtTinhThanh = ""; }
            var sqldiemdi = new SqlParameter("@diemdi", SqlDbType.NVarChar) { Value = txtDiemDi };
            var sqldiemden = new SqlParameter("@diemden", SqlDbType.NVarChar) { Value = txtDiemDen };
            var sqlTinhThanh = new SqlParameter("@CityFromCode", SqlDbType.NVarChar) { Value = txtTinhThanh };
            var sqlCarType = new SqlParameter("@cartype", SqlDbType.NVarChar) { Value = txtCarType };
            result = db.Database.SqlQuery<Carrier.Algorithm.Entity.BangGiaTheoTuyen>("SP_GetAllBangGiaTheoTuyenByParameter @diemdi,@diemden,@CityFromCode,@cartype", sqldiemdi, sqldiemden, sqlTinhThanh, sqlCarType).ToList();
            //return View(result.OrderByDescending(x => x.Id));

            ViewBag.txtDiemDen = txtDiemDen;
            ViewBag.txtTinhThanh = "";
            ViewBag.txtDiemDi = txtDiemDi;
            ViewBag.txtCarType = txtCarType;
            int Size_Of_Page = 100;
            int No_Of_Page = (Page_No ?? 1);
            var result1 = result.ToPagedList(No_Of_Page, Size_Of_Page);
            return View(result1);
        }

        // GET: Admin/BangGiaTheoTuyen/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Carrier.Algorithm.Entity.BangGiaTheoTuyen bangGiaTheoTuyen = db.BangGiaTheoTuyen.Find(id);
            if (bangGiaTheoTuyen == null)
            {
                return HttpNotFound();
            }
            return View(bangGiaTheoTuyen);
        }

        // GET: Admin/BangGiaTheoTuyen/Create
        public ActionResult Create()
        {
            var listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var cartype in listCarType)
            {
                var item = new SelectListItem { Value = cartype.Value, Text = cartype.Name };
                list.Add(item);
            }
            var listTT = db.City.ToList();
            List<SelectListItem> list1 = new List<SelectListItem>();
            foreach (var c in listTT)
            {
                var item = new SelectListItem { Value = c.Name, Text = c.Name };
                list1.Add(item);
            }
            ViewBag.listCarType = list;
            ViewBag.listCT = list1;
            return View();
        }

        // POST: Admin/BangGiaTheoTuyen/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        public async Task<JsonResult> CreateBangGiaTheoTuyen([Bind(Include = "Id,DiemDi,DiemDen,TinhThanh,TypeCar,Price,SoKM")] Carrier.Algorithm.Entity.BangGiaTheoTuyen bangGiaTheoTuyen)
        {
            if (ModelState.IsValid)
            {
                bool bsuccess = false;
                bangGiaTheoTuyen.DiemDi = bangGiaTheoTuyen.DiemDi;
                bangGiaTheoTuyen.DiemDen = bangGiaTheoTuyen.DiemDen;

                string fromAddress = bangGiaTheoTuyen.DiemDi;
                string toAddress = bangGiaTheoTuyen.DiemDen;
                // Lay đến xã trước
                var wardfromSearch = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromWard().Trim()).ToLower();
                var wardtoSearch = Helpers.RemoveUnicodeCharactor(toAddress.StringFromWard()).ToLower();
                var districtFromSearch = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromDistrict()).ToLower();
                var districtToSearch = Helpers.RemoveUnicodeCharactor(toAddress.StringFromDistrict()).ToLower();
                //var cityFrom = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromCity()).ToLower();
                //var cityTo = Helpers.RemoveUnicodeCharactor(toAddress.StringFromCity()).ToLower();

                bangGiaTheoTuyen.XaDiSearch = wardfromSearch;
                bangGiaTheoTuyen.XaDenSearch = wardtoSearch;
                bangGiaTheoTuyen.HuyenDiSearch = districtFromSearch;
                bangGiaTheoTuyen.HuyenDenSearch = districtToSearch;

                ////Cang Cat Lai, Nguyen Thi Dinh, Cat Lai, Quan 2, Ho Chi Minh, Viet Nam
                var arrFrom = fromAddress.Trim().Split(',');
                string cityFrom = arrFrom[arrFrom.Length - 2].Trim();
                string districtFrom = arrFrom[arrFrom.Length - 3].Trim();
                string wardFrom = arrFrom[arrFrom.Length - 4].Trim();

                string cityFromCode = GetCityCode(cityFrom);
                string districtFromCode = GetDistrictCode(districtFrom, cityFromCode);
                string wardFromCode = GetWardCode(wardFrom, districtFromCode);

                var arrTo = fromAddress.Trim().Split(',');
                string cityTo = arrTo[arrTo.Length - 2].Trim();
                string districtTo = arrTo[arrTo.Length - 3].Trim();
                string wardTo = arrTo[arrTo.Length - 4].Trim();

                string cityToCode = GetCityCode(cityTo);
                string districtToCode = GetDistrictCode(districtTo, cityTo);
                string wardToCode = GetWardCode(wardTo, districtTo);

                bangGiaTheoTuyen.CityFromCode = cityFromCode;
                bangGiaTheoTuyen.DistrictFromCode = districtFromCode;
                bangGiaTheoTuyen.WardFromCode = wardFromCode;
                bangGiaTheoTuyen.CityToIdCode = cityToCode;
                bangGiaTheoTuyen.DistrictToCode = districtToCode;
                bangGiaTheoTuyen.WardToCode = wardToCode;

                #region save
                //GeoResponse geoFromAdd = await Helpers.FetchGeoData(bangGiaTheoTuyen.DiemDi);
                //GeoResponse geoToAdd = await Helpers.FetchGeoData(bangGiaTheoTuyen.DiemDen);
                //if(geoFromAdd != null && geoToAdd != null)
                //{
                //    foreach(Result from in geoFromAdd.results)
                //    {
                //bangGiaTheoTuyen.DiemDiSearch = Utitlitys.RemoveUnicodeCharactor(from.formatted_address);
                //foreach (Result to in geoToAdd.results)
                //{
                //bangGiaTheoTuyen.DiemDenSearch = Utitlitys.RemoveUnicodeCharactor(to.formatted_address);
                //bangGiaTheoTuyen.Version = 1;
                //bangGiaTheoTuyen.Updated_At = Ultilities.CurrentDate();
                //db.BangGiaTheoTuyen.Add(bangGiaTheoTuyen);
                //db.SaveChanges();
                //bsuccess = true;
                //}
                //    }
                //}
                #endregion

                bangGiaTheoTuyen.DiemDiSearch = Utitlitys.RemoveUnicodeCharactor(bangGiaTheoTuyen.DiemDi).Replace(",", string.Empty).Replace(" ", string.Empty);
                bangGiaTheoTuyen.DiemDenSearch = Utitlitys.RemoveUnicodeCharactor(bangGiaTheoTuyen.DiemDen).Replace(",", string.Empty).Replace(" ", string.Empty);
                bangGiaTheoTuyen.Version = 1;
                bangGiaTheoTuyen.Price = bangGiaTheoTuyen.Price;
                bangGiaTheoTuyen.TypeCar = bangGiaTheoTuyen.TypeCar;
                bangGiaTheoTuyen.SoKM = bangGiaTheoTuyen.SoKM;
                bangGiaTheoTuyen.Created_By = User.Identity.Name;
                bangGiaTheoTuyen.Updated_At = Ultilities.CurrentDate();
                db.BangGiaTheoTuyen.Add(bangGiaTheoTuyen);
                db.SaveChanges();
                bsuccess = true;
                if (bsuccess == true)
                {
                    return Json("OK", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }

        public string GetCityCode(string value)
        {
            string code = "";
            try
            {
                City city = db.City.Where(o => o.Name == value).Take(1).SingleOrDefault();
                if (city != null)
                {
                    code = city.CityCode;
                }
            }
            catch (Exception ex)
            {
            }

            return code;
        }
        public string GetDistrictCode(string value, string cityCode)
        {
            string code = "";
            try
            {
                District district = db.District.Where(o => o.Name == value && o.CityCode == cityCode).Take(1).SingleOrDefault();
                if (district != null)
                {
                    code = district.DistrictCode;
                }
            }
            catch (Exception ex)
            {
            }

            return code;
        }
        public string GetWardCode(string value, string districtCode)
        {
            string code = "";
            try
            {
                Ward ward = db.Ward.Where(o => o.Name == value && o.DistrictCode == districtCode).Take(1).SingleOrDefault();
                if (ward != null)
                {
                    code = ward.WardCode;
                }
            }
            catch (Exception ex)
            {
            }

            return code;
        }

        #region Get district, ward by parent

        public ActionResult getDistrictByCityCode(string citycode)
        {
            return Json(db.District.Where(o => o.CityCode == citycode).Select(x => new
            {
                DistrictCode = x.DistrictCode,
                Name = x.Name
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getWardByDistrictCode(string districtcode)
        {
            return Json(db.Ward.Where(o => o.DistrictCode == districtcode).Select(x => new
            {
                WardCode = x.WardCode,
                Name = x.Name
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult ThemTuyenChiTiet()
        {
            var listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            var listTT = db.City.ToList();
            List<SelectListItem> list1 = new List<SelectListItem>();
            foreach (var c in listTT)
            {
                var item = new SelectListItem { Value = c.CityCode, Text = c.Name };
                list1.Add(item);
            }
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var cartype in listCarType)
            {
                var item = new SelectListItem { Value = cartype.Value, Text = cartype.Name };
                list.Add(item);
            }

            ViewBag.listCarType = list;
            ViewBag.listCT = list1;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ThemTuyenChiTiet(FormCollection form)
        {
            BangGiaTheoTuyen bangGiaTheoTuyen = new BangGiaTheoTuyen();
            string cityCodeFrom = form.Get("CityFrom") != null ? form.Get("CityFrom") : "";
            string districtCodeFrom = form.Get("DistrictFrom") != null ? form.Get("DistrictFrom") : "";
            string wardCodeFrom = form.Get("WardFrom") != null ? form.Get("WardFrom") : "";

            string cityCodeTo = form.Get("cityTo") != null ? form.Get("cityTo") : "";
            string districtCodeTo = form.Get("DistrictTo") != null ? form.Get("DistrictTo") : "";
            string wardCodeTo = form.Get("WardTo") != null ? form.Get("WardTo") : "";

            string TypeCar = form.Get("TypeCar") != null ? form.Get("TypeCar") : "";
            string Price = form.Get("Price") != null ? form.Get("Price") : "";
            
            bool bsuccess = false;
            //Cang Cat Lai, Nguyen Thi Dinh, Cat Lai, Quan 2, Ho Chi Minh, Viet Nam
            string fromCityName = GetCityNameByCode(cityCodeFrom);
            string fromDistrictName = GetDistrictNameByCode(districtCodeFrom);
            string fromWardName = GetWardNameByCode(wardCodeFrom);

            string toCityName = GetCityNameByCode(cityCodeTo);
            string toDistrictName = GetDistrictNameByCode(districtCodeTo);
            string toWardName = GetWardNameByCode(wardCodeTo);

            string fromAddress = fromWardName + ", " + fromDistrictName + ", " + fromCityName + ", Viet Nam";
            string toAddress = toWardName + ", " + toDistrictName + ", " + toCityName + ", Viet Nam";

            bangGiaTheoTuyen.DiemDi = fromAddress;
            bangGiaTheoTuyen.DiemDen = toAddress;

            // Lay đến xã trước
            var wardfromSearch = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromWard().Trim()).ToLower();
            var wardtoSearch = Helpers.RemoveUnicodeCharactor(toAddress.StringFromWard()).ToLower();
            var districtFromSearch = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromDistrict()).ToLower();
            var districtToSearch = Helpers.RemoveUnicodeCharactor(toAddress.StringFromDistrict()).ToLower();

            bangGiaTheoTuyen.XaDiSearch = wardfromSearch;
            bangGiaTheoTuyen.XaDenSearch = wardtoSearch;
            bangGiaTheoTuyen.HuyenDiSearch = districtFromSearch;
            bangGiaTheoTuyen.HuyenDenSearch = districtToSearch;

            bangGiaTheoTuyen.CityFromCode = cityCodeFrom;
            bangGiaTheoTuyen.DistrictFromCode = districtCodeFrom;
            bangGiaTheoTuyen.WardFromCode = wardCodeFrom;
            bangGiaTheoTuyen.CityToIdCode = cityCodeTo;
            bangGiaTheoTuyen.DistrictToCode = districtCodeTo;
            bangGiaTheoTuyen.WardToCode = wardCodeTo;

            bangGiaTheoTuyen.DiemDiSearch = Utitlitys.RemoveUnicodeCharactor(fromAddress).Replace(",", string.Empty).Replace(" ", string.Empty);
            bangGiaTheoTuyen.DiemDenSearch = Utitlitys.RemoveUnicodeCharactor(toAddress).Replace(",", string.Empty).Replace(" ", string.Empty);
            bangGiaTheoTuyen.Version = 1;
            bangGiaTheoTuyen.Price = double.Parse(Price);
            bangGiaTheoTuyen.TypeCar = TypeCar;
            bangGiaTheoTuyen.SoKM = bangGiaTheoTuyen.SoKM;
            bangGiaTheoTuyen.Created_By = User.Identity.Name;
            bangGiaTheoTuyen.Updated_At = Ultilities.CurrentDate();
            db.BangGiaTheoTuyen.Add(bangGiaTheoTuyen);
            db.SaveChanges();
            bsuccess = true;
            if (bsuccess == true)
            {
                TempData["message"] = "Thêm mới tuyến thành công";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = "Lỗi thêm tuyến";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

            return View();
        }

        public string GetCityNameByCode(string code)
        {
            string Name = "";
            try
            {
                City city = db.City.Where(o => o.Name == code).Take(1).SingleOrDefault();
                if (city != null)
                {
                    Name = city.Name;
                }
            }
            catch (Exception ex)
            {
            }

            return Name;
        }
        public string GetDistrictNameByCode(string code)
        {
            string NAme = "";
            try
            {
                District district = db.District.Where(o => o.DistrictCode == code).Take(1).SingleOrDefault();
                if (district != null)
                {
                    NAme = district.Name;
                }
            }
            catch (Exception ex)
            {
            }

            return NAme;
        }
        public string GetWardNameByCode(string code)
        {
            string Name = "";
            try
            {
                Ward ward = db.Ward.Where(o => o.WardCode == code).Take(1).SingleOrDefault();
                if (ward != null)
                {
                    Name = ward.Name;
                }
            }
            catch (Exception ex)
            {
            }

            return Name;
        }

        public ActionResult ThemTuyenTuongTu()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ThemTuyenTuongTu(FormCollection form)
        {
            string strDiemDi = form.Get("DiemDi") != null ? form.Get("DiemDi") : "";
            string strDiemDen = form.Get("DiemDen") != null ? form.Get("DiemDen") : "";
            string mess = InsertCopy(strDiemDi, strDiemDen);
            if (mess == "")
            {
                TempData["message"] = "Thêm mới tuyến cùng khu vực thành công";
            }
            else
            {
                TempData["error"] = "Lỗi thêm tuyến tương tự: " + mess;
            }

            return View();
        }

        // GET: Admin/BangGiaTheoTuyen/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //var bangGiaTheoTuyen = _unitOfWork.GetRepositoryInstance<BangGiaTheoTuyen>().GetFirstOrDefaultByParameter(x => x.Id == id);
            var bangGiaTheoTuyen = db.BangGiaTheoTuyen.Find(id);
            var listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var cartype in listCarType)
            {
                var item = new SelectListItem { Value = cartype.Value, Text = cartype.Name };
                if (item.Value == bangGiaTheoTuyen.TypeCar)
                {
                    item.Selected = true;
                }
                list.Add(item);
            }
            var listTT = db.City.ToList();
            List<SelectListItem> list1 = new List<SelectListItem>();
            foreach (var c in listTT)
            {
                var item = new SelectListItem { Value = c.Name, Text = c.Name };
                list1.Add(item);
            }
            ViewBag.listCarType = list;
            ViewBag.listCT = list1;


            if (bangGiaTheoTuyen == null)
            {
                return HttpNotFound();
            }
            return PartialView("Edit", bangGiaTheoTuyen);
        }

        // POST: Admin/BangGiaTheoTuyen/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        public JsonResult EditBangGiaTheoTuyen([Bind(Include = "Id,DiemDi,DiemDen,TypeCar,Price,Isactive,SoKM")] Carrier.Algorithm.Entity.BangGiaTheoTuyen bangGiaTheoTuyen)
        {
            if (ModelState.IsValid)
            {
                bangGiaTheoTuyen.DiemDi = bangGiaTheoTuyen.DiemDi;
                bangGiaTheoTuyen.DiemDen = bangGiaTheoTuyen.DiemDen;

                string fromAddress = bangGiaTheoTuyen.DiemDi;
                string toAddress = bangGiaTheoTuyen.DiemDen;
                // Lay đến xã trước
                var wardfromSearch = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromWard().Trim()).ToLower();
                var wardtoSearch = Helpers.RemoveUnicodeCharactor(toAddress.StringFromWard()).ToLower();
                var districtFromSearch = Helpers.RemoveUnicodeCharactor(fromAddress.StringFromDistrict()).ToLower();
                var districtToSearch = Helpers.RemoveUnicodeCharactor(toAddress.StringFromDistrict()).ToLower();

                bangGiaTheoTuyen.XaDiSearch = wardfromSearch;
                bangGiaTheoTuyen.XaDenSearch = wardtoSearch;
                bangGiaTheoTuyen.HuyenDiSearch = districtFromSearch;
                bangGiaTheoTuyen.HuyenDenSearch = districtToSearch;

                var arrFrom = fromAddress.Trim().Split(',');
                string cityFrom = arrFrom[arrFrom.Length - 2].Trim();
                string districtFrom = arrFrom[arrFrom.Length - 3].Trim();
                string wardFrom = arrFrom[arrFrom.Length - 4].Trim();

                string cityFromCode = GetCityCode(cityFrom);
                string districtFromCode = GetDistrictCode(districtFrom, cityFromCode);
                string wardFromCode = GetWardCode(wardFrom, districtFromCode);

                var arrTo = fromAddress.Trim().Split(',');
                string cityTo = arrTo[arrTo.Length - 2].Trim();
                string districtTo = arrTo[arrTo.Length - 3].Trim();
                string wardTo = arrTo[arrTo.Length - 4].Trim();

                string cityToCode = GetCityCode(cityTo);
                string districtToCode = GetDistrictCode(districtTo, cityTo);
                string wardToCode = GetWardCode(wardTo, districtTo);

                bangGiaTheoTuyen.CityFromCode = cityFromCode;
                bangGiaTheoTuyen.DistrictFromCode = districtFromCode;
                bangGiaTheoTuyen.WardFromCode = wardFromCode;
                bangGiaTheoTuyen.CityToIdCode = cityToCode;
                bangGiaTheoTuyen.DistrictToCode = districtToCode;
                bangGiaTheoTuyen.WardToCode = wardToCode;

                bangGiaTheoTuyen.DiemDiSearch = Utitlitys.RemoveUnicodeCharactor(bangGiaTheoTuyen.DiemDi).Replace(",", string.Empty).Replace(" ", string.Empty);
                bangGiaTheoTuyen.DiemDenSearch = Utitlitys.RemoveUnicodeCharactor(bangGiaTheoTuyen.DiemDen).Replace(",", string.Empty).Replace(" ", string.Empty);
                bangGiaTheoTuyen.Version = 1;
                bangGiaTheoTuyen.Price = bangGiaTheoTuyen.Price;
                bangGiaTheoTuyen.TypeCar = bangGiaTheoTuyen.TypeCar;
                bangGiaTheoTuyen.SoKM = bangGiaTheoTuyen.SoKM;
                bangGiaTheoTuyen.Updated_By = User.Identity.Name;

                bangGiaTheoTuyen.IsActive = true;
                bangGiaTheoTuyen.Updated_At = DateTime.Now.ToString("MMM d yyyy h:mmtt", CultureInfo.InvariantCulture);

                db.Entry(bangGiaTheoTuyen).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json("OK", JsonRequestBehavior.AllowGet);
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/BangGiaTheoTuyen/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Carrier.Algorithm.Entity.BangGiaTheoTuyen bangGiaTheoTuyen = db.BangGiaTheoTuyen.Find(id);
            if (bangGiaTheoTuyen == null)
            {
                return HttpNotFound();
            }
            return View(bangGiaTheoTuyen);
        }

        // POST: Admin/BangGiaTheoTuyen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Carrier.Algorithm.Entity.BangGiaTheoTuyen bangGiaTheoTuyen = db.BangGiaTheoTuyen.Find(id);
            db.BangGiaTheoTuyen.Remove(bangGiaTheoTuyen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult DeleteBangGia(string listId)
        {
            Carrier.Algorithm.Entity.BangGiaTheoTuyen result;
            //UserInfo userInfo;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    var IdGia = int.Parse(id);
                    result = db.BangGiaTheoTuyen.Find(IdGia);
                    if (result != null)
                    {
                        db.BangGiaTheoTuyen.Remove(result);
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source">Ngô Quyền, Hải Phòng, Việt Nam</param>
        /// <param name="target">An Lão, Hải Phòng, Việt Nam</param>
        public static string InsertCopy(string source, string target)
        {
            string message = "";
            try
            {


                using (var db = new LogisticDataEntities())
                {
                    var lstSource = db.BangGiaTheoTuyen.Where(x => x.DiemDi.Contains(source)).ToList();
                    var lstTuyen = new List<Algorithm.Entity.BangGiaTheoTuyen>();
                    foreach (var i in lstSource)
                    {
                        var obj = new Algorithm.Entity.BangGiaTheoTuyen();
                        obj.DiemDi = target;
                        obj.DiemDiSearch = Helpers.RemoveUnicodeCharactor(target.Replace(",", string.Empty).Replace(" ", string.Empty));
                        obj.XaDiSearch = Helpers.RemoveUnicodeCharactor(target.StringFromWard().Replace(",", string.Empty).Replace(" ", string.Empty));
                        obj.HuyenDiSearch = Helpers.RemoveUnicodeCharactor(target.StringFromDistrict().Replace(",", string.Empty).Replace(" ", string.Empty));
                        obj.DiemDen = i.DiemDen;
                        obj.DiemDenSearch = i.DiemDenSearch;
                        obj.XaDenSearch = i.XaDenSearch;
                        obj.HuyenDenSearch = i.HuyenDenSearch;
                        obj.Updated_At = DateTime.Now.ToString();
                        obj.Created_At = DateTime.Now;
                        obj.SoKM = i.SoKM;
                        obj.IsActive = i.IsActive;
                        obj.TypeCar = i.TypeCar;
                        obj.Version = i.Version;
                        obj.CityFromCode = i.CityFromCode;
                        obj.CityToIdCode = i.CityToIdCode;
                        obj.DistrictFromCode = i.DistrictFromCode;
                        obj.DistrictToCode = i.DistrictToCode;
                        obj.WardFromCode = i.WardFromCode;
                        obj.WardToCode = i.WardToCode;
                        obj.Price = i.Price;

                        var isExist = db.BangGiaTheoTuyen.Where(x => x.DiemDiSearch.Equals(obj.DiemDiSearch) && x.DiemDenSearch.Equals(obj.DiemDenSearch)).FirstOrDefault() != null ? true : false;
                        if (!isExist)
                        {
                            lstTuyen.Add(obj);
                        }
                    }

                    EFBatchOperation.For(db, db.BangGiaTheoTuyen).InsertAll(lstTuyen);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return message;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
