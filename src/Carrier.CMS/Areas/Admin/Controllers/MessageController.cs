﻿using Carrier.CMS.Common;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;
using Carrier.CMS;
using CMS.Carrier.Models;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_ADMIN)]
    public class MessageController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();

        public MessageController()
        {

        }

        public string _UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public MessageController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Enterprise/EMessage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListMessageOrder()
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            var CurrentUsertId = GetCurrentOrganizatinId(User.Identity.GetUserId());
            DateTime dateCompare = DateTime.Now.AddDays(-1);
            result = (from o in db.Order
                      join a in db.UserInfoes on o.Created_By equals a.ApplicationUserID
                      where o.CreateAt > dateCompare
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = o.UserName,
                          OrganizationId = a.OrganzationId,
                          CreateAt = o.CreateAt,
                          Created_By = o.Created_By
                      }).OrderByDescending(o => o.Id).ToList();

            return View(result);
        }

        public async Task<ActionResult> ListMessageUser(int Quanlity)
        {
            var currentUserLogin = UserManager.Users.Where(x => x.Id.Equals(_UserId)).FirstOrDefault();
            var lstUser = await UserManager.Users.Include(u => u.Roles).ToListAsync();
            //lstUser = lstUser.Where(u => u.UsersInfo.Created_At.ToString().Split(' ')[0] == DateTime.Now.ToString("dd/MM/yyyy")).ToList();
            lstUser = lstUser.OrderByDescending(x => x.UsersInfo.Id).Take(Quanlity).ToList();
            var lstUserViewModel = new List<UserViewModel>();
            foreach (ApplicationUser item in lstUser)
            {
                var rolesDrives = await UserManager.GetRolesAsync(item.Id);
                //if (rolesDrives.Contains(PublicConstant.ROLE_DRIVERS))
                //{
                if (item.UsersInfo != null)
                {
                    var newItem = new UserViewModel();
                    newItem.Address = item.UsersInfo.Address;
                    newItem.FullName = item.UsersInfo.FullName;
                    newItem.MobilePhone = item.UsersInfo.MobilePhone;
                    newItem.UserName = item.UserName;
                    newItem.Id = item.UsersInfo.Id;
                    newItem.LastLogin = item.UsersInfo.LastLogin;
                    var roles = new List<string>();
                    foreach (var role in rolesDrives)
                    {
                        role.ToString();
                        roles.Add(role.ToString());
                    }
                    newItem.Email = item.Email;
                    newItem.Roles = roles;
                    newItem.Status = item.UsersInfo.Status;
                    newItem.Created_At = item.UsersInfo.Created_At;
                    lstUserViewModel.Add(newItem);
                }
                //}
            }

            return View(lstUserViewModel.OrderByDescending(o => o.Id));
        }

        public ActionResult ListMessageNotification()
        {
            var notificationView = new MessageNotificationViewModel();
            var lstNotificationRecords = new List<MessageNotificationModel>();
            List<Messages> lstMessage = db.Messages.Where(o => o.ToUserId == User.Identity.GetUserId()).OrderByDescending(o => o.Id).ToList();
            MessageNotificationModel item;
            foreach (Messages message in lstMessage)
            {
                item = new MessageNotificationModel();
                item.Id = message.Id.ToString();
                item.Message = message.Message;
                item.FromUserId = message.FromUserId;
                item.ToUserId = message.ToUserId;
                item.Created_At = DateTime.Parse(message.Created_At);
                lstNotificationRecords.Add(item);
            }

            lstNotificationRecords = lstNotificationRecords.Where(o => o.ToUserId == User.Identity.GetUserId() && o.Created_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).Take(1000).ToList();

            notificationView.Quanlity = lstNotificationRecords.Count();
            notificationView.listMessageNotification = lstNotificationRecords.ToList();

            return View(notificationView);
        }
        public ActionResult ListMessageOrderTracking()
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            var CurrentUsertId = GetCurrentOrganizatinId(User.Identity.GetUserId());
            DateTime dateCompare = DateTime.Now.AddDays(-1);
            result = (from o in db.Order
                      join t in db.OrderTracking on o.Id equals t.OrderId
                      join a in db.UserInfoes on o.Created_By equals a.ApplicationUserID
                      where
                      t.Status == PublicConstant.ORDER_ACCEPT
                      // && a.OrganzationId == CurrentUsertId
                      && o.CreateAt > dateCompare
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = o.UserName,
                          OrganizationId = a.OrganzationId,
                          CreateAt = o.CreateAt,
                          Created_By = o.Created_By
                      }).OrderByDescending(o => o.CreateAt).Distinct().ToList();

            return View(result);
        }

        public int GetCurrentOrganizatinId(string userId)
        {
            int organizationId = 0;
            try
            {
                organizationId = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault().Id;
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}