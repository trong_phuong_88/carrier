﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using Carrier.Utilities;
using Carrier.Repository;
using Carrier.CMS.Models;
using CMS.Carrier.Areas.Admin.Models;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class SysSettingController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/SysSetting
        public ActionResult Index(string UserId = "")
        {
            List<UserDropdowView> listUser = (from u in db.AspNetUsers
                                          join s in db.SysSetting on u.Id equals s.UserId
                                          select new UserDropdowView
                                          {
                                              UserId = u.Id,
                                              UserName = u.UserName
                                          }).Distinct().ToList();
            ViewBag.User = new SelectList(listUser, "UserId", "UserName");
            List<SysSetting> lst = new List<SysSetting>();
            if (UserId.Length > 0)
            {
                lst = _unitOfWork.GetRepositoryInstance<SysSetting>().GetAllRecords().Where(o=>o.UserId == UserId).ToList();
            }
            else
            {
                lst = _unitOfWork.GetRepositoryInstance<SysSetting>().GetAllRecords().ToList();
            }

            
            return View(lst.OrderByDescending(x => x.Id));

            //var lst = _unitOfWork.GetRepositoryInstance<SysSetting>().GetAllRecords();
            //return View(lst.OrderByDescending(x=>x.Id));
        }
        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
            var _list = (from user in db.AspNetUsers
                                  join dv in db.UserInfoes on user.Id equals dv.ApplicationUserID
                                  where user.UserName.StartsWith(prefix)
                                  select new
                                  {
                                      label = user.UserName,
                                      val = user.Id
                                  }).ToList();

            return Json(_list);
        }
        // GET: Admin/SysSetting/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SysSetting sysSetting = db.SysSetting.Find(id);
            if (sysSetting == null)
            {
                return HttpNotFound();
            }
            return View(sysSetting);
        }

        // GET: Admin/SysSetting/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/SysSetting/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public JsonResult CreateSysSetting([Bind(Include = "Id,UserId,SettingKey,SettingValue")] SysSetting sysSetting)
        {
            if (ModelState.IsValid)
            {                
                _unitOfWork.GetRepositoryInstance<SysSetting>().Add(sysSetting);
                _unitOfWork.SaveChanges();
                return Json("OK", JsonRequestBehavior.AllowGet);
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }


        // GET: Admin/SysSetting/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SysSetting sysSetting = db.SysSetting.Find(id);
            var u = db.AspNetUsers.Where(o => o.Id == sysSetting.UserId).FirstOrDefault();
            ViewBag.UserName = "";
            if(u !=null)
            {
                ViewBag.UserName = u.UserName;
            }
            if (sysSetting == null)
            {
                return HttpNotFound();
            }
            return View(sysSetting);
        }

        // POST: Admin/SysSetting/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public JsonResult EditSysSetting([Bind(Include = "Id,UserId,SettingKey,SettingValue")] SysSetting sysSetting)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.GetRepositoryInstance<SysSetting>().Update(sysSetting);
                _unitOfWork.SaveChanges();
                
                return Json("OK", JsonRequestBehavior.AllowGet);
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/SysSetting/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SysSetting sysSetting = db.SysSetting.Find(id);
            if (sysSetting == null)
            {
                return HttpNotFound();
            }
            return View(sysSetting);
        }

        // POST: Admin/SysSetting/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SysSetting sysSetting = db.SysSetting.Find(id);
            db.SysSetting.Remove(sysSetting);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult DeleteSysSetting(string listId)
        {
            SysSetting sys;
            //UserInfo userInfo;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    sys = _unitOfWork.GetRepositoryInstance<SysSetting>().GetFirstOrDefault(int.Parse(id));
                    _unitOfWork.GetRepositoryInstance<SysSetting>().Remove(sys);
                    _unitOfWork.SaveChanges();
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi xóa: " + ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }

            TempData["info"] = "Xóa thành công!";
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
