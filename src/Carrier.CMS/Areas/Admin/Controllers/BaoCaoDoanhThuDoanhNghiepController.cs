﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using Carrier.Repository;
using Carrier.Utilities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_ADMIN, PublicConstant.ROLE_SUPPER_ADMIN)]
    public class BaoCaoDoanhThuDoanhNghiepController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public BaoCaoDoanhThuDoanhNghiepController()
        {
        }

        public BaoCaoDoanhThuDoanhNghiepController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: Admin/BaoCaoDoanhThuDoanhNGhiep
        public ActionResult Index()
        {
            string txtFromDate = null;
            string txtToDate = null;
            if (Request.Form["txtFromDate"] != null && Request.Form["txtFromDate"].Length > 0)
            {
                txtFromDate = Request.Form["txtFromDate"];
            }
            if (Request.Form["txtToDate"] != null && Request.Form["txtToDate"].Length > 0)
            {
                txtToDate = Request.Form["txtToDate"];
            }
            if (txtFromDate == null)
            {
                txtFromDate = "2017-01-01 00:00:00";
            }
            if (txtToDate == null)
            {
                txtToDate = DateTime.Now.ToString();
            }

            List<SP_Report_Profit_For_Enterprise_Admin_Result> model = new List<SP_Report_Profit_For_Enterprise_Admin_Result>();
            //var enterprises = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetListByParameter(x=>x.OrganzationId==0);
            var _listIdE = RoleManager.FindByName("Enterprise").Users.Select(x => x.UserId);
            var enterprises = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetListByParameter(u => _listIdE.Contains(u.ApplicationUserID)).ToList();
            foreach (var d in enterprises)
            {
                var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = d.ApplicationUserID };
                var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = txtFromDate };
                var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = txtToDate };
                var profit = _unitOfWork.GetRepositoryInstance<SP_Report_Profit_For_Enterprise_Admin_Result>().GetResultBySqlProcedure("SP_Report_Profit_For_Enterprise_Admin @UserId,@FromDate, @ToDate", sqlUser, sqlFromDate, sqlToDate).FirstOrDefault();
                model.Add(profit);
            }
            return View(model);
        }
    }
}