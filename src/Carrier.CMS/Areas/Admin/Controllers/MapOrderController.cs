﻿using Carrier.CMS.Common;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class MapOrderController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/MapOrder
        public ActionResult Index()
        {
            List<MapOrder> listMapOrder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetAllRecords().ToList();
            List<MapOrderViewModel> listMapOrderView = new List<MapOrderViewModel>();
            MapOrderViewModel odView;
            foreach (MapOrder item in listMapOrder)
            {
                odView = new MapOrderViewModel();
                odView.Id = item.ID;
                odView.FromUserId = item.FromUser;
                odView.ToUserId = item.ToUser;
                odView.FromUserName = GetUserName(item.FromUser);
                odView.ToUserName = GetUserName(item.ToUser);
                listMapOrderView.Add(odView);
            }
            return View(listMapOrderView);
        }

        // GET: Admin/Car/Create
        public ActionResult Create()
        {
            List<AspNetUsers> listUser = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetAllRecords().OrderBy(o => o.UserName).ToList();
            ViewBag.listUser = new SelectList(listUser, "Id", "UserName", 0);

            return View();
        }

        // POST: Admin/Car/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            MapOrder mapOrder = new MapOrder();
            if (ModelState.IsValid)
            {
                try
                {
                    string fromUserId = form.Get("ddlFromUser") != null ? form.Get("ddlFromUser") : "";
                    string toUserId = form.Get("ddlToUser") != null ? form.Get("ddlToUser") : "";
                    List<MapOrder> listMapOrder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetListByParameter(o => o.FromUser == fromUserId && o.ToUser == toUserId).ToList();
                    if (listMapOrder.Count == 0)
                    {
                        mapOrder.FromUser = fromUserId;
                        mapOrder.ToUser = toUserId;
                        _unitOfWork.GetRepositoryInstance<MapOrder>().Add(mapOrder);
                        _unitOfWork.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                catch
                {
                }
            }

            return View(mapOrder);
        }

        // GET: Admin/Car/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MapOrder maporder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefaultByParameter(o => o.ID == id);
            if (maporder == null)
            {
                return HttpNotFound();
            }
            List<AspNetUsers> listUser = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetAllRecords().OrderBy(o => o.UserName).ToList();
            ViewBag.listUser = new SelectList(listUser, "Id", "UserName", 0);

            return View(maporder);
        }

        // POST: Admin/Car/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            MapOrder mapOrder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefaultByParameter(o => o.ID == id);
            if (ModelState.IsValid)
            {
                mapOrder.FromUser = form.Get("ddlFromUser") != null ? form.Get("ddlFromUser") : "";
                mapOrder.ToUser = form.Get("ddlToUser") != null ? form.Get("ddlToUser") : "";
                _unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(mapOrder);
        }

        public JsonResult DeleteMapOrder(string listId)
        {
            MapOrder result;
            //UserInfo userInfo;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    var IdGia = int.Parse(id);
                    result = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefault(IdGia);
                    if (result != null)
                    {
                        _unitOfWork.GetRepositoryInstance<MapOrder>().Remove(result);
                        _unitOfWork.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public string GetUserName(string userId)
        {
            string name = "";
            AspNetUsers user = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetFirstOrDefaultByParameter(o => o.Id == userId.ToString());
            if (user != null)
            {
                name = user.UserName;
            }

            return name;
        }
    }
}