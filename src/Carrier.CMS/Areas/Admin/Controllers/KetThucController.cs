﻿using Carrier.CMS.Common;
using Carrier.CMS.Hubs;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using CMS.Carrier.Common;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class KetThucController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/KetThuc
        public ActionResult Index()
        {
            SQLDependencyInit();
            return View();
        }
        public ActionResult ListOrderTracking()
        {
            var userId = User.Identity.GetUserId();
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = "" };
            var sqlUFromDate = new SqlParameter("@UFromDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
            var sqlUToDate = new SqlParameter("@UToDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
            var sqlTFromDate = new SqlParameter("@TFromDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
            var sqlTToDate = new SqlParameter("@TToDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
            var sqlStatus = new SqlParameter("@Status", SqlDbType.Int) { Value = 5 };
            var listData = _unitOfWork.GetRepositoryInstance<SP_VanDon_DaKhopLenh_Result>().GetResultBySqlProcedure("SP_VanDon_DaKhopLenh @UserId,@UFromDate,@UToDate,@TFromDate,@TToDate,@Status", sqlUserId, sqlUFromDate, sqlUToDate, sqlTFromDate, sqlTToDate, sqlStatus);
            return PartialView("_ListViewOrderKetThuc", listData);
        }
        public void SQLDependencyInit()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;

            string listenQuery = @"SELECT [OrderId] ,[Updated_At],[Status],[OwnerId] ,[DriverId] ,[EvidencePath] FROM [dbo].[OrderTracking]";

            // Create instance of the DB Listener
            DatabaseChangeListener changeListener = new DatabaseChangeListener(connectionString);

            // Define what to do when changes were detected
            changeListener.OnChange += () =>
            {
                //ChatHub.SendMessages();
                NotificationHub.UpdateMatchOrder();
                // Reattach listener event - DO NOT TOUCH!
                changeListener.Start(listenQuery);
            };

            // Start listening for changes 
            changeListener.Start(listenQuery);

        }

        public ActionResult OrderDetail(long Id)
        {
            Order order = db.Order.Where(o => o.Id == Id).Single();
            OrderTracking ordertracking = db.OrderTracking.Where(o => o.OrderId == order.Id).OrderByDescending(o => o.ID).Take(1).SingleOrDefault();
            if (ordertracking != null)
            {
                ordertracking.IsView = true;
                db.SaveChanges();
            }
            return PartialView("_OrderDetail", order);
        }
        public ActionResult UserInfoDetail(string DriverId)
        {
            Driver userInfo = db.Driver.Where(o => o.UserId == DriverId).Single();
            return PartialView("_UserInfo", userInfo);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}