﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Carrier.Models;
using System.Data.SqlClient;
using Carrier.Repository;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]

    public class OrderStaticController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private Carrier3Entities db = new Carrier3Entities();
        // GET: OrderStatic
        public ActionResult Index(string currentFilter, string status, int? page, int? pagsiz)
        {
            if (status != null)
            {
                page = 1;
            }
            else
            {
                status = currentFilter;
            }

            ViewBag.CurrentFilter = status;

            //var result = db.Order.OrderByDescending(o => o.Status);
            //if (!String.IsNullOrEmpty(status))
            //{
            //    result = db.Order.Where(o => o.Status == int.Parse(status)).OrderByDescending(o => o.CreateAt);
            //}

            //if (User.IsInRole("Khách"))
            //{
            //    result = db.Order.Where(x => x.UserName == User.Identity.Name && x.Status == int.Parse(status)).OrderByDescending(o => o.Status);
            //}

            List<OrderViewModel> result = new List<OrderViewModel>();
            //if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            //{
            result = (from a in db.AspNetUsers
                      join b in db.UserInfoes on a.Id equals b.ApplicationUserID
                      join o in db.Order on a.UserName equals o.UserName
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = a.UserName,
                          OrganizationId = b.OrganzationId,
                      }).ToList();
            //}
            //else if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            //{
            //    var CurrentUsertId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            //    result = (from a in db.AspNetUsers
            //              join b in db.UserInfoes on a.Id equals b.ApplicationUserID
            //              join o in db.Order on a.UserName equals o.UserName
            //              where o.Status == PublicConstant.ORDER_FINISHED
            //              && b.OrganzationId == CurrentUsertId
            //              select new OrderViewModel
            //              {
            //                  Id = o.Id,
            //                  MaVanDon = o.MaVanDon,
            //                  TenHang = o.TenHang,
            //                  TrongLuong = o.TrongLuong,
            //                  DiemDiChiTiet = o.DiemDiChiTiet,
            //                  DiemDenChiTiet = o.DiemDenChiTiet,
            //                  ThoiGianDi = o.ThoiGianDi,
            //                  ThoiGianDen = o.ThoiGianDen,
            //                  Gia = o.Gia,
            //                  Status = o.Status,
            //                  UserName = a.UserName,
            //                  OrganizationId = b.OrganzationId,
            //              }).ToList();
            //}
            //else
            //{
            //    result = (from a in db.AspNetUsers
            //              join b in db.UserInfoes on a.Id equals b.ApplicationUserID
            //              join o in db.Order on a.UserName equals o.UserName
            //              where o.Status == PublicConstant.ORDER_FINISHED
            //              && a.UserName == User.Identity.Name
            //              select new OrderViewModel
            //              {
            //                  Id = o.Id,
            //                  MaVanDon = o.MaVanDon,
            //                  TenHang = o.TenHang,
            //                  TrongLuong = o.TrongLuong,
            //                  DiemDiChiTiet = o.DiemDiChiTiet,
            //                  DiemDenChiTiet = o.DiemDenChiTiet,
            //                  ThoiGianDi = o.ThoiGianDi,
            //                  ThoiGianDen = o.ThoiGianDen,
            //                  Gia = o.Gia,
            //                  UserName = a.UserName,
            //                  OrganizationId = b.OrganzationId,
            //                  Status = o.Status
            //              }).ToList();
            //}

            if (!String.IsNullOrEmpty(status))
            {
                int statuOrder = int.Parse(status);
                result = result.Where(o => o.Status == statuOrder).ToList();
            }

            if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            {
                result = result.OrderByDescending(o => o.OrganizationId).ToList();
            }
            else if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            {
                result = result.OrderByDescending(o => o.UserName).ToList();
            }
            else
            {
                result = result.OrderByDescending(o => o.Id).ToList();
            }


            int pageSize = int.Parse((pagsiz ?? 5).ToString());
            ViewBag.PageSize = pageSize;
            int pageNumber = int.Parse((page ?? 1).ToString());
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        //public ActionResult Index(string currentFilter, string status, int? page, int? pagsiz)
        //{
        //    if (status != null)
        //    {
        //        page = 1;
        //    }
        //    else
        //    {
        //        status = currentFilter;
        //    }

        //    ViewBag.CurrentFilter = status;

        //    var result = db.Order.OrderByDescending(o => o.Status);
        //    if (!String.IsNullOrEmpty(status))
        //    {
        //        result = db.Order.Where(o => o.Status == int.Parse(status)).OrderByDescending(o => o.CreateAt);
        //    }

        //    if (User.IsInRole("Khách"))
        //    {
        //        result = db.Order.Where(x => x.UserName == User.Identity.Name && x.Status == int.Parse(status)).OrderByDescending(o => o.Status);
        //    }

        //    int pageSize = int.Parse((pagsiz ?? 5).ToString());
        //    ViewBag.PageSize = pageSize;
        //    int pageNumber = int.Parse((page ?? 1).ToString());
        //    return View(result.ToPagedList(pageNumber, pageSize));
        //}
        public ActionResult CountEnterprise()
        {
            List<Car> lstCar = new List<Car>();
            List<Driver> lstDriver = new List<Driver>();
            List<ListCarView> quality = new List<ListCarView>();
            var sqlType = new SqlParameter("@type", System.Data.SqlDbType.Int) { Value = 1 };
            List<SP_GetAllEnterprise_Result> listEnterprise = new List<SP_GetAllEnterprise_Result>();
            listEnterprise = _unitOfWork.GetRepositoryInstance<SP_GetAllEnterprise_Result>().GetResultBySqlProcedure("SP_GetAllEnterprise @type", sqlType).ToList();
            foreach (var item in listEnterprise)
            {
                var lstView = new ListCarView();
                int organizationId = GetCurrentOrganizatinIdByUserName(item.UserName);
                lstCar = _unitOfWork.GetRepositoryInstance<Car>().GetListByParameter(x => x.OrganizationId == organizationId).ToList();
                lstDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(x => x.OrganizationId == organizationId).ToList();
                if(lstCar != null || lstDriver != null)
                {
                    lstView.Enterprise = item.UserName;
                    lstView.TotalCar = lstCar.Count;
                    lstView.TotalDriver = lstDriver.Count;
                }
                quality.Add(lstView);
            }
            return View(quality);
        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}