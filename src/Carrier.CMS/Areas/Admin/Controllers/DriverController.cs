﻿using Carrier.CMS.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using Carrier.Utilities;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.CMS;
using System;
using PagedList;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;
using GoogleMaps.LocationServices;
using Carrier.CMS.Common;
using CKSource.FileSystem;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_ADMIN, PublicConstant.ROLE_SUPPER_ADMIN)]
    public class DriverController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private GenericUnitOfWork _unitOfWorkTemp = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Enterprise/Driver
        public ActionResult Index(string currentFilter, string searchString, int? page, int? pagsiz)
        {
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                List<Organization> listOrganization = new List<Organization>();
                listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();
                IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listOrganizationSelect;
            }

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<Driver> result = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == GetCurrentOrganizatinIdByUserName(User.Identity.Name)).OrderByDescending(o => o.Name).ToList();
            //List<Driver> result = db.Driver.OrderByDescending(o => o.Name).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                result = result.Where(o => o.Name.Contains(searchString) || o.Phone.Contains(searchString)).OrderByDescending(o => o.Name).ToList();
            }

            int pageSize = (pagsiz ?? 5);
            ViewBag.PageSize = pageSize;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: Admin/Drivers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver drivers = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            return View(drivers);
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }

        public string UploadImageDriver(HttpPostedFileBase file, ref string fileNameOut)
        {
            string message = "";
            string fileName = "";
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    //kiem tra file extension lan nua
                    List<string> excelExtension = new List<string>();
                    excelExtension.Add(".png");
                    excelExtension.Add(".jpg");
                    excelExtension.Add(".gif");
                    var extension = Path.GetExtension(file.FileName);
                    if (!excelExtension.Contains(extension.ToLower()))
                    {
                        return "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png";
                    }
                    //kiem tra dung luong file
                    var fileSize = file.ContentLength;
                    var fileMB = (fileSize / 1024f) / 1024f;
                    if (fileMB > 5)
                    {
                        return "Dung lượng ảnh không được lớn hơn 5MB";
                    }
                    // luu ra dia
                    fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
                    fileNameOut = fileName;
                    var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Driver"), fileName);
                    file.SaveAs(physicalPath);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return message;
        }

        // GET: Admin/Drivers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Drivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection form)
        {
            #region Upload Image
            var front_Image = "";
            var back_Image = "";

            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase fileTruoc = Request.Files[0];
                HttpPostedFileBase fileSau = Request.Files[1];
                string message = "";
                message = UploadImageDriver(fileTruoc, ref front_Image);
                if (message.Length > 0)
                {
                    return View();
                }

                message = UploadImageDriver(fileSau, ref back_Image);
                if (message.Length > 0)
                {
                    return View();
                }
            }
            #endregion


            Driver driver = new Driver();
            if (ModelState.IsValid)
            {
                string userName = form.Get("UserName") != null ? form.Get("UserName") : "";
                string password = form.Get("Password") != null ? form.Get("Password") : "";
                string email = form.Get("Email") != null ? form.Get("Email") : "";
                var name = form.Get("Name") != null ? form.Get("Name") : "";
                var address = form.Get("Address") != null ? form.Get("Address") : "";
                var phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                var sex = form.Get("Sex") != null ? form.Get("Sex") : "";
                var birthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";

                var user = new ApplicationUser { UserName = userName, Email = email };

                UserInfo userInfo = new UserInfo();
                userInfo.Address = address;
                userInfo.MobilePhone = phone;
                userInfo.FullName = name;
                userInfo.ApplicationUserID = user.Id;
                userInfo.Sex = sex == "Nữ" ? false : true;
                userInfo.Status = PublicConstant.STATUS_ACTIVE;
                userInfo.Updated_At = DateTime.Now;
                userInfo.LastLogin = DateTime.Now;
                userInfo.Created_At = DateTime.Now;
                user.UsersInfo = userInfo;
                var result = await UserManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_DRIVERS);
                    if (result.Succeeded)
                    {
                        driver.Name = name;
                        driver.Address = address;
                        driver.Phone = phone;
                        driver.Sex = sex;
                        driver.BirthDay = birthDay;
                        driver.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);

                        string message = "";
                        double lat = 0, lng = 0;

                        string latLong = GetLatLongByAddress(ref message, address);
                        if (message.Length == 0)
                        {
                            lat = double.Parse(latLong.Split(':')[0]);
                            lng = double.Parse(latLong.Split(':')[1]);
                        }

                        driver.Lat = lat;
                        driver.Lng = lng;

                        driver.UserId = user.Id;
                        driver.Status = form.Get("Status") != null ? int.Parse(form.Get("Status")) : 0;
                        driver.Created_At = DateTime.Now;
                        driver.Created_At = DateTime.Now;
                        driver.Front_Image = front_Image;
                        driver.Back_Image = back_Image;

                        _unitOfWork.GetRepositoryInstance<Driver>().Add(driver);
                        _unitOfWork.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }

            return View(driver);
        }

        // GET: Admin/Drivers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver drivers = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            //List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //ViewBag.ListOrganization = listOrganization;

            return View(drivers);
        }

        // POST: Admin/Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            string message = "";

            if (ModelState.IsValid)
            {
                if (driver != null)
                {
                    #region Upload Image
                    var front_Image = driver.Front_Image;
                    var back_Image = driver.Back_Image;

                    if (Request.Files.Count > 0)
                    {
                        HttpPostedFileBase fileTruoc = Request.Files[0];
                        HttpPostedFileBase fileSau = Request.Files[1];

                        if (fileTruoc.ContentLength > 0)
                        {
                            message = UploadImageDriver(fileTruoc, ref front_Image);
                            if (message.Length > 0)
                            {
                                return View();
                            }
                        }
                        if (fileTruoc.ContentLength > 0)
                        {
                            message = UploadImageDriver(fileSau, ref back_Image);
                            if (message.Length > 0)
                            {
                                return View();
                            }
                        }
                    }
                    #endregion

                    driver.Name = form.Get("Name") != null ? form.Get("Name") : "";
                    driver.Address = form.Get("Address") != null ? form.Get("Address") : "";
                    driver.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                    driver.Sex = form.Get("Sex") != null ? form.Get("Sex") : "";
                    driver.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";
                    driver.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                    double lat = 0, lng = 0;
                    driver.UserId = driver.UserId;
                    string latLong = GetLatLongByAddress(ref message, driver.Address);
                    if (message.Length == 0)
                    {
                        lat = double.Parse(latLong.Split(':')[0]);
                        lng = double.Parse(latLong.Split(':')[1]);
                    }

                    driver.Lat = lat;
                    driver.Lng = lng;

                    driver.Front_Image = front_Image;
                    driver.Back_Image = back_Image;

                    driver.Status = form.Get("Status") != null ? int.Parse(form.Get("Status")) : 0;
                    driver.Updated_At = DateTime.Now;

                    _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                    _unitOfWork.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(driver);
        }


        [HttpPost]
        public JsonResult DeleteDriver(string listId)
        {
            Driver driver;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            string userId = "";
            AspNetUsers user = new AspNetUsers();
            UserInfoes UserInfo = new UserInfoes();
            foreach (string id in liststrId)
            {
                try
                {
                    driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefault(int.Parse(id));
                    if (driver != null)
                    {
                        Carrier3Entities db = new Carrier3Entities();
                        userId = driver.UserId;

                        if (userId != null)
                        {
                            user = db.AspNetUsers.Where(o => o.Id == userId).SingleOrDefault();
                            if (user != null)
                            {
                                db.AspNetUsers.Remove(user);
                            }

                            UserInfo = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault();
                            if (UserInfo != null)
                            {
                                db.UserInfoes.Remove(UserInfo);
                            }

                            db.SaveChanges();

                            _unitOfWork.GetRepositoryInstance<Driver>().Remove(driver);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public string GetOrganizationName(int? id)
        {
            string name = "";
            if (id != null)
            {
                Organization product = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefault(int.Parse(id.ToString()));
                if (product != null)
                {
                    name = product.Name;
                }
            }

            return name;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        private DbGeography GetLocationFromAddress(string address)
        {
            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(address);
            var address1 = locationService.GetRegionFromLatLong(point.Latitude, point.Longitude);
            var latitude = point.Latitude;
            var longitude = point.Longitude;
            var location = LocationHelper.CreatePoint(latitude, longitude);
            return location;
        }
    }
}
