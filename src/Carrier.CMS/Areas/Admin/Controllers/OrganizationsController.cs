﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.Repository;
using PagedList;
using Carrier.CMS.Common;
using Carrier.Utilities;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Carrier.CMS.Models;
using Microsoft.AspNet.Identity;

namespace Carrier.CMS.Areas.Admin.Controllers
{
    [Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN)]
    public class OrganizationsController : Controller
    {

        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: Enterprise/Customers
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult Index(string currentFilter, string searchString, int? page, int? pagsiz)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<Organization> result = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().OrderByDescending(o => o.Name).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                result = _unitOfWork.GetRepositoryInstance<Organization>().GetListByParameter(o => o.Name.Contains(searchString)).OrderByDescending(o => o.Name).ToList();
            }

            int pageSize = (pagsiz ?? 5);
            ViewBag.PageSize = pageSize;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: Enterprise/Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization customer = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Enterprise/Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            Organization organization = new Organization();
            if (ModelState.IsValid)
            {
                organization.Name = form.Get("Name") != null ? form.Get("Name") : "";
                organization.Address = form.Get("Address") != null ? form.Get("Address") : "";
                organization.WebSite = form.Get("WebSite") != null ? form.Get("WebSite") : "";
                organization.Updated_At = DateTime.Now.ToString("dd/MM/yyyy");
                organization.DispatcherName = form.Get("txtDispatcherName") != null ? form.Get("txtDispatcherName") : "";
                organization.DispatcherPhone = form.Get("txtDispatcherPhone") != null ? form.Get("txtDispatcherPhone") : "";

                _unitOfWork.GetRepositoryInstance<Organization>().Add(organization);
                _unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(organization);
        }

        // GET: Enterprise/Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization customer = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Enterprise/Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Organization organization = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (ModelState.IsValid)
            {
                organization.Name = form.Get("Name") != null ? form.Get("Name") : organization.Name;
                organization.Address = form.Get("Address") != null ? form.Get("Address") : organization.Address;
                organization.WebSite = form.Get("WebSite") != null ? form.Get("WebSite") : organization.WebSite;
                organization.Updated_At = DateTime.Now.ToString("dd/MM/yyyy");
                organization.DispatcherName = form.Get("txtDispatcherName") != null ? form.Get("txtDispatcherName") : organization.DispatcherName;
                organization.DispatcherPhone = form.Get("txtDispatcherPhone") != null ? form.Get("txtDispatcherPhone") : organization.DispatcherPhone;

                _unitOfWork.GetRepositoryInstance<Organization>().Update(organization);
                _unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(organization);
        }

        [HttpPost]
        public JsonResult DeleteOrganization(string listId)
        {
            Organization organization;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    organization = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefault(int.Parse(id));
                    _unitOfWork.GetRepositoryInstance<Organization>().Remove(organization);
                    _unitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> EnterpriseIndex(string currentFilter, string searchString, int? page, int? pagsiz)
        {
            try
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;
                ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                //var lstUser = await UserManager.Users.Include(x => x.Roles.Equals("Enterprise")).ToListAsync();
                var user = UserManager.Users.ToList();
                var lstUser = user.Where(x => x.Roles.Select(roles => roles.RoleId).Contains("3")).ToList();
                var lstUserViewModel = new List<UserViewModel>();
                foreach (ApplicationUser item in lstUser)
                {
                    if (item.UsersInfo != null)
                    {
                        var newItem = new UserViewModel();
                        newItem.Address = item.UsersInfo.Address;
                        newItem.FullName = item.UsersInfo.FullName;
                        newItem.MobilePhone = item.UsersInfo.MobilePhone;
                        newItem.UserName = item.UserName;
                        newItem.LastLogin = item.UsersInfo.LastLogin;
                        var roles = new List<string>();
                        foreach (var role in UserManager.GetRoles(item.Id))
                        {
                            role.ToString();
                            roles.Add(role.ToString());
                        }
                        newItem.Email = item.Email;
                        newItem.Roles = roles;
                        if (item.LockoutEnabled)
                        {
                            newItem.Status = 0;
                        }
                        else
                        {
                            newItem.Status = 1;

                        }
                        newItem.Id = item.UsersInfo.Id;
                        newItem.Created_At = item.UsersInfo.Created_At;
                        newItem.DispatcherName = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.UserId == item.UsersInfo.ApplicationUserID).DispatcherName;
                        newItem.DispatcherPhone = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.UserId == item.UsersInfo.ApplicationUserID).DispatcherPhone;
                        lstUserViewModel.Add(newItem);
                    }
                }
                //if (!String.IsNullOrEmpty(searchString))
                //{
                //    lstUserViewModel = _unitOfWork.GetRepositoryInstance<UserViewModel>().GetListByParameter(o => o.FullName.Contains(searchString)).OrderByDescending(o => o.FullName).ToList();
                //}
                int pageSize = (pagsiz ?? 5);
                ViewBag.PageSize = pageSize;
                int pageNumber = (page ?? 1);
                return View(lstUserViewModel.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception)
            {
                return View();
            }
        }

        public async Task<ActionResult> UsersForwarder(string currentFilter, string searchString, int? page, int? pagsiz)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            //var lstUser = await UserManager.Users.Include(x => x.Roles.Equals("Enterprise")).ToListAsync();
            var user = UserManager.Users.ToList();
            var lstUser = user.Where(x => x.Roles.Select(roles => roles.RoleId).Contains("6")).ToList();
            var lstUserViewModel = new List<UserViewModel>();
            foreach (ApplicationUser item in lstUser)
            {
                if (item.UsersInfo != null)
                {
                    var newItem = new UserViewModel();
                    newItem.Address = item.UsersInfo.Address;
                    newItem.FullName = item.UsersInfo.FullName;
                    newItem.MobilePhone = item.UsersInfo.MobilePhone;
                    newItem.UserName = item.UserName;
                    newItem.LastLogin = item.UsersInfo.LastLogin;
                    var roles = new List<string>();
                    foreach (var role in UserManager.GetRoles(item.Id))
                    {
                        role.ToString();
                        roles.Add(role.ToString());
                    }
                    newItem.Email = item.Email;
                    newItem.Roles = roles;
                    if (item.LockoutEnabled)
                    {
                        newItem.Status = 0;
                    }
                    else
                    {
                        newItem.Status = 1;

                    }
                    newItem.Id = item.UsersInfo.Id;
                    newItem.Created_At = item.UsersInfo.Created_At;
                    newItem.DispatcherName = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.UserId == item.UsersInfo.ApplicationUserID).DispatcherName;
                    newItem.DispatcherPhone = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.UserId == item.UsersInfo.ApplicationUserID).DispatcherPhone;
                    lstUserViewModel.Add(newItem);
                }
            }
            //if (!String.IsNullOrEmpty(searchString))
            //{
            //    lstUserViewModel = _unitOfWork.GetRepositoryInstance<UserViewModel>().GetListByParameter(o => o.FullName.Contains(searchString)).OrderByDescending(o => o.FullName).ToList();
            //}
            int pageSize = (pagsiz ?? 5);
            ViewBag.PageSize = pageSize;
            int pageNumber = (page ?? 1);
            return View(lstUserViewModel.ToPagedList(pageNumber, pageSize));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
