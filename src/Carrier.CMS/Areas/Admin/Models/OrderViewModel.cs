﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Carrier.Models.Entities;

namespace Carrier.CMS.Models
{
    public class OrderViewModel
    {

        public long Id { get; set; }
        public string MaVanDon { get; set; }
        public string TenHang { get; set; }
        public double? TrongLuong { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public double? Gia { get; set; }
        public int? Status { get; set; }
        public string UserName { get; set; }
        public int? OrganizationId { get; set; }
        public DateTime? CreateAt { get; set; }
        public string Created_By { get; set; }
        public Nullable<bool> VAT { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
    }
}