﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class ListCarView
    {
        public string Enterprise { get; set; }
        public int TotalCar { get; set; }
        public int TotalDriver { get; set; }
    }
}