﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class CarInfo
    {
        public int Id { get; set; }
        public string License { get; set; }
        public Nullable<double> Payload { get; set; }
        public Nullable<double> Width_Size { get; set; }
        public Nullable<double> Height_Size { get; set; }
        public Nullable<double> Leng_Size { get; set; }
        public int DriverId { get; set; }
    }
}