﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class UserLocationViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string LoaiXe { get; set; }
        public string TaiTrong { get; set; }
        public string CurrentLocation { get; set; }
        public string GPS { get; set; }
        public DateTime LastUpdate { get; set; }
        public string Status { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}