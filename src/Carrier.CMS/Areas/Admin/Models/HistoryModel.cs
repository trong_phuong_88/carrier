﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class HistoryModel
    {
        public int Id { get; set; }
        public long? OrderId { get; set; }
        public string MaVanDon { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CarName { get; set; }
        public string Status { get; set; }
        public int OrganzationId { get; set; }
        public string UserId { get; set; }
        public string DriverId { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public string OwnerId { get; set; }
        public string EvidencePath { get; set; }
        public string License { get; set; }
        public Nullable<double> Payload { get; set; }
        public Nullable<System.DateTime> Updated_At { get; set; }
        public Nullable<int> StatusOrder { get; set; }
    }
}