﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Models
{
    public class HtmlContentsViewModel
    {
        public int Id { get; set; }
        public int PageId { get; set; }
        [AllowHtml]
        public string Description { get; set; }
    }
}