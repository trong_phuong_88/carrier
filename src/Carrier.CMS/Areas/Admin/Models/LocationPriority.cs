﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Carrier.Models.Entities
{
    public static class EnumHelper
    {
        public static T GetEnumValue<T>(string str) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }
            T val;
            return Enum.TryParse<T>(str, true, out val) ? val : default(T);
        }

        public static T GetEnumValue<T>(int intValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }

            return (T)Enum.ToObject(enumType, intValue);
        }
    }
    public class LocationPriority
    {
        /// <summary>
        /// List city order by geo location  
        /// </summary>
        enum LPriority {NONE = -1, HAGIANG = 0, CAOBANG = 1, LAICHAU = 2, DIENBIEN = 3, BACKAN = 4, TUYENQUANG = 5, HANOI = 6, HAIDUONG = 7, HUE = 8, SAIGON = 9 };
        public int LocationToInt(string strLocation)
        {
            strLocation = strLocation.Replace(" ", "");
            var arrLocationName = Enum.GetNames(typeof(LPriority));
            foreach(string str in arrLocationName)
            {
                var strPraram = convertToUnSign3(strLocation.ToUpper().Trim());
                if (strPraram.Equals(str))
                {
                    return (int)EnumHelper.GetEnumValue<LPriority>(str);
                }
            }
            return (int)LPriority.NONE;
        }
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
    }
}