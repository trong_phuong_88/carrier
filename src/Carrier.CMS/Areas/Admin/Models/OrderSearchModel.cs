﻿using Carrier.Algorithm.Entity;
using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class OrderSearchModel
    {
        public Order order { get; set; }
        public System.Data.Entity.Spatial.DbGeography geolocation { get; set; }
        public Ward Ward { get; set; }
        public District District { get; set; }
        public City City { get; set; }
    }
}