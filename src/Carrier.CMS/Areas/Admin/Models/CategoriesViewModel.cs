﻿using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Models
{
    public class CategoriesViewModel
    {
        public Category categoryInfo { get; set; }
        public bool  isMenu { get; set; }
        public Category parentCategory { get; set; }

    }
}