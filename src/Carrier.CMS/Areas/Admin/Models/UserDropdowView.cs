﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Areas.Admin.Models
{
    public class UserDropdowView
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}