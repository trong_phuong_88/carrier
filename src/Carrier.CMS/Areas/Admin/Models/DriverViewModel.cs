﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Areas.Admin.Models
{
    public class DriverViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Sex { get; set; }
        public string BirthDay { get; set; }
        public Nullable<int> OrganizationId { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lng { get; set; }
        public string UserId { get; set; }
        public Nullable<int> Status { get; set; }
        public DateTime? Created_At { get; set; }
        public DateTime? Updated_At { get; set; }
        public string Front_Image { get; set; }
        public string Back_Image { get; set; }
        public string User_Name { get; set; }
        public bool LockoutEnabled { get; set; }
        public string License { get; set; }
    }
}