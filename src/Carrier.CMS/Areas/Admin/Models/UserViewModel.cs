﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class UserViewModel
    {

        public int Id { get; set; }
        public string ApplicationUserID { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string ConfrimPassWord { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string Address { get; set; }
        public string FullName { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
        public int Status { get; set; }
        public string DispatcherName { get; set; }
        public string DispatcherPhone { get; set; }
    }
}