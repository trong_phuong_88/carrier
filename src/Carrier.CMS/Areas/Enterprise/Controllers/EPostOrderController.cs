﻿using GoogleMaps.LocationServices;
using Carrier.CMS.Common;
using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Carrier.Utilities;
using Carrier.PushNotification;
using static Carrier.Utilities.GlobalCommon;
using Microsoft.AspNet.Identity;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER)]
    public class PostOrderController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        public PostOrderController()
        {
        }
        public ActionResult PostOrder()
        {
            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostOrder(FormCollection formCollection)
        {
            Order order = new Order();
            try
            {
                var ngayDi = formCollection["txtTimeDi"] != null ? formCollection["txtTimeDi"] : "";
                var gioDi = formCollection["ThoiGianBocHangDi"] != null ? formCollection["ThoiGianBocHangDi"] : "";
                var ngayDen = formCollection["txtTimeDen"] != null ? formCollection["txtTimeDen"] : "";
                var gioDen = formCollection["ThoiGianBocHangDen"] != null ? formCollection["ThoiGianBocHangDen"].ToString() : "";
                string fromLocation = formCollection["txtFromLocation"] != null ? formCollection["txtFromLocation"].ToString() : "";
                string toLocation = formCollection["txtToLocation"] != null ? formCollection["txtToLocation"].ToString() : "";

                double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString().Replace(".", "")) : 0;

                double price = formCollection["gia"] != null ? double.Parse(formCollection["gia"].ToString().Replace(".", "")) : 0;

                order.TenHang = formCollection["TenHang"] != null ? formCollection["TenHang"].ToString() : "";
                order.DienThoaiLienHe = formCollection["DienThoaiLienHe"] != null ? formCollection["DienThoaiLienHe"].ToString() : "";
                order.Gia = price;
                order.Status = PublicConstant.STATUS_PENDING;
                order.Note = formCollection["Note"] != null ? formCollection["Note"].ToString() : "";
                order.DiemDiChiTiet = fromLocation;
                order.DiemDenChiTiet = toLocation;
                //order.ThoiGianDi = string.Format("{0} {1}", gioDi, ngayDi);
                //order.ThoiGianDen = string.Format("{0} {1}", gioDen, ngayDen);
                order.UserName = User.Identity.Name;
                var thanhToan = formCollection["rThanhToan"] != null ? formCollection["rThanhToan"].ToString() : "0";
                if (thanhToan.Equals("Tiền mặt"))
                {
                    order.LoaiThanhToan = 0;
                }
                else
                {
                    order.LoaiThanhToan = 1;
                }
                order.MaVanDon = Ultilities.GenerateOrderCode(ngayDi, User.Identity.Name);
                order.CreateAt = DateTime.Now;
                order.UpdateAt = null;

                string message = "";
                double latFrom = 0, lngFrom = 0, latTo = 0, lngTo = 0;

                string latLongFrom = GetLatLongByAddress(ref message, fromLocation);
                if (message.Length == 0)
                {
                    latFrom = double.Parse(latLongFrom.Split(':')[0]);
                    lngFrom = double.Parse(latLongFrom.Split(':')[1]);
                }

                string latLongTo = GetLatLongByAddress(ref message, toLocation);
                if (message.Length == 0)
                {
                    latTo = double.Parse(latLongTo.Split(':')[0]);
                    lngTo = double.Parse(latLongTo.Split(':')[1]);
                }
                order.FromLat = latFrom;
                order.FromLng = lngFrom;
                order.ToLat = latTo;
                order.ToLng = lngTo;

                if (fromLocation.Length > 0 && toLocation.Length > 0)
                {
                    order.SoKmUocTinh = soKmUocTinh;// CalcDistance(latFrom, lngFrom, latTo, lngTo);
                }
                else
                {
                    order.SoKmUocTinh = 0;
                }

                order.Width = formCollection["txtWidth"] != null ? double.Parse(formCollection["txtWidth"].ToString()) : 0;
                order.Height = formCollection["txtHeight"] != null ? double.Parse(formCollection["txtHeight"].ToString()) : 0;
                order.Lenght = formCollection["txtLenght"] != null ? double.Parse(formCollection["txtLenght"].ToString()) : 0;
                order.VAT = formCollection["txtVAT"] != null ? bool.Parse(formCollection["txtVAT"].ToString()) : false;
                order.TrongLuong = formCollection["txtTrongLuong"] != null ? double.Parse(formCollection["txtTrongLuong"].ToString()) : 0;


                if (ModelState.IsValid)
                {
                    db.Order.Add(order);

                    // insert draft order

                    DraftOrder draft = new DraftOrder();
                    draft.Name = order.UserName;
                    draft.TenHang = order.TenHang;
                    draft.DiemDi = order.DiemDiChiTiet;
                    draft.DiemDen = order.DiemDenChiTiet;
                    draft.Gia = order.Gia;
                    //draft.FromLocation = order.FromLocation;
                    //draft.ToLocation = order.ToLocation;
                    draft.FromLat = order.FromLat;
                    draft.FromLng = order.FromLng;
                    draft.ToLat = order.ToLat;
                    draft.ToLng = order.ToLng;
                    draft.Vat = order.VAT;
                    draft.TrongLuong = order.TrongLuong;
                    //draft.ThoiGianDi = order.ThoiGianDi;
                    //draft.ThoiGianDen = order.ThoiGianDen;
                    draft.Note = order.Note;
                    draft.Created_At = order.CreateAt;
                    draft.Height = order.Height;
                    draft.Width = order.Width;
                    draft.Lenght = order.Lenght;
                    db.DraftOrder.Add(draft);

                    db.SaveChanges();
                    PushOrderForDriver(order.Id);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return View(order);
            }
            return View(order);
        }
        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }
        public JsonResult GetPrice(string type, string fromaddress, string toaddress, bool isVAT)
        {
            try
            {
                string message = "";
                double latFrom = 0, lngFrom = 0, latTo = 0, lngTo = 0;

                string latLongFrom = GetLatLongByAddress(ref message, fromaddress);
                if (message.Length == 0)
                {
                    latFrom = double.Parse(latLongFrom.Split(':')[0]);
                    lngFrom = double.Parse(latLongFrom.Split(':')[1]);
                }

                string latLongTo = GetLatLongByAddress(ref message, toaddress);
                if (message.Length == 0)
                {
                    latTo = double.Parse(latLongTo.Split(':')[0]);
                    lngTo = double.Parse(latLongTo.Split(':')[1]);
                }

                var distance = CalcDistance(latFrom, lngFrom, latTo, lngTo);
                distance = (int)Math.Round(distance);

                DataTable dt = null;//import.GetPriceOrder(type, distance, ref message);
                if (message.Length == 0 && dt.Rows.Count > 0)
                {
                    string priceValue = "";
                    if (isVAT)
                    {
                        double price = double.Parse(dt.Rows[0]["DonGia"].ToString());
                        price = (price + (((price / 100) * 10) + 1));

                        priceValue = price.ToString("#,###");
                    }
                    else
                    {
                        var price = double.Parse(dt.Rows[0]["DonGia"].ToString());
                        priceValue = price.ToString("#,###");
                    }

                    //var priceValue = dt.Rows[0]["DonGia"].ToString();

                    return Json(new { distance = distance, price = priceValue, message = "đọc file ok: " + dt.Rows.Count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //return Json("0");
                    return Json(new { message = "Lỗi đọc file excel:" + message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // return Json("0");
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public double CalcDistance(double latFrom, double longFrom, double latTo, double longTo)
        {
            var R = 6371d; // Radius of the earth in km
            var dLat = DegreeToRadian(latTo - latFrom);  // deg2rad below
            var dLon = DegreeToRadian(longTo - longFrom);
            var a =
              Math.Sin(dLat / 2d) * Math.Sin(dLat / 2d) +
              Math.Cos(DegreeToRadian(latFrom)) * Math.Cos(DegreeToRadian(latTo)) *
              Math.Sin(dLon / 2d) * Math.Sin(dLon / 2d);
            var c = 2d * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1d - a));
            var d = R * c; // Distance in km
            return d;
        }
        private double DegreeToRadian(double angle)
        {
            return angle * (Math.PI / 180d);
        }
        public void PushOrderForDriver(long id)
        {
            var order = db.Order.Where(x => x.Id == id).SingleOrDefault();
            var driver = GetFistDriverNearMost(100, 1, order);
            PushMessageForDriver(driver, order);
        }
        public void PushMessageForDriver(Driver driver, Order order)
        {
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + order.Id + "\"}";
          
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driver.UserId)).FirstOrDefault();
            if (device.Platform.ToLower().Equals("ios"))
            {
                // Fake to test
                //15B84D44F356451A1D88A2FC89A8186C97DF3D6BA2A21A9DC95773E22A7460A2
                //PushServices.Instance.PushAPNMessage(jsonMessage, new List<string> { device.TokenDevice });
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, null, null, null);
            }
            else
            {
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, null, null, null);
            }
            var orderTracking = new OrderTracking();
            orderTracking.OrderId = (int)order.Id;
            //orderTracking.UserId = User.Identity.GetUserId();
            orderTracking.Status = PublicConstant.ORDER_PENDING;
            orderTracking.Updated_At = DateTime.Now;
            db.OrderTracking.Add(orderTracking);
            db.SaveChangesAsync();
        }
        public Driver GetFistDriverNearMost(int distance, int page_size, Order order)
        {

            var lstDriver = db.Driver.ToList();
            List<Driver> lstDriverTemp = new List<Driver>();
            double currentDistinct = 0;
            foreach (Driver driver in lstDriver)
            {
                currentDistinct = GetDistance(double.Parse(driver.Lat.ToString()), double.Parse(driver.Lng.ToString()), double.Parse(order.FromLat.ToString()), double.Parse(order.FromLng.ToString()));
                if (currentDistinct < distance)
                {
                   // if (GreatThanDate(driver.Updated_At, DateTime.Now.ToString("HH:mm dd/MM/yyyy")))
                    {
                        lstDriverTemp.Add(driver);
                    }
                }
            }

            return lstDriverTemp.FirstOrDefault();

            //var lstDriver = db.Driver.Where(x => x.Location.Distance(order.FromLocation) < distance).Take(page_size);
            //var lstResult = new List<Driver>();
            //foreach (Driver item in lstDriver)
            //{
            //    if (GreatThanDate(item.Updated_At, DateTime.Now.ToString("HH:mm dd/MM/yyyy")))
            //    {
            //        lstResult.Add(item);
            //    }
            //}
            //return lstResult.FirstOrDefault();
        }
        private static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }
    }
}