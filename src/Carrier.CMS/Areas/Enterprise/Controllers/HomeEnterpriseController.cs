﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.Repository;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using Carrier.CMS.Common;
using System.Device.Location;
using System.Data.SqlClient;
using CMS.Carrier.Common;
using CMS.Carrier.Hubs;
using System.Configuration;
using Carrier.CMS.Hubs;
using System.Data.Entity;
using GoogleMaps.LocationServices;
using System.Xml;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]
    public class HomeEnterpriseController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public UserInfoes UserInfo
        {
            get { return _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId)); }
        }

        public string GetLocationFromAddress(double lat, double lng)
        {
            string addressSos = "";
            var locationService = new GoogleLocationService();
            var result = locationService.GetAddressFromLatLang(lat, lng);
            addressSos = result.Address;
            return addressSos;
        }
        

        // GET: Enterprise/HomeEnterprise
        public ActionResult Index()
        {
            UsersSettings latSetting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "lat");
            UsersSettings lngSetting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "lng");
            if (latSetting != null)
            {
                ViewBag.Lat = latSetting.SettingValue.Replace(",", ".");
            }
            else
            {
                ViewBag.Lat = UserInfo.Lat.ToString().Replace(",", ".");
            }

            if (lngSetting != null)
            {
                ViewBag.Lng = lngSetting.SettingValue.Replace(",", ".");
            }
            else
            {
                ViewBag.Lng = UserInfo.Lng.ToString().Replace(",", ".");
            }
            ViewBag.OrganizationId = UserInfo.Id;
            return View();
        }
        public List<ListCity> GetListCity()
        {
            List<ListCity> lstCity = new List<ListCity>();
            //var listCT = _unitOfWork.GetRepositoryInstance<City>().GetAllRecords();
            //foreach (var item in listCT)
            //{
            //    lstCity.Add(new ListCity() { Name = item.Name, CityCode = item.CityCode });
            //}
            return lstCity;
        }
        public JsonResult LoadListCity()
        {
            string strCode = "";
            List<ListCity> lstCity = GetListCity();
            foreach (var item in lstCity)
            {
                strCode += "<option value = \"" + item.Name + "\" id=\"" + item.CityCode + "\">" + item.Name + "</option>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }
        #region Fetch data for Map
        [JsonNetFilter]

        public JsonResult SearchDriver(string orderCode, string bsxCode)
        {
            if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            {
                //if (!string.IsNullOrEmpty(orderCode))
                //{
                var sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
                var sqlBsxCode = new SqlParameter("@bsxCode", System.Data.SqlDbType.NVarChar) { Value = bsxCode };

                var lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId @userId,@orderCode,@bsxCode",
                     sqlParentId, sqlOrderCode, sqlBsxCode).ToList();
                var dictData = new Dictionary<string, object>() { { "count", lstSearchOrderTracking.Count() }, { "drivers", lstSearchOrderTracking } };
                
                return Json(dictData, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    var sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                //    var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = "" };
                //    var sqlBsxCode = new SqlParameter("@bsxCode", System.Data.SqlDbType.NVarChar) { Value = bsxCode };

                //    var lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId @userId,@orderCode,@bsxCode",
                //         sqlParentId, sqlOrderCode, sqlBsxCode).ToList();
                //    var dictData = new Dictionary<string, object>() { { "count", lstSearchOrderTracking.Count() }, { "drivers", lstSearchOrderTracking } };
                //    return Json(dictData, JsonRequestBehavior.AllowGet);

                //}
            }
            else
            {
                var sqlParentId = new SqlParameter("@userId", System.Data.SqlDbType.NVarChar) { Value = UserId };
                var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
                var sqlBsxCode = new SqlParameter("@bsxCode", System.Data.SqlDbType.NVarChar) { Value = bsxCode };

                var lstSearchOrderTracking = _unitOfWork.GetRepositoryInstance<SP_GetAllDriverTrackingByUserId_Result>().GetResultBySqlProcedure("SP_GetAllDriverTrackingByUserId_Enterprise_New @userId,@orderCode,@bsxCode",
                     sqlParentId, sqlOrderCode, sqlBsxCode).ToList();
                var dictData = new Dictionary<string, object>() { { "count", lstSearchOrderTracking.Count() }, { "drivers", lstSearchOrderTracking } };
                return Json(dictData, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult SearchOrderTracking(string orderCode)
        {
            var sqlLat = new SqlParameter("@lat", System.Data.SqlDbType.Float) { Value = DBNull.Value };
            var sqlLng = new SqlParameter("@long", System.Data.SqlDbType.Float) { Value = DBNull.Value };
            var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = DBNull.Value };
            var sqlStatus = new SqlParameter("@status", System.Data.SqlDbType.Int) { Value = DBNull.Value };
            var sqlParentId = new SqlParameter("@parrentId", System.Data.SqlDbType.Int) { Value = -1 };
            var sqlOrderCode = new SqlParameter("@orderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
            var orderTracking = _unitOfWork.GetRepositoryInstance<SearchOrderTrackingByDistance_Result>().GetResultBySqlProcedure("SearchOrderTrackingByDistance @lat,@long,@distance,@status,@parrentId,@orderCode",
                sqlLat, sqlLng, sqlDistance, sqlStatus, sqlParentId, sqlOrderCode).FirstOrDefault();
            return Json(orderTracking, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchOrder(int distance, int type)
        {
            var lstOrders = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(x => x.UserName.Equals(User.Identity.Name) && x.Status == PublicConstant.STATUS_ACTIVE);
            var dictData = new Dictionary<string, object>() { { "count", lstOrders.Count() }, { "Data", lstOrders } };
            return Json(dictData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllDrivers(string orderCode, string driverName, int type)
        {
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = UserId };
            var sqlOrderCode = new SqlParameter("@OrderCode", System.Data.SqlDbType.NVarChar) { Value = orderCode };
            var sqlDriverName = new SqlParameter("@DriverName", System.Data.SqlDbType.NVarChar) { Value = driverName };
            var sqlType = new SqlParameter("@Type", System.Data.SqlDbType.Int) { Value = type };
            var lstDrivers = _unitOfWork.GetRepositoryInstance<SearchOrderTrackingByDistance_Result>().GetResultBySqlProcedure("SearchOrderTrackingByDistance @UserId,@OrderCode,@DriverName,@Type",
                sqlUserId, sqlOrderCode, sqlDriverName, sqlType).FirstOrDefault();
            return Json(lstDrivers, JsonRequestBehavior.AllowGet);
        }
        #endregion

        /// <summary>
        /// lấy thông báo đẩy về client
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageOrder()
        {
            MessageRespository messageService = new MessageRespository();
            return PartialView("~/Views/Shared/_MessagesListOrder.cshtml", messageService.GetListMessageOrder(User.Identity.GetUserId(), PublicConstant.ROLE_ENTERPRISE));
        }

        /// <summary>
        /// lấy thông báo đơn hàng mới chạy
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageOrderNewRun()
        {
            if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            {
                MessageRespository messageService = new MessageRespository();
                return PartialView("~/Views/Shared/_MessagesListOrderNewRun.cshtml", messageService.GetListMessageOrderByStatus(User.Identity.GetUserId(), PublicConstant.ROLE_FORWARDER, PublicConstant.ORDER_ACCEPT));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// lấy thông báo đơn hàng mới kết thúc
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageOrderNewFinish()
        {
            if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            {
                MessageRespository messageService = new MessageRespository();
                return PartialView("~/Views/Shared/_MessagesListOrderNewFinish.cshtml", messageService.GetListMessageOrderByStatus(User.Identity.GetUserId(), PublicConstant.ROLE_FORWARDER, PublicConstant.ORDER_FINISHED));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// lấy thông báo đơn hàng quá hạn
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageOrderExpired()
        {
            if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            {
                MessageRespository messageService = new MessageRespository();
                return PartialView("~/Views/Shared/_MessagesListOrderExpired.cshtml", messageService.GetListMessageOrderByStatus(User.Identity.GetUserId(), PublicConstant.ROLE_FORWARDER, PublicConstant.ORDER_EXPIRED));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// lấy thông báo đẩy về client
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageNotification()
        {
            MessageRespository orderService = new MessageRespository();
            return PartialView("~/Views/Shared/_MessageListNotification.cshtml", orderService.GetListMessageNotification(User.Identity.GetUserId(), PublicConstant.ROLE_ENTERPRISE));
        }

        /// <summary>
        /// lấy danh sách tài xế của doanh nghiệp mới nhận đơn
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMessageOrderTrackingForEnterprise(long orderId)
        {
            MessageRespository studentService = new MessageRespository();
            return PartialView("~/Views/Shared/_MessageListOrdertrackingOfEnterprise.cshtml", studentService.GetListMessageOrderTrackingForEnterprise(User.Identity.GetUserId(), orderId));
        }



        public JsonResult GetMessages()
        {
            IEnumerable<string> messages = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetAllRecordsIQueryable().Select(x => x.CMND).ToList();
            var dataOutput = Json(messages, JsonRequestBehavior.AllowGet);
            return dataOutput;
        }
         
        //Thông báo cảnh báo hỏng xe
        public ActionResult GetReportSOS()
        {
            ReportSOSRespository messageService = new ReportSOSRespository();
            return PartialView("~/Views/Shared/_ReportSOS.cshtml", messageService.GetListReport(User.Identity.GetUserId(), PublicConstant.ROLE_ENTERPRISE));
        }

        /// <summary>
        /// lấy đơn hàng mới tạo của fowarder thông báo cho enterprise đc tao từ hdtranin (popup)
        /// </summary>
        /// <returns></returns>
        public ActionResult GetOrderNewForEnterpriseFromFowarder()
        {
            MessageRespository messageService = new MessageRespository();
            return PartialView("~/Views/Shared/_PopupMessageNewOrder.cshtml", messageService.GetOrderNewForEnterpriseFromFowarder(User.Identity.GetUserId(), PublicConstant.ROLE_ENTERPRISE));
        }

        /// <summary>
        /// lấy danh sách đơn hàng mới tạo của fowarder thông báo cho enterprise đc tao từ hdtranin (list message)
        /// </summary>
        /// <returns></returns>
        public ActionResult GetListOrderNewForEnterpriseFromFowarder()
        {
            MessageRespository messageService = new MessageRespository();
            return PartialView("~/Views/Shared/_MessageNewOrderFromHdintertrans.cshtml", messageService.GetListOrderNewForEnterpriseFromFowarder(User.Identity.GetUserId(), PublicConstant.ROLE_ENTERPRISE));
        }

        //public string UpdateSystemSettting(string NumberRowInGrid, string TenDieuPhoiVien, string DienThoaiDieuPhoiVien, string city, string address)
        public string UpdateSystemSettting(string NumberRowInGrid, string TenDieuPhoiVien, string DienThoaiDieuPhoiVien, string city)
        {
            string str = "";
            try
            {
                UsersSettings setting = new UsersSettings();
                //NumberRowInGrid
                setting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "NumberRowInGrid");
                if (setting == null)// insert
                {
                    setting = new UsersSettings();
                    setting.UserId = UserId;
                    setting.SettingKey = "NumberRowInGrid";
                    setting.SettingValue = NumberRowInGrid;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Add(setting);
                    _unitOfWork.SaveChanges();
                }
                else // update
                {
                    setting.UserId = UserId;
                    setting.SettingKey = "NumberRowInGrid";
                    setting.SettingValue = NumberRowInGrid;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Update(setting);
                    _unitOfWork.SaveChanges();
                }

                //TenDieuPhoiVien
                setting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "TenDieuPhoiVien");
                if (setting == null)// insert
                {
                    setting = new UsersSettings();
                    setting.UserId = UserId;
                    setting.SettingKey = "TenDieuPhoiVien";
                    setting.SettingValue = TenDieuPhoiVien;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Add(setting);
                    _unitOfWork.SaveChanges();
                }
                else // update
                {
                    setting.UserId = UserId;
                    setting.SettingKey = "TenDieuPhoiVien";
                    setting.SettingValue = TenDieuPhoiVien;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Update(setting);
                    _unitOfWork.SaveChanges();
                }

                //DienThoaiDieuPhoiVien
                setting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "DienThoaiDieuPhoiVien");
                if (setting == null)// insert
                {
                    setting = new UsersSettings();
                    setting.UserId = UserId;
                    setting.SettingKey = "DienThoaiDieuPhoiVien";
                    setting.SettingValue = DienThoaiDieuPhoiVien;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Add(setting);
                    _unitOfWork.SaveChanges();
                }
                else // update
                {
                    setting.UserId = UserId;
                    setting.SettingKey = "DienThoaiDieuPhoiVien";
                    setting.SettingValue = DienThoaiDieuPhoiVien;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Update(setting);
                    _unitOfWork.SaveChanges();
                }


                //city
                setting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "city");
                if (setting == null)// insert
                {
                    setting = new UsersSettings();
                    setting.UserId = UserId;
                    setting.SettingKey = "city";
                    setting.SettingValue = city;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Add(setting);
                    _unitOfWork.SaveChanges();
                }
                else // update
                {
                    setting.UserId = UserId;
                    setting.SettingKey = "city";
                    setting.SettingValue = city;
                    _unitOfWork.GetRepositoryInstance<UsersSettings>().Update(setting);
                    _unitOfWork.SaveChanges();
                }

                //if (address.Length > 0)
                if (city.Length > 0)
                {
                    string message = "";
                    string lat = "0", lng = "0";

                    //string latLongFrom = GetLatLongByAddress(ref message, address);
                    string latLongFrom = GetLatLongByAddress(ref message, city + ", Việt Nam");
                    if (message.Length == 0)
                    {
                        lat = latLongFrom.Split(':')[0];
                        lng = latLongFrom.Split(':')[1];
                    }
                    else
                    {
                        lat = "0";
                        lng = "0";
                    }

                    //lat
                    setting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "lat");
                    if (setting == null)// insert
                    {
                        setting = new UsersSettings();
                        setting.UserId = UserId;
                        setting.SettingKey = "lat";
                        setting.SettingValue = lat;
                        _unitOfWork.GetRepositoryInstance<UsersSettings>().Add(setting);
                        _unitOfWork.SaveChanges();
                    }
                    else // update
                    {
                        setting.UserId = UserId;
                        setting.SettingKey = "lat";
                        setting.SettingValue = lat;
                        _unitOfWork.GetRepositoryInstance<UsersSettings>().Update(setting);
                        _unitOfWork.SaveChanges();
                    }

                    //lng
                    setting = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetFirstOrDefaultByParameter(o => o.UserId == UserId && o.SettingKey == "lng");
                    if (setting == null)// insert
                    {
                        setting = new UsersSettings();
                        setting.UserId = UserId;
                        setting.SettingKey = "lng";
                        setting.SettingValue = lng;
                        _unitOfWork.GetRepositoryInstance<UsersSettings>().Add(setting);
                        _unitOfWork.SaveChanges();
                    }
                    else // update
                    {
                        setting.UserId = UserId;
                        setting.SettingKey = "lng";
                        setting.SettingValue = lng;
                        _unitOfWork.GetRepositoryInstance<UsersSettings>().Update(setting);
                        _unitOfWork.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                str = ex.Message;
            }

            return str;
        }

        #region Get Lat Long
        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }
        public double CalcDistance(double latFrom, double longFrom, double latTo, double longTo)
        {
            var R = 6371d; // Radius of the earth in km
            var dLat = DegreeToRadian(latTo - latFrom);  // deg2rad below
            var dLon = DegreeToRadian(longTo - longFrom);
            var a =
              Math.Sin(dLat / 2d) * Math.Sin(dLat / 2d) +
              Math.Cos(DegreeToRadian(latFrom)) * Math.Cos(DegreeToRadian(latTo)) *
              Math.Sin(dLon / 2d) * Math.Sin(dLon / 2d);
            var c = 2d * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1d - a));
            var d = R * c; // Distance in km
            return d;
        }
        private double DegreeToRadian(double angle)
        {
            return angle * (Math.PI / 180d);
        }
        #endregion

        #region Setting System
        public JsonResult loadInfoSetting()
        {
            List<UsersSettings> lstSettings = new List<UsersSettings>();
            Carrier3Entities db = new Carrier3Entities();
            lstSettings = _unitOfWork.GetRepositoryInstance<UsersSettings>().GetListByParameter(o => o.UserId == UserId).ToList();

            return Json(lstSettings, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
    public class SetttingModel
    {
        public string SettingKey { get; set; }
        public string SettingValue { get; set; }
    }
    public class ListCity
    {
        public string Name { get; set; }
        public string CityCode { get; set; }
    }
}