﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Utilities;
using CMS.Carrier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE)]
    public class ERevenueController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        // GET: Revenue
        public ActionResult Index(string txtFromDate = null, string txtToDate = null, int OrganizationId = 0, string ddlDriver = null)
        {
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                List<Organization> listOrganization = new List<Organization>();
                listOrganization = db.Organization.ToList();
                IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listOrganizationSelect;
            }
            else
            {
                int currentOrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                List<Driver> listDriver = new List<Driver>();
                listDriver = db.Driver.Where(m => m.OrganizationId == currentOrganizationId).ToList();
                IEnumerable<SelectListItem> listDriverSelect = listDriver.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                if (listDriverSelect.Count() > 0)
                {
                    ViewBag.ListDriver = listDriverSelect;
                }
                else
                {
                    ViewBag.ListDriver = null;
                }
            }

            List<RevenueViewModel> listOrder = new List<RevenueViewModel>();

            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                listOrder = (from a in db.AspNetUsers
                             join b in db.UserInfoes on a.Id equals b.ApplicationUserID
                             join o in db.Order on a.UserName equals o.UserName
                             where o.Status == PublicConstant.ORDER_FINISHED
                             select new RevenueViewModel
                             {
                                 OrganizationId = b.OrganzationId,
                                 Gia = o.Gia,
                                 UserId = a.Id,
                                 CrateDate = o.CreateAt,
                                 FullName = b.FullName,
                                 UserName = a.UserName
                             }).ToList();

            }
            else // enterprise
            {
                var CurrentUsertId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                listOrder = (from a in db.AspNetUsers
                             join b in db.UserInfoes on a.Id equals b.ApplicationUserID
                             join o in db.Order on a.UserName equals o.UserName
                             where o.Status == PublicConstant.ORDER_FINISHED
                             && b.OrganzationId == CurrentUsertId
                             select new RevenueViewModel
                             {
                                 OrganizationId = b.OrganzationId,
                                 Gia = o.Gia,
                                 UserId = a.Id,
                                 CrateDate = o.CreateAt,
                                 FullName = b.FullName,
                                 UserName = a.UserName
                             }).ToList();
            }

            if (txtFromDate != null && txtToDate != null)
            {
                var fromdate = DateTime.Parse(txtFromDate);
                var todate = DateTime.Parse(txtToDate);
                listOrder = listOrder.Where(o => o.CrateDate >= fromdate && o.CrateDate <= todate).ToList();
            }
            if (OrganizationId > 0)
            {
                listOrder = listOrder.Where(o => o.OrganizationId == OrganizationId).ToList();
            }
            if (ddlDriver != null)
            {
                listOrder = listOrder.Where(o => o.UserId == ddlDriver).ToList();
            }

            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                listOrder = listOrder.OrderBy(o => o.OrganizationId).Distinct().ToList();
            }
            else
            {
                listOrder = listOrder.OrderBy(o => o.UserName).Distinct().ToList();
            }

            double? totalPrice = 0;
            foreach (var item in listOrder)
            {
                totalPrice = totalPrice + item.Gia;
            }
            ViewBag.TotalMoney = totalPrice;

            return View(listOrder);
        }

        [HttpPost]
        public ActionResult GetDriverByOrganizationId(int Id)
        {
            List<Driver> listDriver = new List<Driver>();
            listDriver = db.Driver.Where(m => m.OrganizationId == Id).ToList();
            SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
            return Json(listDriverReturn);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}
