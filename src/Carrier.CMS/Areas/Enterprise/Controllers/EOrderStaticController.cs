﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Carrier.Models;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Threading;
using NPOI.XSSF.UserModel;
using CMS.Carrier.Common;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]

    public class EOrderStaticController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        // GET: OrderStatic
        public ActionResult Index()
        {
            List<UserFowarder> listOrganization = new List<UserFowarder>();
            var lstForwarder = _unitOfWork.GetRepositoryInstance<MapOrder>().GetListByParameter(x => x.FromUser.Equals(UserId)).ToList();
            if (lstForwarder.Count > 0)
            {
                listOrganization.Add(new UserFowarder()
                {
                    Id = "",
                    UserName = "----"
                });
                foreach (var item in lstForwarder)
                {
                    UserFowarder usInfor = new UserFowarder();
                    var user = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetFirstOrDefaultByParameter(o => o.Id.Equals(item.ToUser));
                    if (user != null)
                    {
                        usInfor.Id = item.ToUser;
                        usInfor.UserName = user.UserName;
                    }
                    else
                    {

                    }
                    listOrganization.Add(usInfor);
                }
            }
            IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.UserName.Trim(), Value = x.Id.ToString() });
            ViewBag.ListOrganization = listOrganizationSelect;
            var userId = User.Identity.GetUserId();
            var txtFromDate = Request.Form["txtFromDate"] != null ? Request.Form["txtFromDate"].ToString() : "";
            var txtToDate = Request.Form["txtToDate"] != null ? Request.Form["txtToDate"].ToString() : "";
            var userFW = Request.Form["userFowarder"] != null ? Request.Form["userFowarder"].ToString() : "";

            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlFromDate = new SqlParameter("@fromDate", System.Data.SqlDbType.NVarChar) { Value = txtFromDate };
            var sqlToDate = new SqlParameter("@toDate", System.Data.SqlDbType.NVarChar) { Value = txtToDate };
            var sqlUserFowarder = new SqlParameter("@userFwId", System.Data.SqlDbType.NVarChar) { Value = userFW };


            var lstdata = _unitOfWork.GetRepositoryInstance<SP_ThongKe_CuocVanChuyen_Result>().GetResultBySqlProcedure("SP_ThongKe_CuocVanChuyen @userId,@fromDate,@toDate,@userFwId", sqlUserId, sqlFromDate, sqlToDate, sqlUserFowarder).ToList();
            TempData["Data"] = lstdata;
            return View(lstdata);
        }

        protected void exportExcel_Click(object sender, EventArgs e)
        {
            List<SP_ThongKe_CuocVanChuyen_Result> lstData = TempData["Data"] as List<SP_ThongKe_CuocVanChuyen_Result>;
            exportExcel_function();
        }

        public void exportExcel_function()
        {
            try
            {
                List<SP_ThongKe_CuocVanChuyen_Result> lstData = TempData["Data"] as List<SP_ThongKe_CuocVanChuyen_Result>;
                UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId));
                int month, year;
                string add = "", addUser="";
                if (userInfo != null)
                {
                    addUser = userInfo.Address;
                }

                SP_ThongKe_CuocVanChuyen_Result firstdata = lstData[0] as SP_ThongKe_CuocVanChuyen_Result;
                month = firstdata.CreateAt.Value.Month;
                year = firstdata.CreateAt.Value.Year;
                double tongcuoc = 0, tongcuocVat = 0, tongthanhtoan = 0;
                Response.ClearContent();
                Response.Buffer = true;
                StringWriter stringWriter = new StringWriter();
                HtmlTextWriter textWriter = new HtmlTextWriter(stringWriter);
                StringBuilder sb = new StringBuilder();
                //string filePath = Path.Combine("H:\\temp.xls");
                string filePath = Path.Combine("C:\\temp.xls");
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                HSSFWorkbook inputWorkbook = new HSSFWorkbook();

                String sheetName = "Sheet1";
                HSSFSheet sheet = (HSSFSheet)inputWorkbook.CreateSheet(sheetName);
                sheet.SetColumnWidth(0, 3000);
                sheet.SetColumnWidth(1, 3600);
                sheet.SetColumnWidth(2, 3000);
                sheet.SetColumnWidth(3, 3000);
                sheet.SetColumnWidth(4, 18000);
                sheet.SetColumnWidth(5, 5000);
                /* datastyle : sử dụng bản ghi chữ thường */
                ICellStyle datastyle = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                IFont font_ = inputWorkbook.CreateFont();
                font_.IsBold = false;
                font_.FontName = "Times New Roman";
                font_.FontHeightInPoints = (short)11;
                datastyle.SetFont(font_);
                datastyle.Alignment = HorizontalAlignment.Right;
                //datastyle.BorderBottom = datastyle.BorderRight = datastyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                datastyle.WrapText = true;

                /* datastyle1: sử dụng bản ghi chữ in đậm cho các cột */
                ICellStyle datastyle1 = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                IFont font = inputWorkbook.CreateFont();
                font.IsBold = true;
                font.FontName = "Times New Roman";
                font.FontHeightInPoints = (short)11;
                datastyle1.BorderBottom = datastyle1.BorderRight = datastyle1.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                datastyle1.SetFont(font);
                datastyle1.Alignment = HorizontalAlignment.Center;
                datastyle1.VerticalAlignment = VerticalAlignment.Center;
                datastyle1.WrapText = true;

                /* datastyle_tieude báo cáo : */
                IFont font_thongtin_tieu_de = inputWorkbook.CreateFont();
                ICellStyle datastyle_tieude = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                font_thongtin_tieu_de.IsBold = true;
                font_thongtin_tieu_de.FontName = "Times New Roman";
                font_thongtin_tieu_de.FontHeightInPoints = (short)13;
                datastyle_tieude.SetFont(font_thongtin_tieu_de);
                datastyle_tieude.Alignment = HorizontalAlignment.Center;
                datastyle_tieude.VerticalAlignment = VerticalAlignment.Center;
                datastyle_tieude.WrapText = false;

                /* datastyle not border */
                IFont font_thongtin_nd = inputWorkbook.CreateFont();
                ICellStyle datastyle_nd = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                ICellStyle datastyle_header = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                font_thongtin_nd.IsBold = false;
                font_thongtin_nd.FontName = "Times New Roman";
                font_thongtin_nd.FontHeightInPoints = (short)11;
                datastyle_nd.BorderBottom = datastyle_nd.BorderRight = datastyle_nd.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                datastyle_nd.SetFont(font_thongtin_nd);
                datastyle_nd.WrapText = true;
                datastyle_header.SetFont(font_thongtin_nd);
                datastyle_header.WrapText = false;
                datastyle_nd.Alignment = HorizontalAlignment.Center;
                datastyle_nd.VerticalAlignment = VerticalAlignment.Center;


                //datastyle cho footer
                IFont font_footer = inputWorkbook.CreateFont();
                ICellStyle datastyle_ft = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                font_footer.IsBold = true;
                font_footer.FontName = "Times New Roman";
                font_footer.FontHeightInPoints = (short)11;
                datastyle_ft.BorderBottom = datastyle_ft.BorderRight = datastyle_ft.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                datastyle_ft.SetFont(font_footer);
                datastyle_ft.WrapText = false;
                datastyle_ft.Alignment = HorizontalAlignment.Center;
                datastyle_ft.VerticalAlignment = VerticalAlignment.Center;

                /* datastyle : chú thích */
                IFont font_hdv = inputWorkbook.CreateFont();
                ICellStyle datastyle_hdv = (HSSFCellStyle)inputWorkbook.CreateCellStyle();
                font_hdv.IsBold = true;
                font_hdv.FontName = "Times New Roman";
                font_hdv.FontHeightInPoints = (short)11;
                datastyle_hdv.SetFont(font_hdv);
                datastyle_hdv.WrapText = false;
                datastyle_hdv.Alignment = HorizontalAlignment.Center;
                datastyle_hdv.VerticalAlignment = VerticalAlignment.Center;


                #region Thông tin 
                HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);
                ICell titleCell = headerRow.CreateCell(0);
                titleCell.CellStyle = inputWorkbook.CreateCellStyle();
                titleCell.CellStyle.SetFont(font);
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 5));
                titleCell.SetCellValue("Bên vận chuyển: Công ty TNHH Công Nghệ Carrier ");
                //header
                for (int dem = 0; dem <= 2; dem = dem + 2)
                {
                    sheet.CreateRow(sheet.LastRowNum + 1);
                    HSSFRow row1 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                    row1.CreateCell(0).CellStyle = datastyle_header;
                    if (dem == 0)
                    {
                        row1.Cells[0].SetCellValue("Địa chỉ: Thôn Xuân Long, Xã Thủy Xuân Tiên, Huyện Chương Mỹ, Thành Phố Hà Nội, Việt Nam");
                    }
                    else if (dem == 2)
                    {
                        row1.Cells[0].SetCellValue("Mã số thuế: 0107709166 ");
                    }
                }

                // tiêu đề 
                sheet.CreateRow(sheet.LastRowNum + 2);
                HSSFRow row = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                ICell tCell = row.CreateCell(0);
                tCell.CellStyle = inputWorkbook.CreateCellStyle();
                tCell.CellStyle.SetFont(font);
                tCell.CellStyle.Alignment = HorizontalAlignment.Center;
                tCell.CellStyle.VerticalAlignment = VerticalAlignment.Center;
                tCell.CellStyle.WrapText = true;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(4, 4, 0, 5));
                tCell.SetCellValue("BẢNG KÊ CHI TIẾT VẬN CHUYỂN THÁNG " + month + "/" +year);
                //tCell.SetCellValue("BẢNG KÊ CHI TIẾT VẬN CHUYỂN " + "Tháng" + month + "/" + year);

                #endregion
                #region Tiêu đề danh sách 
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 0, 0));
                //sheet.SetColumnWidth(0,)
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 1, 1));
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 2, 2));
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 3, 3));
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 4, 4));
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(8, 8, 5, 5));

                sheet.CreateRow(sheet.LastRowNum + 2);
                HSSFRow row2 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                row2.CreateCell(0).CellStyle = datastyle1;
                row2.CreateCell(1).CellStyle = datastyle1;
                row2.CreateCell(2).CellStyle = datastyle1;
                row2.CreateCell(3).CellStyle = datastyle1;
                row2.CreateCell(4).CellStyle = datastyle1;
                row2.CreateCell(5).CellStyle = datastyle1;

                row2.Cells[0].SetCellValue("MVD");
                row2.Cells[1].SetCellValue("Ngày");
                row2.Cells[2].SetCellValue("POD");
                row2.Cells[3].SetCellValue("Số tấn");
                row2.Cells[4].SetCellValue("Diễn giải");
                row2.Cells[5].SetCellValue("Số tiền");
                #endregion
                #region Danh sách vận đơn
                int sovandon = 0;
                foreach (SP_ThongKe_CuocVanChuyen_Result ctk in lstData)
                {
                    tongcuoc += ctk.Gia.Value;
                    sovandon++;
                    sheet.CreateRow(sheet.LastRowNum + 1);
                    HSSFRow row6 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                    row6.CreateCell(0).CellStyle = datastyle_nd;
                    row6.CreateCell(1).CellStyle = datastyle_nd;
                    row6.CreateCell(2).CellStyle = datastyle_nd;
                    row6.CreateCell(3).CellStyle = datastyle_nd;
                    row6.CreateCell(4).CellStyle = datastyle_nd;
                    row6.CreateCell(5).CellStyle = datastyle_nd;


                    row6.Cells[0].SetCellValue(ctk.MaVanDon);
                    row6.Cells[1].SetCellValue(Utitlitys.DatetimeToVN(ctk.CreateAt.Value));
                    row6.Cells[2].SetCellValue("");
                    row6.Cells[3].SetCellValue(ctk.Payload.Value);
                    row6.Cells[4].SetCellValue(ctk.DiemDiChiTiet + " - " + ctk.DiemDenChiTiet);
                    row6.Cells[5].SetCellValue(Ultilities.ConvertToCurrency(ctk.Gia.Value));
                }
                tongcuocVat = tongcuoc * 0.1;
                tongthanhtoan = tongcuoc + tongcuocVat;
                string docsotien = ConvertCurrentcy.convertDocSo(tongthanhtoan);

                #region  Onfooter
                //Tổng cước
                sheet.CreateRow(sheet.LastRowNum + 1);
                HSSFRow row15 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                row15.CreateCell(0).CellStyle = datastyle_ft;
                row15.CreateCell(1).CellStyle = datastyle_ft;
                row15.CreateCell(2).CellStyle = datastyle_ft;
                row15.CreateCell(3).CellStyle = datastyle_ft;
                row15.CreateCell(4).CellStyle = datastyle_ft;
                row15.CreateCell(5).CellStyle = datastyle_ft;
                int currentrow = row15.RowNum;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow, currentrow, 1, 4));

                row15.Cells[1].SetCellValue("Tổng cước");
                row15.Cells[5].SetCellValue(Ultilities.ConvertToCurrency(tongcuoc));

                //Vat
                sheet.CreateRow(sheet.LastRowNum + 1);
                HSSFRow row16 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                row16.CreateCell(0).CellStyle = datastyle_ft;
                row16.CreateCell(1).CellStyle = datastyle_ft;
                row16.CreateCell(2).CellStyle = datastyle_ft;
                row16.CreateCell(3).CellStyle = datastyle_ft;
                row16.CreateCell(4).CellStyle = datastyle_ft;
                row16.CreateCell(5).CellStyle = datastyle_ft;
                int currentrow16 = row16.RowNum;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow16, currentrow16, 1, 4));

                row16.Cells[1].SetCellValue("VAT (10%)");
                row16.Cells[5].SetCellValue(Ultilities.ConvertToCurrency(tongcuocVat));

                //tongthanh toan
                sheet.CreateRow(sheet.LastRowNum + 1);
                HSSFRow row17 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                row17.CreateCell(0).CellStyle = datastyle_ft;
                row17.CreateCell(1).CellStyle = datastyle_ft;
                row17.CreateCell(2).CellStyle = datastyle_ft;
                row17.CreateCell(3).CellStyle = datastyle_ft;
                row17.CreateCell(4).CellStyle = datastyle_ft;
                row17.CreateCell(5).CellStyle = datastyle_ft;
                int currentrow17 = row17.RowNum;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow17, currentrow17, 1, 4));

                row17.Cells[1].SetCellValue("Tổng thanh toán");
                row17.Cells[5].SetCellValue(Ultilities.ConvertToCurrency(tongthanhtoan));

                //Đọc chữ
                sheet.CreateRow(sheet.LastRowNum + 1);
                HSSFRow row18 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                row18.CreateCell(0).CellStyle = datastyle_ft;
                row18.CreateCell(1).CellStyle = datastyle_ft;
                row18.CreateCell(2).CellStyle = datastyle_ft;
                row18.CreateCell(3).CellStyle = datastyle_ft;
                row18.CreateCell(4).CellStyle = datastyle_ft;
                row18.CreateCell(5).CellStyle = datastyle_ft;
                int currentrow18 = row18.RowNum;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow18, currentrow18, 0, 5));

                row18.Cells[0].SetCellValue(docsotien);
                #endregion


                #endregion
                #region footer

                var datenow = DateTime.Now;
                int d = datenow.Day;
                int m = datenow.Month;
                int y = datenow.Year;
                sheet.CreateRow(sheet.LastRowNum + 2);
                HSSFRow row5 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                ICell tCell5 = row5.CreateCell(4);
                IFont font5 = inputWorkbook.CreateFont();
                font5.IsItalic = true;
                font5.FontName = "Times New Roman";
                font5.FontHeightInPoints = (short)11;
                tCell5.CellStyle = inputWorkbook.CreateCellStyle();
                tCell5.CellStyle.SetFont(font5);
                tCell5.CellStyle.Alignment = HorizontalAlignment.Center;
                tCell5.CellStyle.VerticalAlignment = VerticalAlignment.Center;
                tCell5.CellStyle.WrapText = true;
                int currentrow45 = row5.RowNum;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow45, currentrow45, 4, 5));
                tCell5.SetCellValue("................, Ngày "+ d +" tháng " + m + " năm " + y);

                sheet.CreateRow(sheet.LastRowNum + 1);
                HSSFRow row4 = (HSSFRow)sheet.GetRow(sheet.LastRowNum);
                row4.Height = 400;
                row4.CreateCell(0).CellStyle = datastyle_hdv;
                row4.CreateCell(1).CellStyle = datastyle_hdv;
                row4.CreateCell(2).CellStyle = datastyle_hdv;
                row4.CreateCell(3).CellStyle = datastyle_hdv;
                row4.CreateCell(4).CellStyle = datastyle_hdv;
                row4.CreateCell(5).CellStyle = datastyle_hdv;
                int currentrow4 = row4.RowNum;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow4, currentrow4, 0, 3));
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(currentrow4, currentrow4, 4, 5));
                row4.Cells[0].SetCellValue("BÊN THUÊ VẬN CHUYỂN");
                row4.Cells[4].SetCellValue("BÊN VẬN CHUYỂN");
                #endregion

                fs.Close();
                //string filePathdownload = Path.Combine("H:\\template_download.xls");
                string filePathdownload = Path.Combine("C:\\template_download.xls");
                FileStream fsw = new FileStream(filePathdownload, FileMode.Open, FileAccess.Write);
                inputWorkbook.Write(fsw);
                fsw.Close();
                Response.ClearContent();
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "BaoCaoVanChuyen.xls"));
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = Encoding.UTF8.EncodingName;
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                Response.TransmitFile(filePathdownload);
            }
            catch (Exception ex)
            {
                //try
                //{
                //    //stop processing the script and return the current result
                //    HttpContext.Current.Response.End();
                //}
                //catch (ThreadAbortException ex)
                //{
                //    log.Error(ex);
                //}
                //catch (Exception ex)
                //{
                //    log.Error(ex);
                //}
                //finally
                //{
                //    //Sends the response buffer
                //    HttpContext.Current.Response.Flush();
                //    // Prevents any other content from being sent to the browser
                //    HttpContext.Current.Response.SuppressContent = true;
                //    //Directs the thread to finish, bypassing additional processing
                //    HttpContext.Current.ApplicationInstance.CompleteRequest();
                //    //Suspends the current thread
                //    Thread.Sleep(1);
                //    //Thread.EndCriticalRegion();
                //}
                ////Response.End();
            }

        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        public string GetNameUser(string s)
        {
            string st = "";
            var user = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetFirstOrDefaultByParameter(o => o.Id.Equals(s));
            if (user != null)
            {
                st = user.UserName;
            }
            else
            {
                st = string.Empty;
            }
            return st;
        }
        public ActionResult PopUpEnterprise(string userId)
        {
            UserInfoes user = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID.Equals(userId));
            return PartialView("_UserDetail", user);
        }
    }
    public class UserFowarder
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}