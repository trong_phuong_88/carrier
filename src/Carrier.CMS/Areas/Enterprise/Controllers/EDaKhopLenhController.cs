﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.CMS.Models;
using PagedList;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Common;
using Microsoft.AspNet.Identity;
using System.Configuration;
using CMS.Carrier.Common;
using Carrier.CMS.Hubs;
using System.Data.SqlClient;
using Carrier.Repository;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]
    public class EDaKhopLenhController : Controller
    {
        // GetMapSchedule
        // type = 1 with order
        // type = 0 with schedule
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: KhopLenh
        [Authorize]
        public ActionResult Index()
        {
            SQLDependencyInit();

            //List<HistoryModel> listData = new List<HistoryModel>();
            //List<HistoryModel> listDataOut = new List<HistoryModel>();
            //var userId = User.Identity.GetUserId();
            //int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            //var listdriver = (from tracking in db.OrderTracking
            //              join user in db.UserInfoes on tracking.DriverId equals user.ApplicationUserID
            //              where (user.OrganzationId == organizationId)
            //              select tracking.DriverId
            //             ).ToList();
            //listData = (from tracking in db.OrderTracking
            //            join order in db.Order on new { Id = tracking.OrderId.Value } equals new { order.Id }
            //            join driver in db.Driver on tracking.DriverId equals driver.UserId
            //            join drCar in db.DriverCar on driver.Id equals drCar.DriverId
            //            join car in db.Car on drCar.CarId equals car.Id
            //            where (tracking.Status == PublicConstant.ORDER_ACCEPT || tracking.Status == PublicConstant.ORDER_CARRYING)
            //            && (tracking.OwnerId.Equals(userId) || !listdriver.Contains(tracking.DriverId)) 
            //            select new HistoryModel
            //            {
            //                Id = tracking.ID,
            //                OrderId = tracking.OrderId,
            //                DriverId = tracking.DriverId,
            //                MaVanDon = order.MaVanDon,
            //                UserName = order.UserName,
            //                FullName = driver.Name,
            //                Phone = driver.Phone,
            //                ThoiGianDen = order.ThoiGianDen,
            //                ThoiGianDi = order.ThoiGianDi,
            //                Status = tracking.Status.ToString(),
            //                OwnerId = tracking.OwnerId,
            //                Payload = car.Payload,
            //                License = car.License,
            //                Updated_At = tracking.Updated_At
            //            }).OrderByDescending(o => o.Id).ToList();
            //if (!String.IsNullOrEmpty(OrderType))
            //{
            //    if (OrderType == "1")// Vận đơn nội bộ
            //    {
            //        listData = listData.Where(o => o.OwnerId.Equals(userId)).OrderByDescending(o => o.Status).ToList();
            //    }
            //    else // vận đơn từ ngoài
            //    {
            //        listData = listData.Where(o => !listdriver.Contains(o.DriverId)).OrderByDescending(o => o.Status).ToList();
            //    }
            //}
            //DateTime fromdate = DateTime.Now;
            //DateTime todate = DateTime.Now;
            //if (DateTime.TryParse(txtFromDate, out fromdate) && DateTime.TryParse(txtToDate, out todate))
            //{
            //    listData = listData.Where(o => (o.ThoiGianDi > fromdate && o.ThoiGianDen < todate) || (o.ThoiGianDi < fromdate && o.ThoiGianDen < todate) || (o.ThoiGianDi > fromdate && o.ThoiGianDen > todate)).ToList();
            //}
            //if (DateTime.TryParse(txtFromDate, out fromdate) && !DateTime.TryParse(txtToDate, out todate))
            //{
            //    listData = listData.Where(o => o.ThoiGianDen > fromdate).ToList();
            //}
            //if (!DateTime.TryParse(txtFromDate, out fromdate) && DateTime.TryParse(txtToDate, out todate))
            //{
            //    listData = listData.Where(o => o.ThoiGianDi < todate).ToList();
            //}

            return View();
        }
        public ActionResult ListOrderTracking()
        {
            var userId = User.Identity.GetUserId();
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlUFromDate = new SqlParameter("@UFromDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
            var sqlUToDate = new SqlParameter("@UToDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
            var sqlTFromDate = new SqlParameter("@TFromDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
            var sqlTToDate = new SqlParameter("@TToDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
            var sqlStatus = new SqlParameter("@Status", SqlDbType.Int) { Value = 1 };
            var listData = _unitOfWork.GetRepositoryInstance<SP_VanDon_DaKhopLenh_Result>().GetResultBySqlProcedure("SP_VanDon_DaKhopLenh @UserId,@UFromDate,@UToDate,@TFromDate,@TToDate,@Status", sqlUserId, sqlUFromDate, sqlUToDate, sqlTFromDate, sqlTToDate, sqlStatus);
            return PartialView("_ListViewOrderDaKhopLenh", listData);
        }
        public void SQLDependencyInit()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;

            string listenQuery = @"SELECT [OrderId] ,[Updated_At],[Status],[OwnerId] ,[DriverId] ,[EvidencePath] FROM [dbo].[OrderTracking]";

            // Create instance of the DB Listener
            DatabaseChangeListener changeListener = new DatabaseChangeListener(connectionString);

            // Define what to do when changes were detected
            changeListener.OnChange += () =>
            {
                //ChatHub.SendMessages();
                NotificationHub.UpdateMatchOrder();
                // Reattach listener event - DO NOT TOUCH!
                changeListener.Start(listenQuery);
            };

            // Start listening for changes 
            changeListener.Start(listenQuery);

        }
        public ActionResult OrderDetail(long Id)
        {
            Order order = db.Order.Where(o => o.Id == Id).Single();
            OrderTracking ordertracking = db.OrderTracking.Where(o => o.OrderId == order.Id).OrderByDescending(o => o.ID).Take(1).SingleOrDefault();
            if (ordertracking != null)
            {
                ordertracking.IsView = true;
                db.SaveChanges();
            }
            if (order != null)
            {
                var orderid = order.Id;
                var lstPlace = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
                ViewBag.ListPlace = lstPlace;
            }
            return PartialView("_OrderDetail", order);
        }
        public ActionResult UserInfoDetail(string DriverId)
        {
            Driver userInfo = db.Driver.Where(o => o.UserId == DriverId).Single();
            return PartialView("_UserInfo", userInfo);
        }

        [HttpPost]
        public JsonResult ResetOrderAjax(string listId)
        {
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();

            foreach (string id in liststrId)
            {
                try
                {
                    Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(int.Parse(id));
                    if (order != null)
                    {
                        //if (order.Status == 0)
                        //{
                        var orId = int.Parse(id);
                        OrderTracking orTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orId);
                        if (orTracking == null)
                        {
                            TempData["info"] = "Vận đơn chưa được gán!";
                        }
                        else
                        {
                            _unitOfWork.GetRepositoryInstance<OrderTracking>().Remove(orTracking);
                            _unitOfWork.SaveChanges();
                            TempData["info"] = "Resset vận đơn thành công!";
                        }
                        //}
                        //else
                        //{
                        //    TempData["info"] = "Vận đơn phải là đơn push bằng tay!";
                        //}
                    }

                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi reset vận đơn: " + ex.Message;
                }
            }
            TempData["info"] = "Resset vận đơn thành công!";
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        //[ChildActionOnly]
        //public MvcHtmlString DiaDiem(string cityCode, string districtCode, string wardCode)
        //{
        //    var city = db.City.Where(x => x.CityCode.Equals(cityCode)).FirstOrDefault();
        //    var district = db.District.Where(x => x.DistrictCode.Equals(districtCode)).FirstOrDefault();
        //    var ward = db.Ward.Where(x => x.WardCode.Equals(wardCode)).FirstOrDefault();
        //    var result = (ward != null ? ward.Name : string.Empty) + "-" + (district != null ? district.Name : string.Empty) + "-" + (city != null ? city.Name : string.Empty);
        //    return new MvcHtmlString(result);
        //}
        //public ActionResult SartMapping()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public JsonResult SearchTinh(string textSearch)
        //{
        //    var lstTinh = db.City.ToList();
        //    if (textSearch.Trim() != "")
        //    {
        //        lstTinh = db.City.Where(x => x.Name.StartsWith(textSearch)).ToList();
        //    }
        //    return Json(lstTinh, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public JsonResult GetMapSuggestSchedule(int scheduleId)
        //{
        //    List<ScheduleSuggetViewModel> scheduleSuggetViewModels = new List<ScheduleSuggetViewModel>();
        //    List<ScheduleOrder> scheduleMapSchedules = db.ScheduleOrder.Where(x => x.ScheduleJoinId == scheduleId).ToList();
        //    //List<ScheduleOrder> scheduleMapOrders = db.ScheduleOrder.Where(x => x.OrderJoinId == orderId).ToList();
        //    foreach (var item in scheduleMapSchedules)
        //    {
        //        if (item.OrderJoinId != null)
        //        {
        //            Order order = db.Order.Where(x => x.Id == item.OrderJoinId).FirstOrDefault();

        //            string diemdi = db.City.Where(x => x.CityCode == order.DiemDiTinh).FirstOrDefault().Name;
        //            string diemden = db.City.Where(x => x.CityCode == order.DiemDenTinh).FirstOrDefault().Name;
        //            //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //            //{
        //            //    id = order.Id,
        //            //    Time = order.ThoiGianDi,
        //            //    Route = diemdi + " - " + diemden,
        //            //    Type = 1,
        //            //    Status = ((MapOrerScheduleStatus)item.Status).ToString(),
        //            //};
        //            //scheduleSuggetViewModels.Add(scheduleSuggetViewModel);
        //        }
        //        if (item.ScheduleId != null)
        //        {
        //            Schedule schedule = db.Schedule.Where(x => x.Id == item.ScheduleId).FirstOrDefault();

        //            string diemdi = db.City.Where(x => x.CityCode == schedule.DiemDiTinh).FirstOrDefault().Name;
        //            string diemden = db.City.Where(x => x.CityCode == schedule.DiemDenTinh).FirstOrDefault().Name;
        //            //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //            //{
        //            //    id = schedule.Id,
        //            //    Time = schedule.ThoiGianDi,
        //            //    Route = diemdi + " - " + diemden,
        //            //    Type = 0,
        //            //    Status = ((MapOrerScheduleStatus)item.Status).ToString(),
        //            //};
        //            //scheduleSuggetViewModels.Add(scheduleSuggetViewModel);
        //        }
        //    }
        //    return Json(scheduleSuggetViewModels, JsonRequestBehavior.AllowGet);
        //}
        //[HttpGet]
        //public JsonResult GetMapSchedule(int scheduleId)
        //{
        //    Schedule schedule = db.Schedule.Where(x => x.Id == scheduleId).FirstOrDefault();
        //    string diemdi = db.City.Where(x => x.CityCode == schedule.DiemDiTinh).FirstOrDefault().Name;
        //    string diemden = db.City.Where(x => x.CityCode == schedule.DiemDenTinh).FirstOrDefault().Name;
        //    // type = 1 with order
        //    // type = 0 with schedule
        //    List<ScheduleSuggetViewModel> scheduleMap = findSchedule(schedule.DiemDiTinh, schedule.DiemDiHuyen, schedule.DiemDiXa, schedule.DiemDenTinh, schedule.DiemDenHuyen, schedule.DiemDenXa, schedule.ThoiGianDen, schedule.ThoiGianDi);
        //    List<ScheduleSuggetViewModel> orderMap = findScheduleOnOrder(schedule.DiemDiTinh, schedule.DiemDiHuyen, schedule.DiemDiXa, schedule.DiemDenTinh, schedule.DiemDenHuyen, schedule.DiemDenXa, schedule.ThoiGianDi);
        //    scheduleMap.AddRange(orderMap);
        //    return Json(scheduleMap, JsonRequestBehavior.AllowGet);
        //    //return Json(scheduleMap, JsonRequestBehavior.AllowGet);
        //}

        //// schedule : type = 0
        //// order : type = 1
        //[HttpPost]
        //public JsonResult MapScheduleOrder(int scheduleId, int scheduleOrderId, int type)
        //{
        //    ScheduleOrder scheduleOrder = new ScheduleOrder()
        //    {
        //        ScheduleId = scheduleId,
        //        Status = (int)MapOrerScheduleStatus.Wait,
        //        UpdateTime = DateTime.Now,
        //        UserName = User.Identity.Name,
        //        Type = type
        //    };

        //    if (type == 0)
        //    {
        //        scheduleOrder.ScheduleJoinId = scheduleOrderId;
        //        Schedule schedule = db.Schedule.Where(x => x.Id == scheduleOrderId).FirstOrDefault();
        //        if (schedule != null)
        //        {
        //            scheduleOrder.UserName = schedule.UserName;
        //        }
        //    }
        //    else
        //    {
        //        scheduleOrder.OrderJoinId = scheduleOrderId;
        //        Order order = db.Order.Where(x => x.Id == scheduleOrderId).FirstOrDefault();
        //        if (order != null)
        //        {
        //            scheduleOrder.UserName = order.UserName;
        //        }
        //    }

        //    db.ScheduleOrder.Add(scheduleOrder);
        //    db.SaveChanges();
        //    return Json("OK", JsonRequestBehavior.AllowGet);
        //}

        //private List<ScheduleSuggetViewModel> findSchedule(string DiemDiTinh, string DiemDiHuyen, string DiemDiXa, string DiemDenTinh, string DiemDenHuyen, string DiemDenXa, string timeStart, string timeEnd)
        //{
        //    // tìm list schedule
        //    List<Schedule> schedules = db.Schedule.Where(x => x.DiemDenTinh == DiemDiTinh && x.DiemDenHuyen == DiemDiHuyen && x.DiemDenXa == DiemDiXa).ToList();
        //    List<ScheduleSuggetViewModel> schedulesSelect = new List<ScheduleSuggetViewModel>();
        //    foreach (var schedule in schedules)
        //    {

        //        string date = "";

        //        if (GlobalCommon.LessThanOrEqualHour(timeEnd, schedule.ThoiGianDi) == true)
        //        {
        //            string diemdi = db.City.Where(x => x.CityCode == DiemDiTinh).FirstOrDefault().Name;
        //            string diemden = db.City.Where(x => x.CityCode == DiemDenTinh).FirstOrDefault().Name;

        //            //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //            //{
        //            //    id = schedule.Id,
        //            //    Time = schedule.ThoiGianDi + " - " + schedule.ThoiGianDen,
        //            //    Route = diemdi + " - " + diemden,
        //            //    Type = 0
        //            //};
        //            //schedulesSelect.Add(scheduleSuggetViewModel);
        //        }

        //    }
        //    return schedulesSelect;
        //}

        //private List<ScheduleSuggetViewModel> findScheduleOnOrder(string DiemDiTinh, string DiemDiHuyen, string DiemDiXa, string DiemDenTinh, string DiemDenHuyen, string DiemDenXa, string time)
        //{
        //    int locationStart2 = -1;
        //    int locationEnd2 = -1;
        //    int routeIdSelect = -1;
        //    List<ScheduleSuggetViewModel> schedulesSelect = new List<ScheduleSuggetViewModel>();
        //    List<Order> allOrder = db.Order.ToList();
        //    // tìm route chính xác.
        //    foreach (var item in allOrder)
        //    {
        //        if (item.DiemDiTinh != null && item.DiemDenTinh != null)
        //        {
        //            if (item.DiemDiTinh == DiemDenTinh && item.DiemDenTinh == DiemDiTinh && item.DiemDiHuyen == DiemDenHuyen && item.DiemDenHuyen == DiemDiHuyen && item.DiemDiXa == DiemDenXa && item.DiemDenXa == DiemDiXa)
        //            {
        //                City tinhDi = db.City.Where(x => x.CityCode == DiemDiTinh).FirstOrDefault();
        //                City tinhDen = db.City.Where(x => x.CityCode == DiemDenTinh).FirstOrDefault();
        //                //ScheduleSuggetViewModel scheduleSuggetViewModel = new ScheduleSuggetViewModel
        //                //{
        //                //    id = item.Id,
        //                //    Time = item.ThoiGianDi,
        //                //    Route = tinhDen.Name + " -> " + tinhDi.Name,
        //                //    Type = 1
        //                //};
        //                //schedulesSelect.Add(scheduleSuggetViewModel);
        //            }
        //        }
        //    }
        //    return schedulesSelect;
        //}

        //public List<ScheduleSuggetViewModel> SearchAllData(int? pageNumber, int? pageSize)
        //{
        //    var lstData = new List<ScheduleSuggetViewModel>();
        //    var lstSchedule = db.Schedule.Where(x => x.ScheduleOrder.Where(y=>y.ScheduleId == x.Id && x.Status == PublicConstant.STATUS_ACTIVE).Count() == 0).ToList();
        //    var lstOrder = db.Order.Where(x => x.ScheduleOrder.Where(y=>y.OrderJoinId == x.Id && x.Status == PublicConstant.STATUS_ACTIVE).Count() == 0).ToList();
        //    foreach(Schedule schedule in lstSchedule)                                                       
        //    {                                            
        //        foreach (Order order in lstOrder)
        //        {
        //            var condition = GlobalCommon.MatchSheduleWithOrder(schedule,order);
        //            var isMatch1 = condition.MatchTinh && condition.MatchHuyen && condition.MatchXa && condition.MatchDateTimeDen
        //                && (condition.MatchTrongTai1 || condition.MatchTrongTai2);
        //            var isMatch2 = condition.MatchTinh && condition.MatchHuyen && condition.MatchXa && condition.MatchDateTimeDen && condition.MatchDateTimeDi
        //                && (condition.MatchTrongTai1 || condition.MatchTrongTai2);
        //            if (isMatch1 || isMatch2)
        //            {
        //                var sugguets = new ScheduleSuggetViewModel();
        //                sugguets.Type = 0;
        //                sugguets.ItemId = string.Format("{0}-{1}-{2}", schedule.Id, order.Id,sugguets.Type);
        //                sugguets.Order = order;
        //                sugguets.Schedule1 = schedule;
        //                sugguets.Gia = order.Gia.Value;
        //                sugguets.Condition = condition;
        //                lstData.Add(sugguets);
        //            }
        //        }
        //    }
        //    return lstData;
        //}

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
    enum MapOrerScheduleStatus
    {
        Wait = 0,
        Appcept = 1,
        Cancel = 2
    }
    public class ViewIdOrderTracking
    {
        public string UserId { get; set; }
    }
}