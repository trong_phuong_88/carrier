﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.CMS.Models;
using PagedList;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Common;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR.Client;
using Carrier.PushNotification;
using System.Threading.Tasks;
using CMS.Carrier.Common;
using System.Configuration;
using Carrier.CMS.Hubs;
using System.Data.SqlClient;
using EntityFramework.Utilities;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]
    public class EChoKhopLenhController : Controller
    {
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        // GetMapSchedule
        // type = 1 with order
        // type = 0 with schedule
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: KhopLenh
        [Authorize]
        public ActionResult Index()
        {
            SQLDependencyInit();
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">1: Khop lenh tu dong,0: Khop lenh bang tay</param>
        /// <returns></returns>
        public ActionResult ListOrderTracking(int type)
        {
            var userId = User.Identity.GetUserId();
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlType = new SqlParameter("@Type", System.Data.SqlDbType.NVarChar) { Value = type };

            var listData = _unitOfWork.GetRepositoryInstance<SP_GetAllDataTracking_Result>().GetResultBySqlProcedure("SP_GetAllDataTracking @UserId,@Type", sqlUserId, sqlType);
            return PartialView("_ListViewTrackingOrder", listData);
        }
        public void SQLDependencyInit()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;

            string listenQuery = @"SELECT [OrderId] ,[Updated_At],[Status],[OwnerId] ,[DriverId] ,[EvidencePath] FROM [dbo].[OrderTracking]";

            // Create instance of the DB Listener
            DatabaseChangeListener changeListener = new DatabaseChangeListener(connectionString);

            // Define what to do when changes were detected
            changeListener.OnChange += () =>
            {
                //ChatHub.SendMessages();
                NotificationHub.UpdateMatchOrder();
                // Reattach listener event - DO NOT TOUCH!
                changeListener.Start(listenQuery);
            };

            // Start listening for changes 
            changeListener.Start(listenQuery);
        }
        public ActionResult OrderDetail(int Id)
        {
            Order order = db.Order.Where(o => o.Id == Id).Single();
            if (order != null)
            {
                var orderid = order.Id;
                var lstPlace = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
                ViewBag.ListPlace = lstPlace;
            }
            else
            {
                return HttpNotFound();
            }
            return PartialView("_OrderDetail", order);
        }
        public ActionResult UserInfoDetail(string DriverId)
        {
            Driver userInfo = db.Driver.Where(o => o.UserId == DriverId).Single();
            return PartialView("_UserInfo", userInfo);
        }

        public ActionResult DeleteOrder(int id)
        {
            using (var db = new Carrier3Entities())
            {
                OrderTracking ot = db.OrderTracking.Find(id);
                if (ot != null)
                {
                    EFBatchOperation.For(db, db.OrderTracking).Where(b => b.OrderId == id).Delete();
                    var obj = db.Order.Where(x => x.Id == ot.OrderId).FirstOrDefault();
                    db.AttachAndModify(obj).Set(x => x.Status, 2);
                    db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            }
        }

        [HttpPost]
        public JsonResult DeleteOrderId(string listId)
        {
            OrderTracking order;
            //Order orderView;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    order = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefault(int.Parse(id));
                    if (order != null)
                    {
                        order.Status = 3;
                        _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(order);
                        //_unitOfWork.GetRepositoryInstance<OrderTracking>().Remove(order);
                        _unitOfWork.SaveChanges();
                        return Json("OK", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ResetOrderAjax(string listId)
        {
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();

            foreach (string id in liststrId)
            {
                try
                {
                    Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(int.Parse(id));
                    if (order != null)
                    {
                        var order_created = order.Created_By;
                        if ((order.Status == 0) || (order.Status == 1 && !UserId.Equals(order_created)))
                        {
                            var orId = int.Parse(id);
                            OrderTracking orTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orId);
                            if (orTracking == null)
                            {
                                TempData["info"] = "Vận đơn chưa được gán!";
                            }
                            else
                            {
                                _unitOfWork.GetRepositoryInstance<OrderTracking>().Remove(orTracking);
                                _unitOfWork.SaveChanges();
                                TempData["info"] = "Resset vận đơn thành công!";
                            }
                        }
                        else
                        {
                            TempData["info"] = "Vận đơn phải là đơn push bằng tay!";
                        }
                    }

                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi reset vận đơn: " + ex.Message;
                }
            }
            //TempData["info"] = "Resset vận đơn thành công!";
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public List<ListDriver> GetListDriver()
        {
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            List<ListDriver> lstDriver = new List<ListDriver>();
            var listCT = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == organizationId);
            foreach (var item in listCT)
            {
                lstDriver.Add(new ListDriver() { Id = item.Id, Name = item.Name, Phone = item.Phone, UserId = item.UserId });
            }
            return lstDriver;
        }
        public void PushMessageForDriver(string driverId, long orderId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                //PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH;
                //PushServices.SetupPushAPN(false);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            if (result.Equals("Success"))
            {
                long orderId = long.Parse(obj2);
                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
                if (old != null)
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                    old.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                }
                else
                {
                    var obj = new OrderTracking();
                    obj.OrderId = orderId;
                    obj.DriverId = obj1;
                    obj.OwnerId = User.Identity.GetUserId();
                    obj.Created_At = DateTime.Now;
                    obj.Updated_At = DateTime.Now;
                    obj.Created_By = User.Identity.GetUserId();
                    obj.Status = PublicConstant.ORDER_PENDING;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                }
                _unitOfWork.SaveChanges();
            }
            else
            {
                // Show message 
            }
        }
        public JsonResult LoadListDriver(int id)
        {
            string strCode = "";
            List<ListDriver> lstDriver = GetListDriver();
            foreach (var item in lstDriver)
            {
                strCode += "<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-4\"><button type = \"button\" style=\"border-color:white; width:200px;text-align:left \" class =\"btn btn-default col-md-3 btn-primary-pay\" data-value=\"" + item.Phone + "\"  class =\"btn btn-default\" id=\"" + item.Id + "\"  onClick =\"ClickDriver('" + item.Id + "')\" value=\"" + item.UserId + "\"><i style=\"color:#f7e60a\" class=\"fa fa-car\" aria-hidden=\"true\"></i>&nbsp;" + item.Name + "</button>" + "<br /></div>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListDriverInfo(string searchString)
        {
            ViewBag.CurrentFilter = searchString;
            var txtSearch = searchString.Trim().ToLower();
            List<ListDriver> lstDriver = GetListDriver();
            //List<CarInfo> ListCarDetail = new List<CarInfo>();
            //foreach (var item in lstCar)
            //{
            //    var carInfo = new CarInfo();
            //    carInfo.Id = item.Id;
            //    carInfo.License = item.License;
            //    carInfo.Payload = item.Payload;
            //    carInfo.Leng_Size = item.Leng_Size;
            //    carInfo.Width_Size = item.Width_Size;
            //    carInfo.Height_Size = item.Height_Size;
            //    carInfo.DriverId = id;
            //    ListCarDetail.Add(carInfo);
            //}
            if (!String.IsNullOrEmpty(searchString))
            {
                lstDriver = lstDriver.Where(o => o.Name.Trim().ToLower().Contains(txtSearch)).OrderByDescending(o => o.Id).ToList();
            }
            return Json(lstDriver, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> SendNewOrderToDriver(long orderId, string driverId)
        {
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault((int)orderId);
            //var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
            //if (userOnline.IsOnline.Value)
            //{
            //    var hubConnection = new HubConnection("http://api.vantaitoiuu.com/", new Dictionary<string, string>{
            //             { "UserId", driverId }});
            //    var hubProxy = hubConnection.CreateHubProxy("CarrierMonitor");
            //    await hubConnection.Start();
            //    await hubProxy.Invoke("SendData", driverId, order);
            //}
            //else
            //{
            //PushMessageForDriver(driverId, orderId);
            //}
            var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(driverId));
            var oldnew = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(o => o.OrderId == orderId);

            if (old != null)
            {
                old.Status = PublicConstant.ORDER_PENDING;
                old.Updated_At = DateTime.Now;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
            }
            else if (oldnew != null)
            {
                return Json("Bạn phải reset đơn trước khi gửi cho tài xế mới!", JsonRequestBehavior.AllowGet);
            }
            else
            {
                var obj = new OrderTracking();
                obj.OrderId = orderId;
                obj.DriverId = driverId;
                obj.OwnerId = order.Created_By;
                obj.Created_At = DateTime.Now;
                obj.Updated_At = DateTime.Now;
                obj.Created_By = User.Identity.GetUserId();
                obj.Status = PublicConstant.ORDER_PENDING;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
            }
            PushMessageForDriver(driverId, orderId);
            _unitOfWork.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}