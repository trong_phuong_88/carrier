﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.Repository;
using PagedList;
using Carrier.Utilities;
using Carrier.CMS.Common;
using System.Data.Entity.Validation;
using Microsoft.AspNet.Identity;
using System.Globalization;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE)]
    public class ECarController : Controller
    {
        private CultureInfo cen = new CultureInfo("en");
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Enterprise/Driver
        [Authorize]
        public ActionResult Index()
        {
            List<Car> result = new List<Car>();
            if (User.Identity.Name.Equals("carrier"))
            {
                result = _unitOfWork.GetRepositoryInstance<Car>().GetAllRecords().Where(o => o.OrganizationId == 1163).OrderByDescending(o => o.License).ToList();
            }
            else
            {
                var userId = User.Identity.GetUserId();
                var us = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID == userId);
                if (us != null)
                {
                    var prId = us.Id;
                    result = _unitOfWork.GetRepositoryInstance<Car>().GetAllRecords().Where(o => o.OrganizationId == prId).OrderByDescending(o => o.License).ToList();
                }

            }
            return View(result);
        }

        // GET: Admin/Car/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // GET: Admin/Car/Create
        public ActionResult Create()
        {
            //if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            //{
            //    List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //    ViewBag.ListOrganization = listOrganization;
            //}
            //List<CarType> listCar = new List<CarType>();
            var listCar = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            IEnumerable<SelectListItem> listCarSelect = listCar.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Value.ToString() });
            if (listCarSelect.Count() > 0)
            {
                ViewBag.ListCar = listCarSelect;
            }
            else
            {
                ViewBag.ListCar = null;
            }
            //var lstCar = _unitOfWork.GetRepositoryInstance<Car>().GetAllRecords();
            //if (lstCar.Count() > 0)
            //{
            //    ViewBag.Car = lstCar;
            //}
            //else
            //{
            //    ViewBag.Car = null;
            //}
            return View();
        }

        // POST: Admin/Car/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            Car car = new Car();
            if (ModelState.IsValid)
            {
                try
                {
                    car.License = form.Get("License") != null ? form.Get("License") : "";
                    Car checkCar = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(x => x.License.Equals(car.License));
                    if (checkCar == null)
                    {
                        double width = 0, height = 0, leng = 0, payload = 1.25, lat = 0, lng = 0;
                        //double.TryParse(form["txtWidth"], out width);
                        //double.TryParse(form["txtHeight"], out height);
                        //double.TryParse(form["txtLenght"], out leng);
                        //car.Width_Size = width;
                        //car.Height_Size = height;
                        //car.Leng_Size = leng;
                        car.Width_Size = double.Parse(form["txtWidth"], NumberStyles.Float, cen);
                        car.Height_Size = double.Parse(form["txtHeight"], NumberStyles.Float, cen);
                        car.Leng_Size = double.Parse(form["txtLenght"], NumberStyles.Float, cen);
                        double.TryParse(form["Payload"], out payload);
                        car.Payload = double.Parse(form["Payload"], NumberStyles.Float, cen);
                        car.Address = form.Get("Address");
                        if (form.Get("hfLat") != null)
                        {
                            double.TryParse(form["hfLat"], out lat);
                            //car.Lat = lat;
                            car.Lat = double.Parse(form["hfLat"], NumberStyles.Float, cen);

                        }
                        if (form.Get("hfLng") != null)
                        {
                            double.TryParse(form["hfLng"], out lng);
                            //car.Lng = lng;
                            car.Lng = double.Parse(form["hfLng"], NumberStyles.Float, cen);
                        }
                        //car.ContainerSize = form.Get("ContainerSize") != null ? form.Get("ContainerSize") : "";
                        car.Description = form.Get("Description") != null ? form.Get("Description") : "";
                        car.Created_At = DateTime.Now;
                        car.Created_By = User.Identity.GetUserId();
                        car.Updated_By = User.Identity.GetUserId();
                        car.Updated_At = DateTime.Now;
                        //car.Payload = payload;
                        car.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.GetUserName());
                        _unitOfWork.GetRepositoryInstance<Car>().Add(car);
                        _unitOfWork.SaveChanges();
                        return RedirectToAction("Index", "ECar");
                    }
                    else
                    {
                        TempData["Message"] = "Xe đã tồn tại!";
                        return RedirectToAction("Create", "ECar");
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                return RedirectToAction("Index");
            }

            return View(car);
        }

        // GET: Admin/Car/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (car == null)
            {
                return HttpNotFound();
            }
            //List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //ViewBag.ListOrganization = listOrganization;
            var listCar = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            IEnumerable<SelectListItem> listCarSelect = listCar.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Value.ToString() });
            if (listCarSelect.Count() > 0)
            {
                ViewBag.ListCar = listCarSelect;
            }
            else
            {
                ViewBag.ListCar = null;
            }
            return View(car);
        }

        // POST: Admin/Car/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (ModelState.IsValid)
            {
                double width = 0, height = 0, leng = 0, lat = 0, lng = 0, payload=1.25;
                car.License = form.Get("License") != null ? form.Get("License") : "";
                //double.TryParse(form["txtWidth"], out width);
                //double.TryParse(form["txtHeight"], out height);
                //double.TryParse(form["txtLenght"], out leng);
                car.Width_Size = double.Parse(form["txtWidth"], NumberStyles.Float, cen);
                car.Height_Size = double.Parse(form["txtHeight"], NumberStyles.Float, cen);
                car.Leng_Size = double.Parse(form["txtLenght"], NumberStyles.Float, cen);
                car.Payload = double.Parse(form["Payload"], NumberStyles.Float, cen);
                //var payload = form.Get("Payload") != null ? form.Get("Payload") : "";
                //car.ContainerSize = form.Get("ContainerSize") != null ? form.Get("ContainerSize") : "";
                car.Description = form.Get("Description") != null ? form.Get("Description") : "";
                car.Address = form.Get("Address");
                if (form.Get("hfLat") != null && form.Get("hfLat").Length > 0)
                {
                    double.TryParse(form["hfLat"], out lat);
                    //car.Lat = lat;
                    car.Lat = double.Parse(form["hfLat"], NumberStyles.Float, cen);
                }
                if (form.Get("hfLng") != null && form.Get("hfLng").Length > 0)
                {
                    double.TryParse(form["hfLng"], out lng);
                    //car.Lng = lng;
                    car.Lng = double.Parse(form["hfLng"], NumberStyles.Float, cen);
                }
                //car.Payload = payload;
                //if (payload.Contains("1.5"))
                //{
                //    car.Payload = PublicConstant.LOAIXE15T;
                //}
                //else if (payload.Contains("2.5"))
                //{
                //    car.Payload = PublicConstant.LOAIXE25T;
                //}
                //else if (payload.Contains("3.5"))
                //{
                //    car.Payload = PublicConstant.LOAIXE35T;
                //}
                //else if (payload.Contains("5"))
                //{
                //    car.Payload = PublicConstant.LOAIXE5T;
                //}
                //else if (payload.Contains("8"))
                //{
                //    car.Payload = PublicConstant.LOAIXE8T;
                //}
                //else if (payload.Contains("10"))
                //{
                //    car.Payload = PublicConstant.LOAIXE10T;
                //}
                //else if (payload.Contains("3"))
                //{
                //    car.Payload = PublicConstant.LOAIXE3CHAN;
                //}
                //else if (payload.Contains("4"))
                //{
                //    car.Payload = PublicConstant.LOAIXE4CHAN;
                //}
                car.Created_At = car.Created_At;
                car.Created_By = car.Created_By;
                car.Updated_At = DateTime.Now;
                car.Updated_By = User.Identity.Name;
                _unitOfWork.SaveChanges();

                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(car);
        }

        [HttpPost]
        public JsonResult DeleteCar(string listId)
        {
            Car car;
            DriverCar driverCar;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            foreach (string id in liststrId)
            {
                try
                {
                    car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefault(int.Parse(id));
                    var carId = int.Parse(id);
                    driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(x => x.CarId == carId);
                    if (driverCar != null)
                    {
                        _unitOfWork.GetRepositoryInstance<DriverCar>().Remove(driverCar);
                        _unitOfWork.SaveChanges();
                    }
                    _unitOfWork.GetRepositoryInstance<Car>().Remove(car);
                    _unitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public string GetOrganizationName(int? id)
        {
            string name = "";
            if (id != null)
            {
                Organization product = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefault(int.Parse(id.ToString()));
                if (product != null)
                {
                    name = product.Name;
                }
            }

            return name;
        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
