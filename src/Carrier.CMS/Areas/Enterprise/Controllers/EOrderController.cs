﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using Carrier.Models.Entities;
using Carrier.Utilities;
using GoogleMaps.LocationServices;
using Carrier.CMS.Common;
using Newtonsoft.Json.Linq;
using Carrier.PushNotification;
using static Carrier.Utilities.GlobalCommon;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using Carrier.CMS.Models;
using CMS.Carrier.Models;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Client;
using System.Data.Entity.Core.Objects.DataClasses;
using EntityFramework.Utilities;
using Carrier.Algorithm;
using Carrier.Algorithm.Entity;
using System.Globalization;
using System.IO;
using CMS.Carrier.Areas.Enterprise.Models;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    //[RequireHttps]
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]
    public class EOrderController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private CultureInfo cti = new CultureInfo("vi-VN");
        private CultureInfo cen = new CultureInfo("en");
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public ActionResult Index()
        {
            var txtFromDate = Request.Form["txtFromDate"] != null ? Request.Form["txtFromDate"].ToString() : "";
            var txtToDate = Request.Form["txtToDate"] != null ? Request.Form["txtToDate"].ToString() : "";
            var status = Request.Form["status"] != null ? int.Parse(Request.Form["status"].ToString()) : -1;
            var Type = Request.Form["Type"] != null ? int.Parse(Request.Form["Type"].ToString()) : 0;
            var OrderType = Request.Form["OrderType"] != null ? int.Parse(Request.Form["OrderType"].ToString()) : 0;

            var userId = User.Identity.GetUserId();
            var CurrentUsertId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            var RoleType = "";
            string fromdate;
            string todate;

            if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            {
                RoleType = PublicConstant.ROLE_ENTERPRISE;
            }
            if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            {
                RoleType = PublicConstant.ROLE_FORWARDER;
            }
            if (User.IsInRole(PublicConstant.ROLE_USERS))
            {
                RoleType = PublicConstant.ROLE_USERS;
            }

            fromdate = (txtFromDate != null && txtFromDate.Length > 0) ? (txtFromDate.Split('-')[2] + '-' + txtFromDate.Split('-')[1] + '-' + txtFromDate.Split('-')[0]) : "";
            todate = (txtToDate != null && txtToDate.Length > 0) ? (txtToDate.Split('-')[2] + '-' + txtToDate.Split('-')[1] + '-' + txtToDate.Split('-')[0]) : "";
            //fromdate = (txtFromDate != null && txtFromDate.Length > 0) ? txtFromDate: "";
            //todate = (txtToDate != null && txtToDate.Length > 0) ? txtToDate : "";

            List<OrderDetailViewModel> result = new List<OrderDetailViewModel>();
            //var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            //var sqlTypeOrder = new SqlParameter("@typeOrder", System.Data.SqlDbType.Int) { Value = 0 };
            //var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.NVarChar) { Value = fromdate };
            //var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.NVarChar) { Value = todate };
            //var statuss = new SqlParameter("@Status", System.Data.SqlDbType.Int) { Value = status };
            //var type = new SqlParameter("@Type", System.Data.SqlDbType.Int) { Value = Type };

            var sqlRoleType = new SqlParameter("@RoleType", System.Data.SqlDbType.NVarChar) { Value = RoleType };
            var sqlOrderStatus = new SqlParameter("@OrderStatus", System.Data.SqlDbType.Int) { Value = 0 };
            SqlParameter sqlStatus;
            if (status != 7)
            {
                sqlStatus = new SqlParameter("@Status", System.Data.SqlDbType.Int) { Value = status };
            }
            else
            {
                sqlStatus = new SqlParameter("@Status", System.Data.SqlDbType.Int) { Value = -2 };
            }
            var sqlType = new SqlParameter("@Type", System.Data.SqlDbType.Int) { Value = Type };
            var sqlParentId = new SqlParameter("@ParentId", System.Data.SqlDbType.Int) { Value = 0 };
            var sqlOrderType = new SqlParameter("@OrderType", System.Data.SqlDbType.Int) { Value = OrderType };
            var sqlUserId = new SqlParameter("@UserId ", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.NVarChar) { Value = fromdate };
            var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.NVarChar) { Value = todate };

            result = _unitOfWork.GetRepositoryInstance<OrderDetailViewModel>().GetResultBySqlProcedure("SP_GetAllOrdersByParameter_New @RoleType,@OrderStatus,@Status,@Type,@ParentId,@OrderType,@UserId,@FromDate,@ToDate", sqlRoleType, sqlOrderStatus, sqlStatus, sqlType, sqlParentId, sqlOrderType, sqlUserId, sqlFromDate, sqlToDate).ToList();
            var result1 = result.OrderByDescending(o => o.CreateAt);
            List<int> checkHasSubOrder = new List<int>();


            List<OrderDetailViewModel> result2 = new List<OrderDetailViewModel>();
            if (status == 7)
            {
                foreach (var o in result1)
                {
                    if ((o.TrackingStatus == -1 && o.ThoiGianDi != null && o.ThoiGianDi < DateTime.Now) || (o.TrackingStatus == 7) || (o.TrackingStatus == 0 && o.ThoiGianDi != null && o.ThoiGianDi < DateTime.Now))
                    {
                        result2.Add(o);
                    }
                }
                foreach (var r in result2)
                {
                    var t = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(x => x.ParentId == r.Id).ToList();
                    if (t != null && t.Count() > 0)
                    {
                        checkHasSubOrder.Add(1);
                    }
                    else
                    {
                        checkHasSubOrder.Add(0);
                    }
                }
                ViewBag.checkHasSubOrder = checkHasSubOrder;

                // update viewed
                Order oder = new Order();
                foreach (var item in result2)
                {
                    oder = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.Id == item.Id);
                    if (oder != null)
                    {
                        oder.IsViewPopupOrder = true;
                        _unitOfWork.SaveChanges();
                    }
                }
                return View(result2);
            }
            else
            {
                foreach (var r in result1)
                {
                    var t = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(x => x.ParentId == r.Id).ToList();
                    if (t != null && t.Count() > 0)
                    {
                        checkHasSubOrder.Add(1);
                    }
                    else
                    {
                        checkHasSubOrder.Add(0);
                    }
                }
                ViewBag.checkHasSubOrder = checkHasSubOrder;
                return View(result1);
            }


        }

        public ActionResult Indexs(string txtFromDate, string txtToDate, int status = -1, int Type = 0)
        {
            var userId = User.Identity.GetUserId();
            var CurrentUsertId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            string fromdate;
            string todate;

            fromdate = (txtFromDate != null && txtFromDate.Length > 0) ? DateTime.Parse(txtFromDate).ToString("yyyy-mm-dd") : "";
            todate = (txtToDate != null && txtToDate.Length > 0) ? DateTime.Parse(txtToDate).ToString("yyyy-mm-dd") : "";
            List<SP_GetAllOrdersByParameter_Result> result = new List<SP_GetAllOrdersByParameter_Result>();
            var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlTypeOrder = new SqlParameter("@typeOrder", System.Data.SqlDbType.Int) { Value = 1 };
            var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.NVarChar) { Value = fromdate };
            var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.NVarChar) { Value = todate };
            var statuss = new SqlParameter("@Status", System.Data.SqlDbType.Int) { Value = status };
            var type = new SqlParameter("@Type", System.Data.SqlDbType.Int) { Value = Type };
            result = _unitOfWork.GetRepositoryInstance<SP_GetAllOrdersByParameter_Result>().GetResultBySqlProcedure("SP_GetAllOrdersByParameter @UserId,@typeOrder,@FromDate, @ToDate,@Status,@Type", sqlUser, sqlTypeOrder, sqlFromDate, sqlToDate, statuss, type).ToList();

            return View(result.OrderByDescending(o => o.CreateAt));
        }
        public ActionResult PopUpDetail(int? id)
        {
            var userId = User.Identity.GetUserId();
            //var RoleType = "";
            //if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            //{
            //    RoleType = PublicConstant.ROLE_ENTERPRISE;
            //}
            //if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            //{
            //    RoleType = PublicConstant.ROLE_FORWARDER;
            //}
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.Id == id);

            var sqlParentId = new SqlParameter("@parentId", System.Data.SqlDbType.BigInt) { Value = id };
            var subOrders = _unitOfWork.GetRepositoryInstance<SP_Get_ListSubOrder_Result>().GetResultBySqlProcedure("SP_Get_ListSubOrder @parentID", sqlParentId).ToList();

            if (order != null)
            {
                var orderid = order.Id;
                var lstPlace = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
                ViewBag.ListPlace = lstPlace;
            }
            else
            {
                return HttpNotFound();
            }
            if (subOrders != null && subOrders.Count() > 0)
            {
                ViewBag.ParentOrder = order;
                // load danh sách xe
                List<string> dsx = new List<string>();
                foreach (var s in subOrders)
                {
                    var result = (
                    from a in db.DriverCar
                    join o in db.Driver on a.DriverId equals o.Id
                    join b in db.Car on a.CarId equals b.Id
                    join ot in db.OrderTracking on o.UserId equals ot.DriverId
                    where ot.OrderId == id
                    select new Car1
                    {
                        Id = b.Id,
                        License = b.License
                    }
                    ).ToList();
                    if (result.Count > 0)
                    {
                        dsx.Add(result[0].License);
                    }
                    else
                    {
                        dsx.Add("");
                    }
                }
                ViewBag.DSX = dsx;
                return View("ListSubOrder", subOrders);
            }
            else
            {
                return View("DetailOrder", order);
            }
        }
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (order == null)
            {
                return HttpNotFound();
            }

            List<OrderTracking> track = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetListByParameter(o => o.OrderId == id).OrderByDescending(o => o.ID).Take(1).ToList();
            if (track.Count > 0)
            {
                order.Status = track[0].Status;
                var driverId = track[0].DriverId;
                Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.UserId == driverId);
                if (driver != null)
                {
                    ViewBag.Driver = driver;
                }
            }
            else
            {
                order.Status = -1; // không tồn tại trong ordertracking -> dâng khớp lệnh
            }

            var listCar = (from a in db.OrderCar
                           join c in db.CarType on a.CarTypeId equals c.Id
                           where a.OrderId == order.Id
                           select new OrderCarViewModel
                           {
                               Id = a.Id,
                               CarTypeId = c.Id,
                               CarTypeName = c.Name,
                               Number = a.Number,
                               OrderId = a.OrderId,
                               Value = c.Value
                           }).ToList();
            ViewBag.ListOrderCar = listCar;

            // update viewed
            order.IsViewPopupOrder = true;
            _unitOfWork.SaveChanges();

            return View(order);
        }

        public ActionResult Create()
        {
            //List<DraftOrder> listDraftOrder = db.DraftOrder.Where(o => o.Name == User.Identity.Name).ToList();
            //List<DraftOrder> listDraftOrder = db.DraftOrder.ToList();
            //ViewBag.ListDraftOrder = new SelectList(listDraftOrder, "Id", "TenHang", 0);

            string appId = User.Identity.GetUserId();
            UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID == appId);
            if (userInfo != null)
            {
                ViewBag.Lat = userInfo.Lat != null ? userInfo.Lat : 21.033781;
                ViewBag.Lng = userInfo.Lng != null ? userInfo.Lng : 105.814054;
            }
            else
            {
                ViewBag.Lat = 21.033781;
                ViewBag.Lng = 105.814054;
            }

            List<CarType> listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords().ToList();
            Session.Add("LIST_CAR", listCarType);
            return View();
        }


        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection formCollection)
        {
            //SysLogisticValidate;
            Order order = new Order();
            Order orderTemp = new Order();
            try
            {
                //Lấy danh sách Diem tra hang
                var listDiemTraHang = new List<string>();
                var listPlaceLat = new List<float?>();
                var listPlaceLng = new List<float?>();
                if (formCollection["counterDiemTraHang"] != "")
                {
                    //int n = Int32.Parse(formCollection["counterDiemTraHang"]);
                    for (int i = 0; i < PublicConstant.WAYPOINTS; i++)
                    {
                        if (formCollection["txtToLocation_" + i] != null && formCollection["txtToLocation_" + i].Trim() != "")
                        {
                            listDiemTraHang.Add(formCollection["txtToLocation_" + i].Trim());
                            float _lat = 0;
                            float _lng = 0;
                            try
                            {
                                _lat = (formCollection["hfLatTo_" + i] != null && formCollection["hfLatTo_" + i] != "") ? float.Parse(formCollection["hfLatTo_" + i].ToString()) : 0;
                                _lng = (formCollection["hfLngTo_" + i] != null && formCollection["hfLngTo__" + i] != "") ? float.Parse(formCollection["hfLngTo_" + i].ToString()) : 0;

                            }
                            catch
                            {

                            }

                            listPlaceLat.Add(_lat);
                            listPlaceLng.Add(_lng);

                        }

                    }
                }

                var txtTimeDi = formCollection["txtTimeDi"] != null ? formCollection["txtTimeDi"] : "";
                var ThoiGianBocHangDi = formCollection["ThoiGianBocHangDi"] != null ? formCollection["ThoiGianBocHangDi"] : "";
                var txtTimeDen = formCollection["txtTimeDen"] != null ? formCollection["txtTimeDen"] : "";
                var ThoiGianBocHangDen = formCollection["ThoiGianBocHangDen"] != null ? formCollection["ThoiGianBocHangDen"].ToString() : "";
                string fromLocation = formCollection["txtFromLocation"] != null ? formCollection["txtFromLocation"].ToString() : "";
                string toLocation = formCollection["txtToLocation"] != null ? formCollection["txtToLocation"].ToString() : "";

                //double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString().Replace(".", "")) : 0;

                double soKmUocTinh = 0;
                //double soKmUocTinh = 0;
                try
                {
                    //soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString().Replace(".", ",")) : 0;
                    soKmUocTinh = double.Parse(formCollection["txtSoKmUocTinh"], NumberStyles.Float, cen);
                }
                catch
                {

                }


                double price = 0;
                try
                {
                    //price = formCollection["Gia"] != null ? double.Parse(formCollection["Gia"].ToString().Replace(".", "")) : 0;
                    double.TryParse(formCollection["Gia"], out price);
                }
                catch
                {

                }

                order.TenHang = formCollection["TenHang"] != null ? formCollection["TenHang"].ToString() : "";
                order.DienThoaiLienHe = formCollection["DienThoaiLienHe"] != null ? formCollection["DienThoaiLienHe"].ToString() : "";
                order.NguoiLienHe = formCollection["NguoiLienHe"] != null ? formCollection["NguoiLienHe"].ToString() : "";
                order.Gia = price;
                order.Status = formCollection["Status"] != null ? int.Parse(formCollection["Status"].ToString()) : PublicConstant.STATUS_PENDING;
                order.Note = formCollection["Note"] != null ? formCollection["Note"].ToString() : "";
                order.DiemDiChiTiet = fromLocation;
                order.DiemDenChiTiet = toLocation;

                ThoiGianBocHangDi = " " + ThoiGianBocHangDi.Trim();
                if (txtTimeDi.Length > 0 && ThoiGianBocHangDi.Trim().Length > 0)
                {
                    var tgdi = string.Format("{0} {1}", txtTimeDi, ThoiGianBocHangDi);
                    order.ThoiGianDi = DateTime.Parse(tgdi, cti, DateTimeStyles.None);
                    //order.ThoiGianDi = DateTime.Parse(txtTimeDi + ThoiGianBocHangDi);
                }

                ThoiGianBocHangDen = " " + ThoiGianBocHangDen.Trim();
                if (txtTimeDen.Length > 0 && ThoiGianBocHangDen.Trim().Length > 0)
                {
                    var tgden = string.Format("{0} {1}", txtTimeDen, ThoiGianBocHangDen);
                    order.ThoiGianDen = DateTime.Parse(tgden, cti, DateTimeStyles.None);
                    //order.ThoiGianDen = DateTime.Parse(txtTimeDen + ThoiGianBocHangDen);

                    //var tgdi = string.Format("{0} {1}", txtTimeDi, ThoiGianBocHangDi);
                    //order.ThoiGianDi = DateTime.Parse(tgdi, cti, DateTimeStyles.None);
                }

                order.UserName = User.Identity.Name;
                var thanhToan = formCollection["rThanhToan"] != null ? formCollection["rThanhToan"].ToString() : "0";
                if (thanhToan.Equals("Tiền mặt"))
                {
                    order.LoaiThanhToan = 0;
                }
                else
                {
                    order.LoaiThanhToan = 1;
                }
                order.MaVanDon = Ultilities.RandomNumber(8);
                order.CreateAt = DateTime.Now;
                order.UpdateAt = DateTime.Now;

                //string message = "";
                //double latFrom = 0, lngFrom = 0, latTo = 0, lngTo = 0;

                //string latLongFrom = GetLatLongByAddress(ref message, fromLocation);
                //if (message.Length == 0)
                //{
                //    latFrom = double.Parse(latLongFrom.Split(':')[0]);
                //    lngFrom = double.Parse(latLongFrom.Split(':')[1]);
                //}

                //string latLongTo = GetLatLongByAddress(ref message, toLocation);
                //if (message.Length == 0)
                //{
                //    latTo = double.Parse(latLongTo.Split(':')[0]);
                //    lngTo = double.Parse(latLongTo.Split(':')[1]);
                //}
                //order.FromLat = latFrom;
                //order.FromLng = lngFrom;

                //order.ToLat = latTo;
                //order.ToLng = lngTo;

                //double frLat = 0, frLng = 0, tLat = 0, toLng = 0;
                try
                {
                    //order.FromLat = formCollection["hfLatFrom"] != null ? double.Parse(formCollection["hfLatFrom"].ToString().Replace(".", ",")) : 0;
                    //order.FromLng = formCollection["hfLngFrom"] != null ? double.Parse(formCollection["hfLngFrom"].ToString().Replace(".", ",")) : 0;

                    //order.ToLat = formCollection["hfLatTo"] != null ? double.Parse(formCollection["hfLatTo"].ToString().Replace(".", ",")) : 0;
                    //order.ToLng = formCollection["hfLngTo"] != null ? double.Parse(formCollection["hfLngTo"].ToString().Replace(".", ",")) : 0;

                    order.FromLat = double.Parse(formCollection["hfLatFrom"], NumberStyles.Float, cen);
                    order.FromLng = double.Parse(formCollection["hfLngFrom"], NumberStyles.Float, cen);
                    order.ToLat = double.Parse(formCollection["hfLatTo"], NumberStyles.Float, cen);
                    order.ToLng = double.Parse(formCollection["hfLngTo"], NumberStyles.Float, cen);

                    //double.TryParse(formCollection["hfLatFrom"], out frLat);
                    //double.TryParse(formCollection["hfLngFrom"], out frLng);
                    //double.TryParse(formCollection["hfLatTo"], out tLat);
                    //double.TryParse(formCollection["hfLngTo"], out toLng);
                    //order.FromLat = frLat;
                    //order.FromLng = frLng;
                    //order.ToLat = tLat;
                    //order.ToLng = toLng;
                }
                catch
                {
                    order.FromLat = 0;
                    order.FromLng = 0;

                    order.ToLat = 0;
                    order.ToLng = 0;
                }


                if (fromLocation.Length > 0 && toLocation.Length > 0)
                {
                    order.SoKmUocTinh = soKmUocTinh;
                }
                else
                {
                    order.SoKmUocTinh = 0;
                }

                if (formCollection["txtWidth"] != null && formCollection["txtWidth"].Length > 0)
                {
                    order.Width = double.Parse(formCollection["txtWidth"].ToString());
                }
                else
                {
                    order.Width = 0;
                }

                if (formCollection["txtHeight"] != null && formCollection["txtHeight"].Length > 0)
                {
                    order.Height = double.Parse(formCollection["txtHeight"].ToString());
                }
                else
                {
                    order.Height = 0;
                }

                if (formCollection["txtLenght"] != null && formCollection["txtLenght"].Length > 0)
                {
                    order.Lenght = double.Parse(formCollection["txtLenght"].ToString());
                }
                else
                {
                    order.Lenght = 0;
                }

                //order.VAT = formCollection["VAT"] == "false" ? false : true;
                order.VAT = true;
                if (formCollection["txtTrongLuong"] != null && formCollection["txtTrongLuong"].Length > 0)
                {
                    order.TrongLuong = double.Parse(formCollection["txtTrongLuong"].ToString());
                }
                else
                {
                    order.TrongLuong = 0;
                }

                order.Created_By = User.Identity.GetUserId();
                order.IsViewPopupOrder = false;

                if (ModelState.IsValid)
                {
                    try
                    {
                        using (var db = new Carrier3Entities())
                        {
                            orderTemp = order;
                            db.Order.Add(order);
                            db.SaveChanges();
                            #region insert OrderPlaces table
                            var lstOrderPlace = new List<OrderPlaces>();
                            for (int i = 0; i < listDiemTraHang.Count(); i++)
                            {
                                var orderPlace = new OrderPlaces();
                                orderPlace.Created_At = DateTime.Now;
                                orderPlace.OrderId = order.Id;
                                orderPlace.FormatAddress = listDiemTraHang[i];
                                orderPlace.PlaceLat = double.Parse(listPlaceLat[i].ToString().Replace(".", ","));
                                orderPlace.PlaceLng = double.Parse(listPlaceLng[i].ToString().Replace(".", ","));
                                orderPlace.Updated_At = DateTime.Now;
                                lstOrderPlace.Add(orderPlace);
                            }

                            #endregion
                            #region insert OrderCar table
                            string type = "";
                            int quanlity = 0;
                            int typeCarId = 0;
                            OrderCar orderCar;
                            var lstOrderCars = new List<OrderCar>();
                            string strOrderCar = formCollection["hdfStrCarTypeSelect"];
                            //str dropdowlist cartype select return  example: 1_25-1:0:1_4-2:0:2_5-3:0:3_5-4:1:5-5:1:8-6:0:10-7:0:15-8:0:18-9:0:20-10:0:40-11:0:
                            string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                            var countCartype = strCarTypeSelect.Count();
                            int totalCar = 0;
                            for (int idx = 0; idx < countCartype; idx++)
                            {
                                var item = strCarTypeSelect[idx];
                                quanlity = int.Parse(item.Split(':')[1]);
                                totalCar += quanlity;
                            }

                            if (totalCar > 1)
                            {
                                int idx1 = 0;
                                for (int idx = 0; idx < countCartype; idx++)
                                {
                                    var item = strCarTypeSelect[idx];
                                    //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                                    //type = item.Split('-')[0];
                                    typeCarId = int.Parse(item.Split('-')[1].Split(':')[0]);
                                    quanlity = int.Parse(item.Split(':')[1]);
                                    for (int j = 0; j < quanlity; j++)
                                    {
                                        idx1++;
                                        var obj = new Order
                                        {
                                            CreateAt = orderTemp.CreateAt,
                                            Created_By = orderTemp.Created_By,
                                            DiemDenChiTiet = orderTemp.DiemDenChiTiet,
                                            DiemDiChiTiet = orderTemp.DiemDiChiTiet,
                                            DienThoaiLienHe = orderTemp.DienThoaiLienHe,
                                            FromLat = orderTemp.FromLat,
                                            FromLng = orderTemp.FromLng,
                                            Gia = orderTemp.Gia,
                                            LoaiThanhToan = orderTemp.LoaiThanhToan,
                                            NguoiLienHe = orderTemp.NguoiLienHe,
                                            Note = orderTemp.Note,
                                            SoKmUocTinh = orderTemp.SoKmUocTinh,
                                            Status = orderTemp.Status,
                                            TenHang = orderTemp.TenHang,
                                            ThoiGianDen = orderTemp.ThoiGianDen,
                                            ThoiGianDi = orderTemp.ThoiGianDi,
                                            ToLat = orderTemp.ToLat,
                                            ToLng = orderTemp.ToLng,
                                            TrongLuong = orderTemp.TrongLuong,
                                            UpdateAt = orderTemp.UpdateAt,
                                            UserName = orderTemp.UserName,
                                            VAT = orderTemp.VAT,
                                            Width = orderTemp.Width,
                                            Height = orderTemp.Height,
                                            Lenght = orderTemp.Lenght
                                        };
                                        obj.MaVanDon = orderTemp.MaVanDon + "-" + idx1;
                                        obj.ParentId = orderTemp.Id;
                                        db.Order.Add(obj);
                                        db.SaveChanges();

                                        orderCar = new OrderCar();
                                        orderCar.OrderId = obj.Id;
                                        orderCar.CarTypeId = typeCarId;
                                        orderCar.Number = 1;
                                        lstOrderCars.Add(orderCar);
                                    }

                                }
                            }
                            else
                            {
                                for (int idx = 0; idx < countCartype; idx++)
                                {
                                    var item = strCarTypeSelect[idx];
                                    //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                                    //type = item.Split('-')[0];
                                    typeCarId = int.Parse(item.Split('-')[1].Split(':')[0]);
                                    quanlity = int.Parse(item.Split(':')[1]);
                                    for (int j = 0; j < quanlity; j++)
                                    {
                                        orderCar = new OrderCar();
                                        orderCar.OrderId = order.Id;
                                        orderCar.CarTypeId = typeCarId;
                                        orderCar.Number = 1;
                                        lstOrderCars.Add(orderCar);
                                    }
                                }
                            }


                            #endregion
                            EFBatchOperation.For(db, db.OrderPlaces).InsertAll(lstOrderPlace);
                            EFBatchOperation.For(db, db.OrderCar).InsertAll(lstOrderCars);

                            if (order.Status == 1 || User.IsInRole(PublicConstant.ROLE_FORWARDER))
                            {
                                MapOrder orMap = _unitOfWork.GetRepositoryInstance<MapOrder>().GetFirstOrDefaultByParameter(o => o.ToUser.Equals(UserId));
                                if (orMap == null)
                                {
                                    var nearDriver = GetDriverNearOrder(order.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                                    if (nearDriver != null)
                                    {

                                        PushMessageForDriver(nearDriver.UserId, order.Id);
                                    }
                                }
                            }
                        }
                        TempData["info"] = "Thêm vận đơn thành công!";
                    }
                    catch (Exception ex)
                    {
                        TempData["info"] = "Lỗi xảy ra khi gửi đơn!";
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm vận đơn: " + ex.Message;
                return View(order);
            }
            return View(order);
        }

        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
            //var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
            var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId", sqlOrderId).FirstOrDefault();
            return lstSearch;
        }
        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="order"></param>
        public void PushMessageForDriver(string driverId, long orderId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device == null)
            {
                TempData["info"] = "Tài xế chưa kích hoạt quyền nhận thông báo của thiết bị hoặc lỗi khi đăng ký tài khoản!";
                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(driverId));
                if (old != null)
                {
                    old.Updated_At = DateTime.Now;
                    old.Status = PublicConstant.ORDER_CANNOT_SEND;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                    _unitOfWork.SaveChanges();
                }
                return;
            }
            //var obj = new OrderTracking();
            //obj.Status = 0;
            //obj.Updated_At = DateTime.Now;
            //obj.Created_At = DateTime.Now;
            //obj.DriverId = driverId;
            //obj.OwnerId = User.Identity.GetUserId();
            //obj.OrderId = orderId;
            //_unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
            //_unitOfWork.SaveChanges();
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            long orderId = long.Parse(obj2);
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
            var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
            if (old == null)
            {
                var obj = new OrderTracking();
                obj.Status = 0;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                if (result.Equals("Success"))
                {
                    obj.Status = PublicConstant.ORDER_PENDING;
                    TempData["info"] = "Vận đơn đã được gửi cho tài xế thành công!";
                }
                else
                {
                    obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                    TempData["error"] = "Lỗi không thể gửi được vận đơn. Vui lòng thử lại sau!";

                }
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
            else
            {
                old.Status = 0;
                old.Updated_At = DateTime.Now;
                old.DriverId = obj1;
                old.OwnerId = order.Created_By;
                old.OrderId = orderId;
                if (result.Equals("Success"))
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                    TempData["info"] = "Vận đơn đã được gửi cho tài xế thành công!";
                }
                else
                {
                    old.Status = PublicConstant.ORDER_CANNOT_SEND;
                    TempData["error"] = "Lỗi không thể gửi được vận đơn. Vui lòng thử lại sau!";
                }
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                _unitOfWork.SaveChanges();
            }
        }
        public Driver GetFistDriverNearMost(int distance, int page_size, Order order)
        {
            var lstDriver = db.Driver.ToList();
            List<Driver> lstDriverTemp = new List<Driver>();
            double currentDistinct = 0;
            foreach (Driver driver in lstDriver)
            {
                currentDistinct = GetDistance(double.Parse(driver.Lat.ToString()), double.Parse(driver.Lng.ToString()), double.Parse(order.FromLat.ToString()), double.Parse(order.FromLng.ToString()));
                if (currentDistinct < distance)
                {
                    //if (GreatThanDate(driver.Updated_At, DateTime.Now.ToString("HH:mm dd/MM/yyyy")))
                    {
                        lstDriverTemp.Add(driver);
                    }
                }
            }

            return lstDriverTemp.FirstOrDefault();

            //var lstDriver = db.Driver.Where(x => x.Location.Distance(order.FromLocation) < distance).Take(page_size);
            //var lstResult = new List<Driver>();
            //foreach (Driver item in lstDriver)
            //{
            //    if (GreatThanDate(item.Updated_At, DateTime.Now.ToString("HH:mm dd/MM/yyyy")))
            //    {
            //        lstResult.Add(item);
            //    }
            //}
            //return lstResult.FirstOrDefault();
        }

        private static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }
        public double CalcDistance(double latFrom, double longFrom, double latTo, double longTo)
        {
            var R = 6371d; // Radius of the earth in km
            var dLat = DegreeToRadian(latTo - latFrom);  // deg2rad below
            var dLon = DegreeToRadian(longTo - longFrom);
            var a =
              Math.Sin(dLat / 2d) * Math.Sin(dLat / 2d) +
              Math.Cos(DegreeToRadian(latFrom)) * Math.Cos(DegreeToRadian(latTo)) *
              Math.Sin(dLon / 2d) * Math.Sin(dLon / 2d);
            var c = 2d * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1d - a));
            var d = R * c; // Distance in km
            return d;
        }
        private double DegreeToRadian(double angle)
        {
            return angle * (Math.PI / 180d);
        }
        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            string appId = User.Identity.GetUserId();

            if (order.Created_By != appId)
            {
                return HttpNotFound();
            }
            UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID == appId);
            if (userInfo != null)
            {
                ViewBag.Lat = userInfo.Lat != null ? userInfo.Lat : 21.033781;
                ViewBag.Lng = userInfo.Lng != null ? userInfo.Lng : 105.814054;
            }
            else
            {
                ViewBag.Lat = 21.033781;
                ViewBag.Lng = 105.814054;
            }

            var listCar = new List<OrderCarViewModel>();
            var listDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == id);
            //Xu ly đơn con
            if (order.ParentId != null && order.ParentId > 0)
            {
                listCar = (from a in db.OrderCar
                           join c in db.CarType on a.CarTypeId equals c.Id
                           where a.OrderId == order.Id
                           select new OrderCarViewModel
                           {
                               Id = a.Id,
                               CarTypeId = c.Id,
                               CarTypeName = c.Name,
                               Number = a.Number,
                               OrderId = a.OrderId,
                               Value = c.Value
                           }).ToList();

                listDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == order.ParentId);
            }
            else // xu ly don cha
            {
                //Đơn cha nhiều xe
                if (_unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(x => x.ParentId == id).Count() > 0)
                {
                    var subOrderId = (from o in db.Order
                                      where o.ParentId == id
                                      select o.Id).ToList();
                    var orderCars = (from oc in db.OrderCar
                                     where subOrderId.Contains(oc.OrderId.Value)
                                     select new { oc.CarTypeId, oc.Number, oc.OrderId, oc.Id }).ToList();
                    var orderCarsFilter = orderCars.GroupBy(x => x.CarTypeId, x => x.Number, (key, values) => new { CarTypeId = key, Number = values.Sum() });

                    listCar = (from a in orderCarsFilter
                               join c in db.CarType on a.CarTypeId equals c.Id
                               select new OrderCarViewModel
                               {
                                   CarTypeId = c.Id,
                                   CarTypeName = c.Name,
                                   Number = a.Number,
                                   Value = c.Value
                               }).ToList();


                    listDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == id);
                }
                else
                {
                    //Đơn cha một xe
                    listCar = (from a in db.OrderCar
                               join c in db.CarType on a.CarTypeId equals c.Id
                               where a.OrderId == order.Id
                               select new OrderCarViewModel
                               {
                                   Id = a.Id,
                                   CarTypeId = c.Id,
                                   CarTypeName = c.Name,
                                   Number = a.Number,
                                   OrderId = a.OrderId,
                                   Value = c.Value
                               }).ToList();

                    listDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == id);
                }

            }

            ViewBag.ListOrderCar = listCar;

            List<CarType> listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords().ToList();
            Session.Add("LIST_CAR", listCarType);


            ViewBag.ListDiemTraHang = listDiemTraHang;

            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection formCollection)
        {
            var ldth = new List<string>();
            long id = 0;
            Order order = new Order();
            try
            {
                id = long.Parse(formCollection["Id"].ToString());
                order = db.Order.Where(o => o.Id == id).Single();

                string appId = User.Identity.GetUserId();
                if (order.Created_By != appId)
                {
                    return HttpNotFound();
                }

                if (order != null)
                {
                    //Lấy danh sách Diem tra hang
                    var listDiemTraHang = new List<string>();
                    var listPlaceLat = new List<float?>();
                    var listPlaceLng = new List<float?>();
                    if (formCollection["counterDiemTraHang"] != "")
                    {
                        //int n = Int32.Parse(formCollection["counterDiemTraHang"]);
                        for (int i = 0; i < PublicConstant.WAYPOINTS; i++)
                        {
                            if (formCollection["txtToLocation_" + i] != null && formCollection["txtToLocation_" + i].Trim() != "")
                            {
                                listDiemTraHang.Add(formCollection["txtToLocation_" + i].Trim());
                                float _lat = 0;
                                float _lng = 0;
                                try
                                {
                                    _lat = (formCollection["hfLatTo_" + i] != null && formCollection["hfLatTo_" + i] != "") ? float.Parse(formCollection["hfLatTo_" + i].ToString().Replace(".", ",")) : 0;
                                    _lng = (formCollection["hfLngTo_" + i] != null && formCollection["hfLngTo_" + i] != "") ? float.Parse(formCollection["hfLngTo_" + i].ToString().Replace(".", ",")) : 0;

                                }
                                catch
                                {

                                }


                                listPlaceLat.Add(_lat);
                                listPlaceLng.Add(_lng);

                            }

                        }
                    }

                    //var txtTimeDi = formCollection["txtTimeDi"] != null ? DateTime.Parse(formCollection["txtTimeDi"]).ToString("dd/MM/yyyy") : "";
                    var txtTimeDi = formCollection["txtTimeDi"] != null ? formCollection["txtTimeDi"] : "";
                    var ThoiGianBocHangDi = formCollection["ThoiGianBocHangDi"] != null ? formCollection["ThoiGianBocHangDi"] : "";
                    string txtTimeDen = "";
                    if ((formCollection["txtTimeDen"] != null && formCollection["txtTimeDen"].Length > 0))
                    {
                        //txtTimeDen = DateTime.Parse(formCollection["txtTimeDen"]).ToString("dd/MM/yyyy");
                        txtTimeDen = formCollection["txtTimeDen"];
                    }

                    string ThoiGianBocHangDen = "";
                    if ((formCollection["ThoiGianBocHangDen"] != null && formCollection["ThoiGianBocHangDen"].Length > 0))
                    {
                        ThoiGianBocHangDen = formCollection["ThoiGianBocHangDen"].ToString();
                    }

                    string fromLocation = formCollection["txtFromLocation"] != null ? formCollection["txtFromLocation"].ToString() : "";
                    string toLocation = formCollection["txtToLocation"] != null ? formCollection["txtToLocation"].ToString() : "";
                    //double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString().Replace(".", "")) : double.Parse(order.SoKmUocTinh.ToString());
                    double soKmUocTinh = 0;
                    double latFrom = 0;
                    double lngFrom = 0;
                    double latTo = 0;
                    double lngTo = 0;
                    double price = 0;
                    try
                    {
                        soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString()) : double.Parse(order.SoKmUocTinh.ToString());
                        latFrom = (formCollection["hfLatFrom"] != null && formCollection["hfLatFrom"].Length > 0) ? double.Parse(formCollection["hfLatFrom"].ToString()) : double.Parse(order.FromLat.ToString());
                        lngFrom = (formCollection["hfLngFrom"] != null && formCollection["hfLngFrom"].Length > 0) ? double.Parse(formCollection["hfLngFrom"].ToString()) : double.Parse(order.FromLng.ToString());
                        latTo = (formCollection["hfLatTo"] != null && formCollection["hfLatTo"].Length > 0) ? double.Parse(formCollection["hfLatTo"].ToString()) : double.Parse(order.ToLat.ToString());
                        lngTo = (formCollection["hfLngTo"] != null && formCollection["hfLngTo"].Length > 0) ? double.Parse(formCollection["hfLngTo"].ToString()) : double.Parse(order.ToLng.ToString());

                        price = formCollection["gia"] != null ? double.Parse(formCollection["gia"].ToString()) : double.Parse(order.Gia.ToString());


                    }
                    catch
                    {

                    }
                    order.TenHang = formCollection["TenHang"] != null ? formCollection["TenHang"].ToString() : order.TenHang;
                    order.DienThoaiLienHe = formCollection["DienThoaiLienHe"] != null ? formCollection["DienThoaiLienHe"].ToString() : order.DienThoaiLienHe;
                    order.NguoiLienHe = formCollection["NguoiLienHe"] != null ? formCollection["NguoiLienHe"].ToString() : order.NguoiLienHe;

                    order.Gia = price;
                    order.Status = formCollection["Status"] != null ? int.Parse(formCollection["Status"].ToString()) : PublicConstant.STATUS_ACTIVE;
                    order.Note = formCollection["Note"] != null ? formCollection["Note"].ToString() : order.Note;


                    ThoiGianBocHangDi = " " + ThoiGianBocHangDi.Trim();
                    if (txtTimeDi.Length > 0 && ThoiGianBocHangDi.Trim().Length > 0)
                    {
                        //order.ThoiGianDi = DateTime.Parse(txtTimeDi + ThoiGianBocHangDi);
                        var tgdi = string.Format("{0} {1}", txtTimeDi, ThoiGianBocHangDi);
                        //order.ThoiGianDi = DateTime.Parse(tgdi);
                        order.ThoiGianDi = DateTime.Parse(tgdi, cti, DateTimeStyles.None);
                    }

                    ThoiGianBocHangDen = " " + ThoiGianBocHangDen.Trim();
                    if (txtTimeDen.Length > 0 && ThoiGianBocHangDen.Trim().Length > 0)
                    {
                        //order.ThoiGianDen = DateTime.Parse(txtTimeDen + ThoiGianBocHangDen);
                        var tgden = string.Format("{0} {1}", txtTimeDen, ThoiGianBocHangDen);
                        //order.ThoiGianDen = DateTime.Parse(tgden);
                        order.ThoiGianDen = DateTime.Parse(tgden, cti, DateTimeStyles.None);
                    }

                    order.UserName = User.Identity.Name;

                    if (formCollection["LoaiThanhToan"] == "0")
                    {
                        order.LoaiThanhToan = 0;
                    }
                    else
                    {
                        order.LoaiThanhToan = 1;
                    }
                    //order.MaVanDon = Ultilities.GenerateOrderCode(txtTimeDi, User.Identity.Name);
                    order.CreateAt = order.CreateAt;
                    order.UpdateAt = DateTime.Now;

                    order.DiemDiChiTiet = fromLocation;
                    order.DiemDenChiTiet = toLocation;

                    string message = "";
                    //double latFrom = 0, lngFrom = 0, latTo = 0, lngTo = 0;
                    //if (fromLocation.Length > 0)
                    //{
                    //    string latLongFrom = GetLatLongByAddress(ref message, fromLocation);
                    //    if (message.Length == 0)
                    //    {
                    //        latFrom = double.Parse(latLongFrom.Split(':')[0]);
                    //        lngFrom = double.Parse(latLongFrom.Split(':')[1]);
                    //    }
                    //}
                    //else
                    //{
                    //    latFrom = double.Parse(order.FromLat.ToString());
                    //    lngFrom = double.Parse(order.FromLng.ToString());
                    //}

                    //if (toLocation.Length > 0 && toLocation.Split(':').Length < 2)
                    //{
                    //    string latLongTo = GetLatLongByAddress(ref message, toLocation);
                    //    if (message.Length == 0)
                    //    {
                    //        latTo = double.Parse(latLongTo.Split(':')[0]);
                    //        lngTo = double.Parse(latLongTo.Split(':')[1]);
                    //    }
                    //}
                    //else
                    //{
                    //    latTo = double.Parse(order.ToLat.ToString());
                    //    lngTo = double.Parse(order.ToLng.ToString());
                    //}

                    order.FromLat = latFrom;
                    order.ToLat = lngFrom;
                    order.ToLat = latTo;
                    order.ToLng = lngTo;

                    //if (fromLocation.Length > 0 && toLocation.Length > 0)
                    //{
                    //    order.SoKmUocTinh = soKmUocTinh;CalcDistance(latFrom, lngFrom, latTo, lngTo);
                    //}
                    //else
                    //{
                    //    order.SoKmUocTinh = order.SoKmUocTinh;
                    //}
                    order.SoKmUocTinh = soKmUocTinh;//CalcDistance(latFrom, lngFrom, latTo, lngTo);

                    if (formCollection["txtWidth"] != null && formCollection["txtWidth"].Length > 0)
                    {
                        order.Width = double.Parse(formCollection["txtWidth"].ToString());
                    }
                    else
                    {
                        order.Width = order.Width;
                    }

                    if (formCollection["txtHeight"] != null && formCollection["txtHeight"].Length > 0)
                    {
                        order.Height = double.Parse(formCollection["txtHeight"].ToString());
                    }
                    else
                    {
                        order.Height = order.Height;
                    }

                    if (formCollection["txtLenght"] != null && formCollection["txtLenght"].Length > 0)
                    {
                        order.Lenght = double.Parse(formCollection["txtLenght"].ToString());
                    }
                    else
                    {
                        order.Lenght = order.Lenght;
                    }

                    //order.VAT = formCollection["VAT"] == "false" ? false : true;
                    order.VAT = true;
                    if (formCollection["txtTrongLuong"] != null && formCollection["txtTrongLuong"].Length > 0)
                    {
                        order.TrongLuong = double.Parse(formCollection["txtTrongLuong"].ToString());
                    }
                    else
                    {
                        order.TrongLuong = order.TrongLuong;
                    }

                    db.SaveChanges();

                    #region Edit OrderPlaces table

                    _unitOfWork.GetRepositoryInstance<OrderPlaces>().RemoveRangeByWhereClause(o => o.OrderId == order.Id);
                    _unitOfWork.SaveChanges();


                    var lstOrderPlace = new List<OrderPlaces>();
                    for (int i = 0; i < listDiemTraHang.Count(); i++)
                    {
                        var orderPlace = new OrderPlaces();
                        orderPlace.Created_At = DateTime.Now;
                        orderPlace.OrderId = order.Id;
                        orderPlace.FormatAddress = listDiemTraHang[i];
                        orderPlace.PlaceLat = listPlaceLat[i];
                        orderPlace.PlaceLng = listPlaceLng[i];
                        orderPlace.Updated_At = DateTime.Now;
                        lstOrderPlace.Add(orderPlace);
                    }
                    EFBatchOperation.For(db, db.OrderPlaces).InsertAll(lstOrderPlace);
                    #endregion Edit OrderPlaces table

                    #region Delete old Ordertracking, delete old suborder , delete old ordercar

                    //get subOrder
                    var subOrder = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(o => o.ParentId == id);
                    if (subOrder.Count() > 0) // Don nhieu xe
                    {
                        // remove suborder tracking
                        foreach (var v in subOrder)
                        {
                            _unitOfWork.GetRepositoryInstance<OrderTracking>().RemoveRangeByWhereClause(ot => ot.OrderId == v.Id);
                        }
                        //remove suborder
                        foreach (var v in subOrder)
                        {
                            _unitOfWork.GetRepositoryInstance<Order>().RemoveRangeByWhereClause(o => o.Id == v.Id);
                        }
                        //remove ordercar
                        foreach (var v in subOrder)
                        {
                            _unitOfWork.GetRepositoryInstance<OrderCar>().RemoveRangeByWhereClause(oc => oc.OrderId == v.Id);
                        }
                        _unitOfWork.SaveChanges();

                    }
                    else // Don mot xe 
                    {
                        //remove oldertracking

                        _unitOfWork.GetRepositoryInstance<OrderTracking>().RemoveRangeByWhereClause(ot => ot.OrderId == id);
                        //reove old ordercar
                        _unitOfWork.GetRepositoryInstance<OrderCar>().RemoveRangeByWhereClause(o => o.OrderId == order.Id);

                        _unitOfWork.SaveChanges();

                    }
                    #endregion Delete old Ordertracking, delete old suborder , delete old ordercar

                    #region renew suborder, ordercar
                    int quanlity = 0;
                    int typeCarId = 0;
                    OrderCar orderCar;
                    var lstOrderCars = new List<OrderCar>();
                    string strOrderCar = formCollection["hdfStrCarSelect"];
                    //str dropdowlist cartype select return  example: 1_25-1:0:1_4-2:0:2_5-3:0:3_5-4:1:5-5:1:8-6:0:10-7:0:15-8:0:18-9:0:20-10:0:40-11:0:
                    string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                    var countCartype = strCarTypeSelect.Count();
                    int totalCar = 0;
                    for (int idx = 0; idx < countCartype; idx++)
                    {
                        var item = strCarTypeSelect[idx];
                        quanlity = int.Parse(item.Split(':')[1]);
                        totalCar += quanlity;
                    }

                    if (totalCar > 1)
                    {
                        int idx1 = 0;
                        for (int idx = 0; idx < countCartype; idx++)
                        {
                            var item = strCarTypeSelect[idx];
                            //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                            //type = item.Split('-')[0];
                            typeCarId = int.Parse(item.Split('-')[1].Split(':')[0]);
                            quanlity = int.Parse(item.Split(':')[1]);
                            for (int j = 0; j < quanlity; j++)
                            {
                                idx1++;
                                var obj = new Order
                                {
                                    CreateAt = order.CreateAt,
                                    Created_By = order.Created_By,
                                    DiemDenChiTiet = order.DiemDenChiTiet,
                                    DiemDiChiTiet = order.DiemDiChiTiet,
                                    DienThoaiLienHe = order.DienThoaiLienHe,
                                    FromLat = order.FromLat,
                                    FromLng = order.FromLng,
                                    Gia = order.Gia,
                                    LoaiThanhToan = order.LoaiThanhToan,
                                    NguoiLienHe = order.NguoiLienHe,
                                    Note = order.Note,
                                    SoKmUocTinh = order.SoKmUocTinh,
                                    Status = order.Status,
                                    TenHang = order.TenHang,
                                    ThoiGianDen = order.ThoiGianDen,
                                    ThoiGianDi = order.ThoiGianDi,
                                    ToLat = order.ToLat,
                                    ToLng = order.ToLng,
                                    TrongLuong = order.TrongLuong,
                                    UpdateAt = order.UpdateAt,
                                    UserName = order.UserName,
                                    VAT = order.VAT,
                                    Width = order.Width,
                                    Height = order.Height,
                                    Lenght = order.Lenght
                                };
                                obj.MaVanDon = order.MaVanDon + "-" + idx1;
                                obj.ParentId = order.Id;
                                db.Order.Add(obj);
                                db.SaveChanges();

                                orderCar = new OrderCar();
                                orderCar.OrderId = obj.Id;
                                orderCar.CarTypeId = typeCarId;
                                orderCar.Number = 1;
                                lstOrderCars.Add(orderCar);
                            }

                        }
                    }
                    else
                    {
                        for (int idx = 0; idx < countCartype; idx++)
                        {
                            var item = strCarTypeSelect[idx];
                            //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                            //type = item.Split('-')[0];
                            typeCarId = int.Parse(item.Split('-')[1].Split(':')[0]);
                            quanlity = int.Parse(item.Split(':')[1]);
                            for (int j = 0; j < quanlity; j++)
                            {
                                orderCar = new OrderCar();
                                orderCar.OrderId = order.Id;
                                orderCar.CarTypeId = typeCarId;
                                orderCar.Number = 1;
                                lstOrderCars.Add(orderCar);
                            }
                        }
                    }

                    #endregion renew suborder, ordercar
                    EFBatchOperation.For(db, db.OrderCar).InsertAll(lstOrderCars);


                    if (order.Status == 1) // push tự động
                    {
                        var nearDriver = GetDriverNearOrder(order.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                        if (nearDriver != null)
                        {
                            //CarrierHubClient client = new CarrierHubClient();
                            //if (client.isOnline(nearDriver.UserId))
                            //{
                            //    await client.SendMessage(User.Identity.GetUserId(), nearDriver.UserId, order);
                            //}
                            //else
                            //{
                            //var obj = new OrderTracking();
                            //obj.Status = 0;
                            //obj.Updated_At = DateTime.Now;
                            //obj.Created_At = DateTime.Now;
                            //obj.DriverId = nearDriver.UserId;
                            //obj.OwnerId = order.Created_By;
                            //obj.OrderId = order.Id;
                            //_unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                            //_unitOfWork.SaveChanges();
                            PushMessageForDriver(nearDriver.UserId, order.Id);
                            //}
                        }
                    }

                    TempData["info"] = "Cập nhật thành công!";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật vận đơn: " + ex.Message;
            }


            return RedirectToAction("Edit", new { id = order.Id });
        }

        public ActionResult ListDeleteAjax(string listId)
        {
            string[] listOrderId = listId.TrimEnd(',').Split(',').ToArray();

            try
            {
                foreach (string id1 in listOrderId)
                {
                    long id = long.Parse(id1);
                    Order order = db.Order.Find(id);
                    if (order == null)
                    {
                        return HttpNotFound();
                    }
                    if (User.IsInRole(PublicConstant.ROLE_ADMIN))
                    {
                        db.Order.Remove(order);

                        var subOrder = db.Order.Where(o => o.ParentId == id);

                        if (subOrder.Count() > 0) // don nhieu xe
                        {
                            List<long?> subIds = new List<long?>();
                            foreach (var v in subOrder)
                            {
                                subIds.Add(v.Id);
                            }
                            //delete suborder
                            db.Order.RemoveRange(subOrder);
                            //delete Ordertracking
                            var ot = db.OrderTracking.Where(o => subIds.Contains(o.OrderId));
                            if (ot != null)
                            {
                                db.OrderTracking.RemoveRange(ot);
                            }

                            //delete Orderplaces

                            var op = db.OrderPlaces.Where(o => o.OrderId == id);
                            if (op != null)
                            {
                                db.OrderPlaces.RemoveRange(op);
                            }
                            //delete Ordercar
                            var oc = db.OrderCar.Where(o => o.OrderId == id);
                            if (oc != null)
                            {
                                db.OrderCar.RemoveRange(oc);
                            }
                            db.SaveChanges();
                        }
                        else // don mot xe
                        {

                            //delete Ordertracking
                            var ot = db.OrderTracking.Where(o => o.OrderId == id);
                            if (ot != null)
                            {
                                db.OrderTracking.RemoveRange(ot);
                            }
                            //delete Orderplaces

                            var op = db.OrderPlaces.Where(o => o.OrderId == id);
                            if (op != null)
                            {
                                db.OrderPlaces.RemoveRange(op);
                            }
                            //delete Ordercar
                            var oc = db.OrderCar.Where(o => o.OrderId == id);
                            if (oc != null)
                            {
                                db.OrderCar.RemoveRange(oc);
                            }
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        if (String.Compare(order.UserName, User.Identity.Name, false) != 0)
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                        }
                        else
                        {
                            db.Order.Remove(order);

                            var subOrder = db.Order.Where(o => o.ParentId == id);

                            if (subOrder.Count() > 0) // don nhieu xe
                            {
                                List<long?> subIds = new List<long?>();
                                foreach (var v in subOrder)
                                {
                                    subIds.Add(v.Id);
                                }
                                //delete suborder
                                db.Order.RemoveRange(subOrder);
                                //delete Ordertracking
                                var ot = db.OrderTracking.Where(o => subIds.Contains(o.OrderId));
                                if (ot != null)
                                {
                                    db.OrderTracking.RemoveRange(ot);
                                }

                                //delete Orderplaces

                                var op = db.OrderPlaces.Where(o => o.OrderId == id);
                                if (op != null)
                                {
                                    db.OrderPlaces.RemoveRange(op);
                                }
                                //delete Ordercar
                                var oc = db.OrderCar.Where(o => o.OrderId == id);
                                if (oc != null)
                                {
                                    db.OrderCar.RemoveRange(oc);
                                }
                                db.SaveChanges();
                            }
                            else // don mot xe
                            {

                                //delete Ordertracking
                                var ot = db.OrderTracking.Where(o => o.OrderId == id);
                                if (ot != null)
                                {
                                    db.OrderTracking.RemoveRange(ot);
                                }
                                //delete Orderplaces

                                var op = db.OrderPlaces.Where(o => o.OrderId == id);
                                if (op != null)
                                {
                                    db.OrderPlaces.RemoveRange(op);
                                }
                                //delete Ordercar
                                var oc = db.OrderCar.Where(o => o.OrderId == id);
                                if (oc != null)
                                {
                                    db.OrderCar.RemoveRange(oc);
                                }
                                db.SaveChanges();
                            }

                        }
                    }
                }
                TempData["info"] = "Xóa vận đơn thành công!";
                return Json("Xóa vận đơn thành công", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa vận đơn: " + ex.Message;
                return Json("Lỗi xóa vận đơn" + ex.Message, JsonRequestBehavior.AllowGet);
            }


        }
        public ActionResult DeleteAjax(long id)
        {
            try
            {
                Order order = db.Order.Find(id);
                if (order == null)
                {
                    return HttpNotFound();
                }
                if (User.IsInRole(PublicConstant.ROLE_ADMIN))
                {
                    db.Order.Remove(order);
                    List<Order> subOrder = new List<Order>();
                    subOrder = db.Order.Where(o => o.ParentId == id).ToList();

                    if (subOrder.Count > 0) // don nhieu xe
                    {
                        List<long?> subIds = new List<long?>();
                        foreach (var v in subOrder)
                        {
                            subIds.Add(v.Id);
                        }
                        //delete suborder
                        db.Order.RemoveRange(subOrder);
                        //delete Ordertracking
                        var ot = db.OrderTracking.Where(o => subIds.Contains(o.OrderId));
                        if (ot != null)
                        {
                            db.OrderTracking.RemoveRange(ot);
                        }

                        //delete Orderplaces

                        var op = db.OrderPlaces.Where(o => o.OrderId == id);
                        if (op != null)
                        {
                            db.OrderPlaces.RemoveRange(op);
                        }
                        //delete Ordercar
                        var oc = db.OrderCar.Where(o => o.OrderId == id);
                        if (oc != null)
                        {
                            db.OrderCar.RemoveRange(oc);
                        }
                        db.SaveChanges();
                    }
                    else // don mot xe
                    {

                        //delete Ordertracking
                        var ot = db.OrderTracking.Where(o => o.OrderId == id);
                        if (ot != null)
                        {
                            db.OrderTracking.RemoveRange(ot);
                        }
                        //delete Orderplaces

                        var op = db.OrderPlaces.Where(o => o.OrderId == id);
                        if (op != null)
                        {
                            db.OrderPlaces.RemoveRange(op);
                        }
                        //delete Ordercar
                        var oc = db.OrderCar.Where(o => o.OrderId == id);
                        if (oc != null)
                        {
                            db.OrderCar.RemoveRange(oc);
                        }
                        db.SaveChanges();
                    }
                }
                else
                {
                    if (String.Compare(order.UserName, User.Identity.Name, false) != 0)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        db.Order.Remove(order);
                        List<Order> subOrder = new List<Order>();
                        subOrder = db.Order.Where(o => o.ParentId == id).ToList();

                        if (subOrder.Count > 0) // don nhieu xe
                        {
                            List<long?> subIds = new List<long?>();
                            foreach (var v in subOrder)
                            {
                                subIds.Add(v.Id);
                            }
                            //delete suborder
                            db.Order.RemoveRange(subOrder);
                            //delete Ordertracking
                            var ot = db.OrderTracking.Where(o => subIds.Contains(o.OrderId));
                            if (ot != null)
                            {
                                db.OrderTracking.RemoveRange(ot);
                            }

                            //delete Orderplaces

                            var op = db.OrderPlaces.Where(o => o.OrderId == id);
                            if (op != null)
                            {
                                db.OrderPlaces.RemoveRange(op);
                            }
                            //delete Ordercar
                            var oc = db.OrderCar.Where(o => o.OrderId == id);
                            if (oc != null)
                            {
                                db.OrderCar.RemoveRange(oc);
                            }
                            db.SaveChanges();
                        }
                        else // don mot xe
                        {

                            //delete Ordertracking
                            var ot = db.OrderTracking.Where(o => o.OrderId == id);
                            if (ot != null)
                            {
                                db.OrderTracking.RemoveRange(ot);
                            }
                            //delete Orderplaces

                            var op = db.OrderPlaces.Where(o => o.OrderId == id);
                            if (op != null)
                            {
                                db.OrderPlaces.RemoveRange(op);
                            }
                            //delete Ordercar
                            var oc = db.OrderCar.Where(o => o.OrderId == id);
                            if (oc != null)
                            {
                                db.OrderCar.RemoveRange(oc);
                            }
                            db.SaveChanges();
                        }
                    }
                }

                TempData["info"] = "Xóa vận đơn thành công!";
                return Json("Xóa vận đơn thành công", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa vận đơn: " + ex.Message;
                return Json("Lỗi xóa vận đơn" + ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        // GET: Orders/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {

                Order order = db.Order.Find(id);
                if (order == null)
                {
                    return HttpNotFound();
                }
                if (User.IsInRole(PublicConstant.ROLE_ADMIN))
                {
                    db.Order.Remove(order);
                    db.SaveChanges();
                }
                else
                {
                    if (String.Compare(order.UserName, User.Identity.Name, false) != 0)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        db.Order.Remove(order);
                        db.SaveChanges();
                    }
                }

                TempData["info"] = "Xóa vận đơn thành công!";
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa vận đơn: " + ex.Message;
            }

            return RedirectToAction("Index");
        }

        // GET: Orders/DanhSachXe/5
        public JsonResult DanhSachXe(long id)
        {
            List<Car1> result = new List<Car1>();
            List<long> listSubId = new List<long>();
            // đơn nhiều xe
            listSubId = (from a in db.Order
                         where a.ParentId == id
                         select a.Id
                              ).ToList();
            if (listSubId.Count() > 0)
            {
                result = (
                    from a in db.DriverCar
                    join o in db.Driver on a.DriverId equals o.Id
                    join b in db.Car on a.CarId equals b.Id
                    join ot in db.OrderTracking on o.UserId equals ot.DriverId
                    where listSubId.Contains(ot.OrderId.Value)
                    select new Car1
                    {
                        Id = b.Id,
                        License = b.License
                    }
                    ).ToList();
            }
            else
            {
                result = (
                    from a in db.DriverCar
                    join o in db.Driver on a.DriverId equals o.Id
                    join b in db.Car on a.CarId equals b.Id
                    join ot in db.OrderTracking on o.UserId equals ot.DriverId
                    where ot.OrderId == id
                    select new Car1
                    {
                        Id = b.Id,
                        License = b.License
                    }
                    ).ToList();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        public double GetDiscount(double Gia)
        {
            double discount = Gia;

            //select from database ...
            //if (Gia > 1000000) return Gia * 0.9;
            //if (Gia > 300000 && Gia < 1000000) return Gia * 0.95;

            //return Gia * 0.9;
            return Gia;
        }
        /// <summary>
        /// Tính giá theo file excel mới
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPriceNew(string address, string lat, string lng, bool isVAT, string strOrderCar)
        {
            try
            {
                //SysLogisticValidate.IsValid();
                string message = "";
                double distance1 = 0;
                double distance = 0;
                double totalPrice = 0;
                string totalPriceNoVat = "";
                double price = 0;
                string type = "";
                int quanlity = 0;
                var arrayAddress = address.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLat = lat.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLng = lng.Split(new[] { "|" }, StringSplitOptions.None);
                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                int checkDistance = 0;
                foreach (var item in strCarTypeSelect)
                {
                    //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                    type = item.Split('-')[0];
                    quanlity = int.Parse(item.Split(':')[1]);
                    if (quanlity > 0)
                    {
                        for (int j = 0; j < arrayAddress.Length - 1; j++)
                        {

                            price += 0;// import.GetPriceForOrder(type, arrayAddress[j], arrayAddress[j + 1], double.Parse(arrayLat[j]), double.Parse(arrayLng[j]), double.Parse(arrayLat[j + 1]), double.Parse(arrayLng[j + 1]), ref message, ref distance1);
                            if (checkDistance == 0)
                            {
                                distance += distance1;
                            }
                        }

                        if (price > 0)
                        {
                            totalPrice = totalPrice + (price * quanlity);
                        }
                        checkDistance = 1;
                    }


                }

                totalPriceNoVat = totalPrice.ToString("#,###");

                if (message.Length == 0)
                {
                    string priceValue = "";
                    if (isVAT)
                    {
                        //totalPrice = totalPrice * 1.1;
                        totalPrice = GetDiscount(totalPrice);
                        priceValue = totalPrice.ToString("#,###");
                    }
                    else
                    {
                        totalPrice = GetDiscount(totalPrice);
                        priceValue = totalPrice.ToString("#,###");
                    }
                    distance = Math.Floor(distance * 100) / 100;
                    return Json(new { distance = distance, totalPrice = priceValue, oldPrice = totalPriceNoVat, message = "tính giá thành công" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                    return Json(new { message = "Lỗi tính giá vận đơn:" + message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Tính giá theo file excel mới
        /// </summary>
        /// <param name="fromaddress"></param>
        /// <param name="toaddress"></param>
        /// <param name="isVAT"></param>
        /// <param name="strOrderCar"></param>
        /// <returns></returns>
        public JsonResult GetPrice(string fromaddress, string toaddress, double latfrom, double lnfrom, double latto, double lnto, bool isVAT,
        string strOrderCar)
        {
            try
            {

                string message = "";
                double distance = 0;
                double totalPrice = 0;
                string totalPriceNoVat = "";
                double price = 0;
                string type = "";
                int quanlity = 0;

                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                foreach (var item in strCarTypeSelect)
                {
                    //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                    type = item.Split('-')[0];
                    quanlity = int.Parse(item.Split(':')[1]);
                    if (quanlity > 0)
                    {
                        price = 0;// import.GetPriceForOrder(type, fromaddress, toaddress, latfrom, lnfrom, latto, lnto, ref message, ref distance);
                        if (price > 0)
                        {
                            totalPrice = totalPrice + (price * quanlity);
                        }
                    }
                }

                totalPriceNoVat = totalPrice.ToString("#,###");

                if (message.Length == 0)
                {
                    string priceValue = "";
                    if (isVAT)
                    {
                        //totalPrice = totalPrice * 1.1;
                        totalPrice = GetDiscount(totalPrice);
                        priceValue = totalPrice.ToString("#,###");
                    }
                    else
                    {
                        totalPrice = GetDiscount(totalPrice);
                        priceValue = totalPrice.ToString("#,###");
                    }
                    return Json(new { distance = distance, totalPrice = priceValue, oldPrice = totalPriceNoVat, message = "tính giá thành công" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                    return Json(new { message = "Lỗi tính giá vận đơn:" + message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetPriceAPI(string addresssug, string addressApi, string address, string lat, string lng, string strOrderCar, string newdistance, string origirndistance, string newtime, string origirntime, string arrayDistance)
        {
            try
            {
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ",";
                provider.NumberGroupSeparator = ".";
                provider.NumberGroupSizes = new int[] { 3 };
                //load cau hinh
                string uid = User.Identity.GetUserId();
                var tinhgiarieng = _unitOfWork.GetRepositoryInstance<SysSetting>().GetFirstOrDefaultByParameter(s => s.UserId == uid && s.SettingKey == "BANG_GIA_VAN");

                string message = "";
                string totalPriceNoVat = "";
                double price = 0;
                double priceOne = 0;
                string type = "";
                int quanlity = 0;
                var arrayAddresssug = addresssug.Split(new[] { "|" }, StringSplitOptions.None);
                var arratAddressApi = addressApi.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayAddress = address.Split(new[] { "|" }, StringSplitOptions.None);
                var _arrayDistance = arrayDistance.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLat = lat.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLng = lng.Split(new[] { "|" }, StringSplitOptions.None);
                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                List<Location> listLC = new List<Location>();
                List<string> ListAddress = new List<string>();
                double Lat, Lng;
                for (int i = 0; i < arrayLat.Length; i++)
                {
                    Location lc = new Location();
                    //arrayLat[i] = arrayLat[i].Replace(".", ",");
                    //arrayLng[i] = arrayLng[i].Replace(".", ",");
                    //Lat = Convert.ToDouble(arrayLat[i]);
                    //Lng = Convert.ToDouble(arrayLng[i]);

                    Lat = double.Parse(arrayLat[i], NumberStyles.Any, cen);
                    Lng = double.Parse(arrayLng[i], NumberStyles.Any, cen);

                    lc.Lat = Lat;
                    lc.Lng = Lng;
                    ListAddress.Add(arrayAddress[i]);
                    listLC.Add(lc);

                }

                float newDistance = newdistance != null ? float.Parse(newdistance) : 0;
                float origirnDistance = origirndistance != null ? float.Parse(origirndistance) : 0;
                long newTime = newtime != null ? long.Parse(newtime) : 0;
                long origirnTime = origirntime != null ? long.Parse(origirntime) : 0;

                string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();


                if (SysLogisticValidate.IsValid(newDistance, origirnDistance, newTime, origirnTime, listLC, ListAddress))
                {
                    foreach (var item in strCarTypeSelect)
                    {
                        //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                        type = item.Split('-')[0];
                        quanlity = int.Parse(item.Split(':')[1]);
                        priceOne = 0;
                        if (quanlity > 0)
                        {
                            if (tinhgiarieng != null && tinhgiarieng.SettingValue != "0")
                            {
                                priceOne = SysLogistic.GetPriceForEnterprise(uid, type, origirnDistance / 1000, ListAddress);
                                if (priceOne == 0)
                                {
                                    return Json(new { message = "Bảng giá chưa có sẵn cho tuyến này" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ListAddress, arrayAddresssug, arratAddressApi, out message);
                                if (priceOne == 0)
                                {
                                    //priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ListAddress, arratAddressApi, out message);
                                    //{
                                    //    if(priceOne == 0)
                                    //    {
                                    //        return Json(new { message = "Bảng giá chưa có sẵn cho tuyến này" }, JsonRequestBehavior.AllowGet);
                                    //    }
                                    //}
                                    if (priceOne == 0)
                                    {
                                        return Json(new { message = "Bảng giá chưa có sẵn cho tuyến này" }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                            if (priceOne > 0)
                            {
                                price = price + (priceOne * quanlity);
                            }

                        }
                        if (message.Length > 0)
                        {
                            break;
                        }

                    }
                    if (message.Length == 0 && price > 0)
                    {
                        price = GetDiscount(price);
                        string p = price.ToString("#,###");
                        return Json(new { distance = newDistance, totalPrice = p, oldPrice = totalPriceNoVat, message = "Tính giá thành công" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (message.Length == 0 && price == 0)
                        {
                            return Json(new { message = "Bảng giá chưa có sẵn cho tuyến này" }, JsonRequestBehavior.AllowGet);
                        }
                        //TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                        return Json(new { message = "Lỗi tính giá vận đơn:" + message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //TempData["error"] = "Đường đi không phù hợp vì vượt quá thời gian hoặc quá dài: " + message;
                    return Json(new { message = "Đường đi không phù hợp vì vượt quá thời gian hoặc quá dài" }, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                //TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                string log = @"C:/Log/ExceptionCMS.txt";
                System.IO.File.AppendAllText(log, System.Reflection.MethodBase.GetCurrentMethod().Name + DateTime.Now + ex.InnerException.Message + System.Environment.NewLine);
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult AutoCompleteDraftOrder(string prefix)
        {
            DraftOrder entities = new DraftOrder();
            var listDraft = (from devicePush in db.DraftOrder
                             where devicePush.Name.StartsWith(prefix)
                             select new
                             {
                                 label = devicePush.Name,
                                 val = devicePush.Id
                             }).ToList();

            return Json(listDraft);
        }

        [HttpPost]
        public JsonResult LoadDraftOrder(int id)
        {
            DraftOrder draftOrder = _unitOfWork.GetRepositoryInstance<DraftOrder>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (draftOrder != null)
            {
                return Json(new
                {
                    TenHang = draftOrder.TenHang != null ? draftOrder.TenHang : "",
                    DiemDi = draftOrder.DiemDi != null ? draftOrder.DiemDi : "",
                    DiemDen = draftOrder.DiemDen != null ? draftOrder.DiemDen : "",
                    Gia = draftOrder.Gia != null ? draftOrder.Gia : 0,
                    FromLat = draftOrder.FromLat != null ? draftOrder.FromLat : 0,
                    FromLng = draftOrder.FromLng != null ? draftOrder.FromLng : 0,
                    ToLat = draftOrder.ToLat != null ? draftOrder.ToLat : 0,
                    ToLng = draftOrder.ToLng != null ? draftOrder.ToLng : 0,
                    Vat = draftOrder.Vat != null ? draftOrder.Vat : false,
                    TrongLuong = draftOrder.TrongLuong != null ? draftOrder.TrongLuong : 0,
                    ThoiGianDi = draftOrder.ThoiGianDi != null ? draftOrder.ThoiGianDi : "",
                    ThoiGianDen = draftOrder.ThoiGianDen != null ? draftOrder.ThoiGianDi : "",
                    Note = draftOrder.Note != null ? draftOrder.Note : "",
                    Width = draftOrder.Width != null ? draftOrder.Width : 0,
                    Height = draftOrder.Height != null ? draftOrder.Height : 0,
                    Lenght = draftOrder.Lenght != null ? draftOrder.Lenght : 0,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "lỗi lấy dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        public ActionResult GetDriverOfOrder(int OrderId)
        {
            Carrier3Entities db = new Carrier3Entities();
            var listTrackings = (from a in db.OrderTracking select new { OrderId = a.OrderId, DriverId = a.DriverId }).Distinct().Where(o => o.OrderId == OrderId).ToList();

            List<Driver> listDriver = new List<Driver>();
            Driver driver;
            foreach (var item in listTrackings)
            {
                driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.UserId == item.DriverId);
                if (driver != null)
                {
                    listDriver.Add(driver);
                }
            }

            return PartialView("ListDriverOfOrder", listDriver);
        }

        public List<ListDriver> GetListDriver()
        {
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            List<ListDriver> lstDriver = new List<ListDriver>();
            var listCT = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == organizationId);
            List<OrderTracking> listOrderTrack = new List<OrderTracking>();
            foreach (Driver item in listCT)
            {
                listOrderTrack = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetAllRecords().Where(o => o.DriverId == item.UserId && o.Status == PublicConstant.ORDER_ACCEPT).ToList(); // check tài xế nào đã nhận đơn thì ko load
                if (listOrderTrack.Count == 0)
                {
                    lstDriver.Add(new ListDriver() { Id = item.Id, Name = item.Name, Phone = item.Phone, UserId = item.UserId });
                }
            }
            return lstDriver;
        }
        public JsonResult LoadListDriver(int id)
        {
            string strCode = "";
            List<ListDriver> lstDriver = GetListDriver();
            foreach (var item in lstDriver)
            {
                strCode += "<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-4\"><button type = \"button\" style=\"border-color:white; width:200px;text-align:left \" class =\"btn btn-default col-md-3 btn-primary-pay\" data-value=\"" + item.Phone + "\"  class =\"btn btn-default\" id=\"" + item.Id + "\"  onClick =\"ClickDriver('" + item.Id + "')\" value=\"" + item.UserId + "\"><i style=\"color:#f7e60a\" class=\"fa fa-car\" aria-hidden=\"true\"></i>&nbsp;" + item.Name + "</button>" + "<br /></div>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> SendNewOrderToDriver(int orderId, string driverId)
        {
            #region Dont Using SignalR this version
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(orderId);
            //var userOnline = _unitOfWork.GetRepositoryInstance<UsersOnline>().GetFirstOrDefaultByParameter(x => x.UserId.Equals(driverId));
            //if (userOnline.IsOnline.Value)
            //{
            //    var hubConnection = new HubConnection("http://api.vantaitoiuu.com/", new Dictionary<string, string>{
            //             { "UserId", driverId }});
            //    var hubProxy = hubConnection.CreateHubProxy("CarrierMonitor");
            //    await hubConnection.Start();
            //    await hubProxy.Invoke("SendData", driverId, order);
            //}
            //else
            //{
            //    PushMessageForDriver(driverId, order);
            //}
            #endregion

            PushMessageForDriver(driverId, orderId);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public bool CheckOrderTracking(long orderId)
        {
            bool isExit = false;
            try
            {
                List<OrderTracking> listOrderTracking = db.OrderTracking.Where(o => o.OrderId == orderId).ToList();
                if (listOrderTracking.Count > 0)
                {
                    isExit = true;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return isExit;
        }
        public JsonResult PushOrderForWarDer(long orderForwarderId)
        {
            var nearDriver = GetDriverNearOrder(orderForwarderId, PublicConstant.DEFAULT_DISTANCE_SEARCH);
            if (nearDriver != null)
            {
                //var obj = new OrderTracking();
                //obj.OrderId = orderForwarderId;
                //obj.DriverId = nearDriver.UserId;
                //obj.OwnerId = User.Identity.GetUserId();
                //obj.Created_At = DateTime.Now;
                //obj.Updated_At = DateTime.Now;
                //obj.Created_By = User.Identity.GetUserId();
                //obj.Status = PublicConstant.ORDER_PENDING;
                //_unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                PushMessageForDriver(nearDriver.UserId, orderForwarderId);
                TempData["info"] = "Vận đơn đã được gửi cho tài xế thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = "Lỗi không thể gửi được vận đơn. Vui lòng thử lại sau!";
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="thoigiandi">Thoi gian di la thoi gian di cua don hang</param>
        /// <param name="status">Trang thai don hang lay tu bang OrderTracking</param>
        /// <returns></returns>
        public string ShowStatusToView(DateTime thoigiandi, int status, int orderType)
        {
            string strResult = string.Empty;
            switch (status)
            {
                case 1:
                    {
                        strResult = @"<span style='color:yellowgreen' > Đã khớp lệnh</span>";
                        break;
                    }
                case 2:
                    {
                        strResult = @"<span style='color:red' > Tài xế hủy</span>";
                        break;
                    }
                case 3:
                    {
                        strResult = @"<span style='color:red' > Chủ hàng hủy</span>";
                        break;
                    }
                case 4:
                    {
                        strResult = @"<span style='color:yellowgreen' > Đang vận chuyển</span>";
                        break;
                    }
                case 5:
                    {
                        strResult = @"<span style='color:blue' > Hoàn thành</span>";
                        break;
                    }
                case 6:
                    {
                        strResult = @"<span style='color:red' > Gửi tài xế lỗi</span>";
                        break;
                    }
                case 7:
                    {
                        strResult = @"<span style='color:red' > Đơn hết hạn</span>";
                        break;
                    }
                case 0:
                    {
                        if (orderType == 0)
                        {
                            if (thoigiandi != null && thoigiandi < DateTime.Now)
                            {
                                strResult = @"<span style=""color:red"">Đơn hết hạn</span>";
                            }
                            else
                            {
                                strResult = @"<span style='color:dodgerblue' > Đang khớp lệnh</span>";
                            }
                        }
                        else
                        {
                            strResult = @"<span style='color:dodgerblue' > Đang khớp lệnh</span>";
                        }
                        break;
                    }
                case -1:
                    {
                        if (thoigiandi != null && thoigiandi < DateTime.Now)
                        {
                            strResult = @"<span style=""color:red"">Đơn hết hạn</span>";
                        }
                        else
                        {
                            strResult = @"<span style='color:dodgerblue' > Tạo mới</span>";

                        }
                        break;
                    }
                default:
                    {

                        break;
                    }
                    //}
            }
            return strResult;
        }
        public ActionResult ResetOrder(int id)
        {
            try
            {

                Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(id);
                if (order == null)
                {
                    return HttpNotFound();
                }

                else
                {
                    if (order.Status == 0)
                    {
                        OrderTracking orTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == id);
                        if (orTracking == null)
                        {
                            TempData["info"] = "Vận đơn chưa được gán!";
                        }
                        else
                        {
                            _unitOfWork.GetRepositoryInstance<OrderTracking>().Remove(orTracking);
                            _unitOfWork.SaveChanges();
                            TempData["info"] = "Resset vận đơn thành công!";
                        }
                    }
                    else
                    {
                        TempData["info"] = "Vận đơn phải là đơn push bằng tay!";
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi reset vận đơn: " + ex.Message;
            }

            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult ResetOrderAjax(string listId)
        {
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();

            foreach (string id in liststrId)
            {
                try
                {

                    Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefault(int.Parse(id));
                    if (order != null)
                    {
                        if (order.Status == 0)
                        {
                            var orId = int.Parse(id);
                            OrderTracking orTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orId);
                            if (orTracking == null)
                            {
                                TempData["info"] = "Vận đơn chưa được gán!";
                            }
                            else
                            {
                                _unitOfWork.GetRepositoryInstance<OrderTracking>().Remove(orTracking);
                                _unitOfWork.SaveChanges();
                                TempData["info"] = "Resset vận đơn thành công!";
                            }
                        }
                        else
                        {
                            TempData["info"] = "Vận đơn phải là đơn push bằng tay!";
                        }
                    }

                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi reset vận đơn: " + ex.Message;
                }
            }
            TempData["info"] = "Resset vận đơn thành công!";
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        public string GetNameUser(string s)
        {
            string st = "";
            var user = _unitOfWork.GetRepositoryInstance<AspNetUsers>().GetFirstOrDefaultByParameter(o => o.Id.Equals(s));
            if (user != null)
            {
                st = user.UserName;
            }
            else
            {
                st = string.Empty;
            }
            return st;
        }
        public ActionResult PopUpEnterprise(string userId)
        {
            UserInfoes user = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID.Equals(userId));
            return PartialView("_UserDetail", user);
        }
    }
    public class ListDriver
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string UserId { get; set; }
    }

    public class Car1
    {
        public int Id { get; set; }
        public string License { get; set; }
    }

}
