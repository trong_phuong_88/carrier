﻿using Carrier.CMS.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using Carrier.Utilities;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.CMS;
using System;
using PagedList;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;
using GoogleMaps.LocationServices;
using Carrier.CMS.Common;
using Microsoft.AspNet.Identity;
using CKSource.FileSystem;
using System.Data.SqlClient;
using System.IO;
using System.Web.Helpers;
using System.Web.Configuration;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    public class EDriverController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private GenericUnitOfWork _unitOfWorkTemp = new GenericUnitOfWork();
        private GenericUnitOfWork _unitOfWorkTemp2 = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Enterprise/Driver
        [Authorize]
        public ActionResult Index()
        {
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            var usersNameOrganizati = User.Identity.Name;

            List<Driver> result = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == organizationId).OrderByDescending(o => o.Name).ToList();
            List<EDriverViewModel> resultView = new List<EDriverViewModel>();
            foreach (var item in result)
            {
                var viewModel = new EDriverViewModel();
                var user = UserManager.FindById(item.UserId);
                DriverCar driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(o => o.DriverId == item.Id);
                if (driverCar != null)
                {
                    Car carId = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id == driverCar.CarId);
                    if (carId != null)
                    {
                        viewModel.License = carId.License;
                        viewModel.Payload = carId.Payload;
                    }
                    else
                    {
                        viewModel.License = "0";
                        viewModel.Payload = 0;
                    }
                }
                else
                {
                    viewModel.License = "0";
                    viewModel.Payload = 0;
                }
                viewModel.Id = item.Id;
                viewModel.Address = item.Address;
                viewModel.Back_Image = item.Back_Image;
                viewModel.BirthDay = item.BirthDay;
                viewModel.Created_At = item.Created_At;
                viewModel.Front_Image = item.Front_Image;
                viewModel.Lng = item.Lng;
                viewModel.Lat = item.Lat;
                viewModel.Name = item.Name;
                viewModel.OrganizationId = item.OrganizationId;
                viewModel.Phone = item.Phone;
                viewModel.Sex = item.Sex;
                viewModel.Status = item.Status;
                viewModel.Updated_At = item.Updated_At;
                viewModel.UserId = item.UserId;
                if (user != null)
                {
                    viewModel.User_Name = user.UserName.Remove(user.UserName.Length - usersNameOrganizati.Length - 1);
                    viewModel.LockoutEnabled = user.LockoutEnabled;
                }

                resultView.Add(viewModel);
            }
            return View(resultView);
        }

        // GET: Admin/Drivers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver drivers = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            return View(drivers);
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }

        //public string UploadImageDriver(HttpPostedFileBase file, ref string fileNameOut)
        //{
        //    string message = "";
        //    string fileName = "";
        //    try
        //    {
        //        if (file != null && file.ContentLength > 0)
        //        {
        //            //kiem tra file extension lan nua
        //            List<string> excelExtension = new List<string>();
        //            excelExtension.Add(".png");
        //            excelExtension.Add(".jpg");
        //            excelExtension.Add(".gif");
        //            var extension = Path.GetExtension(file.FileName);
        //            if (!excelExtension.Contains(extension.ToLower()))
        //            {
        //                return "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png";
        //            }
        //            //kiem tra dung luong file
        //            var fileSize = file.ContentLength;
        //            var fileMB = (fileSize / 1024f) / 1024f;
        //            if (fileMB > 5)
        //            {
        //                return "Dung lượng ảnh không được lớn hơn 5MB";
        //            }
        //            // luu ra dia
        //            fileName = Path.GetFileNameWithoutExtension(file.FileName);
        //            fileName = fileName + "-" + DateTime.Now.ToString("dd-MM-yy-hh-ss") + extension;
        //            fileNameOut = fileName;
        //            var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Driver"), fileName);
        //            file.SaveAs(physicalPath);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }

        //    return message;
        //}


        /// <summary>
        /// Uploading and resizing an image, Currently it is used to upload member profile pic, provider service banner image and category image
        /// </summary>
        /// <param name="message"></param>
        /// <param name="originalImage"></param>
        /// <param name="imageName"></param>
        /// <param name="rootPathOriginal"></param>
        /// <param name="rootPathThumbNail"></param>
        /// <param name="server"></param>
        private void UploadImage(ref string message, HttpPostedFileBase originalImage, string imageName, string rootPathOriginal, string rootPathThumbNail, HttpServerUtilityBase server)
        {
            int MaxContentLength = 1024 * 1024 * 2; //Size = 1 MB  
            if (imageName.LastIndexOf('.') < 0)
            {
                message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                return;
            }

            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
            var ext = imageName.Substring(imageName.LastIndexOf('.'));
            var extension = ext.ToLower();
            if (!AllowedFileExtensions.Contains(extension))
            {

                message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                return;
            }
            else if (originalImage.ContentLength > MaxContentLength)
            {

                message = string.Format("Dung lượng file vượt quá 1 mb.");
                return;
            }
            try
            {
                bool existsOriginal = Directory.Exists(PublicConstant.IMAGE_PAHT_TEMP);
                if (!existsOriginal)
                {
                    Directory.CreateDirectory(PublicConstant.IMAGE_PAHT_TEMP);
                }
                originalImage.SaveAs(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                WebImage img1 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                img1.Resize(849, 320);
                bool exists = Directory.Exists(rootPathOriginal);
                if (!exists)
                    Directory.CreateDirectory(rootPathOriginal);
                img1.Save(System.IO.Path.Combine(rootPathOriginal + "/" + imageName));

                WebImage img2 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                img2.Resize(274, 175);
                bool exists2 = Directory.Exists(rootPathThumbNail);
                if (!exists2)
                    Directory.CreateDirectory(rootPathThumbNail);
                img2.Save(System.IO.Path.Combine(rootPathThumbNail + "/" + imageName));

                System.IO.File.Delete(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                #region save
                //bool existsOriginal = System.IO.Directory.Exists(server.MapPath(Constant.IMAGE_PAHT_TEMP));
                //if (!existsOriginal)
                //{
                //    System.IO.Directory.CreateDirectory(server.MapPath(Constant.IMAGE_PAHT_TEMP));
                //}
                //originalImage.SaveAs(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));

                //WebImage img1 = new WebImage(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                //img1.Resize(849, 320);
                //bool exists = System.IO.Directory.Exists(server.MapPath(rootPathOriginal));
                //if (!exists)
                //    System.IO.Directory.CreateDirectory(server.MapPath(rootPathOriginal));
                //img1.Save(Path.Combine(server.MapPath(rootPathOriginal + "/" + imageName)));

                //WebImage img2 = new WebImage(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                //img2.Resize(274, 175);
                //bool exists2 = System.IO.Directory.Exists(server.MapPath(rootPathThumbNail));
                //if (!exists2)
                //    System.IO.Directory.CreateDirectory(server.MapPath(rootPathThumbNail));
                //img2.Save(Path.Combine(server.MapPath(rootPathThumbNail + "/" + imageName)));

                //System.IO.File.Delete(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                #endregion
            }
            catch (Exception ex)
            {
                message = "Lỗi upload ảnh: " + ex.Message;
            }
        }

        // GET: Admin/Drivers/Create
        public ActionResult Create()
        {
            return View();
        }
        public string CheckUserName(string input)
        {
            List<string> driverUsers = new List<string>();
            string driverName = input.Trim() + ":" + User.Identity.Name;
            var users = UserManager.Users.ToList();
            bool ifuser = false;
            foreach (var u in users)
            {
                string uname = u.UserName;
                if (uname.Equals(driverName))
                {
                    ifuser = true;
                    break;
                }
            }

            if (ifuser == false)
            {
                return "Available";
            }

            if (ifuser == true)
            {
                return "Not Available";
            }

            return "";
        }

        // POST: Admin/Drivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection form)
        {
            #region Upload Image
            var front_Image = "";
            var back_Image = "";

            ViewBag.UName = form.Get("UserName") != null ? form.Get("UserName") : "";
            ViewBag.Password = form.Get("Password") != null ? form.Get("Password") : "";
            ViewBag.Email = form.Get("Email") != null ? form.Get("Email") : "";
            ViewBag.Name = form.Get("Name") != null ? form.Get("Name") : "";
            ViewBag.Address = form.Get("Address") != null ? form.Get("Address") : "";
            ViewBag.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
            ViewBag.Sex = form.Get("Sex") != null ? form.Get("Sex") : "";
            ViewBag.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";

            if (Request.Files.Count > 0)
            {
                string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                HttpPostedFileBase fileTruoc = Request.Files[0];
                HttpPostedFileBase fileSau = Request.Files[1];
                string filename1 = ViewBag.UName + "image_front" + fileTruoc.FileName;
                string filename2 = ViewBag.UName + "image_back" + fileSau.FileName;

                string message = "";
                //message = UploadImageDriver(fileTruoc, ref front_Image);
                if (fileTruoc.ContentLength > 0)
                {
                    front_Image = publicRootPath + filename1;
                    UploadImage(ref message, fileTruoc, filename1, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                }


                if (message.Length > 0)
                {
                    TempData["error"] = "Lỗi upload ảnh mặt trước";
                    return View();
                }

                if (fileSau.ContentLength > 0)
                {
                    back_Image = publicRootPath + filename2;
                    UploadImage(ref message, fileSau, filename2, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                }
                //message = UploadImageDriver(fileSau, ref back_Image);
                if (message.Length > 0)
                {
                    TempData["error"] = "Lỗi upload ảnh mặt sau";
                    return View();
                }
            }
            #endregion

            Driver driver = new Driver();
            try
            {
                if (ModelState.IsValid)
                {
                    string userName = form.Get("UserName") != null ? form.Get("UserName") : "";
                    string password = form.Get("Password") != null ? form.Get("Password") : "";
                    string email = form.Get("Email") != null ? form.Get("Email") : "";
                    var name = form.Get("Name") != null ? form.Get("Name") : "";
                    var address = form.Get("Address") != null ? form.Get("Address") : "";
                    var phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                    var sex = form.Get("Sex") != null ? form.Get("Sex") : "";
                    var birthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";

                    var user = new ApplicationUser { UserName = userName + ":" + User.Identity.Name, Email = email };
                    UserInfo userInfo = new UserInfo();
                    userInfo.Address = address;
                    userInfo.MobilePhone = phone;
                    userInfo.FullName = name;
                    userInfo.ApplicationUserID = user.Id;
                    userInfo.Sex = sex == "Nữ" ? false : true;
                    userInfo.Status = PublicConstant.STATUS_ACTIVE;
                    userInfo.Updated_At = DateTime.Now;
                    userInfo.LastLogin = DateTime.Now;
                    userInfo.Created_At = DateTime.Now;
                    userInfo.OrganzationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                    user.UsersInfo = userInfo;
                    var result = await UserManager.CreateAsync(user, password);
                    if (result.Succeeded)
                    {
                        var userDriver = await UserManager.FindByIdAsync(userInfo.ApplicationUserID);
                        userDriver.LockoutEnabled = false;
                        UserManager.Update(userDriver);
                        result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_DRIVERS);
                        if (result.Succeeded)
                        {
                            driver.Name = name;
                            driver.Address = address;
                            driver.Phone = phone;
                            driver.Sex = sex;
                            driver.BirthDay = birthDay;
                            driver.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);

                            double lat = 0, lng = 0;
                            string message = "";
                            string latLongFrom = GetLatLongByAddress(ref message, address);
                            if (message.Length == 0)
                            {
                                lat = double.Parse(latLongFrom.Split(':')[0]);
                                lng = double.Parse(latLongFrom.Split(':')[1]);
                            }
                            driver.Lat = lat;
                            driver.Lng = lng;
                            driver.UserId = user.Id;
                            driver.Status = form.Get("Status") != null ? int.Parse(form.Get("Status")) : 0;
                            driver.Created_At = DateTime.Now;
                            driver.Updated_At = DateTime.Now;
                            driver.Front_Image = front_Image;
                            driver.Back_Image = back_Image;

                            _unitOfWork.GetRepositoryInstance<Driver>().Add(driver);
                            _unitOfWork.SaveChanges();
                            TempData["info"] = "Thêm tài xế thành công!";
                            return RedirectToAction("Index");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm tài xế: " + ex.Message;
            }
            TempData["error"] = "Đăng ký không thành công:Tên đăng nhập đã tồn tại";
            return View(driver);
        }

        // GET: Admin/Drivers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver drivers = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            //List<Organization> listOrganization = _unitOfWork.GetRepositoryInstance<Organization>().GetAllRecords().ToList();

            //ViewBag.ListOrganization = listOrganization;

            return View(drivers);
        }

        // POST: Admin/Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id == id);
            string message = "";
            driver.Name = form.Get("Name") != null ? form.Get("Name") : "";
            driver.Address = form.Get("Address") != null ? form.Get("Address") : "";
            driver.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
            driver.Sex = form.Get("Sex") != null ? form.Get("Sex") : "";
            driver.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";

            try
            {
                if (ModelState.IsValid)
                {
                    if (driver != null)
                    {
                        #region Upload Image
                        var front_Image = "";
                        var back_Image = "";

                        if (Request.Files.Count > 0)
                        {
                            string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                            HttpPostedFileBase fileTruoc = Request.Files[0];
                            HttpPostedFileBase fileSau = Request.Files[1];
                            string filename1 = UserManager.FindById(driver.UserId).UserName.Split(':')[0] + "image_front" + fileTruoc.FileName;
                            string filename2 = UserManager.FindById(driver.UserId).UserName.Split(':')[0] + "image_back" + fileSau.FileName;


                            //message = UploadImageDriver(fileTruoc, ref front_Image);

                            if (fileTruoc.ContentLength > 0)
                            {
                                front_Image = publicRootPath + filename1;
                                UploadImage(ref message, fileTruoc, filename1, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                            }

                            if (message.Length > 0)
                            {
                                TempData["error"] = "Lỗi upload ảnh mặt trước";
                                return View(driver);
                            }


                            if (fileSau.ContentLength > 0)
                            {
                                back_Image = publicRootPath + filename2;
                                UploadImage(ref message, fileSau, filename2, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                            }

                            //message = UploadImageDriver(fileSau, ref back_Image);
                            if (message.Length > 0)
                            {
                                TempData["error"] = "Lỗi upload ảnh mặt sau";
                                return View(driver);
                            }
                        }
                        #endregion


                        driver.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                        double lat = 0, lng = 0;
                        driver.UserId = driver.UserId;
                        string latLong = GetLatLongByAddress(ref message, driver.Address);
                        if (message.Length == 0)
                        {
                            lat = double.Parse(latLong.Split(':')[0]);
                            lng = double.Parse(latLong.Split(':')[1]);
                        }

                        driver.Lat = lat;
                        driver.Lng = lng;

                        if (front_Image.Length > 0)
                        {
                            driver.Front_Image = front_Image;
                        }
                        if (back_Image.Length > 0)
                        {
                            driver.Back_Image = back_Image;
                        }


                        driver.Status = form.Get("Status") != null ? int.Parse(form.Get("Status")) : 0;
                        driver.Updated_At = DateTime.Now;

                        _unitOfWork.GetRepositoryInstance<Driver>().Update(driver);
                        _unitOfWork.SaveChanges();
                        TempData["info"] = "Cập nhật thông tin tài xế thành công!";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật thông tin tài xế: " + ex.Message;
            }
            return View(driver);
        }


        [HttpPost]
        public JsonResult DeleteDriver(string listId)
        {
            Driver driver;
            //UserInfo userInfo;
            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            string userId = "";
            AspNetUsers user = new AspNetUsers();
            UserInfoes UserInfo = new UserInfoes();
            OrderTracking orderTracking = new OrderTracking();
            foreach (string id in liststrId)
            {
                try
                {
                    driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefault(int.Parse(id));
                    if (driver != null)
                    {
                        userId = driver.UserId;
                        orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.DriverId == userId);
                        if (orderTracking == null)
                        {
                            Carrier3Entities db = new Carrier3Entities();
                            user = db.AspNetUsers.Where(o => o.Id == userId).SingleOrDefault();
                            UserInfo = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault();
                            if (UserInfo != null)
                            {
                                db.UserInfoes.Remove(UserInfo);
                                if (user != null)
                                {
                                    db.AspNetUsers.Remove(user);
                                }
                            }

                            var dc = db.DriverCar.Where(o => o.DriverId.Value == driver.Id).SingleOrDefault();
                            if (dc != null)
                            {
                                _unitOfWork.GetRepositoryInstance<DriverCar>().Remove(dc);
                            }
                            var c = db.Car.Where(o => o.Created_By.Equals(driver.UserId)).SingleOrDefault();
                            if (c != null)
                            {
                                _unitOfWork.GetRepositoryInstance<Car>().Remove(c);
                            }
                            TempData["info"] = "Xóa tài xế thành công!";
                            _unitOfWork.GetRepositoryInstance<Driver>().Remove(driver);
                            _unitOfWork.SaveChanges();
                            db.SaveChanges();
                        }
                        else
                        {
                            TempData["error"] = "Tài xế đã từng chạy đơn hàng không được xóa";
                            return Json("Error", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi xóa tài xế: " + ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public string GetOrganizationName(int? id)
        {
            string name = "";
            if (id != null)
            {
                Organization product = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefault(int.Parse(id.ToString()));
                if (product != null)
                {
                    name = product.Name;
                }
            }

            return name;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        private DbGeography GetLocationFromAddress(string address)
        {
            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(address);
            var address1 = locationService.GetRegionFromLatLong(point.Latitude, point.Longitude);
            var latitude = point.Latitude;
            var longitude = point.Longitude;
            var location = LocationHelper.CreatePoint(latitude, longitude);
            return location;
        }

        public async Task<ActionResult> ResetPassword(FormCollection fm)
        {
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                id = int.Parse(Request.QueryString["id"].ToString());
                Session["ID"] = id;
            }
            var idUser = int.Parse(Session["ID"].ToString());
            Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefault(idUser);
            if (driver != null)
            {
                if (fm["Newpass"] != null)
                {
                    if (driver.UserId != null)
                    {
                        string code = await UserManager.GeneratePasswordResetTokenAsync(driver.UserId);
                        IdentityResult result = await UserManager.ResetPasswordAsync(driver.UserId, code, fm["Newpass"].ToString());

                        if (result.Succeeded)
                        {
                            var userReset = UserManager.FindById(driver.UserId);
                            TempData["info"] = "Reset mât khẩu thành công!";
                            return RedirectToAction("Index", "EDriver");
                        }
                        else
                        {
                            TempData["error"] = "Lỗi reset mật khẩu";
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            else
            {
                TempData["error"] = "Lái xe không tồn tại trên hệ thống";
                return View();
            }
        }
        [HttpGet]
        public JsonResult setPassword()
        {
            //var newPass = Utitlity.Utility.RandomString(pass);
            var newPass = Ultilities.CreatePassword(6);
            return Json(newPass, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> ChangeLockout(int id)
        {
            Driver userDriver;
            userDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefault(id);
            var user = await UserManager.FindByIdAsync(userDriver.UserId);
            if (user == null)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (user.LockoutEnabled == true)
                {
                    user.LockoutEnabled = false;
                }
                else
                {
                    user.LockoutEnabled = true;
                }
                UserManager.Update(user);
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CarDetail(string License)
        {
            Car carInfo = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.License.Equals(License));
            return PartialView("_CarDetail", carInfo);
        }

        public List<ListCarNotExit> GetListCar(int id)
        {
            List<ListCarNotExit> lstCar = new List<ListCarNotExit>();
            var sqlDriverId = new SqlParameter("@driverId", System.Data.SqlDbType.Int) { Value = id };
            List<SP_DriverCarNotExit_Result> listCar = new List<SP_DriverCarNotExit_Result>();
            listCar = _unitOfWork.GetRepositoryInstance<SP_DriverCarNotExit_Result>().GetResultBySqlProcedure("SP_DriverCarNotExit @driverId", sqlDriverId).ToList();
            foreach (var item in listCar)
            {
                lstCar.Add(new ListCarNotExit() { Id = item.Id, License = item.License, Height_Size = item.Height_Size, Width_Size = item.Width_Size, Leng_Size = item.Leng_Size, Payload = item.Payload });
            }
            return lstCar;
        }
        //public ActionResult ListCarInfo(int id, string currentFilter, string searchString, int? page, int? pagsiz)
        //{
        //    if (searchString != null)
        //    {
        //        page = 1;
        //    }
        //    else
        //    {
        //        searchString = currentFilter;
        //    }

        //    ViewBag.CurrentFilter = searchString;
        //    List<ListCarNotExit> lstCar = GetListCar(id);
        //    List<CarInfo> ListCarDetail = new List<CarInfo>();
        //    foreach (var item in lstCar)
        //    {
        //        var carInfo = new CarInfo();
        //        carInfo.Id = item.Id;
        //        carInfo.License = item.License;
        //        carInfo.Payload = item.Payload;
        //        carInfo.Leng_Size = item.Leng_Size;
        //        carInfo.Width_Size = item.Width_Size;
        //        carInfo.Height_Size = item.Height_Size;
        //        carInfo.DriverId = id;
        //        ListCarDetail.Add(carInfo);
        //    }
        //    if (!String.IsNullOrEmpty(searchString))
        //    {
        //        ListCarDetail = ListCarDetail.Where(o => o.License.Contains(searchString)).OrderByDescending(o => o.License).ToList();
        //    }

        //    int pageSize = (pagsiz ?? 10);
        //    ViewBag.PageSize = pageSize;
        //    int pageNumber = (page ?? 1);
        //    return View("_ListCar", ListCarDetail.ToPagedList(pageNumber, pageSize));
        //}

        public JsonResult ListCarInfo(int id, string searchString)
        {
            ViewBag.CurrentFilter = searchString;
            var txtSearch = searchString.Trim().ToLower();
            List<ListCarNotExit> lstCar = GetListCar(id);
            List<CarInfo> ListCarDetail = new List<CarInfo>();
            foreach (var item in lstCar)
            {
                var carInfo = new CarInfo();
                carInfo.Id = item.Id;
                carInfo.License = item.License;
                carInfo.Payload = item.Payload;
                carInfo.Leng_Size = item.Leng_Size;
                carInfo.Width_Size = item.Width_Size;
                carInfo.Height_Size = item.Height_Size;
                carInfo.DriverId = id;
                ListCarDetail.Add(carInfo);
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                ListCarDetail = ListCarDetail.Where(o => o.License.Trim().ToLower().Contains(txtSearch)).OrderByDescending(o => o.License).ToList();
            }
            return Json(ListCarDetail, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadListCar(int id)
        {
            string strCode = "";
            List<ListCarNotExit> lstCar = GetListCar(id);
            foreach (var item in lstCar)
            {
                strCode += "<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-4\"><button type = \"button\" style=\"border-color:white; width:200px;text-align:left \" class =\"btn btn-default col-md-3 btn-primary-pay\" data-value=\"" + item.License + "\"  class =\"btn btn-default\" id=\"" + item.Id + "\"  onClick =\"ClickCar('" + item.Id + "')\" value=\"" + item.Id + "\"><i style=\"color:#f7e60a\" class=\"fa fa-car\" aria-hidden=\"true\"></i>&nbsp;" + item.License + "</button>" + "<br /></div>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCarToDriver(int driverId, int carId)
        {
            DriverCar driverCar;
            driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(x => x.DriverId == driverId);
            if (driverCar != null)
            {
                driverCar.CarId = carId;
                _unitOfWork.GetRepositoryInstance<DriverCar>().Update(driverCar);
                _unitOfWork.SaveChanges();
            }
            else
            {
                driverCar = new DriverCar();
                driverCar.DriverId = driverId;
                driverCar.CarId = carId;
                _unitOfWork.GetRepositoryInstance<DriverCar>().Add(driverCar);
                _unitOfWork.SaveChanges();
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteDriverCar(int driverId)
        {
            DriverCar driverCar;
            driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(x => x.DriverId == driverId);
            if (driverCar != null)
            {
                _unitOfWork.GetRepositoryInstance<DriverCar>().Remove(driverCar);
                _unitOfWork.SaveChanges();
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("NOTCAR", JsonRequestBehavior.AllowGet);
            }
        }
    }
    public class ListCarNotExit
    {
        public int Id { get; set; }
        public string License { get; set; }
        public Nullable<double> Payload { get; set; }
        public Nullable<double> Width_Size { get; set; }
        public Nullable<double> Height_Size { get; set; }
        public Nullable<double> Leng_Size { get; set; }
    }
}