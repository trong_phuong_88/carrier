﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER)]

    public class EOrderMissController : Controller
    {
        Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Enterprise/EOrderMiss
        public ActionResult Index(string mavandon = null)
        {
            int currentOrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            if (mavandon == null)
            {
                mavandon = "";
            }
            var Organization = new SqlParameter("@OrganizationId", System.Data.SqlDbType.Int) { Value = currentOrganizationId };
            var MaVanDon = new SqlParameter("@MaVanDon", System.Data.SqlDbType.VarChar) { Value = mavandon };
            List<SP_GetOrderTrackingHistoryMiss_Result> listCar = new List<SP_GetOrderTrackingHistoryMiss_Result>();
            var listOrdertrackingMiss = _unitOfWork.GetRepositoryInstance<SP_GetOrderTrackingHistoryMiss_Result>().GetResultBySqlProcedure("SP_GetOrderTrackingHistoryMiss @OrganizationId,@MaVanDon",  Organization, MaVanDon).ToList();
            
            return View(listOrdertrackingMiss);
        }

        public ActionResult GanDonHangChoTaiXe(int OrderId)
        {
            //code xử lý gán đơn hàng
            return View();
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}