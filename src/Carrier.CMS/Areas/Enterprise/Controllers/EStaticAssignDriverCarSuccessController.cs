﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Carrier.Models;
using Carrier.Repository;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER)]

    public class EStaticAssignDriverCarSuccessController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        [Roles(PublicConstant.ROLE_ENTERPRISE)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: OrderStatic
        public ActionResult Index(int currentFilter, int driverId, int? page, int? pagsiz)
        {
            if (driverId > 0)
            {
                page = 1;
            }
            else
            {
                driverId = currentFilter;
            }

            ViewBag.CurrentFilter = driverId;


            List<OrderViewModel> result = new List<OrderViewModel>();
            if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            {
                var CurrentUsertId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                result = (from a in db.AspNetUsers
                          join b in db.UserInfoes on a.Id equals b.ApplicationUserID
                          join o in db.Order on a.UserName equals o.UserName
                          where o.Status == PublicConstant.ORDER_FINISHED
                          && b.OrganzationId == CurrentUsertId
                          select new OrderViewModel
                          {
                              Id = o.Id,
                              MaVanDon = o.MaVanDon,
                              TenHang = o.TenHang,
                              TrongLuong = o.TrongLuong,
                              DiemDiChiTiet = o.DiemDiChiTiet,
                              DiemDenChiTiet = o.DiemDenChiTiet,
                              ThoiGianDi = o.ThoiGianDi,
                              ThoiGianDen = o.ThoiGianDen,
                              Gia = o.Gia,
                              Status = o.Status,
                              UserName = a.UserName,
                              OrganizationId = b.OrganzationId,
                          }).ToList();
            }
            else
            {
                result = (from a in db.AspNetUsers
                          join b in db.UserInfoes on a.Id equals b.ApplicationUserID
                          join o in db.Order on a.UserName equals o.UserName
                          where o.Status == PublicConstant.ORDER_FINISHED
                          && a.UserName == User.Identity.Name
                          select new OrderViewModel
                          {
                              Id = o.Id,
                              MaVanDon = o.MaVanDon,
                              TenHang = o.TenHang,
                              TrongLuong = o.TrongLuong,
                              DiemDiChiTiet = o.DiemDiChiTiet,
                              DiemDenChiTiet = o.DiemDenChiTiet,
                              ThoiGianDi = o.ThoiGianDi,
                              ThoiGianDen = o.ThoiGianDen,
                              Gia = o.Gia,
                              UserName = a.UserName,
                              OrganizationId = b.OrganzationId,
                              Status = o.Status
                          }).ToList();
            }

            if (driverId > 0)
            {
                int Id = driverId;
                result = result.Where(o => o.Status == driverId).ToList();
            }

            if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            {
                result = result.OrderByDescending(o => o.OrganizationId).ToList();
            }
            else if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE))
            {
                result = result.OrderByDescending(o => o.UserName).ToList();
            }
            else
            {
                result = result.OrderByDescending(o => o.Id).ToList();
            }

            int pageSize = int.Parse((pagsiz ?? 5).ToString());
            ViewBag.PageSize = pageSize;
            int pageNumber = int.Parse((page ?? 1).ToString());
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}