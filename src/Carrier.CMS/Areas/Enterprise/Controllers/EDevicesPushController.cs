﻿using Carrier.CMS;
using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.PushNotification;
using Carrier.Repository;
using Carrier.Utilities;
using CMS.Carrier.Areas.Admin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER)]
    public class EDevicesPushController : Controller
    { 
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        Carrier3Entities db = new Carrier3Entities();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Enterprise/DevicesPush
        [Authorize]
        public ActionResult Index()
        {
            List<DriverViewModel> resultView = new List<DriverViewModel>();
            List<Driver> result;
            var organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.GetUserName());
            result = _unitOfWork.GetRepositoryInstance<Driver>().GetAllRecords().Where(o => o.OrganizationId == organizationId).OrderByDescending(o => o.Name).ToList();

            foreach (var item in result)
            {
                var viewModel = new DriverViewModel();
                var user = UserManager.FindById(item.UserId);
                viewModel.Id = item.Id;
                viewModel.Name = item.Name;
                viewModel.BirthDay = item.BirthDay;
                viewModel.OrganizationId = item.OrganizationId;
                viewModel.Phone = item.Phone;
                viewModel.Sex = item.Sex;
                viewModel.Status = item.Status;
                viewModel.UserId = item.UserId;
                viewModel.Updated_At = item.Updated_At;
                if (user != null)
                {
                    viewModel.User_Name = user.UserName;
                    viewModel.LockoutEnabled = user.LockoutEnabled;
                }

                resultView.Add(viewModel);
            }

            return View(resultView);
        }

        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
            var devicePushList = (from user in db.AspNetUsers
                                  join dv in db.Driver on user.Id equals dv.UserId
                                  where user.UserName.StartsWith(prefix)
                                  select new
                                  {
                                      label = user.UserName,
                                      val = user.Id
                                  }).ToList();

            return Json(devicePushList);
        }

        public async void Push(DevicesPush item, string message)
        {
            //PushCompletedCallBack callback = PushNotificationCallBack;
            string errorMessage = string.Empty;
            var jsonMessage = string.Empty;
            if (item.Platform.ToLower().Equals("ios"))
            {
                jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"" + message + "\",\"sound\":\"default\"}}";
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { item.DeviceToken }, null, null, null);
            }
            else
            {
                jsonMessage = "{\"message\":\"" +message +"\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { item.DeviceToken }, null, null, null);
            }
            Messages suMessages = new Messages();
            suMessages.Message = message;
            suMessages.ToUserId = item.UserId;
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            string fromUserId = user.Id;
            suMessages.FromUserId = fromUserId;
            suMessages.Created_At = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            _unitOfWork.GetRepositoryInstance<Messages>().Add(suMessages);
            _unitOfWork.SaveChanges();
        }
        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            if (result.Equals("Success"))
            {
             
            }
            else
            {
                // Show message 
            }
        }

        public JsonResult PustMessage(string strUserId, string message)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"" + message + "\",\"sound\":\"default\"}}";
            try
            {
                string[] listUserId = strUserId.TrimEnd(',').Split(',').ToArray();
                DevicesPush device;
                bool check = false;
                foreach (var userId in listUserId)
                {
                    device = db.DevicesPush.Where(x => x.UserId.Equals(strUserId)).FirstOrDefault();
                    if (device != null)
                    {
                        if (device.Platform.ToLower().Equals("ios"))
                        {
                            PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                            PushServices.SetupPushAPN(true);
                            PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken },null,null,null);
                        }
                        else
                        {
                            jsonMessage = "{\"message\":\"" + message + "\"}";
                            PushServices.GcmKey = PublicConstant.GCM_KEY;
                            PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                            PushServices.SetupPushGCM();
                            PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, null, null, callback);
                        }
                        //Insert Messages
                        Messages suMessages = new Messages();
                        suMessages.Message = message;
                        suMessages.ToUserId = userId;
                        var user = UserManager.FindByName(User.Identity.Name);
                        string fromUserId = user.Id;
                        suMessages.FromUserId = fromUserId;
                        suMessages.Created_At = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                        _unitOfWork.GetRepositoryInstance<Messages>().Add(suMessages);
                        _unitOfWork.SaveChanges();

                        check = true;
                    }
                    
                }
                if (check)
                {
                    return Json("ok");
                }
                else
                {
                    return Json("error");
                }

            }
            catch (Exception ex)
            {
                return Json("Eror:" + ex.Message);
            }
            
        }

        public void PushNotificationCallBack(string result)
        {
            Console.WriteLine(result);
        }

        // GET: EDevicesPush/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EDevicesPush/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<ActionResult> Create(FormCollection cl)
        {
            string name = cl["userName"] != null ? cl["userName"].ToString() : string.Empty;
            string mess = cl["content"] != null ? cl["content"].ToString() : string.Empty;
            var user = await UserManager.FindByNameAsync(name + ":" + User.Identity.Name);
            if(user != null)
            {
                string userId = user.Id;
                DevicesPush devicePush = _unitOfWork.GetRepositoryInstance<DevicesPush>().GetFirstOrDefaultByParameter(o => o.UserId.Equals(userId));
                Push(devicePush, mess);
                TempData["Message"] = "Gửi thông báo thành công!";
            }
            else
            {
                TempData["Message"] = "Gửi thông báo thất bại. Vui lòng thử lại!";
            }
            return View();
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

    }
}