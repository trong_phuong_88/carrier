﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.CMS.Models;
using PagedList;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Carrier.CMS.Common;
using Microsoft.AspNet.Identity;
using Carrier.Repository;
using System.Configuration;
using CMS.Carrier.Common;
using Carrier.CMS.Hubs;
using System.Data.SqlClient;
using CMS.Carrier.Areas.Enterprise.Models;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]
    public class EKetThucController : Controller
    {
        // GetMapSchedule
        // type = 1 with order
        // type = 0 with schedule
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: KhopLenh
        [Authorize]
        public ActionResult Index(string OrderType, string txtFromDate, string txtToDate)
        {
            SQLDependencyInit();
            return View();
            //List<HistoryModel> listData = new List<HistoryModel>();
            //var userId = User.Identity.GetUserId();
            //int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            //var listdriver = (from tracking in db.OrderTracking
            //                  join user in db.UserInfoes on tracking.DriverId equals user.ApplicationUserID
            //                  where (user.OrganzationId == organizationId)
            //                  select tracking.DriverId
            //            ).ToList();
            //listData = (from tracking in db.OrderTracking
            //            join order in db.Order on new { Id = tracking.OrderId.Value } equals new { order.Id }
            //            join driver in db.Driver on tracking.DriverId equals driver.UserId
            //            join drCar in db.DriverCar on driver.Id equals drCar.DriverId
            //            join car in db.Car on drCar.CarId equals car.Id
            //            where (tracking.Status == PublicConstant.ORDER_FINISHED)
            //            && (tracking.OwnerId.Equals(userId) || listdriver.Contains(tracking.DriverId))
            //            select new HistoryModel
            //            {
            //                Id = tracking.ID,
            //                OrderId = tracking.OrderId,
            //                DriverId = tracking.DriverId,
            //                MaVanDon = order.MaVanDon,
            //                UserName = order.UserName,
            //                FullName = driver.Name,
            //                Phone = driver.Phone,
            //                ThoiGianDen = order.ThoiGianDen,
            //                ThoiGianDi = order.ThoiGianDi,
            //                Status = tracking.Status.ToString(),
            //                OwnerId = tracking.OwnerId,
            //                Payload = car.Payload,
            //                License = car.License,
            //                EvidencePath = tracking.EvidencePath,
            //                Updated_At = tracking.Updated_At
            //            }).OrderByDescending(o => o.Id).ToList();
            //if (!String.IsNullOrEmpty(OrderType))
            //{
            //    if (OrderType == "1")// Vận đơn nội bộ
            //    {
            //        listData = listData.Where(o => o.OwnerId.Equals(userId)).OrderByDescending(o => o.Status).ToList();
            //    }
            //    else // vận đơn từ ngoài
            //    {
            //        listData = listData.Where(o => !listdriver.Contains(o.DriverId)).OrderByDescending(o => o.Status).ToList();
            //    }
            //}

            //DateTime fromdate = DateTime.Now;
            //DateTime todate = DateTime.Now;
            //if (DateTime.TryParse(txtFromDate, out fromdate) && DateTime.TryParse(txtToDate, out todate))
            //{
            //    listData = listData.Where(o => (o.ThoiGianDi > fromdate && o.ThoiGianDen < todate) || (o.ThoiGianDi < fromdate && o.ThoiGianDen < todate) || (o.ThoiGianDi > fromdate && o.ThoiGianDen > todate)).ToList();
            //}
            //if (DateTime.TryParse(txtFromDate, out fromdate) && !DateTime.TryParse(txtToDate, out todate))
            //{
            //    listData = listData.Where(o => o.ThoiGianDen > fromdate).ToList();
            //}
            //if (!DateTime.TryParse(txtFromDate, out fromdate) && DateTime.TryParse(txtToDate, out todate))
            //{
            //    listData = listData.Where(o => o.ThoiGianDi < todate).ToList();
            //}
            //return View(listData);
        }
        public ActionResult ListOrderTracking()
        {
            var userId = User.Identity.GetUserId();
            var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlUFromDate = new SqlParameter("@UFromDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
            var sqlUToDate = new SqlParameter("@UToDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
            var sqlTFromDate = new SqlParameter("@TFromDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MinValue };
            var sqlTToDate = new SqlParameter("@TToDate", SqlDbType.DateTime) { Value = System.Data.SqlTypes.SqlDateTime.MaxValue };
            var sqlStatus = new SqlParameter("@Status", SqlDbType.Int) { Value = 5 };
            var listData = _unitOfWork.GetRepositoryInstance<SP_VanDon_DaKhopLenh_Result>().GetResultBySqlProcedure("SP_VanDon_DaKhopLenh @UserId,@UFromDate,@UToDate,@TFromDate,@TToDate,@Status", sqlUserId, sqlUFromDate, sqlUToDate, sqlTFromDate, sqlTToDate, sqlStatus);

            List<EKetThucViewModel> resultView = new List<EKetThucViewModel>();
            
            foreach(var item in listData)
            {
                var viewModel = new EKetThucViewModel();

                viewModel.DriverId = item.DriverId;
                viewModel.DriverName = item.DriverName;
                viewModel.DriverPhone = item.DriverPhone;
                viewModel.EvidencePath = item.EvidencePath;
                viewModel.Id = item.Id;
                viewModel.License = item.License;
                viewModel.MaVanDon = item.MaVanDon;
                viewModel.OrderId = item.OrderId;
                viewModel.Payload = item.Payload;
                viewModel.Status = item.Status;
                viewModel.rn = item.rn;
                viewModel.StatusOrder = item.StatusOrder;
                viewModel.ThoiGianDen = item.ThoiGianDen;
                viewModel.ThoiGianDi = item.ThoiGianDi;
                viewModel.Updated_At = item.Updated_At;
                var odId = item.OrderId;
                var lstImage = _unitOfWork.GetRepositoryInstance<OrderMedias>().GetListByParameter(x => x.OrderId == odId && x.MediaType == 0).ToList();
                var urlImage = String.Empty;
                var lstUrl = new List<string>();
                foreach (var i in lstImage)
                {
                    urlImage = i.FilePath;
                    lstUrl.Add(urlImage);
                }
                viewModel.ListImage = lstUrl;
                resultView.Add(viewModel);
            }

            return PartialView("_ListViewOrderKetThuc", resultView);
        }
        public void SQLDependencyInit()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;

            string listenQuery = @"SELECT [OrderId] ,[Updated_At],[Status],[OwnerId] ,[DriverId] ,[EvidencePath] FROM [dbo].[OrderTracking]";

            // Create instance of the DB Listener
            DatabaseChangeListener changeListener = new DatabaseChangeListener(connectionString);

            // Define what to do when changes were detected
            changeListener.OnChange += () =>
            {
                //ChatHub.SendMessages();
                NotificationHub.UpdateMatchOrder();
                // Reattach listener event - DO NOT TOUCH!
                changeListener.Start(listenQuery);
            };

            // Start listening for changes 
            changeListener.Start(listenQuery);

        }

        public ActionResult OrderDetail(long Id)
        {
            Order order = db.Order.Where(o => o.Id == Id).Single();
            OrderTracking ordertracking = db.OrderTracking.Where(o => o.OrderId == order.Id).OrderByDescending(o => o.ID).Take(1).SingleOrDefault();
            if (ordertracking != null)
            {
                ordertracking.IsView = true;
                db.SaveChanges();
            }
            if (order != null)
            {
                var orderid = order.Id;
                var lstPlace = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
                ViewBag.ListPlace = lstPlace;
            }
            return PartialView("_OrderDetail", order);
        }
        public ActionResult UserInfoDetail(string DriverId)
        {
            Driver userInfo = db.Driver.Where(o => o.UserId == DriverId).Single();
            return PartialView("_UserInfo", userInfo);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}