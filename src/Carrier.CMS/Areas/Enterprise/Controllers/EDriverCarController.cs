﻿using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE)]
    public class EDriverCarController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/DriverCar
        public ActionResult Index()
        {
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);

            if (Request.QueryString["ddlDriver"] != null)
            {
                List<Driver> listDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(o => o.OrganizationId == organizationId).OrderBy(o => o.Name).ToList();
                SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
                ViewBag.ListDriver = listDriverReturn;

                int DriverId = int.Parse(Request.QueryString["ddlDriver"]);
                if (DriverId > 0)
                {
                    ViewBag.currentselecedId = DriverId;
                    var sqlDriverId = new SqlParameter("@DriverId", System.Data.SqlDbType.Int) { Value = DriverId };
                    List<SP_GetAllCarByDriverId_Result> listCar = new List<SP_GetAllCarByDriverId_Result>();
                    listCar = _unitOfWork.GetRepositoryInstance<SP_GetAllCarByDriverId_Result>().GetResultBySqlProcedure("SP_GetAllCarByDriverId @DriverId", sqlDriverId).ToList();

                    return View(listCar);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                List<Driver> listDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(o => o.OrganizationId == organizationId).OrderBy(o => o.Name).ToList();
                SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
                ViewBag.ListDriver = listDriverReturn;
                if (listDriver.Count ==1)
                {
                    int driverId = listDriver[0].Id;
                    ViewBag.currentselecedId = driverId;
                    var sqlDriverId = new SqlParameter("@DriverId", System.Data.SqlDbType.Int) { Value = driverId };
                    List<SP_GetAllCarByDriverId_Result> listCar = new List<SP_GetAllCarByDriverId_Result>();
                    listCar = _unitOfWork.GetRepositoryInstance<SP_GetAllCarByDriverId_Result>().GetResultBySqlProcedure("SP_GetAllCarByDriverId @DriverId", sqlDriverId).ToList();

                    return View(listCar);
                }
                else
                {
                    return View();
                }
            }
        }

        //public ActionResult LoadCarByDriver(int DriverId = 0)
        //{
        //    int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
        //    List<Driver> listDriver = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(o => o.OrganizationId == organizationId).OrderBy(o => o.Name).ToList();
        //    SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
        //    ViewBag.ListDriver = listDriverReturn;

        //    if (DriverId > 0)
        //    {
        //        var sqlDriverId = new SqlParameter("@DriverId", System.Data.SqlDbType.Int) { Value = DriverId };
        //        List<SP_GetAllCarByDriverId_Result> listCar = new List<SP_GetAllCarByDriverId_Result>();
        //        listCar = _unitOfWork.GetRepositoryInstance<SP_GetAllCarByDriverId_Result>().GetResultBySqlProcedure("SP_GetAllCarByDriverId @DriverId", sqlDriverId).ToList();

        //        //return View(listCar);
        //        return View("Enterprise/EDriverCar/Index", listCar);
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        [HttpPost]
        public JsonResult UpdateDriverCar(string listId, int driverId)
        {
            DriverCar driverCar = new DriverCar();
            // xóa tất cả các bản ghi cũ đã gán cho tài xế đó -> update lại
            if (_unitOfWork.GetRepositoryInstance<DriverCar>().GetListByParameter(o => o.DriverId == driverId).Count() > 0)
            {
                _unitOfWork.GetRepositoryInstance<DriverCar>().RemoveByWhereClause(o => o.DriverId == driverId);
                _unitOfWork.SaveChanges();
            }

            string[] liststrId = listId.TrimEnd(',').Split(',').ToArray();
            bool isExits = false;
            foreach (string id in liststrId)
            {
                if (id.Length > 0)
                {
                    try
                    {
                        int carId = int.Parse(id);
                        driverCar = _unitOfWork.GetRepositoryInstance<DriverCar>().GetFirstOrDefaultByParameter(o => o.CarId == carId && o.DriverId != driverId);
                        if (driverCar == null)
                        {
                            driverCar = new DriverCar();
                            driverCar.CarId = carId;
                            driverCar.DriverId = driverId;
                            _unitOfWork.GetRepositoryInstance<DriverCar>().Add(driverCar);
                            //_unitOfWork.SaveChanges();
                        }
                        else
                        {
                            isExits = true;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json("Error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            if (isExits == true)
            {
                Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.Id != driverCar.DriverId);
                Car car = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(o => o.Id != driverCar.CarId);
                if (driver != null && car != null)
                {
                    TempData["error"] = "xe " + car.License + " đã được gán cho tài xế " + driver.Name + " vui lòng chọn xe khác!";
                }
                else
                {
                    TempData["error"] = "gán xe thất bại 1 trong só xe đã được gán cho tài xế khác vui lòng chọn xe khác!";
                }

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            _unitOfWork.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}
