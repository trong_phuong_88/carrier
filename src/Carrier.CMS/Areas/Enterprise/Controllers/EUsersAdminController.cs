﻿using Carrier.CMS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections.Generic;
using Carrier.Utilities;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
using Carrier.Repository;
using Carrier.CMS;
using GoogleMaps.LocationServices;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE)]
    public class EUsersAdminController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public EUsersAdminController()
        {
        }

        public string _UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public EUsersAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /Users/
        //public async Task<ActionResult> Index()
        //{
        //    return View(await UserManager.Users.Include(u =>u.Roles).ToListAsync());
        //}

        public async Task<ActionResult> Index()
        {
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            var currentUserLogin = UserManager.Users.Where(x => x.Id.Equals(_UserId)).FirstOrDefault();
            var lstUser = await UserManager.Users.Include(u => u.Roles).ToListAsync();
            lstUser = lstUser.Where(u => u.UsersInfo.OrganzationId == currentUserLogin.UsersInfo.Id).ToList();
            var lstUserViewModel = new List<UserViewModel>();
            foreach (ApplicationUser item in lstUser)
            {
                var rolesDrives = await UserManager.GetRolesAsync(item.Id);
                if (rolesDrives.Contains(PublicConstant.ROLE_DRIVERS))
                {
                    if (item.UsersInfo != null)
                    {
                        var newItem = new UserViewModel();
                        newItem.Address = item.UsersInfo.Address;
                        newItem.FullName = item.UsersInfo.FullName;
                        newItem.MobilePhone = item.UsersInfo.MobilePhone;
                        newItem.UserName = item.UserName;
                        newItem.Id = item.UsersInfo.Id;
                        newItem.LastLogin = item.UsersInfo.LastLogin;
                        var roles = new List<string>();
                        foreach (var role in rolesDrives)
                        {
                            role.ToString();
                            roles.Add(role.ToString());
                        }
                        newItem.Email = item.Email;
                        newItem.Roles = roles;
                        newItem.Status = item.UsersInfo.Status;
                        newItem.Created_At = item.UsersInfo.Created_At;
                        lstUserViewModel.Add(newItem);
                    }
                }
            }
            return View(lstUserViewModel);
        }

        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);

            return View(user);
        }

        //
        // GET: /Users/Create
        public async Task<ActionResult> Create()
        {
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(UserViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = userViewModel.UserName, Email = userViewModel.Email };

                UserInfo userInfo = new UserInfo();
                userInfo.Address = userViewModel.Address;
                userInfo.MobilePhone = userViewModel.MobilePhone;
                userInfo.FullName = userViewModel.FullName;
                userInfo.ApplicationUserID = user.Id;
                userInfo.Sex = false;
                userInfo.Status = PublicConstant.STATUS_ACTIVE;
                userInfo.Updated_At = System.DateTime.Now;
                userInfo.LastLogin = System.DateTime.Now;
                userInfo.Created_At = System.DateTime.Now;

                string message = "";
                if (userViewModel.Address.Length > 0)
                {
                    string latLongFrom = GetLatLongByAddress(ref message, userViewModel.Address);
                    if (message.Length == 0)
                    {
                        userInfo.Lat = double.Parse(latLongFrom.Split(':')[0]);
                        userInfo.Lng = double.Parse(latLongFrom.Split(':')[1]);
                    }
                }

                user.UsersInfo = userInfo;

                var result = await UserManager.CreateAsync(user, userViewModel.PassWord);
                if (result.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                    }
                    else
                    {
                        result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
                    }
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "EUsersAdmin");
                    }
                }
            }
            return View();
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (System.Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }

        //
        // GET: /Users/Edit/1
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            UserInfoes UserAdmin = db.UserInfoes.Find(int.Parse(id));
            var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await UserManager.GetRolesAsync(user.Id);
            return View(new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })
            });
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Email,Id")] EditUserViewModel editUser, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                user.UserName = editUser.Email;
                user.Email = editUser.Email;

                var userRoles = await UserManager.GetRolesAsync(user.Id);

                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Something failed.");
            return View();
        }

        //
        // GET: /Users/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var user = await UserManager.FindByIdAsync(id);
            UserInfoes UserAdmin = db.UserInfoes.Find(int.Parse(id));
            var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //var user = await UserManager.FindByIdAsync(id);
                UserInfoes UserAdmin = db.UserInfoes.Find(int.Parse(id));
                var user = await UserManager.FindByIdAsync(UserAdmin.ApplicationUserID);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                else
                {
                    db.UserInfoes.Remove(UserAdmin);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


        [HttpPost]
        public JsonResult SetPassword(string id, string newpass)
        {
            var token = UserManager.GeneratePasswordResetToken(id);
            UserManager.ResetPassword(id, token, newpass);

            return Json(new { success = true });
        }

        [HttpPost]
        public JsonResult lockUser(string id)
        {

            MembershipUser u = Membership.GetUser(id);
            if (u.IsLockedOut)
            {
                u.UnlockUser();
                return Json(new { success = false });
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    Membership.ValidateUser(id, "wellthisistheverywrongpassword");
                }

            }
            return Json(new { success = true });
        }

        [HttpPost]
        public async Task<JsonResult> ChangeLockout(int id)
        {
            UserInfoes userInfo;
            userInfo = db.UserInfoes.Find(id);
            var user = await UserManager.FindByIdAsync(userInfo.ApplicationUserID);
            if (user == null)
            {
                return Json(new { success = false });
            }
            else
            {
                if (user.LockoutEnabled == true)
                {
                    user.LockoutEnabled = false;
                }
                else
                {
                    user.LockoutEnabled = true;
                }
                UserManager.Update(user);
                return Json(new { success = true });
            }
        }
    }
}
