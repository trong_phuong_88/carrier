﻿using Carrier.CMS;
using Carrier.CMS.Common;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]

    public class EBaoCaoTongHopController : Controller
    {
        public string _UserId
        {
            get { return User.Identity.GetUserId(); }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private Carrier3Entities db = new Carrier3Entities();
        // GET: Enterprise/EBaoCaoTongHop
        [Authorize]
        public async Task<ActionResult> Index()
        {
            List<SP_Report_TotalView_By_UserId_Result> totalView = new List<SP_Report_TotalView_By_UserId_Result>();
            List<SP_DashBoard_Orders_By_UserId_Result> totalDashBoard = new List<SP_DashBoard_Orders_By_UserId_Result>();
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            var userId = user.Id;
            if (User.IsInRole(PublicConstant.ROLE_ENTERPRISE) || User.IsInRole(PublicConstant.ROLE_USERS))
            {                
                var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
                var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = "2016-09-11 00:27:54.787" };
                var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Now };
                totalView = _unitOfWork.GetRepositoryInstance<SP_Report_TotalView_By_UserId_Result>().GetResultBySqlProcedure("SP_Report_TotalView_By_UserId @UserId,@FromDate, @ToDate", sqlUser, sqlFromDate, sqlToDate).ToList();

                var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
                var sqlFrDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = "2016-09-10 00:27:54.787" };
                var sqlTDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Now };
                totalDashBoard = _unitOfWork.GetRepositoryInstance<SP_DashBoard_Orders_By_UserId_Result>().GetResultBySqlProcedure("SP_DashBoard_Orders_By_UserId @UserId,@FromDate, @ToDate", sqlUserId, sqlFrDate, sqlTDate).ToList();

                foreach (var i in totalDashBoard)
                {
                    Session["TotalOrderSuccess"] = i.TotalOrderSuccess;
                    Session["TotalOrderRegisted"] = i.TotalOrderRegisted;
                    Session["TotalOrderSuccessOut"] = i.TotalOrderSuccessOut;
                    Session["TotalOrderSuccessReceived"] = i.TotalOrderSuccessReceived;
                    Session["TotalOrderPending"] = i.TotalOrderPending;
                }
                return View(totalView);
            }
            
            if (User.IsInRole(PublicConstant.ROLE_FORWARDER))
            {
                var date = DateTime.Now;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                var sqlUserId1 = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
                var sqlFrDate1 = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = "2016-09-10 00:27:54.787" };
                var sqlTDate1 = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Now };
                var totalDashBoard1 = _unitOfWork.GetRepositoryInstance<SP_Forwader_DashBoard_Orders_By_UserId_Result>().GetResultBySqlProcedure("SP_Forwader_DashBoard_Orders_By_UserId @UserId,@FromDate, @ToDate", sqlUserId1, sqlFrDate1, sqlTDate1).ToList();
                
                foreach (var i in totalDashBoard1)
                {
                    Session["TotalOrderSuccess"] = i.ForwaderTotalOrderSuccess;
                    Session["TotalOrderRegisted"] = i.ForwaderTotalOrderRegisted;
                    Session["TotalOrderSuccessReceived"] = i.ForwaderTotalOrderAccepted;
                    Session["TotalOrderPending"] = i.ForwaderTotalOrderPending;
                }
                return View("IndexForwader", totalDashBoard1);
            }
            return View();
        }
        public async Task<JsonResult> GetDashBoardOrdersByUserId()
        {
            List<SP_DashBoard_Orders_By_UserId_Result> totalView = new List<SP_DashBoard_Orders_By_UserId_Result>();
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            var userId = user.Id;
            var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = "2016-09-11 00:27:54.787" };
            var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Now };
            totalView = _unitOfWork.GetRepositoryInstance<SP_DashBoard_Orders_By_UserId_Result>().GetResultBySqlProcedure("SP_DashBoard_Orders_By_UserId @UserId,@FromDate, @ToDate", sqlUser, sqlFromDate, sqlToDate).ToList();
            return Json(totalView, JsonRequestBehavior.AllowGet);
        }
        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
        public async Task<ActionResult> BaoCaoDoanhThu()
        {
            List<SP_Report_Profit_For_Drivers_Result> totalView = new List<SP_Report_Profit_For_Drivers_Result>();
            int organizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
            List<Driver> listDriver = new List<Driver>();
            listDriver = db.Driver.Where(m => m.OrganizationId == organizationId).ToList();
            IEnumerable<SelectListItem> listDriverSelect = listDriver.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
            if (listDriverSelect.Count() > 0)
            {
                ViewBag.ListDriver = listDriverSelect;
            }
            else
            {
                ViewBag.ListDriver = null;
            }
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            var userId = user.Id;
            var sqlUser = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
            var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = DBNull.Value };
            var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DBNull.Value };
            totalView = _unitOfWork.GetRepositoryInstance<SP_Report_Profit_For_Drivers_Result>().GetResultBySqlProcedure("SP_Report_Profit_For_Drivers @UserId,@FromDate, @ToDate", sqlUser, sqlFromDate, sqlToDate).ToList();
            return View(totalView);
        }
        public JsonResult ProfitByUserId(int id)
        {
            Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.Id == id);
            if (driver != null)
            {
                var userId = driver.UserId;
                List<SP_Report_Profit_By_UserId_Result> profitUser = new List<SP_Report_Profit_By_UserId_Result>();
                var sqlUserId = new SqlParameter("@UserId", System.Data.SqlDbType.NVarChar) { Value = userId };
                var sqlFromDate = new SqlParameter("@FromDate", System.Data.SqlDbType.DateTime) { Value = "2017-01-01 00:00:00" };
                var sqlToDate = new SqlParameter("@ToDate", System.Data.SqlDbType.DateTime) { Value = DateTime.Now };
                profitUser = _unitOfWork.GetRepositoryInstance<SP_Report_Profit_By_UserId_Result>().GetResultBySqlProcedure("SP_Report_Profit_By_UserId  @UserId, @FromDate, @ToDate", sqlUserId, sqlFromDate, sqlToDate).ToList();
                //foreach (var i in profitUser)
                //{
                //    Session["TotalMoneySuccess"] = i.TotalMoneySuccess != null ? i.TotalMoneySuccess : 0;
                //    Session["TotalMoneyOrder"] = i.TotalMoneyOrder != null ? i.TotalMoneyOrder : 0;
                //    Session["TotalMoneyIn"] = i.TotalMoneyIn != null ? i.TotalMoneyIn : 0;
                //    Session["TotalMoneyOut"] = i.TotalMoneyOut != null ? i.TotalMoneyOut : 0;
                //    Session["TotalMoneyOrderPending"] = i.TotalMoneyOrderPending != null ? i.TotalMoneyOrderPending : 0;
                //}
                return Json(profitUser, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }
    }
}