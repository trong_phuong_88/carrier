﻿using Carrier.CMS.Common;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER)]

    public class ERevenueReportByTimeController : Controller
    {
        // GET: Enterprise/ERevenueByTime
        public ActionResult Index()
        {
            return View();
        }
    }
}