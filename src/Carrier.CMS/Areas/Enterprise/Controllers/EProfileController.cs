﻿using Carrier.CMS;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using GoogleMaps.LocationServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    public class EProfileController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public UserInfoes UserInfo
        {
            get { return _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId)); }
        }

        //[ValidateAntiForgeryToken]
        public ActionResult CapNhatProfile()
        {
            Organization organization = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.UserId.Equals(UserId));
            if (organization != null)
            {
                return View(organization);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CapNhatProfile(FormCollection form)
        {
            //int id = form.Get("Id") != null ? int.Parse(form.Get("Id")) : 0;
            Organization organization = _unitOfWork.GetRepositoryInstance<Organization>().GetFirstOrDefaultByParameter(o => o.UserId.Equals(UserId));
            UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(x => x.ApplicationUserID.Equals(UserId));
            if (organization != null)
            {
                organization.Name = form.Get("Name") != null ? form.Get("Name") : "";
                organization.Address = form.Get("Address") != null ? form.Get("Address") : "";
                organization.WebSite = form.Get("WebSite") != null ? form.Get("WebSite") : "";
                organization.Updated_At = DateTime.Now.ToString("dd/MM/yyyy");
                organization.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";

                _unitOfWork.GetRepositoryInstance<Organization>().Update(organization);
                _unitOfWork.SaveChanges();
                if (userInfo != null)
                {
                    var address = form.Get("Address") != null ? form.Get("Address") : "";
                    string message = "";
                    if (address.Length > 0)
                    {
                        string latLongFrom = GetLatLongByAddress(ref message, address);
                        if (message.Length == 0)
                        {
                            userInfo.Lat = double.Parse(latLongFrom.Split(':')[0]);
                            userInfo.Lng = double.Parse(latLongFrom.Split(':')[1]);
                            _unitOfWork.GetRepositoryInstance<UserInfoes>().Update(userInfo);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
                return RedirectToAction("Index", "HomeEnterprise");
            }
            else
            {
                organization = new Organization();
                organization.Name = form.Get("Name") != null ? form.Get("Name") : "";
                organization.Address = form.Get("Address") != null ? form.Get("Address") : "";
                organization.WebSite = form.Get("WebSite") != null ? form.Get("WebSite") : "";
                organization.Updated_At = DateTime.Now.ToString("dd/MM/yyyy");
                organization.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                organization.UserId = UserId;

                _unitOfWork.GetRepositoryInstance<Organization>().Add(organization);
                _unitOfWork.SaveChanges();
                return RedirectToAction("Index", "HomeEnterprise");
            }
            //return View(organization);
        }
        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (System.Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }
    }
}