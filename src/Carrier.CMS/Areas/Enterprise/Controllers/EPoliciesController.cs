﻿using Carrier.Models.Entities;
using Carrier.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    public class EPoliciesController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Enterprise/EPolicies
        public ActionResult Index()
        {
            Articles art = _unitOfWork.GetRepositoryInstance<Articles>().GetFirstOrDefaultByParameter(x => x.Id == 40);
            if (art != null)
            {
                return View(art);
            }
            else
            {
                return View();
            }
        }
    }
}