﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Enterprise/Error
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Maintenance()
        {
            return View();
        }
    }
}