﻿using Carrier.CMS.Common;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;
using Carrier.CMS;
using CMS.Carrier.Common;
using CMS.Carrier.Models;
using System.Data;
using System.Data.SqlClient;
using CMS.Carrier.Areas.Enterprise.Models;
using GoogleMaps.LocationServices;
using System.Data.Entity.Spatial;
using System.Xml;

namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    [Roles(PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER)]

    public class EMessageController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public EMessageController()
        {

        }

        public string _UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        public EMessageController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Enterprise/EMessage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListMessageOrder()
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            var CurrentUsertId = GetCurrentOrganizatinId(User.Identity.GetUserId());
            DateTime dateCompare = DateTime.Now.AddDays(-1);
            result = (from o in db.Order
                      join a in db.UserInfoes on o.Created_By equals a.ApplicationUserID
                      where
                      a.OrganzationId == CurrentUsertId
                      && o.CreateAt > dateCompare
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = o.UserName,
                          OrganizationId = a.OrganzationId,
                          CreateAt = o.CreateAt,
                          Created_By = o.Created_By
                      }).OrderByDescending(o => o.Id).ToList();

            return View(result);
        }
        public async Task<ActionResult> ListMessageUser()
        {
            var currentUserLogin = UserManager.Users.Where(x => x.Id.Equals(_UserId)).FirstOrDefault();
            var lstUser = await UserManager.Users.Include(u => u.Roles).ToListAsync();
            lstUser = lstUser.Where(u => u.UsersInfo.OrganzationId == currentUserLogin.UsersInfo.Id && u.UsersInfo.Created_At.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).ToList();
            var lstUserViewModel = new List<UserViewModel>();
            foreach (ApplicationUser item in lstUser)
            {
                var rolesDrives = await UserManager.GetRolesAsync(item.Id);
                if (rolesDrives.Contains(PublicConstant.ROLE_DRIVERS))
                {
                    if (item.UsersInfo != null)
                    {
                        var newItem = new UserViewModel();
                        newItem.Address = item.UsersInfo.Address;
                        newItem.FullName = item.UsersInfo.FullName;
                        newItem.MobilePhone = item.UsersInfo.MobilePhone;
                        newItem.UserName = item.UserName;
                        newItem.Id = item.UsersInfo.Id;
                        newItem.LastLogin = item.UsersInfo.LastLogin;
                        var roles = new List<string>();
                        foreach (var role in rolesDrives)
                        {
                            role.ToString();
                            roles.Add(role.ToString());
                        }
                        newItem.Email = item.Email;
                        newItem.Roles = roles;
                        newItem.Status = item.UsersInfo.Status;
                        newItem.Created_At = item.UsersInfo.Created_At;
                        lstUserViewModel.Add(newItem);
                    }
                }
            }

            return View(lstUserViewModel.OrderByDescending(o => o.Id));
        }
        public ActionResult ListMessageNotification()
        {
            var notificationView = new MessageNotificationViewModel();
            var lstNotificationRecords = new List<MessageNotificationModel>();
            var currentUser = User.Identity.GetUserId();
            List<Messages> lstMessage = _unitOfWork.GetRepositoryInstance<Messages>().GetListByParameter(o => o.ToUserId == currentUser).OrderByDescending(o => o.Id).ToList();
            MessageNotificationModel item;
            foreach (Messages message in lstMessage)
            {
                item = new MessageNotificationModel();
                item.Id = message.Id.ToString();
                item.Message = message.Message;
                item.FromUserId = message.FromUserId;
                item.ToUserId = message.ToUserId;
                item.Created_At = DateTime.Parse(message.Created_At);
                lstNotificationRecords.Add(item);
            }

            lstNotificationRecords = lstNotificationRecords.Where(o => o.ToUserId == User.Identity.GetUserId() && o.Created_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();

            notificationView.Quanlity = lstNotificationRecords.Count();
            notificationView.listMessageNotification = lstNotificationRecords.ToList();

            return View(notificationView);
        }
        public ActionResult ListMessageOrderTracking()
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            var CurrentUsertId = GetCurrentOrganizatinId(User.Identity.GetUserId());
            DateTime dateCompare = DateTime.Now.AddDays(-1);
            result = (from o in db.Order
                      join t in db.OrderTracking on o.Id equals t.OrderId
                      join a in db.UserInfoes on o.Created_By equals a.ApplicationUserID
                      where
                      t.Status == PublicConstant.ORDER_ACCEPT
                      && a.OrganzationId == CurrentUsertId
                      && o.CreateAt > dateCompare
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = o.UserName,
                          OrganizationId = a.OrganzationId,
                          CreateAt = o.CreateAt,
                          Created_By = o.Created_By
                      }).OrderByDescending(o => o.CreateAt).Distinct().ToList();

            return View(result);
        }

        public ActionResult ListMessageSOS()
        {
            List<EReportSOSViewModel> result = new List<EReportSOSViewModel>();
            DateTime dateCompare = DateTime.Now.AddDays(-1);
            result = (from r in db.ReportSOS
                      join t in db.Driver on r.DriverId equals t.UserId
                      join o in db.Order on r.OrderId equals o.Id
                      where r.UserId == User.Identity.GetUserId() orderby r.Created_At descending
                      select new EReportSOSViewModel
                      {
                          DriverId = r.DriverId,
                          DriverName = t.Name,
                          MaVanDon = o.MaVanDon,
                          AddressSOS = GetLocationFromAddress(r.Lat.ToString(), r.Lng.ToString()),
                          ReportDescription = r.Reason,
                          Created_At = r.Created_At,

                      }).Distinct().ToList();

            //foreach (EReportSOSViewModel item in result)
            //{
            //    item.AddressSOS = GetLocationFromAddress(item.Lat.ToString(), item.Lng.ToString());
            //}

            return View(result);
        }

        public string GetLocationFromAddress(string longitude, string latitude)
        {
            string fullAddress = "";
            try
            {
                string coordinate = longitude + "," + latitude;

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("https://maps.googleapis.com/maps/api/geocode/xml?latlng=" + coordinate);

                XmlNodeList xNodelst = xDoc.GetElementsByTagName("result");
                XmlNode xNode = xNodelst.Item(0);
                fullAddress = xNode.SelectSingleNode("formatted_address").InnerText;
            }
            catch (Exception)
            { }
            return fullAddress;
        }

        public ActionResult Details(long id)
        {
            Messages mes = new Messages();
            try
            {
                mes = _unitOfWork.GetRepositoryInstance<Messages>().GetFirstOrDefaultByParameter(o => o.Id == id);
                if (mes != null)
                {
                    mes.IsView = true;
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            return View(mes);
        }

        public int GetCurrentOrganizatinId(string userId)
        {
            int organizationId = 0;
            try
            {
                organizationId = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault().Id;
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}