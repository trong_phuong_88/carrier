﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Carrier.CMS;
using Carrier.Models.Entities;
using Carrier.Repository;
using CMS.Carrier.Areas.Enterprise.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
namespace CMS.Carrier.Areas.Enterprise.Controllers
{
    public class EReportSOSController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public string userId
        {
            get { return User.Identity.GetUserId(); }
        }
        // GET: Enterprise/EReportSOS
        public ActionResult Index()
        {
            var lstData = _unitOfWork.GetRepositoryInstance<ReportSOS>().GetListByParameter(x => x.UserId == userId).ToList();
            List<EReportViewModel> resultView = new List<EReportViewModel>();

            if (lstData.Count > 0)
            {
                foreach (var item in lstData)
                {
                    var viewmodel = new EReportViewModel();
                    Driver dr = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(x => x.UserId == item.DriverId);
                    Order od = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.Id == item.OrderId);
                    if (dr != null)
                    {
                        viewmodel.DriverName = dr.Name;
                    }
                    else { viewmodel.DriverName = string.Empty; }
                    List<OrderTracking> or = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetListByParameter(x => x.DriverId == item.DriverId && (x.Status == 1 || x.Status == 0)).ToList();
                    string lstorder = string.Empty;
                    for (int i = 0; i < or.Count; i++)
                    {
                        if (i == 0)
                        {
                            lstorder = or[i].OrderId.ToString();
                        }
                        else
                        {
                            lstorder = lstorder + ";" + or[i].OrderId;
                        }
                    }
                    if (od != null)
                    {
                        viewmodel.MaVanDon = od.MaVanDon;
                    }
                    else
                    {
                        viewmodel.MaVanDon = string.Empty;
                    }
                    viewmodel.DriverId = item.DriverId;
                    viewmodel.OrderId = lstorder;
                    viewmodel.Lat = item.Lat;
                    viewmodel.Lng = item.Lng;
                    viewmodel.ReportDescription = item.Reason;
                    viewmodel.Created_At = item.Created_At.Value;
                    viewmodel.ReportType = item.ReportTypeId.Value;
                    var odId = item.OrderId;
                    var lstImage = _unitOfWork.GetRepositoryInstance<OrderMedias>().GetListByParameter(x => x.OrderId == odId && x.MediaType == 1).ToList();
                    var urlImage = String.Empty;
                    var lstUrl = new List<string>();
                    foreach (var i in lstImage)
                    {
                        urlImage = i.FilePath;
                        lstUrl.Add(urlImage);
                    }
                    viewmodel.LstImage = lstUrl;
                    resultView.Add(viewmodel);
                }
            }
            return View(resultView);
        }

        // GET: Enterprise/EReportSOS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportSOS reportSOS = _unitOfWork.GetRepositoryInstance<ReportSOS>().GetFirstOrDefault(id.Value);
            if (reportSOS == null)
            {
                return HttpNotFound();
            }
            return View(reportSOS);
        }

        public ActionResult DriverDetail(string driverId)
        {
            Driver drInfo = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.UserId.Equals(driverId));
            return PartialView("_DriverDetail", drInfo);
        }

        public ActionResult OrderDetail(int Id)
        {
            //Order order = db.Order.Where(o => o.Id == Id).Single();
            Order order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.Id == Id);
            if (order != null)
            {
                var orderid = order.Id;
                var lstPlace = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == orderid).ToList();
                ViewBag.ListPlace = lstPlace;
            }
            else
            {
                return HttpNotFound();
            }
            return PartialView("_OrderDetail", order);
        }

        public string GetLocationFromAddress(string longitude, string latitude)
        {
            string fullAddress = "";
            try
            {
                string coordinate = longitude + "," + latitude;

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("https://maps.googleapis.com/maps/api/geocode/xml?latlng=" + coordinate);

                XmlNodeList xNodelst = xDoc.GetElementsByTagName("result");
                XmlNode xNode = xNodelst.Item(0);
                fullAddress = xNode.SelectSingleNode("formatted_address").InnerText;
            }
            catch (Exception)
            { }
            return fullAddress;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
