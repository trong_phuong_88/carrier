﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Areas.Enterprise.Models
{
    public class EOrderViewModel
    {
        public long Id { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string MaVanDon { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public Nullable<System.DateTime> ThoiGianDi { get; set; }
        public Nullable<System.DateTime> ThoiGianDen { get; set; }
        public Nullable<double> Gia { get; set; }
        public Nullable<int> LoaiThanhToan { get; set; }
        public Nullable<System.DateTime> CreateAt { get; set; }
        public string Created_By { get; set; }
        public Nullable<int> OrderStatus { get; set; }
        public Nullable<int> TrackingStatus { get; set; }
        public string DriverId { get; set; }
        public List<string> DiemTraHang { get; set; }
    }
}