﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Areas.Enterprise.Models
{
    public class EReportViewModel
    {
        public string DriverName { get; set; }
        public int ReportType { get; set; }
        public string DriverId { get; set; }
        public Nullable<System.DateTime> Created_At { get; set; }
        public string UserId { get; set; }
        public string OrderId { get; set; }
        public string MaVanDon { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lng { get; set; }
        public string ReportDescription { get; set; }
        public List<string> LstImage { get; set; }
    }
}