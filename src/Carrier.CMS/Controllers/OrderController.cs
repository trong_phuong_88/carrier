﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using Carrier.Models.Entities;
using Carrier.Utilities;
using GoogleMaps.LocationServices;
using Carrier.CMS.Common;
using Newtonsoft.Json.Linq;
using Carrier.PushNotification;
using static Carrier.Utilities.GlobalCommon;
using Carrier.Repository;
using Microsoft.AspNet.Identity;
using Carrier.CMS.Models;
using CMS.Carrier.Models;
using System.Data.SqlClient;
using Carrier.Algorithm.Entity;

namespace Carrier.CMS.Controllers
{
    [Compress]
    //[RequireHttps]
    [Roles(PublicConstant.ROLE_USERS)]
    public class OrderController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private LogisticDataEntities dbLogictis = new LogisticDataEntities();

        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public ActionResult Index(string currentFilter, string currentStatus, string searchString, string status, int? page, int? pagsiz)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            if (status != null)
            {
                page = 1;
            }
            else
            {
                status = currentStatus;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.CurrentStatus = status;

            List<OrderViewModel> result = new List<OrderViewModel>();
            result = (from a in db.AspNetUsers
                      join o in db.Order on a.UserName equals o.UserName
                      where o.Created_By == User.Identity.GetUserId()
                      select new OrderViewModel
                      {
                          Id = o.Id,
                          MaVanDon = o.MaVanDon,
                          TenHang = o.TenHang,
                          TrongLuong = o.TrongLuong,
                          DiemDiChiTiet = o.DiemDiChiTiet,
                          DiemDenChiTiet = o.DiemDenChiTiet,
                          ThoiGianDi = o.ThoiGianDi,
                          ThoiGianDen = o.ThoiGianDen,
                          Gia = o.Gia,
                          Status = o.Status,
                          UserName = a.UserName,
                          OrganizationId = a.UsersInfo_Id,
                          CreateAt = o.CreateAt
                      }).ToList();

            if (!String.IsNullOrEmpty(searchString))
            {
                result = result.Where(o => o.TenHang.Contains(searchString) || o.DiemDiChiTiet.Contains(searchString) || o.DiemDenChiTiet.Contains(searchString)).OrderByDescending(o => o.Status).ToList();
            }

            if (!String.IsNullOrEmpty(status))
            {
                int statuOrder = int.Parse(status);
                result = result.Where(o => o.Status == statuOrder).ToList();
            }

            result = result.OrderByDescending(o => o.CreateAt).ToList();

            int pageSize = (pagsiz ?? 5);
            ViewBag.PageSize = pageSize;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: Orders/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            OrderTracking track = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetListByParameter(o => o.OrderId == id).OrderByDescending(o => o.Created_At).Take(1).SingleOrDefault();
            if (track != null)
            {
                Driver driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(o => o.UserId == track.DriverId);
                if (driver != null)
                {
                    ViewBag.Driver = driver;
                }
            }

            var listCar = (from a in db.OrderCar
                           join c in db.CarType on a.CarTypeId equals c.Id
                           where a.OrderId == order.Id
                           select new OrderCarViewModel
                           {
                               Id = a.Id,
                               CarTypeId = c.Id,
                               CarTypeName = c.Name,
                               Number = a.Number,
                               OrderId = a.OrderId,
                               Value = c.Value
                           }).ToList();
            ViewBag.ListOrderCar = listCar;

            return View(order);
        }

        public ActionResult GetHuyenByTinhId(string tinhid)
        {
            List<District> objhuyen = new List<District>();
            objhuyen = dbLogictis.District.Where(x => x.CityCode == tinhid).ToList();
            SelectList obghuyen = new SelectList(objhuyen, "DistrictCode", "Name", 0);
            return Json(obghuyen);
        }

        public ActionResult GetXaByHuyenId(string huyenid)
        {
            List<Ward> objxa = new List<Ward>();
            objxa = dbLogictis.Ward.Where(x => x.DistrictCode == huyenid).ToList();
            SelectList obgxa = new SelectList(objxa, "WardCode", "Name", 0);
            return Json(obgxa);
        }

        public ActionResult Create()
        {
            //List<DraftOrder> listDraftOrder = db.DraftOrder.Where(o => o.Name == User.Identity.Name).ToList();
            List<DraftOrder> listDraftOrder = db.DraftOrder.ToList();
            ViewBag.ListDraftOrder = new SelectList(listDraftOrder, "Id", "TenHang", 0);

            string appId = User.Identity.GetUserId();
            UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID == appId);
            if (userInfo != null)
            {
                ViewBag.Lat = userInfo.Lat != null ? userInfo.Lat : 21.033781;
                ViewBag.Lng = userInfo.Lng != null ? userInfo.Lng : 105.814054;
            }
            else
            {
                ViewBag.Lat = 21.033781;
                ViewBag.Lng = 105.814054;
            }

            List<CarType> listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords().ToList();
            ViewBag.ListCarType = listCarType;

            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formCollection)
        {
            Order order = new Order();
            try
            {
                var ngayDi = formCollection["txtTimeDi"] != null ? formCollection["txtTimeDi"] : "";
                var gioDi = formCollection["ThoiGianBocHangDi"] != null ? formCollection["ThoiGianBocHangDi"] : "";
                var ngayDen = formCollection["txtTimeDen"] != null ? formCollection["txtTimeDen"] : "";
                var gioDen = formCollection["ThoiGianBocHangDen"] != null ? formCollection["ThoiGianBocHangDen"].ToString() : "";
                string fromLocation = formCollection["txtFromLocation"] != null ? formCollection["txtFromLocation"].ToString() : "";
                string toLocation = formCollection["txtToLocation"] != null ? formCollection["txtToLocation"].ToString() : "";

                //double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString().Replace(".", "")) : 0;
                double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString()) : 0;
                double price = formCollection["gia"] != null ? double.Parse(formCollection["gia"].ToString().Replace(".", "")) : 0;

                order.TenHang = formCollection["TenHang"] != null ? formCollection["TenHang"].ToString() : "";
                order.DienThoaiLienHe = formCollection["DienThoaiLienHe"] != null ? formCollection["DienThoaiLienHe"].ToString() : "";
                order.NguoiLienHe = formCollection["NguoiLienHe"] != null ? formCollection["NguoiLienHe"].ToString() : "";
                order.Gia = price;
                order.Status = formCollection["Status"] != null ? int.Parse(formCollection["Status"].ToString()) : PublicConstant.STATUS_PENDING;
                order.Note = formCollection["Note"] != null ? formCollection["Note"].ToString() : "";
                order.DiemDiChiTiet = fromLocation;
                order.DiemDenChiTiet = toLocation;
                gioDi = " " + gioDi.Trim();
                if (ngayDi.Length > 0 && gioDi.Trim().Length > 0)
                {
                    order.ThoiGianDi = DateTime.Parse(ngayDi + gioDi);
                }

                gioDen = " " + gioDen.Trim();
                if (ngayDen.Length > 0 && gioDen.Trim().Length > 0)
                {
                    order.ThoiGianDen = DateTime.Parse(ngayDen + gioDen);
                }
                order.UserName = User.Identity.Name;
                var thanhToan = formCollection["rThanhToan"] != null ? formCollection["rThanhToan"].ToString() : "0";
                if (thanhToan.Equals("Tiền mặt"))
                {
                    order.LoaiThanhToan = 0;
                }
                else
                {
                    order.LoaiThanhToan = 1;
                }
                order.MaVanDon = Ultilities.RandomNumber(8);
                order.CreateAt = DateTime.Now;
                order.UpdateAt = null;

                //string message = "";
                //double latFrom = 0, lngFrom = 0, latTo = 0, lngTo = 0;

                //string latLongFrom = GetLatLongByAddress(ref message, fromLocation);
                //if (message.Length == 0)
                //{
                //    latFrom = double.Parse(latLongFrom.Split(':')[0]);
                //    lngFrom = double.Parse(latLongFrom.Split(':')[1]);
                //}

                //string latLongTo = GetLatLongByAddress(ref message, toLocation);
                //if (message.Length == 0)
                //{
                //    latTo = double.Parse(latLongTo.Split(':')[0]);
                //    lngTo = double.Parse(latLongTo.Split(':')[1]);
                //}
                //order.FromLat = latFrom;
                //order.FromLng = lngFrom;

                //order.ToLat = latTo;
                //order.ToLng = lngTo;

                order.FromLat = formCollection["hfLatFrom"] != null ? double.Parse(formCollection["hfLatFrom"].ToString()) : 0; 
                order.FromLng = formCollection["hfLngFrom"] != null ? double.Parse(formCollection["hfLngFrom"].ToString()) : 0; 

                order.ToLat = formCollection["hfLatTo"] != null ? double.Parse(formCollection["hfLatTo"].ToString()) : 0; 
                order.ToLng = formCollection["hfLngTo"] != null ? double.Parse(formCollection["hfLngTo"].ToString()) : 0; 

                if (fromLocation.Length > 0 && toLocation.Length > 0)
                {
                    order.SoKmUocTinh = soKmUocTinh;// CalcDistance(latFrom, lngFrom, latTo, lngTo);
                }
                else
                {
                    order.SoKmUocTinh = 0;
                }

                if (formCollection["txtWidth"] != null && formCollection["txtWidth"].Length > 0)
                {
                    order.Width = double.Parse(formCollection["txtWidth"].ToString());
                }
                else
                {
                    order.Width = 0;
                }

                if (formCollection["txtHeight"] != null && formCollection["txtHeight"].Length > 0)
                {
                    order.Height = double.Parse(formCollection["txtHeight"].ToString());
                }
                else
                {
                    order.Height = 0;
                }

                if (formCollection["txtLenght"] != null && formCollection["txtLenght"].Length > 0)
                {
                    order.Lenght = double.Parse(formCollection["txtLenght"].ToString());
                }
                else
                {
                    order.Lenght = 0;
                }

                order.VAT = formCollection["VAT"] == "false" ? false : true;
                if (formCollection["txtTrongLuong"] != null && formCollection["txtTrongLuong"].Length > 0)
                {
                    order.TrongLuong = double.Parse(formCollection["txtTrongLuong"].ToString());
                }
                else
                {
                    order.TrongLuong = 0;
                }
                order.Created_By = User.Identity.GetUserId();
                order.IsViewPopupOrder = false;
                if (ModelState.IsValid)
                {
                    db.Order.Add(order);

                    #region insert OrderCar table

                    Order orderNew = _unitOfWork.GetRepositoryInstance<Order>().GetAllRecords().OrderByDescending(o => o.Id).Take(1).SingleOrDefault();

                    if (orderNew != null)
                    {
                        string type = "";
                        int quanlity = 0;
                        int typeCarId = 0;
                        OrderCar orderCar;

                        string strOrderCar = formCollection["hdfStrCarTypeSelect"];
                        //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                        string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                        foreach (var item in strCarTypeSelect)
                        {
                            try
                            {
                                //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                                type = item.Split('-')[0];
                                typeCarId = int.Parse(item.Split('-')[1].Split(':')[0]);
                                quanlity = int.Parse(item.Split(':')[1]);

                                if (quanlity > 0)
                                {
                                    orderCar = new OrderCar();
                                    orderCar.OrderId = orderNew.Id;
                                    orderCar.CarTypeId = typeCarId;
                                    orderCar.Number = quanlity;
                                    _unitOfWork.GetRepositoryInstance<OrderCar>().Add(orderCar);
                                }
                            }
                            catch (Exception ex)
                            {
                                break;
                            }
                        }

                        _unitOfWork.SaveChanges();
                    }

                    #endregion

                    #region insert draft order
                    //var isSaveDraft = formCollection["VAT"] == "false" ? false : true;
                    //if (isSaveDraft)
                    //{
                    //    DraftOrder draft = new DraftOrder();
                    //    draft.Name = order.UserName;
                    //    draft.TenHang = order.TenHang;
                    //    draft.DiemDi = order.DiemDiChiTiet;
                    //    draft.DiemDen = order.DiemDenChiTiet;
                    //    draft.Gia = order.Gia;
                    //    draft.FromLat = order.FromLat;
                    //    draft.FromLng = order.FromLng;
                    //    draft.ToLat = order.ToLat;
                    //    draft.ToLng = order.ToLng;
                    //    draft.Vat = order.VAT;
                    //    draft.TrongLuong = order.TrongLuong;
                    //    draft.ThoiGianDi = order.ThoiGianDi;
                    //    draft.ThoiGianDen = order.ThoiGianDen;
                    //    draft.Note = order.Note;
                    //    draft.Created_At = order.CreateAt;
                    //    draft.Created_By = User.Identity.Name;
                    //    draft.Height = order.Height;
                    //    draft.Width = order.Width;
                    //    draft.Lenght = order.Lenght;
                    //    db.DraftOrder.Add(draft);
                    //}
                    #endregion

                    db.SaveChanges();
                    PushOrderForDriver(order.Id);
                    TempData["info"] = "Thêm vận đơn thành công!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm vận đơn: " + ex.Message;
                return View(order);
            }
            return View(order);
        }
        public void PushOrderForDriver(long id)
        {
            var order = db.Order.Where(x => x.Id == id).SingleOrDefault();
            var driver = GetFistDriverNearMost(100, 1, order);
        }
    

        public Driver GetFistDriverNearMost(int distance, int page_size, Order order)
        {
            var lstDriver = db.Driver.ToList();
            List<Driver> lstDriverTemp = new List<Driver>();
            double currentDistinct = 0;
            foreach (Driver driver in lstDriver)
            {
                currentDistinct = GetDistance(double.Parse(driver.Lat.ToString()), double.Parse(driver.Lng.ToString()), double.Parse(order.FromLat.ToString()), double.Parse(order.FromLng.ToString()));
                if (currentDistinct < distance)
                {
                    //if (GreatThanDate(driver.Updated_At, DateTime.Now.ToString("HH:mm dd/MM/yyyy")))
                    {
                        lstDriverTemp.Add(driver);
                    }
                }
            }

            return lstDriverTemp.FirstOrDefault();

            //var lstDriver = db.Driver.Where(x => x.Location.Distance(order.FromLocation) < distance).Take(page_size);
            //var lstResult = new List<Driver>();
            //foreach (Driver item in lstDriver)
            //{
            //    if (GreatThanDate(item.Updated_At, DateTime.Now.ToString("HH:mm dd/MM/yyyy")))
            //    {
            //        lstResult.Add(item);
            //    }
            //}
            //return lstResult.FirstOrDefault();
        }

        private static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }
        public double CalcDistance(double latFrom, double longFrom, double latTo, double longTo)
        {
            var R = 6371d; // Radius of the earth in km
            var dLat = DegreeToRadian(latTo - latFrom);  // deg2rad below
            var dLon = DegreeToRadian(longTo - longFrom);
            var a =
              Math.Sin(dLat / 2d) * Math.Sin(dLat / 2d) +
              Math.Cos(DegreeToRadian(latFrom)) * Math.Cos(DegreeToRadian(latTo)) *
              Math.Sin(dLon / 2d) * Math.Sin(dLon / 2d);
            var c = 2d * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1d - a));
            var d = R * c; // Distance in km
            return d;
        }
        private double DegreeToRadian(double angle)
        {
            return angle * (Math.PI / 180d);
        }
        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            var listCar = (from a in db.OrderCar
                           join c in db.CarType on a.CarTypeId equals c.Id
                           where a.OrderId == order.Id
                           select new OrderCarViewModel
                           {
                               Id = a.Id,
                               CarTypeId = c.Id,
                               CarTypeName = c.Name,
                               Number = a.Number,
                               OrderId = a.OrderId,
                               Value = c.Value
                           }).ToList();
            ViewBag.ListOrderCar = listCar;

            List<CarType> listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords().ToList();
            ViewBag.ListCarType = listCarType;

            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection formCollection)
        {
            long id = 0;
            Order order = new Order();
            try
            {
                id = long.Parse(formCollection["Id"].ToString());
                order = db.Order.Where(o => o.Id == id).Single();

                if (order != null)
                {
                    var ngayDi = formCollection["ngayDi"] != null ? DateTime.Parse(formCollection["ngayDi"]).ToString("dd/MM/yyyy") : "";
                    var gioDi = formCollection["gioBocDi"] != null ? formCollection["gioBocDi"] : "";
                    string ngayDen = "";
                    if ((formCollection["ngayDen"] != null && formCollection["ngayDen"].Length > 0))
                    {
                        ngayDen = DateTime.Parse(formCollection["ngayDen"]).ToString("dd/MM/yyyy");
                    }

                    string gioDen = "";
                    if ((formCollection["gioBocDen"] != null && formCollection["gioBocDen"].Length > 0))
                    {
                        gioDen = formCollection["gioBocDen"].ToString();
                    }
                    string fromLocation = formCollection["txtFromLocation"] != null ? formCollection["txtFromLocation"].ToString() : "";
                    string toLocation = formCollection["txtToLocation"] != null ? formCollection["txtToLocation"].ToString() : "";
                    //double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString().Replace(".", "")) : double.Parse(order.SoKmUocTinh.ToString());
                    double soKmUocTinh = formCollection["txtSoKmUocTinh"] != null ? double.Parse(formCollection["txtSoKmUocTinh"].ToString()) : double.Parse(order.SoKmUocTinh.ToString());
                    double latFrom = (formCollection["hfLatFrom"] != null && formCollection["hfLatFrom"].Length > 0) ? double.Parse(formCollection["hfLatFrom"].ToString().Replace(".", "")) : double.Parse(order.FromLat.ToString());
                    double lngFrom = (formCollection["hfLngFrom"] != null && formCollection["hfLngFrom"].Length > 0) ? double.Parse(formCollection["hfLngFrom"].ToString().Replace(".", "")) : double.Parse(order.FromLng.ToString());
                    double latTo = (formCollection["hfLatTo"] != null && formCollection["hfLatTo"].Length > 0) ? double.Parse(formCollection["hfLatTo"].ToString().Replace(".", "")) : double.Parse(order.ToLat.ToString());
                    double lngTo = (formCollection["hfLngTo"] != null && formCollection["hfLngTo"].Length > 0) ? double.Parse(formCollection["hfLngTo"].ToString().Replace(".", "")) : double.Parse(order.ToLng.ToString());

                    double price = formCollection["gia"] != null ? double.Parse(formCollection["gia"].ToString()) : double.Parse(order.Gia.ToString());

                    order.TenHang = formCollection["TenHang"] != null ? formCollection["TenHang"].ToString() : order.TenHang;
                    order.DienThoaiLienHe = formCollection["DienThoaiLienHe"] != null ? formCollection["DienThoaiLienHe"].ToString() : order.DienThoaiLienHe;
                    order.NguoiLienHe = formCollection["NguoiLienHe"] != null ? formCollection["NguoiLienHe"].ToString() : order.NguoiLienHe;

                    order.Gia = price;
                    order.Status = formCollection["Status"] != null ? int.Parse(formCollection["Status"].ToString()) : PublicConstant.STATUS_ACTIVE;
                    order.Note = formCollection["Note"] != null ? formCollection["Note"].ToString() : order.Note;

                    gioDi = " " + gioDi.Trim();
                    if (ngayDi.Length > 0 && gioDi.Trim().Length > 0)
                    {
                        order.ThoiGianDi = DateTime.Parse(ngayDi + gioDi);
                    }

                    gioDen = " " + gioDen.Trim();
                    if (ngayDen.Length > 0 && gioDen.Trim().Length > 0)
                    {
                        order.ThoiGianDen = DateTime.Parse(ngayDen + gioDen);
                    }

                    order.UserName = User.Identity.Name;
                    var thanhToan = formCollection["rThanhToan"] != null ? formCollection["rThanhToan"].ToString() : "0";
                    if (thanhToan.Equals("Tiền mặt"))
                    {
                        order.LoaiThanhToan = 0;
                    }
                    else
                    {
                        order.LoaiThanhToan = 1;
                    }
                    //order.MaVanDon = Ultilities.GenerateOrderCode(ngayDi, User.Identity.Name);
                    order.CreateAt = order.CreateAt;
                    order.UpdateAt = DateTime.Now;

                    order.DiemDiChiTiet = fromLocation;
                    order.DiemDenChiTiet = toLocation;

                    string message = "";
                    //double latFrom = 0, lngFrom = 0, latTo = 0, lngTo = 0;
                    //if (fromLocation.Length > 0)
                    //{
                    //    string latLongFrom = GetLatLongByAddress(ref message, fromLocation);
                    //    if (message.Length == 0)
                    //    {
                    //        latFrom = double.Parse(latLongFrom.Split(':')[0]);
                    //        lngFrom = double.Parse(latLongFrom.Split(':')[1]);
                    //    }
                    //}
                    //else
                    //{
                    //    latFrom = double.Parse(order.FromLat.ToString());
                    //    lngFrom = double.Parse(order.FromLng.ToString());
                    //}

                    //if (toLocation.Length > 0 && toLocation.Split(':').Length < 2)
                    //{
                    //    string latLongTo = GetLatLongByAddress(ref message, toLocation);
                    //    if (message.Length == 0)
                    //    {
                    //        latTo = double.Parse(latLongTo.Split(':')[0]);
                    //        lngTo = double.Parse(latLongTo.Split(':')[1]);
                    //    }
                    //}
                    //else
                    //{
                    //    latTo = double.Parse(order.ToLat.ToString());
                    //    lngTo = double.Parse(order.ToLng.ToString());
                    //}

                    order.FromLat = latFrom;
                    order.ToLat = lngFrom;
                    order.ToLat = latTo;
                    order.ToLng = lngTo;

                    //if (fromLocation.Length > 0 && toLocation.Length > 0)
                    //{
                    //    order.SoKmUocTinh = soKmUocTinh;CalcDistance(latFrom, lngFrom, latTo, lngTo);
                    //}
                    //else
                    //{
                    //    order.SoKmUocTinh = order.SoKmUocTinh;
                    //}
                    order.SoKmUocTinh = soKmUocTinh;//CalcDistance(latFrom, lngFrom, latTo, lngTo);
                    if (formCollection["txtWidth"] != null && formCollection["txtWidth"].Length > 0)
                    {
                        order.Width = double.Parse(formCollection["txtWidth"].ToString());
                    }
                    else
                    {
                        order.Width = order.Width;
                    }

                    if (formCollection["txtHeight"] != null && formCollection["txtHeight"].Length > 0)
                    {
                        order.Height = double.Parse(formCollection["txtHeight"].ToString());
                    }
                    else
                    {
                        order.Height = order.Height;
                    }

                    if (formCollection["txtLenght"] != null && formCollection["txtLenght"].Length > 0)
                    {
                        order.Lenght = double.Parse(formCollection["txtLenght"].ToString());
                    }
                    else
                    {
                        order.Lenght = order.Lenght;
                    }

                    order.VAT = formCollection["VAT"] == "false" ? false : true;
                    if (formCollection["txtTrongLuong"] != null && formCollection["txtTrongLuong"].Length > 0)
                    {
                        order.TrongLuong = double.Parse(formCollection["txtTrongLuong"].ToString());
                    }
                    else
                    {
                        order.TrongLuong = order.TrongLuong;
                    }

                    db.SaveChanges();

                    #region insert OrderCar table

                    if (order != null)
                    {
                        string type = "";
                        int quanlity = 0;
                        int typeCarId = 0;
                        OrderCar orderCar;

                        string strOrderCar = formCollection["hdfStrCarTypeSelect"];
                        //str dropdowlist cartype select return  example:1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                        string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();

                        _unitOfWork.GetRepositoryInstance<OrderCar>().RemoveRangeByWhereClause(o => o.OrderId == order.Id);
                        _unitOfWork.SaveChanges();

                        foreach (var item in strCarTypeSelect)
                        {
                            try
                            {
                                //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                                type = item.Split('-')[0];
                                typeCarId = int.Parse(item.Split('-')[1].Split(':')[0]);
                                quanlity = int.Parse(item.Split(':')[1]);

                                if (quanlity > 0)
                                {
                                    orderCar = new OrderCar();
                                    orderCar.OrderId = order.Id;
                                    orderCar.CarTypeId = typeCarId;
                                    orderCar.Number = quanlity;
                                    _unitOfWork.GetRepositoryInstance<OrderCar>().Add(orderCar);
                                }
                            }
                            catch (Exception ex)
                            {
                                break;
                            }
                        }

                        _unitOfWork.SaveChanges();
                    }

                    if (order.Status == 1) // push tự động
                    {
                        var nearDriver = GetDriverNearOrder(order.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                        if (nearDriver != null)
                        {
                            //CarrierHubClient client = new CarrierHubClient();
                            //if (client.isOnline(nearDriver.UserId))
                            //{
                            //    await client.SendMessage(User.Identity.GetUserId(), nearDriver.UserId, order);
                            //}
                            //else
                            //{
                            PushMessageForDriver(nearDriver.UserId, order.Id);
                            //}
                        }
                    }

                    #endregion

                    TempData["info"] = "Cập nhật thành công!";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi cập nhật vận đơn: " + ex.Message;
            }


            //ViewBag.Driver = new SelectList(db.Driver, "id", "Name", order.Driver);
            return View(order);
        }

        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
            //var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
            var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId", sqlOrderId).FirstOrDefault();
            return lstSearch;
        }

        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="order"></param>
        public void PushMessageForDriver(string driverId, long orderId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
        }

        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            if (result.Equals("Success"))
            {
                long orderId = long.Parse(obj2);
                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
                if (old != null)
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                    old.Updated_At = DateTime.Now;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                }
                else
                {
                    var obj = new OrderTracking();
                    obj.OrderId = orderId;
                    obj.DriverId = obj1;
                    obj.OwnerId = User.Identity.GetUserId();
                    obj.Created_At = DateTime.Now;
                    obj.Updated_At = DateTime.Now;
                    obj.Created_By = User.Identity.GetUserId();
                    obj.Status = PublicConstant.ORDER_PENDING;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                }
                _unitOfWork.SaveChanges();
                TempData["info"] = "Vận đơn đã được gửi cho tài xế thành công!";
            }
            else
            {
                TempData["error"] = "Lỗi không thể gửi được vận đơn. Vui lòng thử lại sau!";
                // Show message 
                long orderId = long.Parse(obj2);
                var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
                var obj = new OrderTracking();
                obj.OrderId = orderId;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.Created_At = DateTime.Now;
                obj.Updated_At = DateTime.Now;
                obj.Created_By = "0c7d3527-6b5e-4cda-9547-b2c4a0f0011b";//Carrier
                obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
            }
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                db.Order.Remove(order);
                db.SaveChanges();
            }
            else
            {
                if (String.Compare(order.UserName, User.Identity.Name, false) != 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    db.Order.Remove(order);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Tính giá theo file excel mới
        /// </summary>
        /// <param name="fromaddress"></param>
        /// <param name="toaddress"></param>
        /// <param name="isVAT"></param>
        /// <param name="strOrderCar"></param>
        /// <returns></returns>
        public JsonResult GetPrice(string fromaddress, string toaddress, double latfrom, double lnfrom, double latto, double lnto, bool isVAT,
        string strOrderCar)
        {
            try
            {

                string message = "";
                double distance = 0;
                double totalPrice = 0;
                string totalPriceNoVat = "";
                double price = 0;
                string type = "";
                int quanlity = 0;

                //str dropdowlist cartype select return  example: 1_25-1:0:1_4-2:0:2_5-3:0:3_5-4:0:5-5:0:8-6:0:10-7:0:15-8:0:18-9:0:20-10:0:40-11:0:
                string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                foreach (var item in strCarTypeSelect)
                {
                    //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                    type = item.Split('-')[0];
                    quanlity = int.Parse(item.Split(':')[1]);
                    if (quanlity > 0)
                    {
                        price = 0;//import.GetPriceForOrder(type, fromaddress, toaddress,latfrom,lnfrom,latto,lnto, ref message, ref distance);
                        if (price > 0)
                        {
                            totalPrice = totalPrice + (price * quanlity);
                        }
                    }
                }

                totalPriceNoVat = totalPrice.ToString("#,###");

                if (message.Length == 0)
                {
                    string priceValue = "";
                    if (isVAT)
                    {
                        totalPrice = totalPrice * 1.1;

                        priceValue = totalPrice.ToString("#,###");
                    }
                    else
                    {
                        priceValue = totalPrice.ToString("#,###");
                    }

                    return Json(new { distance = distance, totalPrice = priceValue, oldPrice = totalPriceNoVat, message = "tính giá thành công" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                    return Json(new { message = "Lỗi tính giá vận đơn:" + message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AutoCompleteDraftOrder(string prefix)
        {
            DraftOrder entities = new DraftOrder();
            var listDraft = (from devicePush in db.DraftOrder
                             where devicePush.Name.StartsWith(prefix)
                             select new
                             {
                                 label = devicePush.Name,
                                 val = devicePush.Id
                             }).ToList();

            return Json(listDraft);
        }

        [HttpPost]
        public JsonResult LoadDraftOrder(int id)
        {
            DraftOrder draftOrder = _unitOfWork.GetRepositoryInstance<DraftOrder>().GetFirstOrDefaultByParameter(o => o.Id == id);
            if (draftOrder != null)
            {
                return Json(new
                {
                    TenHang = draftOrder.TenHang != null ? draftOrder.TenHang : "",
                    DiemDi = draftOrder.DiemDi != null ? draftOrder.DiemDi : "",
                    DiemDen = draftOrder.DiemDen != null ? draftOrder.DiemDen : "",
                    Gia = draftOrder.Gia != null ? draftOrder.Gia : 0,
                    FromLat = draftOrder.FromLat != null ? draftOrder.FromLat : 0,
                    FromLng = draftOrder.FromLng != null ? draftOrder.FromLng : 0,
                    ToLat = draftOrder.ToLat != null ? draftOrder.ToLat : 0,
                    ToLng = draftOrder.ToLng != null ? draftOrder.ToLng : 0,
                    Vat = draftOrder.Vat != null ? draftOrder.Vat : false,
                    TrongLuong = draftOrder.TrongLuong != null ? draftOrder.TrongLuong : 0,
                    ThoiGianDi = draftOrder.ThoiGianDi != null ? draftOrder.ThoiGianDi : "",
                    ThoiGianDen = draftOrder.ThoiGianDen != null ? draftOrder.ThoiGianDi : "",
                    Note = draftOrder.Note != null ? draftOrder.Note : "",
                    Width = draftOrder.Width != null ? draftOrder.Width : 0,
                    Height = draftOrder.Height != null ? draftOrder.Height : 0,
                    Lenght = draftOrder.Lenght != null ? draftOrder.Lenght : 0,
                    message = ""
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "lỗi lấy dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="thoigiandi">Thoi gian di la thoi gian di cua don hang</param>
        /// <param name="status">Trang thai don hang lay tu bang OrderTracking</param>
        /// <returns></returns>
        public string ShowStatusToView(int status)
        {
            string strResult = string.Empty;
            switch (status)
            {
                case 1:
                    {
                        strResult = @"<span style='color:yellowgreen' > Đã khớp lệnh</span>";
                        break;
                    }
                case 2:
                    {
                        strResult = @"<span style='color:red' > Tài xế hủy</span>";
                        break;
                    }
                case 3:
                    {
                        strResult = @"<span style='color:red' > Chủ hàng hủy</span>";
                        break;
                    }
                case 4:
                    {
                        strResult = @"<span style='color:yellowgreen' > Đang vận chuyển</span>";
                        break;
                    }
                case 5:
                    {
                        strResult = @"<span style='color:blue' > Hoàn thành</span>";
                        break;
                    }
                case 6:
                    {
                        strResult = @"<span style='color:red' > Gửi tài xế lỗi</span>";
                        break;
                    }
                case 7:
                    {
                        strResult = @"<span style='color:red' > Đơn hết hạn</span>";
                        break;
                    }
                default:
                    {
                        strResult = @"<span style='color:dodgerblue' > Đang khớp lệnh</span>";
                        break;
                    }
            }
            return strResult;
        }
    }
}