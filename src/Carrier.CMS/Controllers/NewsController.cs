﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using Carrier.CMS.Common;
namespace Carrier.CMS.Controllers
{
    [Compress]
    public class NewsController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();

        // GET: News
        public ActionResult Index()
        {
            var articles = db.Articles.Include(a => a.Category);
            return View(articles.ToList());
        }

        // GET: News/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            ViewBag.CurrentCateId = articles.Category.Id >0? articles.Category.Id: 0;
            ViewBag.RelatedNews = db.Articles.Where(x => x.CategoryId == articles.CategoryId && x.Id != articles.Id && articles.Status == (int)CMS_STATUS.ACTIVE).Take(5);
            return View(articles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
