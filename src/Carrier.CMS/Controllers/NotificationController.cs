﻿using Carrier.CMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrier.CMS.Controllers
{
    public class NotificationController : Controller
    {
        // GET: Notification
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetNotification()
        {
            NotificationRepository notificationRepository = new NotificationRepository();
            return PartialView("ListNotification", notificationRepository.GetNotifications());
        }
    }
}