﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.Repository;
using Carrier.Models.Entities;
using Carrier.Utilities;
using Microsoft.AspNet.Identity;
using Carrier.CMS.Common;
using System.Device.Location;
namespace CMS.Carrier.Controllers
{
    [Compress]

    [Roles(PublicConstant.ROLE_USERS)]
    public class HomeUserController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public string UserId
        {
            get { return User.Identity.GetUserId(); }
        }
        // GET: Enterprise/HomeEnterprise
        public ActionResult Index()
        {
            return View();
        }

        #region Fetch data for Map
        public JsonResult SearchDriver(int distance, int type)
        {
            //var center = new GeoCoordinate(fromLocation.Lat.Value, fromLocation.Lng.Value);
            //var lstLocationNearess = _unitOfWork.GetRepositoryInstance<Order>().GetAllRecordsIQueryable().Select(x => new GeoCoordinate(x.FromLat.Value, x.FromLng.Value))
            //          .Where(x => x.GetDistanceTo(center) < distance).ToList();

            var lstDrivers = _unitOfWork.GetRepositoryInstance<Driver>().GetListByParameter(x => x.Status == type);
            var dictData = new Dictionary<string, object>() { { "count", lstDrivers.Count() }, { "drivers", lstDrivers } };
            return Json(dictData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchOrder(int distance, int type)
        {
            var lstOrders = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(x => x.UserName.Equals(User.Identity.Name) && x.Status == PublicConstant.STATUS_ACTIVE);
            var dictData = new Dictionary<string, object>() { { "count", lstOrders.Count() }, { "Data", lstOrders } };
            return Json(dictData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
