﻿using Carrier.CMS.Models;
using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.Utilities;
using Carrier.CMS.Common;

namespace Carrier.CMS.Controllers
{
    [Roles(PublicConstant.ROLE_USERS)]
    public class HistoryController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        // GET: History
        public ActionResult Index(string status = null, string mavandon = null, int OrganizationId = 0, string ddlDriver = null)
        {
            if (User.IsInRole(PublicConstant.ROLE_ADMIN))
            {
                List<Organization> listOrganization = new List<Organization>();
                listOrganization = db.Organization.ToList();
                IEnumerable<SelectListItem> listOrganizationSelect = listOrganization.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListOrganization = listOrganizationSelect;
            }
            else
            {
                int currentOrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.Name);
                List<Driver> listDriver = new List<Driver>();
                listDriver = db.Driver.Where(m => m.OrganizationId == currentOrganizationId).ToList();
                IEnumerable<SelectListItem> listDriverSelect = listDriver.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Id.ToString() });
                ViewBag.ListDriver = listDriverSelect;
            }

            #region save
            //List<HistoryModel> listOrdertracking = new List<HistoryModel>();
            //if (status != null || mavandon != null || OrganizationId != 0 || ddlDriver != null)
            //{
            //    if (int.Parse(status) > 0)
            //    {
            //        listOrdertracking = (from tracking in db.OrderTracking
            //                             join info in db.UserInfoes on tracking.UserId equals info.ApplicationUserID
            //                             join driver in db.Driver on info.ApplicationUserID equals driver.UserId
            //                             join order in db.Order on new { Id = (long)tracking.OrderId } equals new { order.Id }
            //                             select new HistoryModel
            //                             {
            //                                 Id = tracking.ID,
            //                                 OrderId = tracking.OrderId.ToString(),
            //                                 FullName = info.FullName,
            //                                 Phone = info.MobilePhone,
            //                                 CreateDate = order.CreateAt,
            //                                 Status = info.Status.ToString(),
            //                                 MaVanDon = order.MaVanDon,
            //                                 OrganzationId = info.OrganzationId,
            //                                 UserId = driver.UserId
            //                             }).Where(o => o.Status == status || o.MaVanDon == mavandon || o.OrganzationId == OrganizationId || o.UserId == ddlDriver).OrderByDescending(o => o.Id).ToList();
            //    }
            //    else
            //    {
            //        listOrdertracking = (from tracking in db.OrderTracking
            //                             join info in db.UserInfoes on tracking.UserId equals info.ApplicationUserID
            //                             join driver in db.Driver on info.ApplicationUserID equals driver.UserId
            //                             join order in db.Order on new { Id = (long)tracking.OrderId } equals new { order.Id }
            //                             where (tracking.Status == PublicConstant.ORDER_FINISHED || tracking.Status == PublicConstant.ORDER_CANCEL) && ((order.MaVanDon == mavandon) || (info.FullName.Contains(FullName)))
            //                             select new HistoryModel
            //                             {
            //                                 Id = tracking.ID,
            //                                 OrderId = tracking.OrderId.ToString(),
            //                                 FullName = info.FullName,
            //                                 Phone = info.MobilePhone,
            //                                 CreateDate = order.CreateAt,
            //                                 Status = info.Status.ToString(),
            //                                 MaVanDon = order.MaVanDon
            //                             }).OrderByDescending(o => o.Id).ToList();
            //    }
            //}
            //else
            //{
            //    listOrdertracking = (from tracking in db.OrderTracking
            //                         join info in db.UserInfoes on tracking.UserId equals info.ApplicationUserID
            //                         join driver in db.Driver on info.ApplicationUserID equals driver.UserId
            //                         join order in db.Order on new { Id = (long)tracking.OrderId } equals new { order.Id }
            //                         where (tracking.Status == PublicConstant.ORDER_FINISHED || tracking.Status == PublicConstant.ORDER_CANCEL)
            //                         select new HistoryModel
            //                         {
            //                             Id = tracking.ID,
            //                             OrderId = tracking.OrderId.ToString(),
            //                             FullName = info.FullName,
            //                             Phone = info.MobilePhone,
            //                             CreateDate = order.CreateAt,
            //                             Status = info.Status.ToString(),
            //                             MaVanDon = order.MaVanDon
            //                         }).OrderByDescending(o => o.Id).ToList();
            //}
            #endregion

            List<HistoryModel> listOrdertracking = new List<HistoryModel>();

            //listOrdertracking = (from tracking in db.OrderTracking
            //                     join info in db.UserInfoes on tracking.UserId equals info.ApplicationUserID
            //                     join driver in db.Driver on info.ApplicationUserID equals driver.UserId
            //                     join order in db.Order on new { Id = (long)tracking.OrderId } equals new { order.Id }
            //                     where (tracking.Status == PublicConstant.ORDER_FINISHED || tracking.Status == PublicConstant.ORDER_CANCEL)
            //                     select new HistoryModel
            //                     {
            //                         Id = tracking.ID,
            //                         OrderId = tracking.OrderId.ToString(),
            //                         UserId = info.ApplicationUserID,
            //                         FullName = info.FullName,
            //                         Phone = info.MobilePhone,
            //                         CreateDate = order.CreateAt,
            //                         Status = order.Status.ToString(),
            //                         MaVanDon = order.MaVanDon
            //                     }).ToList();

            if (status != null)
            {
                listOrdertracking = listOrdertracking.Where(o => o.Status == status).ToList();
            }
            if (mavandon != null)
            {
                listOrdertracking = listOrdertracking.Where(o => o.MaVanDon == mavandon).ToList();
            }
            if (OrganizationId > 0)
            {
                listOrdertracking = listOrdertracking.Where(o => o.OrganzationId == OrganizationId).ToList();
            }
            if (ddlDriver != null)
            {
                listOrdertracking = listOrdertracking.Where(o => o.UserId == ddlDriver).ToList();
            }

            return View(listOrdertracking);
        }

        [HttpPost]
        public ActionResult GetDriverByOrganizationId(int Id)
        {
            List<Driver> listDriver = new List<Driver>();
            listDriver = db.Driver.Where(m => m.OrganizationId == Id).ToList();
            SelectList listDriverReturn = new SelectList(listDriver, "Id", "Name", 0);
            return Json(listDriverReturn);
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}
