﻿using System.Globalization;
using Carrier.CMS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Carrier.Utilities;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.CMS.Common;

namespace Carrier.CMS.Controllers
{
    [Compress]
    [Authorize]
    public class AccountController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var us = await UserManager.FindAsync(model.UserName, model.Password);
            if(us !=null && !us.LockoutEnabled)
            {
                var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            var user = await UserManager.FindAsync(model.UserName, model.Password);
                            if (user != null && !user.LockoutEnabled)
                            {
                                if (string.IsNullOrEmpty(returnUrl))
                                {
                                    if (UserManager.IsInRole(user.Id, PublicConstant.ROLE_SUPPER_ADMIN) || UserManager.IsInRole(user.Id, PublicConstant.ROLE_ADMIN))
                                    {
                                        //return RedirectToAction("Index", "Admin/HomeBackend");
                                        return Redirect("~/Admin/HomeBackend");
                                    }
                                    //role Admin go to Admin page
                                    if (UserManager.IsInRole(user.Id, PublicConstant.ROLE_ENTERPRISE) || UserManager.IsInRole(user.Id, PublicConstant.ROLE_FORWARDER) || UserManager.IsInRole(user.Id, PublicConstant.ROLE_USERS))
                                    {
                                        //return RedirectToAction("Index", "Enterprise/HomeEnterprise");
                                        return Redirect("~/Enterprise/HomeEnterprise");
                                    }
                                }
                                else
                                {
                                    return RedirectToLocal(returnUrl);
                                }
                            }
                            else
                            {
                                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                            }
                            return RedirectToLocal(returnUrl);

                        }
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            var user = await UserManager.FindByIdAsync(await SignInManager.GetVerifiedUserIdAsync());
            if (user != null)
            {
                ViewBag.Status = "For DEMO purposes the current " + provider + " code is: " + await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: false, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email};
                UserInfo userInfo = new UserInfo();
                userInfo.Address = Request.Form["Address"];
                userInfo.MobilePhone = Request.Form["Phone"];
                userInfo.FullName = Request.Form["FullName"];
                userInfo.ApplicationUserID = user.Id;
                userInfo.Sex = false;
                userInfo.Status = PublicConstant.STATUS_ACTIVE;
                userInfo.Updated_At = DateTime.Now;
                userInfo.LastLogin = System.DateTime.Now;
                userInfo.Created_At = DateTime.Now;
                userInfo.Lat = 0;
                userInfo.Lng = 0;
                user.UsersInfo = userInfo;
                Organization organ = new Organization();
                organ.Name = userInfo.FullName;
                organ.Address = userInfo.Address;
                organ.UserId = user.Id;
                organ.Phone = userInfo.MobilePhone;
                _unitOfWork.GetRepositoryInstance<Organization>().Add(organ);
                _unitOfWork.SaveChanges();
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    //result = await UserManager.AddToRolesAsync(user.Id, "4");
                    //    string[] role = new string[1];
                    //role[0] = "4";
                    //var s = await UserManager.AddToRolesAsync("e5febe0b-733a-4c0c-9736-fe6635342e66", role);
                    
                    if (!result.Succeeded)
                    {
                        return View("Login");
                    }
                    UserManager.AddToRole(user.Id, PublicConstant.ROLE_USERS.ToString());

                    //bool chkTaixe = bool.Parse(Request.Form.GetValues("chkTaixe")[0]);
                    //bool chkCanhan = bool.Parse(Request.Form.GetValues("chkCanhan")[0]);
                    //bool chkDoanhnghiep = bool.Parse(Request.Form.GetValues("chkDoanhnghiep")[0]);

                    //// add role
                    //if (chkTaixe)
                    //{
                    //    UserManager.AddToRole(user.Id, PublicConstant.ROLE_DRIVERS.ToString());
                    //}
                    //if (chkCanhan)
                    //{
                    //    UserManager.AddToRole(user.Id, PublicConstant.ROLE_USERS.ToString());
                    //}
                    //if (chkDoanhnghiep)
                    //{
                    //    UserManager.AddToRole(user.Id, PublicConstant.ROLE_ENTERPRISE.ToString());
                    //}

                    //if (chkTaixe)
                    //{
                    //    // save car
                    //    Double trongTaiXe;
                    //    Double.TryParse(Request.Form["txtTrongTaiXe"], out trongTaiXe);
                    //    float width, lenght, height;
                    //    float.TryParse(Request.Form["txtWidth"], out width);
                    //    float.TryParse(Request.Form["txtLenght"], out lenght);
                    //    float.TryParse(Request.Form["txtHeight"], out height);

                    //    Car car = new Car
                    //    {
                    //        License = Request.Form["txtBienSoXe"],
                    //        Payload = trongTaiXe,
                    //        Width_Size = width,
                    //        Leng_Size = lenght,
                    //        Height_Size = height,
                    //        Created_At = DateTime.Now,
                    //        Created_By = Request.Form["FullName"]
                    //    };
                    //    _unitOfWork.GetRepositoryInstance<Car>().Add(car);
                    //    _unitOfWork.SaveChanges();
                    //    if (car != null)
                    //    {
                    //        DriverCar driverCar = new DriverCar
                    //        {
                    //            CarId = car.Id,
                    //            DriverId = userInfo.Id,
                    //        };
                    //        _unitOfWork.GetRepositoryInstance<DriverCar>().Add(driverCar);
                    //        _unitOfWork.SaveChanges();
                    //    }
                    //}
                    TempData["Message"] = "Đăng ký thành công!";
                    return View();
                }
                else
                {
                    TempData["Message"] = "Đăng ký thất bại!";
                    AddErrors(result);
                    return View();
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            if (ViewBag.CurrentCateId == null)
            {
                ViewBag.CurrentCateId = 0;
            }

            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", "Hãy reset mật khẩu của bạn bằng cách click vào link: <a href=\"" + callbackUrl + "\">link</a>");
                ViewBag.Link = callbackUrl;
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            if (ViewBag.CurrentCateId == null)
            {
                ViewBag.CurrentCateId = 0;
            }

            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> LogOff()
        {
            //var userId = User.Identity.GetUserId();
            //var token = await UserManager.GeneratePasswordResetTokenAsync(userId);
            //var result = await UserManager.ResetPasswordAsync(userId, token, "carrier2017");

            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}