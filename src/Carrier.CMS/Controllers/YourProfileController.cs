﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carrier.Models.Entities;
using System.Threading.Tasks;
using Carrier.CMS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.IO;
using Carrier.CMS.Common;

namespace Carrier.CMS.Controllers
{
    [Compress]
    public class YourProfileController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();

        public YourProfileController()
        {
        }

        public YourProfileController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: YourProfile
        public ActionResult Index()
        {
            if (ViewBag.CurrentCateId == null)
            {
                ViewBag.CurrentCateId = 0;
            }
            return View();
        }

        [Authorize]
        public ActionResult CapNhatProfile()
        {
            if (ViewBag.CurrentCateId == null)
            {
                ViewBag.CurrentCateId = 0;
            }

            UserInfoes suUsersInfo = new UserInfoes();
            AspNetUsers aspNetUser = db.AspNetUsers.Where(o => o.UserName == User.Identity.Name).Single();
            string id = User.Identity.GetUserId();
            if (aspNetUser != null)
            {
                suUsersInfo = db.UserInfoes.Where(o => o.ApplicationUserID == aspNetUser.Id).SingleOrDefault();
            }

            if (suUsersInfo == null)
            {
                return HttpNotFound();
            }

            return View(suUsersInfo);
            //return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult CapNhatProfile(FormCollection form, IEnumerable<HttpPostedFileBase> files)
        {
            // string name = Request.Files["file"];
            var fileName = string.Empty;

            string hoTen = form.Get("txtfullname");
            string CMND = form.Get("txtcmnn");
            string dienthoai = form.Get("txtphone");
            string diachi = form.Get("txtaddress");
            string ngaysinh = form.Get("txtbirhday");
            string userId = User.Identity.GetUserId();
            UserInfoes userInfo = new UserInfoes();
            userInfo = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault();
            try
            {
                if (userInfo != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        var file = Request.Files[0];

                        if (file != null && file.ContentLength > 0)
                        {
                            if (file != null && file.ContentLength > 0)
                            {   // xóa  file cũ
                                if (userInfo.Picture.Length > 0)
                                {
                                    if (System.IO.File.Exists(Server.MapPath("~/Uploads/Account/" + userInfo.Picture)))
                                    {
                                        System.IO.File.Delete(Server.MapPath("~/Uploads/Account/" + userInfo.Picture));
                                    }
                                }

                                //kiem tra file extension lan nua
                                List<string> excelExtension = new List<string>();
                                excelExtension.Add(".png");
                                excelExtension.Add(".jpg");
                                excelExtension.Add(".gif");
                                var extension = Path.GetExtension(file.FileName);
                                if (!excelExtension.Contains(extension.ToLower()))
                                {
                                    return Json(new { success = 0, message = "File không đúng định dạng cho phép, bạn hãy chọn file có định dạng .jpg, gif hoặc png" });
                                }
                                //kiem tra dung luong file
                                var fileSize = file.ContentLength;
                                var fileMB = (fileSize / 1024f) / 1024f;
                                if (fileMB > 5)
                                {
                                    return Json(new { success = 0, message = "Dung lượng ảnh không được lớn hơn 5MB" });
                                }
                                // luu ra dia
                                fileName = Path.GetFileNameWithoutExtension(file.FileName);
                                fileName = fileName + "_" + userId+ DateTime.Now.ToString("dd_MM_yy") + extension;
                                fileName = fileName + extension;
                                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Account"), fileName);
                                file.SaveAs(physicalPath);
                            }
                        }
                    }

                    userInfo.Picture = fileName;
                    userInfo.FullName = hoTen;
                    userInfo.CMND = CMND;
                    userInfo.MobilePhone = dienthoai;
                    userInfo.Address = diachi;
                    userInfo.BirthDay = ngaysinh;

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
            }
            return View(userInfo);
        }

        [Authorize]
        public ActionResult DoiMatKhau()
        {
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DoiMatKhau(ChangePasswordViewModel model)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}
            string oldPass = Request.Form["txtOldPassword"];
            string newPass = Request.Form["txtNewPassWord"];
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), oldPass, newPass);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            //AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }
    }
}