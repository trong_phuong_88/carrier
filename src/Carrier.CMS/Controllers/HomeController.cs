﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using System.Data.Entity;
using Carrier.CMS.Common;
using Carrier.Utilities;
using Carrier.Models.Entities;
using System.Collections.Generic;
using CMS.Carrier.Common;
using System.Configuration;
using Carrier.CMS.Hubs;
using Carrier.Repository;
using Carrier.Algorithm;
using Carrier.Algorithm.Entity;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web;
using Microsoft.AspNet.Identity;
using GoogleMaps.LocationServices;
using Microsoft.AspNet.Identity.Owin;
using System.IO;
using System.Web.Helpers;
using Carrier.CMS.Models;
using CMS.Carrier.Models;
using CMS.Carrier;
using System.Data.SqlClient;
using EntityFramework.Utilities;
using Carrier.PushNotification;
using System.Data.Entity.Validation;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using PagedList;

namespace Carrier.CMS.Controllers
{
    //[Authorize(Roles = PublicConstant.ROLE_USERS)]
    //[Localization("en")]
    [Compress]
    public class HomeController : Controller
    {
        private Carrier3Entities db = new Carrier3Entities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public async Task<JsonResult> SendMessage(string phone, string fullname, string content)
        {
            try
            {
                string message = fullname + "\n" + phone + "\n" + content;
                //TelegramHelper.SendMessage("123");
                await TelegramHelper.sendMessageTele(message);
                string mess = "success";
                return Json(mess, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string mess = "error";
                return Json(mess, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Index()
        {

            List<Articles> listHomeService = listHomeService = db.Articles.Where(x => (x.Status == (int)CMS_STATUS.ACTIVE) && (x.Category.ParentId == 4)).OrderByDescending(o => o.Id).ToList();
            ViewBag.HomeService = listHomeService;

            Articles thiTruongVanTai = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 11).OrderByDescending(o => o.Id).Take(1).SingleOrDefault();
            Articles thucTrang = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 12).OrderByDescending(o => o.Id).Take(1).SingleOrDefault();
            Articles giaiPhap = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 13).OrderByDescending(o => o.Id).Take(1).SingleOrDefault();

            ViewBag.thiTruongVanTai = thiTruongVanTai;
            ViewBag.thucTrang = thucTrang;
            ViewBag.giaiPhap = giaiPhap;

            ViewBag.LatestNew = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 6).OrderByDescending(o => o.Id).Take(4).ToList();

            List<Articles> listArticle = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 6).OrderByDescending(o => o.Id).Take(10).ToList();
            if (listArticle.Count > 4)
            {
                listArticle.RemoveRange(0, 4);
            }
            ViewBag.ArticleOlder = listArticle;
            //  ViewBag.ArticleOlder = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 6).OrderByDescending(o => o.Id).Skip(4).Take(6).ToList(); //

            var listCar = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            IEnumerable<SelectListItem> listCarSelect = listCar.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Value.ToString() });
            if (listCarSelect.Count() > 0)
            {
                ViewBag.ListCar = listCarSelect;
            }
            else
            {
                ViewBag.ListCar = null;
            }

            return View();
        }

        public ActionResult IndexFrontEnd()
        {
            return View();
        }

        public ActionResult About()
        {
            HtmlContent htmlContent = db.HtmlContent.Where(x => x.PageId == 1).FirstOrDefault();
            return View(htmlContent);
        }
        public ActionResult Services(int? page = null)
        {
            var lstServices = db.Articles.Include(x => x.Category).Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 6).ToList();
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            var modelPageList = lstServices.ToPagedList(pageNumber, pageSize);
            return View(modelPageList);
        }
        public ActionResult DichVuVanChuyen()
        {
            HtmlContent htmlCT = db.HtmlContent.Where(x => x.PageId == 2).FirstOrDefault();
            return View(htmlCT);
        }
        public ActionResult TermOfUse()
        {
            var termofuse = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.Id == 41).FirstOrDefault();
            return View(termofuse);
        }
        public ActionResult PrivacyPolicy()
        {
            var privacypoly = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.Id == 42).FirstOrDefault();
            return View(privacypoly);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult RegisterDriver()
        {
            var listCar = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords();
            IEnumerable<SelectListItem> listCarSelect = listCar.Select(x => new SelectListItem() { Text = x.Name.Trim(), Value = x.Value.ToString() });
            if (listCarSelect.Count() > 0)
            {
                ViewBag.ListCar = listCarSelect;
            }
            else
            {
                ViewBag.ListCar = null;
            }
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> RegisterDriver(FormCollection form)
        {
            #region Upload Image
            var front_Image = "";
            var back_Image = "";

            ViewBag.UName = form.Get("UserName") != null ? form.Get("UserName") : "";
            ViewBag.Password = form.Get("Password") != null ? form.Get("Password") : "";
            ViewBag.Email = form.Get("Email") != null ? form.Get("Email") : "";
            ViewBag.Name = form.Get("Name") != null ? form.Get("Name") : "";
            ViewBag.Address = form.Get("Address") != null ? form.Get("Address") : "";
            ViewBag.Phone = form.Get("Phone") != null ? form.Get("Phone") : "";
            ViewBag.Sex = form.Get("Sex") != null ? form.Get("Sex") : "";
            ViewBag.BirthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";

            if (Request.Files.Count > 0)
            {
                string publicRootPath = WebConfigurationManager.AppSettings["ImageDrivers"].ToString();
                HttpPostedFileBase fileTruoc = Request.Files[0];
                HttpPostedFileBase fileSau = Request.Files[1];
                string filename1 = ViewBag.UName + "image_front" + fileTruoc.FileName;
                string filename2 = ViewBag.UName + "image_back" + fileSau.FileName;

                string message = "";
                //message = UploadImageDriver(fileTruoc, ref front_Image);
                if (fileTruoc.ContentLength > 0)
                {
                    front_Image = publicRootPath + filename1;
                    UploadImage(ref message, fileTruoc, filename1, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                }


                if (message.Length > 0)
                {
                    TempData["error"] = "Lỗi upload ảnh mặt trước";
                    return View();
                }

                if (fileSau.ContentLength > 0)
                {
                    back_Image = publicRootPath + filename2;
                    UploadImage(ref message, fileSau, filename2, PublicConstant.DRIVER_IMAGES, PublicConstant.DRIVER_IMAGES, null);
                }
                //message = UploadImageDriver(fileSau, ref back_Image);
                if (message.Length > 0)
                {
                    TempData["error"] = "Lỗi upload ảnh mặt sau";
                    return View();
                }
            }
            #endregion

            Driver driver = new Driver();
            try
            {
                if (ModelState.IsValid)
                {
                    string userName = form.Get("UserName") != null ? form.Get("UserName") : "";
                    string password = form.Get("Password") != null ? form.Get("Password") : "";
                    string email = form.Get("Email") != null ? form.Get("Email") : "";
                    var name = form.Get("Name") != null ? form.Get("Name") : "";
                    var cmnd = form.Get("CMND") != null ? form.Get("CMND") : "";
                    var address = form.Get("Address") != null ? form.Get("Address") : "";
                    var phone = form.Get("Phone") != null ? form.Get("Phone") : "";
                    var sex = form.Get("Sex") != null ? form.Get("Sex") : "";
                    var birthDay = form.Get("BirthDay") != null ? form.Get("BirthDay") : "";

                    var user = new ApplicationUser { UserName = userName + ":carrier", Email = email, LockoutEnabled = true };
                    UserInfo userInfo = new UserInfo();
                    userInfo.Address = address;
                    userInfo.MobilePhone = phone;
                    userInfo.FullName = name;
                    userInfo.CMND = cmnd;
                    userInfo.ApplicationUserID = user.Id;
                    userInfo.Sex = sex == "Nữ" ? false : true;
                    userInfo.Status = PublicConstant.STATUS_ACTIVE;
                    userInfo.Updated_At = DateTime.Now;
                    userInfo.LastLogin = DateTime.Now;
                    userInfo.Created_At = DateTime.Now;
                    userInfo.OrganzationId = 1163;
                    user.UsersInfo = userInfo;
                    var result = await UserManager.CreateAsync(user, password);
                    if (result.Succeeded)
                    {
                        var userDriver = await UserManager.FindByIdAsync(userInfo.ApplicationUserID);
                        userDriver.LockoutEnabled = false;
                        UserManager.Update(userDriver);
                        result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_DRIVERS);
                        if (result.Succeeded)
                        {
                            #region Thêm thông tin tài xế
                            driver.Name = name;
                            driver.Address = address;
                            driver.Phone = phone;
                            driver.Sex = sex;
                            driver.BirthDay = birthDay;
                            driver.OrganizationId = 1163;

                            double lat = 0, lng = 0;
                            string message = "";
                            string latLongFrom = GetLatLongByAddress(ref message, address);
                            if (message.Length == 0)
                            {
                                lat = double.Parse(latLongFrom.Split(':')[0]);
                                lng = double.Parse(latLongFrom.Split(':')[1]);
                            }
                            driver.Lat = lat;
                            driver.Lng = lng;
                            driver.UserId = user.Id;
                            driver.Status = form.Get("Status") != null ? int.Parse(form.Get("Status")) : 0;
                            driver.Created_At = DateTime.Now;
                            driver.Updated_At = DateTime.Now;
                            driver.Front_Image = front_Image;
                            driver.Back_Image = back_Image;

                            _unitOfWork.GetRepositoryInstance<Driver>().Add(driver);
                            //_unitOfWork.SaveChanges();
                            #endregion


                            #region Thêm xe cho tài xế
                            Car car = new Car();
                            try
                            {
                                car.License = form.Get("License") != null ? form.Get("License") : "";
                                Car checkCar = _unitOfWork.GetRepositoryInstance<Car>().GetFirstOrDefaultByParameter(x => x.License.Equals(car.License));
                                if (checkCar == null)
                                {
                                    car.Width_Size = form.Get("txtWidth") != null ? double.Parse(form.Get("txtWidth").ToString().Replace(".", ",")) : 0;
                                    car.Height_Size = form.Get("txtHeight") != null ? double.Parse(form.Get("txtHeight").ToString().Replace(".", ",")) : 0;
                                    car.Leng_Size = form.Get("txtLenght") != null ? double.Parse(form.Get("txtLenght").ToString().Replace(".", ",")) : 0;
                                    var payload = form.Get("Payload") != null ? form.Get("Payload") : "";
                                    car.Address = form.Get("AddressCar");
                                    if (form.Get("hfLat") != null)
                                    {
                                        car.Lat = double.Parse(form.Get("hfLat").ToString().Replace(".", ","));
                                    }
                                    if (form.Get("hfLng") != null)
                                    {
                                        car.Lng = double.Parse(form.Get("hfLng").ToString().Replace(".", ","));
                                    }
                                    //car.ContainerSize = form.Get("ContainerSize") != null ? form.Get("ContainerSize") : "";
                                    car.Description = form.Get("Description") != null ? form.Get("Description") : "";
                                    car.Created_At = DateTime.Now;
                                    car.Created_By = User.Identity.GetUserId();
                                    car.Updated_By = User.Identity.GetUserId();
                                    car.Updated_At = DateTime.Now;
                                    car.Payload = double.Parse(payload.Replace(".", ","));
                                    car.OrganizationId = GetCurrentOrganizatinIdByUserName(User.Identity.GetUserName());
                                    _unitOfWork.GetRepositoryInstance<Car>().Add(car);

                                    _unitOfWork.SaveChanges();
                                    TempData["info"] = "Đăng ký thành công!";
                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    TempData["Message"] = "Xe đã tồn tại!";
                                    return RedirectToAction("Create", "Home");
                                }
                            }
                            catch (DbEntityValidationException e)
                            {
                                foreach (var eve in e.EntityValidationErrors)
                                {
                                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                            ve.PropertyName, ve.ErrorMessage);
                                    }
                                }
                                throw;
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi Đăng ký: " + ex.Message;
            }
            TempData["error"] = "Đăng ký không thành công:Tên đăng nhập đã tồn tại";
            return View(driver);
        }

        public async Task<ActionResult> RegisterEnterprise()
        {
            ViewBag.RoleId1 = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            ViewBag.RoleId = new SelectList(new List<SelectListItem>
            {
                new SelectListItem {Text = "Enterprise", Value = "Enterprise"},
                new SelectListItem {Text = "Forwarder", Value ="Forwarder"},
            }, "Value", "Text");
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> RegisterEnterprise(UserFronEndViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = userViewModel.UserName, Email = userViewModel.Email, LockoutEnabled = true };

                UserInfo userInfo = new UserInfo();
                userInfo.Address = userViewModel.Address;
                userInfo.MobilePhone = userViewModel.MobilePhone;
                userInfo.FullName = userViewModel.FullName;
                userInfo.ApplicationUserID = user.Id;
                userInfo.Sex = false;
                userInfo.Status = PublicConstant.STATUS_ACTIVE;
                userInfo.Updated_At = System.DateTime.Now;
                userInfo.LastLogin = System.DateTime.Now;
                userInfo.Created_At = System.DateTime.Now;

                string message = "";
                if (userViewModel.Address.Length > 0)
                {
                    string latLongFrom = GetLatLongByAddress(ref message, userViewModel.Address);
                    if (message.Length == 0)
                    {
                        userInfo.Lat = double.Parse(latLongFrom.Split(':')[0]);
                        userInfo.Lng = double.Parse(latLongFrom.Split(':')[1]);
                    }
                }
                user.UsersInfo = userInfo;

                var result = await UserManager.CreateAsync(user, userViewModel.PassWord);
                if (result.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                    }
                    else
                    {
                        result = await UserManager.AddToRolesAsync(user.Id, PublicConstant.ROLE_USERS);
                    }
                    if (result.Succeeded)
                    {
                        TempData["info"] = "Đăng ký thành công!";
                        return RedirectToAction("Index");
                    }
                }
            }
            return View();
        }

        public int GetCurrentOrganizatinIdByUserName(string userName)
        {
            Carrier3Entities db = new Carrier3Entities();
            int organizationId = 0;
            try
            {
                //organizationId
                var user = (from a in db.AspNetUsers join b in db.UserInfoes on a.Id equals b.ApplicationUserID where a.UserName == userName select new { b.OrganzationId, b.Id }).Single();
                if (user.OrganzationId != 0)
                {
                    organizationId = user.OrganzationId;
                }
                else
                {
                    organizationId = user.Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }

        public string GetLatLongByAddress(ref string message, string address)
        {
            string result = "";
            try
            {
                var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);

                var latitude = point.Latitude;
                var longitude = point.Longitude;
                result = latitude.ToString() + ":" + longitude.ToString();
            }
            catch (Exception ex)
            {
                message = "Lỗi lấy lat lng address: " + ex.Message;
            }
            return result;
        }

        private void UploadImage(ref string message, HttpPostedFileBase originalImage, string imageName, string rootPathOriginal, string rootPathThumbNail, HttpServerUtilityBase server)
        {
            int MaxContentLength = 1024 * 1024 * 2; //Size = 1 MB  
            if (imageName.LastIndexOf('.') < 0)
            {
                message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                return;
            }

            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
            var ext = imageName.Substring(imageName.LastIndexOf('.'));
            var extension = ext.ToLower();
            if (!AllowedFileExtensions.Contains(extension))
            {

                message = string.Format("Định dạng ảnh upload .jpeg,.jpg,.gif,.png.");
                return;
            }
            else if (originalImage.ContentLength > MaxContentLength)
            {

                message = string.Format("Dung lượng file vượt quá 1 mb.");
                return;
            }
            try
            {
                bool existsOriginal = Directory.Exists(PublicConstant.IMAGE_PAHT_TEMP);
                if (!existsOriginal)
                {
                    Directory.CreateDirectory(PublicConstant.IMAGE_PAHT_TEMP);
                }
                originalImage.SaveAs(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                WebImage img1 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                img1.Resize(849, 320);
                bool exists = Directory.Exists(rootPathOriginal);
                if (!exists)
                    Directory.CreateDirectory(rootPathOriginal);
                img1.Save(System.IO.Path.Combine(rootPathOriginal + "/" + imageName));

                WebImage img2 = new WebImage(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);
                img2.Resize(274, 175);
                bool exists2 = Directory.Exists(rootPathThumbNail);
                if (!exists2)
                    Directory.CreateDirectory(rootPathThumbNail);
                img2.Save(System.IO.Path.Combine(rootPathThumbNail + "/" + imageName));

                System.IO.File.Delete(PublicConstant.IMAGE_PAHT_TEMP + "/" + imageName);

                #region save
                //bool existsOriginal = System.IO.Directory.Exists(server.MapPath(Constant.IMAGE_PAHT_TEMP));
                //if (!existsOriginal)
                //{
                //    System.IO.Directory.CreateDirectory(server.MapPath(Constant.IMAGE_PAHT_TEMP));
                //}
                //originalImage.SaveAs(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));

                //WebImage img1 = new WebImage(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                //img1.Resize(849, 320);
                //bool exists = System.IO.Directory.Exists(server.MapPath(rootPathOriginal));
                //if (!exists)
                //    System.IO.Directory.CreateDirectory(server.MapPath(rootPathOriginal));
                //img1.Save(Path.Combine(server.MapPath(rootPathOriginal + "/" + imageName)));

                //WebImage img2 = new WebImage(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                //img2.Resize(274, 175);
                //bool exists2 = System.IO.Directory.Exists(server.MapPath(rootPathThumbNail));
                //if (!exists2)
                //    System.IO.Directory.CreateDirectory(server.MapPath(rootPathThumbNail));
                //img2.Save(Path.Combine(server.MapPath(rootPathThumbNail + "/" + imageName)));

                //System.IO.File.Delete(server.MapPath(Constant.IMAGE_PAHT_TEMP + "/" + imageName));
                #endregion
            }
            catch (Exception ex)
            {
                message = "Lỗi upload ảnh: " + ex.Message;
            }
        }

        public string CheckUserName(string input)
        {
            List<string> driverUsers = new List<string>();
            string driverName = input.Trim() + ":carrier";
            var users = UserManager.Users.ToList();
            bool ifuser = false;
            foreach (var u in users)
            {
                string uname = u.UserName;
                if (uname.Equals(driverName))
                {
                    ifuser = true;
                    break;
                }
            }

            if (ifuser == false)
            {
                return "Available";
            }

            if (ifuser == true)
            {
                return "Not Available";
            }

            return "";
        }

        public JsonResult GetOnliner()
        {
            //var i = Membership.GetNumberOfUsersOnline().ToString();
            var i = 0;
            return Json(i, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPriceAPI(string addressApi, string address, string lat, string lng, string strOrderCar, string newdistance, string origirndistance, string newtime, string origirntime, string arrayDistance)
        {
            try
            {
                string message = "";
                string totalPriceNoVat = "";
                double price = 0;
                //double distance = 0;
                //double totalPrice = 0;
                string type = "";
                int quanlity = 0;

                var arratAddressApi = addressApi.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayAddress = address.Split(new[] { "|" }, StringSplitOptions.None);
                var _arrayDistance = arrayDistance.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLat = lat.Split(new[] { "|" }, StringSplitOptions.None);
                var arrayLng = lng.Split(new[] { "|" }, StringSplitOptions.None);
                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                List<Location> listLC = new List<Location>();
                List<string> ListAddress = new List<string>();
                for (int i = 0; i < arrayLat.Length; i++)
                {
                    Location lc = new Location();
                    arrayLat[i] = arrayLat[i].Replace(".", ",");
                    arrayLng[i] = arrayLng[i].Replace(".", ",");
                    double Lat = Convert.ToDouble(arrayLat[i]);
                    double Lng = Convert.ToDouble(arrayLng[i]);

                    lc.Lat = Lat;
                    lc.Lng = Lng;
                    ListAddress.Add(arrayAddress[i]);
                    listLC.Add(lc);
                }

                float newDistance = newdistance != null ? float.Parse(newdistance) : 0;
                float origirnDistance = origirndistance != null ? float.Parse(origirndistance) : 0;
                long newTime = newtime != null ? long.Parse(newtime) : 0;
                long origirnTime = origirntime != null ? long.Parse(origirntime) : 0;

                string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();

                if (SysLogisticValidate.IsValid(newDistance, origirnDistance, newTime, origirnTime, listLC, ListAddress))
                {
                    if (strCarTypeSelect.Length > 0)
                    {
                        double priceOne = 0;
                        foreach (string item in strCarTypeSelect)
                        {
                            //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                            type = item.Split('-')[0];
                            //quanlity = int.Parse(item.Split(':')[1]);
                            quanlity = 1;
                            priceOne = 0;
                            if (quanlity > 0)
                            {
                                //priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ad, listLC, arrayAddress, out message);
                                priceOne = SysLogistic.GetPrice(type, origirnDistance / 1000, ListAddress, arrayAddress, arratAddressApi, out message);
                                if (priceOne > 0)
                                {
                                    price = price + (priceOne * quanlity);
                                }
                            }
                            if (message.Length > 0)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        // price = SysLogistic.GetPrice(strCarTypeSelect[0], origirnDistance / 1000, ad, listLC, arrayAddress, out message);
                    }

                    if (message.Length == 0 && price > 0)
                    {
                        price = GetDiscount(price);
                        string p = price.ToString("#,###");
                        return Json(new { distance = newDistance, totalPrice = p, oldPrice = totalPriceNoVat, message = "Tính giá thành công" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                        return Json(new { message = "Lỗi tính giá vận đơn:" + message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //TempData["error"] = "Đường đi không phù hợp vì vượt quá thời gian hoặc quá dài: " + message;
                    return Json(new { message = "Đường đi không phù hợp vì vượt quá thời gian hoặc quá dài" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public double GetDiscount(double Gia)
        {
            double discount = Gia;

            //select from database ...
            //if (Gia > 1000000) return Gia * 0.9;
            //if (Gia > 300000 && Gia < 1000000) return Gia * 0.95;

            //return Gia * 0.9; // trừ đi 10 %
            return Gia;
        }
        public ActionResult Order(string id = null)
        {
            var _message = new List<string>();
            ViewBag.Lat = "20.9978737";
            ViewBag.Lng = "105.795412";
            if (id != null)
            {
                //Order
                var _order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.MaVanDon == id);

                if (_order != null)
                {
                    ViewBag.Order = _order;
                    ViewBag.TrangThai = "Tạo mới";

                    var _orderTracking = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(ot => ot.OrderId == _order.Id);
                    if (_orderTracking != null)
                    {
                        ViewBag.MaTrangThai = _orderTracking.Status;
                        string strResult = string.Empty;
                        switch (_orderTracking.Status)
                        {
                            case 1:
                                {
                                    strResult = @"<span style='color:yellowgreen' > Đã khớp lệnh</span>";
                                    break;
                                }
                            case 2:
                                {
                                    strResult = @"<span style='color:red' > Tài xế hủy</span>";
                                    break;
                                }
                            case 3:
                                {
                                    strResult = @"<span style='color:red' > Chủ hàng hủy</span>";
                                    break;
                                }
                            case 4:
                                {
                                    strResult = @"<span style='color:yellowgreen' > Đang vận chuyển</span>";
                                    break;
                                }
                            case 5:
                                {
                                    strResult = @"<span style='color:blue' > Hoàn thành</span>";
                                    break;
                                }
                            case 6:
                                {
                                    strResult = @"<span style='color:red' > Gửi tài xế lỗi</span>";
                                    break;
                                }
                            case 7:
                                {
                                    strResult = @"<span style='color:red' > Đơn hết hạn</span>";
                                    break;
                                }
                            case 0:
                                {
                                    if (_order.Status == 0)
                                    {
                                        if (_order.ThoiGianDi != null && _order.ThoiGianDi < DateTime.Now)
                                        {
                                            strResult = @"<span style=""color:red"">Đơn hết hạn</span>";
                                        }
                                        else
                                        {
                                            strResult = @"<span style='color:dodgerblue' > Đang khớp lệnh</span>";
                                        }
                                    }
                                    else
                                    {
                                        strResult = @"<span style='color:dodgerblue' > Đang khớp lệnh</span>";
                                    }
                                    break;
                                }
                            default:
                                {

                                    break;
                                }
                        }
                        ViewBag.TrangThai = strResult;
                        if (_orderTracking.DriverId.Length > 0)
                        {
                            var _driver = _unitOfWork.GetRepositoryInstance<Driver>().GetFirstOrDefaultByParameter(dr => dr.UserId == _orderTracking.DriverId);
                            if (_driver != null)
                            {
                                var _carLicense = (from dc in db.DriverCar
                                                   join dr in db.Driver
                                                   on dc.DriverId equals dr.Id
                                                   join c in db.Car
                                                   on dc.CarId equals c.Id
                                                   where dr.UserId == _driver.UserId
                                                   select c.License).ToList();
                                ViewBag.CarLicense = _carLicense[0];
                                ViewBag.DrOrigin = _driver;
                            }

                        }
                    }
                    else
                    {
                    }

                    long orderId = long.Parse(id);
                    var listDiemTraHang = _unitOfWork.GetRepositoryInstance<OrderPlaces>().GetListByParameter(x => x.OrderId == _order.Id).OrderBy(o => o.Id).ToList();
                    ViewBag.ListDiemTraHang = listDiemTraHang;

                }
                else
                {
                    _message.Add("Không tồn tại vận đơn");
                }

            }
            else
            {
                _message.Add("Không có vận đơn nào được yêu cầu");
            }
            if (_message.Count > 0)
            {
                return Content(String.Join("|", _message));
            }
            return View();
        }
        public ActionResult FinishOrder(string diemdi, string diemden, string loaixe, string tongtien, string latFrom, string lngFrom, string latTo, string lngTo)
        {
            try
            {

                string appId = User.Identity.GetUserId();
                UserInfoes userInfo = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID == appId);
                if (userInfo != null)
                {
                    ViewBag.Lat = userInfo.Lat != null ? userInfo.Lat : 21.033781;
                    ViewBag.Lng = userInfo.Lng != null ? userInfo.Lng : 105.814054;
                }
                else
                {
                    ViewBag.Lat = 21.033781;
                    ViewBag.Lng = 105.814054;
                }

                List<CarType> listCarType = _unitOfWork.GetRepositoryInstance<CarType>().GetAllRecords().ToList();
                Session.Add("LIST_CAR", listCarType);
                ViewBag.diemdi = diemdi;
                ViewBag.diemden = diemden;
                ViewBag.loaixe = loaixe;
                ViewBag.tongtien = tongtien;
                ViewBag.latFrom = latFrom;
                ViewBag.lngFrom = lngFrom;
                ViewBag.latTo = latTo;
                ViewBag.lngTo = lngTo;

            }
            catch (Exception)
            {
            }
            //Order order = new Carrier.Models.Entities.Order();
            return PartialView("CreateOrder");
            //return View("");
        }
        /// <summary>
        /// Tính giá theo file excel mới
        /// </summary>
        /// <param name="fromaddress"></param>
        /// <param name="toaddress"></param>
        /// <param name="isVAT"></param>
        /// <param name="strOrderCar"></param>
        /// <returns></returns>
        public JsonResult GetPrice(string fromaddress, string toaddress, double latfrom, double lnfrom, double latto, double lnto, bool isVAT,
        string strOrderCar)
        {
            try
            {

                string message = "";
                double distance = 0;
                double totalPrice = 0;
                string totalPriceNoVat = "";
                double price = 0;
                string type = "";
                int quanlity = 0;

                //str dropdowlist cartype select return  example: 1_25-1:0|1_4-2:0|2_5-3:0|3_5-4:0|5-5:0|8-6:0|10-7:0|15-8:0|18-9:0|20-10:0|40-11:0|
                string[] strCarTypeSelect = strOrderCar.TrimEnd('|').Replace("_", ".").Split('|').ToArray();
                foreach (var item in strCarTypeSelect)
                {
                    //1.25-1:0 -> 1.25: car type, 1: Id of cartype recored in db, 0: value select 0: not select 
                    type = item.Split('-')[0];
                    quanlity = int.Parse(item.Split(':')[1]);
                    if (quanlity > 0)
                    {
                        price = 0;// import.GetPriceForOrder(type, fromaddress, toaddress, latfrom, lnfrom, latto, lnto, ref message, ref distance);
                        if (price > 0)
                        {
                            totalPrice = totalPrice + (price * quanlity);
                        }
                    }
                }

                totalPriceNoVat = totalPrice.ToString("#,###");

                if (message.Length == 0)
                {
                    string priceValue = "";
                    if (isVAT)
                    {
                        //totalPrice = totalPrice * 1.1;
                        totalPrice = GetDiscount(totalPrice);
                        priceValue = totalPrice.ToString("#,###");
                    }
                    else
                    {
                        totalPrice = GetDiscount(totalPrice);
                        priceValue = totalPrice.ToString("#,###");
                    }
                    return Json(new { distance = distance, totalPrice = priceValue, oldPrice = totalPriceNoVat, message = "tính giá thành công" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["error"] = "Lỗi tính giá vận đơn: " + message;
                    return Json(new { message = "Lỗi tính giá vận đơn:" + message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi tính giá vận đơn: " + ex.Message;
                return Json(new { message = "Lỗi tính giá vận đơn: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Roles(PublicConstant.ROLE_SUPPER_ADMIN, PublicConstant.ROLE_ADMIN, PublicConstant.ROLE_ENTERPRISE, PublicConstant.ROLE_FORWARDER, PublicConstant.ROLE_USERS)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateOrder(FormCollection formCollection, string hdfStrCarTypeSelect)
        {
            //SysLogisticValidate;
            Order order = new Order();
            Order orderTemp = new Order();
            try
            {
                string fromLocation = formCollection["txtFromLocation"] != null ? formCollection["txtFromLocation"].ToString() : "";
                string toLocation = formCollection["txtToLocation"] != null ? formCollection["txtToLocation"].ToString() : "";
                string phoneNumber = formCollection["PhoneNumber"] != null ? formCollection["PhoneNumber"].ToString() : "";

                double soKmUocTinh = 0;
                try
                {
                    soKmUocTinh = formCollection["hdfSoKmUocTinh"] != null ? double.Parse(formCollection["hdfSoKmUocTinh"].ToString().Replace(".", ",")) : 0;
                }
                catch
                {
                }

                double price = 0;
                try
                {
                    price = formCollection["Gia"] != null ? double.Parse(formCollection["Gia"].ToString().Replace(".", "")) : 0;
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Lỗi tính giá đơn hàng: " + ex.Message;
                    return View(order);
                }

                if (price <= 0)
                {
                    TempData["error"] = "Lỗi tính giá đơn hàng";
                    return View(order);
                }

                order.TenHang = "";
                order.DienThoaiLienHe = formCollection["PhoneNumber"] != null ? formCollection["PhoneNumber"].ToString() : "";
                order.NguoiLienHe = formCollection["PhoneNumber"] != null ? formCollection["PhoneNumber"].ToString() : "";
                order.Gia = price;
                order.Status = PublicConstant.STATUS_PENDING;
                order.Note = formCollection["Mota"] != null ? formCollection["Mota"].ToString() : "";
                order.DiemDiChiTiet = fromLocation;
                order.DiemDenChiTiet = toLocation;

                order.UserName = User.Identity.Name;
                order.LoaiThanhToan = 0;
                order.MaVanDon = Ultilities.RandomNumber(8);
                order.CreateAt = DateTime.Now;
                order.UpdateAt = DateTime.Now;

                try
                {
                    order.FromLat = formCollection["hfLatFrom"] != null ? double.Parse(formCollection["hfLatFrom"].ToString().Replace(".", ",")) : 0;
                    order.FromLng = formCollection["hfLngFrom"] != null ? double.Parse(formCollection["hfLngFrom"].ToString().Replace(".", ",")) : 0;

                    order.ToLat = formCollection["hfLatTo"] != null ? double.Parse(formCollection["hfLatTo"].ToString().Replace(".", ",")) : 0;
                    order.ToLng = formCollection["hfLngTo"] != null ? double.Parse(formCollection["hfLngTo"].ToString().Replace(".", ",")) : 0;
                }
                catch
                {
                    order.FromLat = 0;
                    order.FromLng = 0;

                    order.ToLat = 0;
                    order.ToLng = 0;
                }

                if (fromLocation.Length > 0 && toLocation.Length > 0)
                {
                    order.SoKmUocTinh = soKmUocTinh;
                }
                else
                {
                    order.SoKmUocTinh = 0;
                }

                order.Width = 0;
                order.Height = 0;
                order.Lenght = 0;
                order.VAT = true;
                order.TrongLuong = 0;
                order.Created_By = User.Identity.GetUserId();

                if (ModelState.IsValid)
                {
                    try
                    {
                        using (var db = new Carrier3Entities())
                        {
                            orderTemp = order;
                            db.Order.Add(order);
                            #region insert OrderCar table
                            int typeCarId = 0;
                            OrderCar orderCar;
                            string typeCar = formCollection["Payload"] != null ? formCollection["Payload"].ToString() : "";
                            CarType cartype = db.CarType.Where(o => o.Value.Equals(typeCar)).SingleOrDefault();
                            if (cartype != null)
                            {
                                typeCarId = cartype.Id;
                            }
                            if (typeCarId > 0)
                            {
                                orderCar = new OrderCar();
                                orderCar.OrderId = order.Id;
                                orderCar.CarTypeId = typeCarId;
                                orderCar.Number = 1;
                                db.OrderCar.Add(orderCar);
                            }
                            else
                            {
                                TempData["error"] = "Lỗi thêm loại xe";
                                return View(order);
                            }

                            db.SaveChanges();

                            #endregion

                            if (order.Status == 1 || User.IsInRole(PublicConstant.ROLE_FORWARDER))
                            {
                                var nearDriver = GetDriverNearOrder(order.Id, PublicConstant.DEFAULT_DISTANCE_SEARCH);
                                if (nearDriver != null)
                                {

                                    PushMessageForDriver(nearDriver.UserId, order.Id);
                                }
                            }

                            // thêm số điện thoại và thông tin đơn hàng và nút tạo đơn khi bấm nút tạo đơn thì mình sẽ tạo 1 tk là số đt và pass cũng là số đó luôn cho khách đồng thời tạo đơn hàng đó lên admin và trên admin sẽ có chuông
                            #region tạo tài khoản
                            string dienthoai = formCollection["PhoneNumber"] != null ? formCollection["PhoneNumber"].ToString() : "";
                            if (dienthoai != "")
                            {
                                try
                                {
                                    var user = new ApplicationUser { UserName = dienthoai, Email = dienthoai };
                                    UserInfo userInfo = new UserInfo();
                                    userInfo.Address = "";
                                    userInfo.MobilePhone = dienthoai;
                                    userInfo.FullName = dienthoai;
                                    userInfo.ApplicationUserID = user.Id;
                                    userInfo.Sex = false;
                                    userInfo.Status = PublicConstant.STATUS_ACTIVE;
                                    userInfo.Updated_At = DateTime.Now;
                                    userInfo.LastLogin = System.DateTime.Now;
                                    userInfo.Created_At = DateTime.Now;
                                    userInfo.Lat = 0;
                                    userInfo.Lng = 0;
                                    user.UsersInfo = userInfo;
                                    Organization organ = new Organization();
                                    organ.Name = userInfo.FullName;
                                    organ.Address = userInfo.Address;
                                    organ.UserId = user.Id;
                                    organ.Phone = userInfo.MobilePhone;
                                    _unitOfWork.GetRepositoryInstance<Organization>().Add(organ);
                                    _unitOfWork.SaveChanges();
                                    var result = await UserManager.CreateAsync(user, dienthoai);

                                    if (result.Succeeded)
                                    {
                                        UserManager.AddToRole(user.Id, PublicConstant.ROLE_USERS.ToString());
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                            #endregion
                        }
                        string message = "Điểm đi: " + fromLocation +"\nĐiểm đến: "+toLocation+"\nSđt: "+phoneNumber;
                        await TelegramHelper.sendMessageTele(message);
                        TempData["info"] = "Thêm vận đơn thành công!";
                    }
                    catch (Exception)
                    {
                        TempData["info"] = "Lỗi xảy ra khi thêm vận đơn!";
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm vận đơn: " + ex.Message;
                return View(order);
            }
            return View(order);
        }
        /// <summary>
        /// Get Driver near the order
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private SearchDriverNearOrder_Result GetDriverNearOrder(long orderId, float distance)
        {
            var sqlOrderId = new SqlParameter("@orderId", System.Data.SqlDbType.BigInt) { Value = orderId };
            //var sqlDistance = new SqlParameter("@distance", System.Data.SqlDbType.Float) { Value = distance };
            var lstSearch = _unitOfWork.GetRepositoryInstance<SearchDriverNearOrder_Result>().GetResultBySqlProcedure("SearchDriverNearOrder @orderId", sqlOrderId).FirstOrDefault();
            return lstSearch;
        }

        /// <summary>
        /// Push message to Driver by Push Notification
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="order"></param>
        public void PushMessageForDriver(string driverId, long orderId)
        {
            PushCompletedCallBack callback = PushNotificationCallBack;
            string jsonMessage = "{\"aps\":{\"badge\":1,\"alert\":\"Bạn có vận đơn mới\",\"sound\":\"default\"},\"OrderId\":\"" + orderId + "\"}";
            DevicesPush device = db.DevicesPush.Where(x => x.UserId.Equals(driverId)).FirstOrDefault();
            if (device == null)
            {
                TempData["info"] = "Tài xế chưa kích hoạt quyền nhận thông báo của thiết bị hoặc lỗi khi đăng ký tài khoản!";
                var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(driverId));
                if (old != null)
                {
                    old.Updated_At = DateTime.Now;
                    old.Status = PublicConstant.ORDER_CANNOT_SEND;
                    _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                    _unitOfWork.SaveChanges();
                }
                return;
            }
            if (device.Platform.ToLower().Equals("ios"))
            {
                PushServices.strFileP12 = PublicConstant.IOS_CERTIFICATE_PATH_PRODUCT;
                PushServices.SetupPushAPN(true);
                PushServices.PushAPNMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
            else
            {
                jsonMessage = "{\"message\":\"Bạn có vận đơn mới\",\"OrderId\":\"" + orderId + "\"}";
                PushServices.GcmKey = PublicConstant.GCM_KEY;
                PushServices.GcmSenderId = PublicConstant.GCM_SENDER;
                PushServices.SetupPushGCM();
                PushServices.PushGCMMessage(jsonMessage, new List<string> { device.DeviceToken }, driverId, orderId.ToString(), callback);
            }
        }

        public void PushNotificationCallBack(string result, string obj1, string obj2)
        {
            long orderId = long.Parse(obj2);
            var order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(x => x.Id == orderId);
            var old = _unitOfWork.GetRepositoryInstance<OrderTracking>().GetFirstOrDefaultByParameter(x => x.OrderId == orderId && x.DriverId.Equals(obj1));
            if (old == null)
            {
                var obj = new OrderTracking();
                obj.Status = 0;
                obj.Updated_At = DateTime.Now;
                obj.Created_At = DateTime.Now;
                obj.DriverId = obj1;
                obj.OwnerId = order.Created_By;
                obj.OrderId = orderId;
                if (result.Equals("Success"))
                {
                    obj.Status = PublicConstant.ORDER_PENDING;
                    TempData["info"] = "Vận đơn đã được gửi cho tài xế thành công!";
                }
                else
                {
                    obj.Status = PublicConstant.ORDER_CANNOT_SEND;
                    TempData["error"] = "Lỗi không thể gửi được vận đơn. Vui lòng thử lại sau!";

                }
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Add(obj);
                _unitOfWork.SaveChanges();
            }
            else
            {
                old.Status = 0;
                old.Updated_At = DateTime.Now;
                old.DriverId = obj1;
                old.OwnerId = order.Created_By;
                old.OrderId = orderId;
                if (result.Equals("Success"))
                {
                    old.Status = PublicConstant.ORDER_PENDING;
                    TempData["info"] = "Vận đơn đã được gửi cho tài xế thành công!";
                }
                else
                {
                    old.Status = PublicConstant.ORDER_CANNOT_SEND;
                    TempData["error"] = "Lỗi không thể gửi được vận đơn. Vui lòng thử lại sau!";
                }
                _unitOfWork.GetRepositoryInstance<OrderTracking>().Update(old);
                _unitOfWork.SaveChanges();
            }
        }

        public JsonResult CountOrderNew()
        {
            Order order = new Order();
            string output = "";
            try
            {
                List<Order> lstCard = _unitOfWork.GetRepositoryInstance<Order>().GetListByParameter(o => (o.Status == PublicConstant.STATUS_PENDING)).ToList();
                if (lstCard.Count > 0)
                {
                    output = lstCard.Count.ToString();
                }
            }
            catch (Exception)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
    }
}
