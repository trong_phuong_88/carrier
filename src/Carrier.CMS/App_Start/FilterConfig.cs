﻿using CMS.Carrier.Common;
using System.Web.Mvc;

namespace Carrier.CMS
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new CustomRequireHttpsFilter());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new JsonNetFilterAttribute());
        }
    }
}
