﻿using Carrier.CMS.Common;
using System.Web.Mvc;
using System.Web.Routing;

namespace Carrier.CMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add("AboutIndex", new SeoFriendlyRoute("vi/gioi-thieu",
new RouteValueDictionary(new { controller = "Home", action = "About", lang = "vi" }),
new MvcRouteHandler()));
            routes.Add("TinTucIndex", new SeoFriendlyRoute("vi/van-tai-carrier",
 new RouteValueDictionary(new { controller = "Home", action = "Services" }),
 new MvcRouteHandler()));
            routes.Add("TinTucDetail", new SeoFriendlyRoute("vi/van-tai-carrier/{id}",
   new RouteValueDictionary(new { controller = "News", action = "Details" }),
   new MvcRouteHandler()));
            routes.Add("AboutIndex_en", new SeoFriendlyRoute("en/gioi-thieu",
new RouteValueDictionary(new { controller = "Home", action = "About", lang = "en" }),
new MvcRouteHandler()));
            routes.Add("TinTucIndex_en", new SeoFriendlyRoute("en/van-tai-carrier",
 new RouteValueDictionary(new { controller = "Home", action = "Services" }),
 new MvcRouteHandler()));
            routes.Add("TinTucDetail_en", new SeoFriendlyRoute("en/van-tai-carrier/{id}",
   new RouteValueDictionary(new { controller = "News", action = "Details" }),
   new MvcRouteHandler()));
            routes.Add("HomeIndex", new SeoFriendlyRoute("vi/trang-chu",
new RouteValueDictionary(new { controller = "Home", action = "Index", lang = "vi" }),
new MvcRouteHandler()));
            routes.Add("HomeIndex_en", new SeoFriendlyRoute("en/home",
new RouteValueDictionary(new { controller = "Home", action = "Index", lang = "en" }),
new MvcRouteHandler()));
            routes.Add("Home_Service", new SeoFriendlyRoute("vi/dich-vu-van-chuyen",
new RouteValueDictionary(new { controller = "Home", action = "DichVuVanChuyen",lang="vi" }),
new MvcRouteHandler()));
            routes.Add("Home_Service_en", new SeoFriendlyRoute("en/dich-vu-van-chuyen",
new RouteValueDictionary(new { controller = "Home", action = "DichVuVanChuyen", lang = "en" }),
new MvcRouteHandler()));

            routes.MapRoute(
         name: "DefaultLocalized",
         url: "{lang}/{controller}/{action}/{id}",
         constraints: new { lang = @"(\w{2})|(\w{2}-\w{2})" },   // en or en-US
         defaults: new { lang = "vi", controller = "Home", action = "Index", id = UrlParameter.Optional }
         );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}