﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class MessageOrderModel
    {
        public string Id { get; set; }
        public string OrderCode { get; set; }
        public DateTime CreateDate { get; set; }
        public string Url { get; set; }
        public string Create_By { get; set; }
        public int Quanlity { get; set; }

    }

    public class MessageOrderViewModel
    {
        public int Quanlity { get; set; }
        public List<MessageOrderModel> listMessageOrder { get; set; }
    }
}