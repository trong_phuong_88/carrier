﻿using System;
using System.Collections.Generic;

namespace CMS.Carrier.Models
{
    public class MessageNotificationModel
    {
        public string Id { get; set; }
        public string Message { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }
        public DateTime Created_At { get; set; }
        public bool IsView { get; set; }
    }

    public class MessageNotificationViewModel
    {
        public int Quanlity { get; set; }
        public List<MessageNotificationModel> listMessageNotification { get; set; }
    }
}