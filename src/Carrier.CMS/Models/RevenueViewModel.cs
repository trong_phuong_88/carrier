﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Models
{
    public class RevenueViewModel
    {
        public double? Gia { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string UserId { get; set; }
        public int? OrganizationId { get; set; }
        public DateTime? CrateDate { get; set; }
        public int? Status { get; set; }
    }
}