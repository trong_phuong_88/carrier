﻿using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentitySamp.Models
{
    public class SheduleViewModel
    {
        public int Id { get; set; }
        public int Driverid { get; set; }
        public int Carid { get; set; }
        public string Time { get; set; }
        public string Loaded { get; set; }
        public string Code { get; set; }
        public string Location { get; set; }
        public DateTime NgayDi { get; set; }
        public DateTime NgayDen { get; set; }
        public int DiemDiTinh { get; set; }
        public int DiemDenTinh { get; set; }
        public int DiemDiHuyen { get; set; }
        public int DiemDenHuyen { get; set; }
        public int DiemDiXa { get; set; }
        public int DiemDenXa { get; set; }
        public DateTime GioDi { get; set; }
        public DateTime GioDen { get; set; }
        public double TaiTrong { get; set; }
        public double DaCho { get; set; }
        public double ConTrong { get; set; }
        public string UserName { get; set; }

        public Car Car { get; set; }
        public Driver Driver { get; set; }
        public City TINH { get; set; }
    }
}