﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class MessageUserModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public DateTime CreateDate { get; set; }
        public string Url { get; set; }
    }

    public class MessageUserViewModel
    {
        public int Quanlity { get; set; }
        public List<MessageUserModel> listMessageUser { get; set; }
    }

    public class OrderNewPopupModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string MaVanDon { get; set; }
        public DateTime CreateDate { get; set; }
    }
}