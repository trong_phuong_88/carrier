﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utilities;
using Models.Entities;
using static Utilities.GlobalCommon;

namespace IdentitySamp.Models
{
    public class ScheduleSuggetViewModel
    {
        public Schedule Schedule1 { get; set; }
        public Schedule Schedule2 { get; set; }
        public Order Order { get; set; }
        public MatchCondition Condition { get; set; }
        public City CityF { get; set; }
        public District DistrictF { get; set; }
        public Ward WardF { get; set; }
        public City CityT { get; set; }
        public District DistrictT { get; set; }
        public Ward WardT { get; set; }

        public double Gia { get; set; }
        public string ItemId { get; set; }

        // =0 Lịch trình ghép với vận đơn
        // =1 Lịch trình ghép với lịch trình
        public int Type { get; set; }
    }
}