﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Models
{
    public class OrderCarViewModel
    {
        public long Id { get; set; }
        public int CarTypeId { get; set; }
        public string CarTypeName { get; set; }
        public long? OrderId { get; set; }
        public int? Number { get; set; }
        public string Value { get; set; }
    }
}