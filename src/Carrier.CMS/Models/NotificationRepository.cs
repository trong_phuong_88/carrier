﻿using Carrier.CMS.Hubs;
using Carrier.Models.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace Carrier.CMS.Models
{
    public class NotificationRepository
    {
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public IEnumerable<DevicesPush> GetNotifications()
        {
            var devicesPush = new List<DevicesPush>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT [Id] , [TokenDevice] , [Platform] , [SystemVersion] , [RegistedDate] , [AppVersion] , [UserName] FROM [dbo].[DevicesPush]", connection))
                    {
                        command.Notification = null;

                        var dependency = new SqlDependency(command);
                        dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        DevicesPush devicePushItem = new DevicesPush();
                        while (reader.Read())
                        {
                            try
                            {
                                devicePushItem = new DevicesPush();
                                devicePushItem.Id = reader["Id"] != null ? int.Parse(reader["Id"].ToString().Trim()) : 0;

                                devicePushItem.DeviceToken = reader["TokenDevice"] != null ? reader["TokenDevice"].ToString() : "";

                                devicePushItem.Platform = reader["Platform"] != null ? reader["Platform"].ToString() : "";

                                devicePushItem.SystemVersion = reader["SystemVersion"] != null ? reader["SystemVersion"].ToString() : "";

                                devicePushItem.DeviceToken = (reader["RegistedDate"].ToString().Length > 0) ? reader["RegistedDate"].ToString() : DateTime.Now.ToString();

                                devicePushItem.AppVersion = reader["AppVersion"] != null ? reader["AppVersion"].ToString() : "";

                                devicePushItem.UserId = reader["UserId"] != null ? reader["UserId"].ToString() : "";

                                devicesPush.Add(devicePushItem);
                            }
                            catch (System.Exception ex)
                            {
                            }
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {
            }
            return devicesPush;
        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                NotificationHub.SendMessages("");
            }
        }
    }
}