﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Models
{
    public class MessageOrderTrackingModel
    {
        //public string Id { get; set; }
        public long OrderId { get; set; }
        public string DriverName { get; set; }
        public DateTime Create_At { get; set; }
    }

    public class MessageOrderTrackingViewModel
    {
        public int Quanlity { get; set; }
        public List<MessageOrderTrackingModel> listMessageOrderTracking { get; set; }
    }
}