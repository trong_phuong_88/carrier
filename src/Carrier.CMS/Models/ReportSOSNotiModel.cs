﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Models
{
    public class ReportSOSNotiModel
    {
        public string Id { get; set; }
        public string DriverId { get; set; }
        public DateTime Created_At { get; set; }
        public Nullable<int> ReportTypeId { get; set; }
        public string UserId { get; set; }
        public Nullable<long> OrderId { get; set; }
        public string ReportDescription { get; set; }
        public int Quanlity { get; set; }

    }
    public class ReportSOSNotiViewModel
    {
        public int Quanlity { get; set; }
        public List<ReportSOSNotiModel> listReport { get; set; }
    }
}