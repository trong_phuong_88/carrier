﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Common
{
    public class LocationHelper
    {
        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var point = string.Format(CultureInfo.InvariantCulture.NumberFormat,
                                     "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(point, 4326);
        }
        public static string NiceLocation(double latitude, double longitude)
        {
            int latSeconds = (int)Math.Round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.Abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int)Math.Round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.Abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            string strLocation = string.Format("{0}° {1}' {2}\" {3}, {4}° {5}' {6}\" {7}", Math.Abs(latDegrees),
                 latMinutes,
                 latSeconds,
                 latDegrees >= 0 ? "N" : "S",
                 Math.Abs(longDegrees),
                 longMinutes,
                 longSeconds,
                 latDegrees >= 0 ? "E" : "W");
            return strLocation;
        }
    }
}