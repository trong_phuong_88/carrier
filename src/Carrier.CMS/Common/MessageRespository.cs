﻿using Carrier.CMS.Hubs;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using CMS.Carrier.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CMS.Carrier.Common
{
    public class MessageRespository
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        static string dbConnectionSettings = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;
        public MessageRespository()
        {
        }
        /// <summary>
        /// Adds SQLDependency for change notification and passes the information to Student Hub for broadcasting
        /// </summary>
        /// <param name="sqlCommand"></param>
        public static void AddSQLDependency(SqlCommand sqlCommand)
        {
            try
            {
                sqlCommand.Notification = null;

                var dependency = new SqlDependency(sqlCommand);
                dependency.OnChange += (sender, sqlNotificationEvents) =>
                {
                    if (sqlNotificationEvents.Type == SqlNotificationType.Change)
                    {
                        NotificationHub.SendMessages(sqlNotificationEvents.Info.ToString());
                        NotificationHub.SendMessages(sqlNotificationEvents.Info.ToString());
                    }
                };
            }
            catch (Exception ex)
            {
            }
        }

        public MessageOrderViewModel GetListMessageOrder(string userId, string role)
        {
            var lstOrderView = new MessageOrderViewModel();
            var lstOrderRecords = new List<MessageOrderModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();

                    var id = GetCurrentOrganizatinId(userId);
                    string sqlCommandText = "";
                    if (role.Equals(PublicConstant.ROLE_ADMIN))
                    {
                        sqlCommandText = "SELECT  dbo.[Order].Id ,  dbo.[Order].Status , TenHang , UserName , MaVanDon , CreateAt , Created_By FROM    dbo.[Order] " +
                                        " INNER JOIN dbo.UserInfoes ON dbo.[Order].Created_By = dbo.UserInfoes.ApplicationUserID WHERE dbo.[Order].IsViewPopupOrder = 0  ORDER BY Created_At DESC";
                    }
                    else
                    {
                        sqlCommandText = "SELECT  dbo.[Order].Id ,  dbo.[Order].Status , TenHang , UserName , MaVanDon , CreateAt , Created_By FROM    dbo.[Order] " +
                                        " INNER JOIN dbo.UserInfoes ON dbo.[Order].Created_By = dbo.UserInfoes.ApplicationUserID WHERE  OrganzationId = " + id + " AND dbo.[Order].IsViewPopupOrder = 0  ORDER BY Created_At DESC";
                    }
                    using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                    {
                        AddSQLDependency(sqlCommand);

                        if (dbConnection.State == ConnectionState.Closed)
                            dbConnection.Open();

                        var reader = sqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        if (dt.Rows.Count > 0)
                        {
                            MessageOrderModel item;
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new MessageOrderModel();
                                item.Id = row["Id"].ToString();
                                item.OrderCode = row["MaVanDon"].ToString();
                                item.CreateDate = DateTime.Parse(row["CreateAt"].ToString());
                                item.Create_By = row["Created_By"].ToString();
                                lstOrderRecords.Add(item);
                            }
                            //lstOrderRecords = lstOrderRecords.Where(o => DateTime.Parse(o.CreateDate.ToString()).ToString("dd/MM/yyyy") == DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy")).OrderByDescending(o => o.Id).ToList();
                            lstOrderRecords = lstOrderRecords.Where(o => o.CreateDate > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();
                            lstOrderView.Quanlity = lstOrderRecords.Count;
                            lstOrderView.listMessageOrder = lstOrderRecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstOrderView;
        }

        /// <summary>
        /// lấy danh sách thông báo theo trạng thái
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        /// <param name="status">trạng thái</param>
        /// <returns></returns>
        public MessageOrderViewModel GetListMessageOrderByStatus(string userId, string role, int status = 0)
        {
            var lstOrderView = new MessageOrderViewModel();
            var lstOrderRecords = new List<MessageOrderModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();

                    var id = GetCurrentOrganizatinId(userId);
                    string sqlCommandText = "";
                    if (role.Equals(PublicConstant.ROLE_ADMIN))
                    {
                        sqlCommandText = "SELECT DISTINCT  dbo.[Order].Id ,  dbo.[Order].Status , TenHang , UserName , MaVanDon , CreateAt , dbo.[Order].Created_By FROM    dbo.[Order] " +
                                           " INNER JOIN  dbo.OrderTracking ON dbo.[Order].Id = dbo.OrderTracking.OrderId " +
                                           " INNER JOIN dbo.UserInfoes ON dbo.[Order].Created_By = dbo.UserInfoes.ApplicationUserID WHERE dbo.OrderTracking.Status = " + status + " AND OrderTracking.IsView = 0 ORDER BY dbo.[Order].CreateAt DESC";
                    }
                    if (role.Equals(PublicConstant.ROLE_ENTERPRISE))
                    {
                        UserInfoes user = _unitOfWork.GetRepositoryInstance<UserInfoes>().GetFirstOrDefaultByParameter(o => o.ApplicationUserID == userId);
                        sqlCommandText = "SELECT DISTINCT  dbo.[Order].Id ,  dbo.[Order].Status , TenHang , UserName , MaVanDon , CreateAt , dbo.[Order].Created_By FROM    dbo.[Order] " +
                                           " INNER JOIN  dbo.OrderTracking ON dbo.[Order].Id = dbo.OrderTracking.OrderId " +
                                           " INNER JOIN dbo.UserInfoes ON dbo.[Order].Created_By = dbo.UserInfoes.ApplicationUserID WHERE dbo.UserInfoes.OrganzationId = '" + user.Id + "' AND dbo.OrderTracking.Status = " + status + " AND OrderTracking.IsView = 0 ORDER BY dbo.[Order].CreateAt DESC";
                    }
                    if (role.Equals(PublicConstant.ROLE_FORWARDER))
                    {
                        sqlCommandText = "SELECT DISTINCT  dbo.[Order].Id ,  dbo.[Order].Status , TenHang , UserName , MaVanDon , CreateAt , dbo.[Order].Created_By FROM    dbo.[Order] " +
                                           " INNER JOIN  dbo.OrderTracking ON dbo.[Order].Id = dbo.OrderTracking.OrderId " +
                                           " INNER JOIN dbo.UserInfoes ON dbo.[Order].Created_By = dbo.UserInfoes.ApplicationUserID WHERE dbo.[Order].Created_By = '" + userId + "' AND dbo.OrderTracking.Status = " + status + " AND OrderTracking.IsView = 0 ORDER BY dbo.[Order].CreateAt DESC";
                    }
                    else
                    {
                        sqlCommandText = "SELECT DISTINCT  dbo.[Order].Id ,  dbo.[Order].Status , TenHang , UserName , MaVanDon , CreateAt , dbo.[Order].Created_By FROM    dbo.[Order] " +
                                           " INNER JOIN  dbo.OrderTracking ON dbo.[Order].Id = dbo.OrderTracking.OrderId " +
                                           " INNER JOIN dbo.UserInfoes ON dbo.[Order].Created_By = dbo.UserInfoes.ApplicationUserID WHERE dbo.[Order].Created_By = '" + userId + "' AND dbo.OrderTracking.Status = " + status + " AND OrderTracking.IsView = 0 ORDER BY dbo.[Order].CreateAt DESC";
                    }
                    using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                    {
                        AddSQLDependency(sqlCommand);

                        if (dbConnection.State == ConnectionState.Closed)
                            dbConnection.Open();

                        var reader = sqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        if (dt.Rows.Count > 0)
                        {
                            MessageOrderModel item;
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new MessageOrderModel();
                                item.Id = row["Id"].ToString();
                                item.OrderCode = row["MaVanDon"].ToString();
                                item.CreateDate = DateTime.Parse(row["CreateAt"].ToString());
                                item.Create_By = row["Created_By"].ToString();
                                lstOrderRecords.Add(item);
                            }
                            lstOrderRecords = lstOrderRecords.Where(o => o.CreateDate > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();
                            lstOrderView.Quanlity = lstOrderRecords.Count;
                            lstOrderView.listMessageOrder = lstOrderRecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstOrderView;
        }

        public MessageUserViewModel GetListMessageAccount(string userId, string role)
        {
            var userView = new MessageUserViewModel();
            var lstUserRecords = new List<MessageUserModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();
                    var id = GetCurrentOrganizatinId(userId);
                    string sqlCommandText = "";
                    if (role.Equals(PublicConstant.ROLE_ADMIN))
                    {
                        sqlCommandText = "SELECT  dbo.UserInfoes.Id , ApplicationUserID , Created_At , Status , UserName , OrganzationId " +
                                        "FROM dbo.UserInfoes INNER JOIN dbo.AspNetUsers ON dbo.UserInfoes.Id = dbo.AspNetUsers.UsersInfo_Id WHERE UserInfoes.IsView = 0 ORDER BY Id DESC";
                    }
                    else
                    {
                        sqlCommandText = "SELECT  dbo.UserInfoes.Id , ApplicationUserID , Created_At , Status , UserName , OrganzationId " +
                                          "FROM dbo.UserInfoes INNER JOIN dbo.AspNetUsers ON dbo.UserInfoes.Id = dbo.AspNetUsers.UsersInfo_Id " +
                                          "WHERE OrganzationId = " + id + " AND UserInfoes.IsView = 0 ORDER BY Id DESC";
                    }
                    using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                    {
                        AddSQLDependency(sqlCommand);

                        if (dbConnection.State == ConnectionState.Closed)
                            dbConnection.Open();

                        var reader = sqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        if (dt.Rows.Count > 0)
                        {
                            MessageUserModel item;
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new MessageUserModel();
                                item.Id = row["Id"].ToString();
                                item.UserName = row["UserName"].ToString();
                                item.CreateDate = DateTime.Parse(row["Created_At"].ToString());
                                lstUserRecords.Add(item);
                            }

                            lstUserRecords = lstUserRecords.Where(o => o.CreateDate > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();
                            userView.Quanlity = lstUserRecords.Count;
                            //lstUserRecords = lstUserRecords.Where(o => o.CreateDate.ToString("dd/MM/yyyy") == DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy")).OrderByDescending(o => o.Id).Take(5).ToList();
                            userView.listMessageUser = lstUserRecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return userView;
        }

        public MessageNotificationViewModel GetListMessageNotification(string userId, string role)
        {
            var notificationView = new MessageNotificationViewModel();
            var lstNotificationRecords = new List<MessageNotificationModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();
                    string sqlCommandText = "";
                    sqlCommandText = "SELECT Id , Message , ToUserId , FromUserId , Created_At FROM dbo.Messages WHERE IsView = 0 ORDER BY Id DESC";
                    using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                    {
                        AddSQLDependency(sqlCommand);

                        if (dbConnection.State == ConnectionState.Closed)
                            dbConnection.Open();

                        var reader = sqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        if (dt.Rows.Count > 0)
                        {
                            MessageNotificationModel item;
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new MessageNotificationModel();
                                item.Id = row["Id"].ToString();
                                item.Message = row["Message"].ToString();
                                item.FromUserId = row["FromUserId"].ToString();
                                item.ToUserId = row["ToUserId"].ToString();
                                item.Created_At = DateTime.Parse(row["Created_At"].ToString());
                                item.IsView = row["IsView"] == null || bool.Parse(row["IsView"].ToString()) == false ? false : true;
                                lstNotificationRecords.Add(item);
                            }
                            if (role.Equals(PublicConstant.ROLE_ADMIN))
                            {
                                lstNotificationRecords = lstNotificationRecords.Where(o => o.Created_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();
                            }
                            else
                            {
                                lstNotificationRecords = lstNotificationRecords.Where(o => o.ToUserId == userId && o.Created_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();
                            }

                            notificationView.Quanlity = lstNotificationRecords.Count();
                            //lstNotificationRecords = lstNotificationRecords.Where(o => o.Created_At.ToString("dd/MM/yyyy") == DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy")).OrderByDescending(o => o.Id).Take(5).ToList();
                            notificationView.listMessageNotification = lstNotificationRecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return notificationView;
        }

        public MessageOrderTrackingViewModel GetListMessageOrderTracking(string userId, string role)
        {
            var OrderTrackingView = new MessageOrderTrackingViewModel();
            var lstOrderTrackingRdecords = new List<MessageOrderTrackingModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();
                    string sqlCommandText = "";
                    sqlCommandText = "SELECT  Name , OrderId,dbo.OrderTracking.Created_At FROM dbo.OrderTracking INNER JOIN dbo.Driver ON dbo.OrderTracking.DriverId = dbo.Driver.UserId WHERE OrderTracking.IsView = 0  ORDER BY Created_At DESC";
                    using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                    {
                        AddSQLDependency(sqlCommand);

                        if (dbConnection.State == ConnectionState.Closed)
                            dbConnection.Open();

                        var reader = sqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        if (dt.Rows.Count > 0)
                        {
                            MessageOrderTrackingModel item;
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new MessageOrderTrackingModel();
                                item.DriverName = row["Name"].ToString();
                                item.OrderId = long.Parse(row["OrderId"].ToString());
                                item.Create_At = DateTime.Parse(row["Created_At"].ToString());
                                lstOrderTrackingRdecords.Add(item);
                            }
                            lstOrderTrackingRdecords = lstOrderTrackingRdecords.Where(o => o.Create_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Create_At).GroupBy(g => new { g.OrderId, g.DriverName, g.Create_At }).Select(g => g.First()).ToList();
                            OrderTrackingView.Quanlity = lstOrderTrackingRdecords.Count;
                            OrderTrackingView.listMessageOrderTracking = lstOrderTrackingRdecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return OrderTrackingView;
        }

        public MessageOrderTrackingViewModel GetListMessageOrderTrackingForEnterprise(string userId, long orderId)
        {
            var OrderTrackingView = new MessageOrderTrackingViewModel();
            var lstOrderTrackingRdecords = new List<MessageOrderTrackingModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();
                    string sqlCommandText = "";
                    string sqlCommandText2 = "";
                    sqlCommandText = "SELECT Name ,OrderId, dbo.OrderTracking.Created_At FROM dbo.OrderTracking INNER JOIN dbo.Driver ON dbo.OrderTracking.DriverId = dbo.Driver.UserId INNER JOIN dbo.UserInfoes ON dbo.Driver.UserId = dbo.UserInfoes.ApplicationUserID WHERE OrganizationId = " + userId + " AND OrderId <>" + orderId + " AND OrderTracking.IsView = 0 ORDER BY Created_At DESC";
                    sqlCommandText2 = "SELECT top 1 Name ,OrderId, dbo.OrderTracking.Created_At FROM dbo.OrderTracking INNER JOIN dbo.Driver ON dbo.OrderTracking.DriverId = dbo.Driver.UserId INNER JOIN dbo.UserInfoes ON dbo.Driver.UserId = dbo.UserInfoes.ApplicationUserID WHERE OrganizationId = " + userId + " AND OrderId =" + orderId + " AND OrderTracking.IsView = 0 ORDER BY Created_At DESC";

                    var sqlCommand = new SqlCommand(sqlCommandText, dbConnection);
                    var sqlCommand2 = new SqlCommand(sqlCommandText2, dbConnection);

                    if (dbConnection.State == ConnectionState.Closed)
                        dbConnection.Open();

                    var reader = sqlCommand.ExecuteReader();
                    var reader2 = sqlCommand2.ExecuteReader();

                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    DataTable dt2 = new DataTable();
                    dt2.Load(reader2);
                    if (dt2.Rows.Count > 0)
                    {
                        MessageOrderTrackingModel item = new MessageOrderTrackingModel();
                        item.DriverName = dt2.Rows[0]["Name"].ToString();
                        item.OrderId = long.Parse(dt2.Rows[0]["OrderId"].ToString());
                        item.Create_At = DateTime.Parse(dt2.Rows[0]["Created_At"].ToString());
                        lstOrderTrackingRdecords.Add(item);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new MessageOrderTrackingModel();
                                item.DriverName = row["Name"].ToString();
                                item.OrderId = long.Parse(row["OrderId"].ToString());
                                item.Create_At = DateTime.Parse(row["Created_At"].ToString());
                                lstOrderTrackingRdecords.Add(item);
                            }

                            lstOrderTrackingRdecords = lstOrderTrackingRdecords.Where(o => o.Create_At > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Create_At).GroupBy(g => new { g.OrderId, g.DriverName, g.Create_At }).Select(g => g.First()).ToList();
                            OrderTrackingView.Quanlity = lstOrderTrackingRdecords.Count;
                            OrderTrackingView.listMessageOrderTracking = lstOrderTrackingRdecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return OrderTrackingView;
        }

        public OrderNewPopupModel GetOrderNewForEnterpriseFromFowarder(string userId, string role)
        {
            var orderView = new OrderNewPopupModel();
            var lstRecords = new List<OrderNewPopupModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();
                    var id = GetCurrentOrganizatinId(userId);
                    string sqlCommandText = "";
                    if (role.Equals(PublicConstant.ROLE_ENTERPRISE))
                    {
                        sqlCommandText = "SELECT top 1 [Order].Id,[Order].MaVanDon,[Order].UserName,[Order].CreateAt " +
                                           " FROM dbo.[Order]  INNER JOIN dbo.MapOrder on[Order].Created_By = MapOrder.ToUser  " +
                                           "  where MapOrder.FromUser = '" + userId + "' AND [Order].Status = "+ PublicConstant.STATUS_ACTIVE + " AND [Order].IsViewPopupOrder = 0  ORDER BY[Order].Id DESC";

                        using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                        {
                            AddSQLDependency(sqlCommand);

                            if (dbConnection.State == ConnectionState.Closed)
                                dbConnection.Open();

                            var reader = sqlCommand.ExecuteReader();

                            DataTable dt = new DataTable();
                            dt.Load(reader);
                            if (dt.Rows.Count > 0)
                            {
                                long orderId = long.Parse(dt.Rows[0]["Id"].ToString());
                                orderView = new OrderNewPopupModel();
                                orderView.Id = orderId;
                                orderView.MaVanDon = dt.Rows[0]["MaVanDon"].ToString();
                                orderView.UserName = dt.Rows[0]["UserName"].ToString();
                                orderView.CreateDate = DateTime.Parse(dt.Rows[0]["CreateAt"].ToString());

                                Order order = new Order();
                                order = _unitOfWork.GetRepositoryInstance<Order>().GetFirstOrDefaultByParameter(o => o.Id == orderId);
                                if (order != null)
                                {
                                    order.IsViewPopupOrder = true;
                                    _unitOfWork.GetRepositoryInstance<Order>().Update(order);
                                    _unitOfWork.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return orderView;
        }

        public MessageOrderViewModel GetListOrderNewForEnterpriseFromFowarder(string userId, string role)
        {
            var lstOrderView = new MessageOrderViewModel();
            var lstOrderRecords = new List<MessageOrderModel>();
            var lstRecords = new List<OrderNewPopupModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();
                    var id = GetCurrentOrganizatinId(userId);
                    string sqlCommandText = "";
                    if (role.Equals(PublicConstant.ROLE_ENTERPRISE))
                    {
                        sqlCommandText = "SELECT top 5 [Order].Id,[Order].MaVanDon,[Order].UserName,[Order].CreateAt " +
                                           " FROM dbo.[Order]  " +
                                           "          INNER JOIN dbo.MapOrder on[Order].Created_By = MapOrder.ToUser  " +
                                           "  where MapOrder.FromUser = '" + userId + "' AND [Order].IsViewPopupOrder = 0 AND [Order].Status = " + PublicConstant.STATUS_ACTIVE + " ORDER BY[Order].Id DESC";

                        using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                        {
                            AddSQLDependency(sqlCommand);

                            if (dbConnection.State == ConnectionState.Closed)
                                dbConnection.Open();

                            var reader = sqlCommand.ExecuteReader();

                            DataTable dt = new DataTable();
                            dt.Load(reader);
                            if (dt.Rows.Count > 0)
                            {
                                MessageOrderModel item;
                                foreach (DataRow row in dt.Rows)
                                {
                                    item = new MessageOrderModel();
                                    item.Id = row["Id"].ToString();
                                    item.OrderCode = row["MaVanDon"].ToString();
                                    item.CreateDate = DateTime.Parse(row["CreateAt"].ToString());
                                    item.Create_By = row["UserName"].ToString();
                                    lstOrderRecords.Add(item);
                                }

                                lstOrderRecords = lstOrderRecords.Where(o => o.CreateDate > DateTime.Now.AddDays(-1)).OrderByDescending(o => o.Id).ToList();
                                lstOrderView.Quanlity = lstOrderRecords.Count;
                                lstOrderView.listMessageOrder = lstOrderRecords.Take(5).ToList();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstOrderView;
        }

        public int GetCurrentOrganizatinId(string userId)
        {
            int organizationId = 0;
            try
            {
                using (Carrier3Entities db = new Carrier3Entities())
                {
                    organizationId = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault().Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}