﻿using Carrier.CMS.Hubs;
using Carrier.CMS.Models;
using Carrier.Models.Entities;
using Carrier.Repository;
using Carrier.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CMS.Carrier.Common
{
    public class ReportSOSRespository
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        static string dbConnectionSettings = ConfigurationManager.ConnectionStrings["slqConString"].ConnectionString;
        public ReportSOSRespository()
        {

        }
        /// <summary>
        /// Adds SQLDependency for change notification and passes the information to Student Hub for broadcasting
        /// </summary>
        /// <param name="sqlCommand"></param>
        public static void AddSQLDependency(SqlCommand sqlCommand)
        {
            try
            {
                sqlCommand.Notification = null;

                var dependency = new SqlDependency(sqlCommand);
                dependency.OnChange += (sender, sqlNotificationEvents) =>
                {
                    if (sqlNotificationEvents.Type == SqlNotificationType.Change)
                    {
                        NotificationHub.SendMessages(sqlNotificationEvents.Info.ToString());
                    }
                };
            }
            catch (Exception ex)
            {
            }
        }
        public ReportSOSNotiViewModel GetListReport(string userId, string role)
        {
            var lstOrderView = new ReportSOSNotiViewModel();
            var lstOrderRecords = new List<ReportSOSNotiModel>();
            try
            {
                using (var dbConnection = new SqlConnection(dbConnectionSettings))
                {
                    dbConnection.Open();

                    var id = GetCurrentOrganizatinId(userId);
                    string sqlCommandText = "";
                    if (role.Equals(PublicConstant.ROLE_ENTERPRISE))
                    {
                        sqlCommandText = "SELECT  dbo.[ReportSOS].Id ,  dbo.[ReportSOS].DriverId , Created_At , ReportTypeId , UserId , OrderId , Lat, Lng, ReportDescription FROM    dbo.[ReportSOS] WHERE UserId =" + id + "";
                    }
                    using (var sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                    {
                        AddSQLDependency(sqlCommand);

                        if (dbConnection.State == ConnectionState.Closed)
                            dbConnection.Open();

                        var reader = sqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        if (dt.Rows.Count > 0)
                        {
                            ReportSOSNotiModel item;
                            foreach (DataRow row in dt.Rows)
                            {
                                item = new ReportSOSNotiModel();
                                item.Id = row["Id"].ToString();
                                item.OrderId = long.Parse(row["OrderId"].ToString());
                                item.DriverId = row["DriverId"].ToString();
                                item.Created_At = DateTime.Parse(row["Created_At"].ToString());
                                lstOrderRecords.Add(item);
                            }
                            //lstOrderRecords = lstOrderRecords.Where(o => DateTime.Parse(o.CreateDate.ToString()).ToString("dd/MM/yyyy") == DateTime.Parse(DateTime.Now.ToString()).ToString("dd/MM/yyyy")).OrderByDescending(o => o.Id).ToList();
                    
                            lstOrderView.Quanlity = lstOrderRecords.Count;
                            lstOrderView.listReport = lstOrderRecords.Take(5).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstOrderView;
        }

        public int GetCurrentOrganizatinId(string userId)
        {
            int organizationId = 0;
            try
            {
                using (Carrier3Entities db = new Carrier3Entities())
                {
                    organizationId = db.UserInfoes.Where(o => o.ApplicationUserID == userId).SingleOrDefault().Id;
                }
            }
            catch (Exception)
            {
            }
            return organizationId;
        }
    }
}