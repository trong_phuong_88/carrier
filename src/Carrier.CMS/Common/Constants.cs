﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrier.CMS.Common
{

   public enum CMS_STATUS
    {
        ACTIVE = 1,
        PENDING = 2,
        DELETED = 3
    }
    public enum CMS_IMAGE_TYPE
    {
        ARTICLE_TYPE = 0,
        PRODUCT_TYPE
    }

    public enum CMS_REPORT_TYPE
    {
        ORTHER = 0, // Khác
        WHEEL = 1,  // hỏng bánh xe
        FAIL = 2 // chết máy

    }
}