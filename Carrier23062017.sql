USE [master]
GO
/****** Object:  Database [vanc4b1b_CarrierV1]    Script Date: 6/23/2017 10:11:45 AM ******/
CREATE DATABASE [vanc4b1b_CarrierV1]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'vanc4b1b_CarrierV1', FILENAME = N'C:\Program Files (x86)\Parallels\Plesk\Databases\MSSQL\MSSQL11.MSSQLSERVER2012\MSSQL\DATA\vanc4b1b_CarrierV1.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'vanc4b1b_CarrierV1_log', FILENAME = N'C:\Program Files (x86)\Parallels\Plesk\Databases\MSSQL\MSSQL11.MSSQLSERVER2012\MSSQL\DATA\vanc4b1b_CarrierV1_log.ldf' , SIZE = 1792KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [vanc4b1b_CarrierV1].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET ARITHABORT OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET  ENABLE_BROKER 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET  MULTI_USER 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET DB_CHAINING OFF 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [vanc4b1b_CarrierV1]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 6/23/2017 10:11:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Articles]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Summary] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [int] NULL,
	[ThumbURL] [nvarchar](250) NULL,
	[Status] [int] NULL,
	[CreateBy] [nvarchar](50) NULL,
	[UpdateBy] [nvarchar](50) NULL,
	[CreateAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[HotNew] [bit] NULL,
 CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[PrivateKey] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[UsersInfo_Id] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Car]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[License] [nvarchar](150) NULL,
	[Payload] [nvarchar](50) NULL,
	[ContainerSize] [nvarchar](50) NULL,
	[Description] [nvarchar](550) NULL,
	[Created_At] [nvarchar](12) NULL,
	[Updated_At] [nvarchar](12) NULL,
	[Created_By] [nvarchar](128) NULL,
	[Updated_By] [nvarchar](128) NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
	[URL] [nvarchar](250) NULL,
	[Type] [int] NULL,
	[Status] [int] NULL,
	[ParentId] [int] NULL,
	[CreateBy] [nvarchar](50) NULL,
	[CreateAt] [datetime] NULL,
	[UpdateBy] [nvarchar](50) NULL,
	[UpdateAt] [datetime] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[City]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[CityCode] [varchar](50) NULL,
	[Name] [nvarchar](250) NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_md_City] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Phone] [nvarchar](50) NULL,
	[Sex] [bit] NULL,
	[BirthDay] [nvarchar](12) NULL,
	[Note] [nvarchar](550) NULL,
	[Created_At] [nvarchar](12) NULL,
	[Updated_At] [nvarchar](12) NULL,
	[OrganizationId] [int] NULL,
	[Created_By] [nvarchar](128) NULL,
	[Updated_By] [nvarchar](128) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DevicesPush]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DevicesPush](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TokenDevice] [nvarchar](150) NULL,
	[Platform] [nvarchar](50) NULL,
	[SystemVersion] [nvarchar](50) NULL,
	[RegistedDate] [datetime] NULL,
	[AppVersion] [nvarchar](50) NULL,
	[UserId] [nvarchar](128) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_DevicesManager] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[District]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[DistrictCode] [nvarchar](50) NULL,
	[Name] [nvarchar](250) NULL,
	[Type] [nvarchar](50) NULL,
	[Location] [nvarchar](50) NULL,
	[CityCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_md_District] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DraftOrder]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DraftOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[TenHang] [nvarchar](250) NULL,
	[DiemDi] [nvarchar](250) NULL,
	[DiemDen] [nvarchar](250) NULL,
	[Gia] [float] NULL,
	[FromLocation] [geography] NULL,
	[ToLocation] [geography] NULL,
	[Vat] [bit] NULL,
	[TrongLuong] [float] NULL,
	[ThoiGianDi] [nvarchar](50) NULL,
	[ThoiGianDen] [nvarchar](50) NULL,
	[Note] [nvarchar](1000) NULL,
	[Created_At] [datetime] NULL,
	[Created_By] [nvarchar](50) NULL,
	[Width] [float] NULL,
	[Height] [float] NULL,
	[Lenght] [float] NULL,
 CONSTRAINT [PK_DraftOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Driver]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Address] [nvarchar](250) NULL,
	[Phone] [nvarchar](250) NULL,
	[Sex] [nvarchar](50) NULL,
	[BirthDay] [nvarchar](50) NULL,
	[OrganizationId] [int] NULL,
	[Location] [geography] NULL,
	[UserId] [nvarchar](128) NULL,
	[Status] [int] NULL,
	[Created_At] [nvarchar](20) NULL,
	[Updated_At] [nvarchar](20) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileUpload]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileUpload](
	[FileId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[UrlFile] [varchar](500) NULL,
	[Filetype] [varchar](5) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_FileUpload] PRIMARY KEY CLUSTERED 
(
	[FileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Gia] [float] NULL,
	[Status] [int] NULL,
	[Note] [nvarchar](1000) NULL,
	[SoKmUocTinh] [float] NULL,
	[TenHang] [nvarchar](250) NULL,
	[DiemDiChiTiet] [nvarchar](250) NULL,
	[DiemDenChiTiet] [nvarchar](250) NULL,
	[CarType] [nvarchar](50) NULL,
	[ThoiGianDi] [nvarchar](50) NULL,
	[ThoiGianDen] [nvarchar](50) NULL,
	[UserName] [nvarchar](150) NULL,
	[LoaiThanhToan] [int] NULL,
	[MaVanDon] [nvarchar](50) NULL,
	[CreateAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[FromLocation] [geography] NULL,
	[ToLocation] [geography] NULL,
	[Width] [float] NULL,
	[Height] [float] NULL,
	[Lenght] [float] NULL,
	[VAT] [bit] NULL,
	[TrongLuong] [float] NULL,
	[DienThoaiLienHe] [nvarchar](50) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderTracking]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderTracking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[UserId] [nvarchar](128) NULL,
	[Updated_At] [nvarchar](20) NULL,
	[Status] [int] NULL,
	[Created_At] [nvarchar](20) NULL,
 CONSTRAINT [PK_OrderTracking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Organization]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[WebSite] [nvarchar](150) NULL,
	[Updated_At] [nvarchar](12) NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserInfoes]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInfoes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationUserID] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
	[Sex] [bit] NOT NULL,
	[MobilePhone] [nvarchar](max) NULL,
	[BirthDay] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[Created_At] [nvarchar](max) NULL,
	[Updated_At] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[Location] [geography] NULL,
	[LastLogin] [datetime] NOT NULL,
	[FullName] [nvarchar](max) NULL,
	[OrganzationId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.UserInfoes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WalletInfo]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WalletInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Balance] [float] NULL,
	[Commission] [float] NULL,
	[Updated_At] [nvarchar](12) NULL,
	[UserId] [nvarchar](128) NULL,
 CONSTRAINT [PK_WalletInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ward]    Script Date: 6/23/2017 10:11:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ward](
	[ItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[WardCode] [nvarchar](50) NULL,
	[Name] [nvarchar](250) NULL,
	[Type] [nvarchar](50) NULL,
	[Location] [nvarchar](50) NULL,
	[DistrictCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_md_Ward] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UsersInfo_Id]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE NONCLUSTERED INDEX [IX_UsersInfo_Id] ON [dbo].[AspNetUsers]
(
	[UsersInfo_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 6/23/2017 10:11:47 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FileUpload] ADD  CONSTRAINT [DF_FileUpload_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[FileUpload] ADD  CONSTRAINT [DF_FileUpload_UrlFile]  DEFAULT ('') FOR [UrlFile]
GO
ALTER TABLE [dbo].[FileUpload] ADD  CONSTRAINT [DF_FileUpload_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Category]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.UserInfoes_UsersInfo_Id] FOREIGN KEY([UsersInfo_Id])
REFERENCES [dbo].[UserInfoes] ([Id])
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.UserInfoes_UsersInfo_Id]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trọng tải xe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Car', @level2type=N'COLUMN',@level2name=N'Payload'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Kích thước thùng xe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Car', @level2type=N'COLUMN',@level2name=N'ContainerSize'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'au: autido, video, st: sticker, img: image' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileUpload', @level2type=N'COLUMN',@level2name=N'Filetype'
GO
USE [master]
GO
ALTER DATABASE [vanc4b1b_CarrierV1] SET  READ_WRITE 
GO
